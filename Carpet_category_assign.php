<?php 

use Magento\Framework\App\Bootstrap;
require 'app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
$categoryId = 3;
$category = $categoryFactory->create()->load($categoryId);

$categoryProducts = $category->getProductCollection()->addAttributeToSelect('*')->addAttributeToFilter('type_id', 'configurable');

foreach ($categoryProducts as $product)
{
     $check_product1 = $objectManager->get('\Magento\Catalog\Model\Product');
     $categoryIdsArray = array();
     if($check_product1->getIdBySku($product->getSku())) {

        $oldConfig = $objectManager->get('\Magento\Catalog\Model\ProductRepository')->get($product->getSku());

        $sizeOptionArray = array();
        $sizeTextFilterArray = array();
        $sizeOptionArray = explode(",", $oldConfig->getData('carpet_style'));
        
        foreach ($sizeOptionArray as $sizeOptionId) {
            $attribute = $oldConfig->getResource()->getAttribute('carpet_style');
            if ($attribute->usesSource()) {
                $sizeTextFilterArray[$sizeOptionId] = $attribute->getSource()->getOptionText($sizeOptionId);
            }
        }

        $sizeOptionArray1 = array();
        $sizeTextFilterArray1 = array();
        $sizeOptionArray1 = explode(",", $oldConfig->getData('carpet_fibre'));
        
        foreach ($sizeOptionArray1 as $sizeOptionId1) {
            $attribute = $oldConfig->getResource()->getAttribute('carpet_fibre');
            if ($attribute->usesSource()) {
                $sizeTextFilterArray1[$sizeOptionId1] = $attribute->getSource()->getOptionText($sizeOptionId1);
            }
        }

        $colorOptionArray = array();
        $colorTextFilterArray = array();
        $colorOptionArray = explode(",", $oldConfig->getData('carpet_colour_group'));
    
        foreach ($colorOptionArray as $colorOptionId) {
            $attribute = $oldConfig->getResource()->getAttribute('carpet_colour_group');
            if ($attribute->usesSource()) {
                $colorTextFilterArray[$colorOptionId] = $attribute->getSource()->getOptionText($colorOptionId);
            }
        }

        $sizeAndColorCatArray = array_merge($sizeTextFilterArray,$colorTextFilterArray);
        $newArray = array_merge($sizeAndColorCatArray,$sizeTextFilterArray1);
        $newArray[] = 'Carpet';


        if($newArray > 0 ){
            /*foreach ($newArray as $catName) {
                $sizeAndColorCategory = $objectManager->create('\Magento\Catalog\Model\CategoryFactory')->create()->getCollection()->addAttributeToFilter('name',trim($catName))->setPageSize(1);
                $categoryIdsArray[] = $sizeAndColorCategory->getFirstItem()->getId();
            }*/
            $parent_category_id = 3;
            $categoryObj = $objectManager->get('\Magento\Catalog\Model\CategoryRepository')->get($parent_category_id);
            $subcategories = $categoryObj->getChildrenCategories();
            foreach($subcategories as $subcategorie) {
                foreach ($newArray as $catName) {
                    if($subcategorie->getName() == $catName){
                        $categoryIdsArray[] = $subcategorie->getId();
                    }
                }
            }
        }

        $categoryIdsArray[] = $parent_category_id;

        echo "<pre>";
        print_r($categoryIdsArray);

        $oldConfig->setStoreId(1);
        $oldConfig->setWebsiteIds(array(1));
        $oldConfig->setCategoryIds($categoryIdsArray);
        $oldConfig->save();

        print_r($oldConfig->getSku());

     }
}

?>


