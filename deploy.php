<?php

namespace Deployer;

require 'vendor/jalogut/magento2-deployer-plus/recipe/magento_2_2_5.php';

// Use timestamp for release name
set('release_name', function () {
    return date('YmdHis');
});

// Magento dir into the project root. Set "." if magento is installed on project root
set('magento_dir', '.');
// [Optional] Git repository. Only needed if not using build + artifact strategy
set('repository', 'git@bitbucket.org:predikkta/carpet-call.git');
// Space separated list of languages for static-content:deploy
set('languages', 'en_US');
// Enable all caches after deployment
set('cache_enabled_caches', 'all');
// keep releases
set('keep_releases', 3);

set('shared_files', [
    '{{magento_dir}}/app/etc/env.php',
    '{{magento_dir}}/pub/robots.txt',
]);

set('shared_dirs', [
    '{{magento_dir}}/pub/media',
    '{{magento_dir}}/var/log',
    '{{magento_dir}}/var/backups',
    '{{magento_dir}}/var/session',
    '{{magento_dir}}/var/report',
    '{{magento_dir}}/var/import',
    '{{magento_dir}}/var/export',
    '{{magento_dir}}/importcsv',
    '{{magento_dir}}/archive',
]);

set('app_git_checkout_items', ['pub/.htaccess']);

set('static_deploy_options', '-f');

task(
    'files:static_assets', 
    '{{bin/php}} {{magento_bin}} setup:static-content:deploy {{languages}} {{static_deploy_options}}'
);

// override files:permissions task to exclude pub/media after setting up EFS to avoid timeout errors
// [Symfony\Component\Process\Exception\ProcessTimedOutException]
// exceeded the timeout of 900 seconds
task(
    'files:permissions',
    'cd {{magento_dir}} && chmod -R g+w var vendor pub/static app/etc && chmod u+x bin/magento'
);

// override files:permissions task to set custom file permissions
task('app:permissions', function () {
    run('cd {{deploy_path}}/current && find . -type d -exec chmod 755 {} + && find . -type f -exec chmod 644 {} +');
});
// after('cache:enable', 'app:permissions');

task('app:deploy:git-checkout', function () {
    $activePath = get('deploy_path') . '/' . (test('[ -L {{deploy_path}}/release ]') ? 'release' : 'current');
    foreach (get('app_git_checkout_items', []) as $checkoutItem) {
        run('cd {{release_path}} && git checkout ' . escapeshellarg($checkoutItem));
    }
});
after('deploy:vendors', 'app:deploy:git-checkout');

// OPcache configuration
task('cache:clear:opcache', 'sudo systemctl reload php-fpm');
// after('cache:clear', 'cache:clear:opcache');

// remove .user.ini
task('app:remove:userini', function () {
    run('cd {{deploy_path}}/current && rm .user.ini');
    run('cd {{deploy_path}}/current/pub && rm .user.ini');
});
after('cache:clear', 'app:remove:userini');

// Build host
localhost('build');

//////////////// STAGING ///////////////////
host('carpet-call-staging')
    ->hostname('13.236.157.164')
    ->user('camt2368')
    ->port(22)
    ->set('deploy_path', '/mnt/data/home/camt2368/public_html')
    ->set('branch', 'dev')
    ->set('default_timeout', null)
    ->set('is_production', 1)
    ->stage('staging')
    //->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->roles('dev');

//////////////// PIM akeneo ///////////////////
host('carpet-call-pim')
    ->hostname('52.62.247.121')
    ->user('stcc3492')
    ->port(22)
    ->set('deploy_path', '/mnt/data/home/stcc3492/public_html')
    ->set('branch', 'pim')
    ->set('default_timeout', null)
    ->set('is_production', 1)
    ->stage('pim')
    //->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->roles('pim');

//////////////// UAT ///////////////////
host('carpet-call-uat')
    ->hostname('13.236.157.164')
    ->user('tecap221')
    ->port(22)
    ->set('deploy_path', '/mnt/data/home/tecap221/public_html')
    ->set('branch', 'uat')
    ->set('default_timeout', null)
    ->set('is_production', 1)
    ->stage('uat')
    //->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->roles('uat');

//////////////// PRODUCTION ///////////////////
host('carpet-call-production')
    ->hostname('52.63.207.132')
    ->user('capli4477')
    ->port(22)
    ->set('deploy_path', '/mnt/data/home/capli4477/public_html')
    ->set('branch', 'master')
    ->set('default_timeout', null)
    ->set('is_production', 1)
    ->stage('production')
    //->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->roles('master');
