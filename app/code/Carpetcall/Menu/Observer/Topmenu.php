<?php
namespace Carpetcall\Menu\Observer;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Event\ObserverInterface;
class Topmenu implements ObserverInterface
{
    protected $eavConfig;
    public function __construct(
       \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavConfig = $eavConfig;
    }
    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        /** @var \Magento\Framework\Data\Tree\Node $menu */
        $menu = $observer->getMenu();

        $attribute = $this->eavConfig->getAttribute('catalog_product', 'color');
        $options = $attribute->getSource()->getAllOptions();
        
        if (!empty($options)) {
            foreach ($options as $value) {

                if ($value['value'] == '') {
                    //echo '1';die();
                }else{
                $children = $menu->getChildren();

                foreach ($children as $child) {
                    // echo "<pre>";
                    // print_r($options);die();
                    if ($child->getName() == 'Carpet') {
                    //if($child->getChildren()->count() > 0){ //Only add menu items if it already has a dropdown (this could be removed)
                        $childTree = $child->getTree();

                        $data = [
                            'name'      => __('COLORS'),
                            'id'        => 'colors',
                            'class'        => 'inactiveLink',
                            'url'       => '#',
                            'is_active' => FALSE
                        ];
                        $node = new Node($data, 'id', $childTree, $child);
                        $childTree->addNode($node, $child);
                        
                        
                        $data1 = [
                            'name'      => ucwords($value['label']),
                            'id'        => $value['label'].'_'.$value['value'],
                            'url'       => $child->getUrl().'?color='.$value['value'],
                            'is_active' => FALSE
                        ];
                        $node1 = new Node($data1, 'id', $childTree, $child);
                        $childTree->addNode($node1, $child);

                    //}
                    }
                }
                }
            }
        }
        

        
        return $this;
    }
}