<?php
namespace Carpetcall\Productdetailspage\Block;
class Categoriesdetails extends \Magento\Framework\View\Element\Template
{
    protected $_storeManager;
    protected $_categoryCollectionFactory;
    protected $_productRepository;
    protected $_registry;
    protected $_currencysymbol;
    protected $_categoryFactory;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $CurrencyFactory,
        \Magento\Framework\App\Action\Context $context_data,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
        )
        {
            $this->_categoryCollectionFactory = $categoryCollectionFactory;
            $this->_productRepository = $productRepository;
            $this->_registry = $registry;
            $this->_storeManager = $storeManager;
            $this->_currencysymbol = $CurrencyFactory;
            $this->context_data = $context_data;
            $this->_categoryFactory = $categoryFactory;
            parent::__construct($context, $data);
    }
    
    /**
     * Get category collection
     *
     * @param bool $isActive
     * @param bool|int $level
     * @param bool|string $sortBy
     * @param bool|int $pageSize
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection or array
     */
    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
    
    public function getProductById($id)
    {        
        return $this->_productRepository->getById($id);
    }
    
    public function getCurrentProduct()
    {        
        return $this->_registry->registry('current_product');
    }   

    public function getBaseurl()
    {
        $baseurl = $this->_storeManager->getStore()->getBaseUrl();
        return $baseurl;
    }

    public function getMediaurl()
    {
        $mediaurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaurl;
    }

    public function getCurrencySymbol()
    {
        $currencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();
        $currency = $this->_currencysymbol->create()->load($currencyCode); 
        $currencySymbol = $currency->getCurrencySymbol();
        return $currencySymbol;
    }

    public function getCurrentUrl()
    {
        $request = $this->context_data->getRequest();
        return $request;
    }

    public function getParentCategoryDetails($currentCategoryName)
    {
        //echo $currentCategoryName;
        $categoryTitle = ucwords(str_replace('-', ' ', $currentCategoryName));
        $collection = $this->_categoryCollectionFactory
                ->create()
                ->addAttributeToFilter('name',$categoryTitle)
                ->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();

            $collection = $this->_categoryCollectionFactory->create();

            # Specifically select the parent_id attribute
            $collection->addAttributeToSelect('parent_id');

            # Only select categories with certain entity_ids (category ids)
            $collection->addFieldToFilter('entity_id', ['in' => $categoryId]);

            # Iterate over results and print them out!
            foreach ($collection as $category) {
                $parentID = $category->getParentId();
                $parentCategoryName = $this->getParentCategoryName($parentID);

                if ($parentCategoryName == 'Hard Flooring') {
                    return 'hard-flooring';
                }elseif ($parentCategoryName == 'Rugs') {
                    return 'rugs';
                }else{
                    return '';
                }
            }
        }
    }

    public function getParentCategoryName($parentID)
    {
        $category = $this->_categoryFactory->create()->load($parentID);
        $categoryName = $category->getName();
        return $categoryName;
    }

}
?>