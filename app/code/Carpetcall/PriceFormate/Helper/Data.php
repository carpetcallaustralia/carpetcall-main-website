<?php
namespace Carpetcall\PriceFormate\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $registry;
    protected $precisionModel;
    protected $categoryRepository;
    private $_objectManager;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->registry = $registry;
        $this->categoryRepository = $categoryRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_objectManager = $objectManager;
    }

    public function getCurrentCategory()
    {      
        //return $this->registry->registry('current_category');
        $category = $this->_objectManager->get('Magento\Framework\Registry')->registry('current_category');
        if($category){
            $currentCatName = $category->getName();
        }else{
            $currentCatName = null;
        }
        
        return $currentCatName; 
    } 

    public function getMainCategoryArray(){
        
        $categoryTitle = 'Rugs';
        $categoryArray = array();
        $categoryArray[] = $categoryTitle;

        $collection = $this->_categoryFactory->create()->getCollection()
              ->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();
        }

        if($categoryId){
            $category = $this->categoryRepository->get($categoryId);
            $subCategories = $category->getChildrenCategories();
            foreach($subCategories as $subCategory) {
                $categoryArray[] = $subCategory->getName();
            }
        }

        return $categoryArray;
    }
}


