<?php
namespace Carpetcall\PriceFormate\Model\Directory\Plugin;

use Magento\Directory\Model\Currency;

class Precision
{
    protected $helper;

    public function __construct(
        \Carpetcall\PriceFormate\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    public function aroundFormatPrecision(
        Currency  $subject,
        \Closure $proceed,
        $price,
        $precision = 2,
        $options = [],
        $includeContainer = true,
        $addBrackets = false
    ) {
        /*$currentCategory = $this->helper->getCurrentCategory();
        $categoryArray = $this->helper->getMainCategoryArray();

        if($currentCategory){
            $categoryName = $currentCategory->getName();
            if($categoryArray){
                if (in_array($categoryName, $categoryArray))
                {
                    $precision = 0;
                }
            }
        }*/

        $categoryName = $this->helper->getCurrentCategory();
        $categoryArray = $this->helper->getMainCategoryArray();

        if($categoryName != null){
            if($categoryArray){
                if (in_array($categoryName, $categoryArray))
                {
                    $precision = 0;
                }
            }
        }

        return $proceed(
            $price,
            $precision,
            $options,
            $includeContainer,
            $addBrackets);
    }
}