<?php

namespace Carpetcall\PriceFormate\Model\Directory\Plugin;

class Format
{
    protected $helper;

    public function __construct(
        \Carpetcall\PriceFormate\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    public function afterGetPriceFormat($subject, $result)
    {
        $precision = 2;
        /*$currentCategory = $this->helper->getCurrentCategory();
        $categoryArray = $this->helper->getMainCategoryArray();

        if($currentCategory){
            $categoryName = $currentCategory->getName();
            if($categoryArray){
                if (in_array($categoryName, $categoryArray))
                {
                    $precision = 0;
                }
            }
        }*/
        $categoryName = $this->helper->getCurrentCategory();
        $categoryArray = $this->helper->getMainCategoryArray();

        if($categoryName != null){
            if($categoryArray){
                if (in_array($categoryName, $categoryArray))
                {
                    $precision = 0;
                }
            }
        }

        $result['precision'] = $precision;
        $result['requiredPrecision'] = $precision;

        return $result;
    }
}