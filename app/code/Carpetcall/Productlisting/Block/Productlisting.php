<?php
namespace Carpetcall\Productlisting\Block;
use Magento\Store\Model\StoreManagerInterface;
class Productlisting extends \Magento\Framework\View\Element\Template
{

	private $storeManager;
	protected $_registry;
	protected $CategoryFactory;
	protected $_scopeConfig;
    protected $_stockInterface;
    protected $_productRepository;
    protected $_connection;

	public function __construct(
		 StoreManagerInterface $storeManager,
		 \Magento\Framework\View\Element\Template\Context $context,
		 \Magento\Framework\Registry $registry,
		 \Magento\Catalog\Model\CategoryFactory $CategoryFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\CatalogInventory\Api\StockStateInterface $stockInterface,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\ResourceConnection $resource,
		 array $data = []
	 ) {
		 $this->storeManager = $storeManager;
		 $this->_registry = $registry;
		 $this->_categoryfactory = $CategoryFactory;
		 $this->_scopeConfig = $scopeConfig;
        $this->_stockInterface = $stockInterface;
        $this->_productRepository = $productRepository;
        $this->_connection = $resource->getConnection();
		 parent::__construct($context, $data);
	 }

	public function getCurrentCategory() {
		$category = $this->_registry->registry('current_category');
		return $category->getName();
	 	//return $this->storeManager->getStore()->getCurrentUrl();
	}

	public function getProductcount($cateid)
	{
		$allcategoryproduct = $this->_categoryfactory->create()->load($cateid)->getProductCollection()->addAttributeToSelect('*');
        $count = $allcategoryproduct->count();
        return $count;
	}

	public function getStockMessage($productId){
        $_product = $this->getProductById($productId);
        $_stock = $this->getStock($_product);
        // if($_stock <= $this->getThresoldQty()){
        //     return __('Only %1 left', $_stock);
        // }
        if($_stock <= $this->getThresoldQty()){
            return __('Low stock', $_stock);
        }
        return '';
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getStock($_product)
    {
        return $this->_stockInterface->getStockQty($_product->getId(), $_product->getStore()->getWebsiteId());
    }

    public function getThresoldQty(){
        return $this->_scopeConfig->getValue('cataloginventory/options/stock_threshold_qty', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCategories()
    {
        // $myTable = $this->_connection->getTableName('mageplaza_blog_post_category');
        // $sql     = $this->_connection->select()->from(["tn" => $myTable],'category_id')->where('post_id = ?', $id); 
        // $result  = $this->_connection->fetchAll($sql);

        $catmyTable = $this->_connection->getTableName('mageplaza_blog_category');
        $fields = array('name','description');
        $sql     = $this->_connection->select()->from(["tn" => $catmyTable],$fields); 
        $cate_name  = $this->_connection->fetchAll($sql);

        // $cat = array();
        // $cat_desc = array();
        // foreach ($result as $key => $value) {
            
        //     foreach ($cate_name as $key => $value) {
        //         $cat[] = ['name' => $value['name'], 'description' =>$value['description']];
                
        //     }
        // }
        // echo "<pre>";
        // print_r($cate_name);
        // die();
        return $cate_name;
    }

    public function getCategoryDataofPost($id)
    {
        $myTable = $this->_connection->getTableName('mageplaza_blog_post_category');
        $sql     = $this->_connection->select()->from(["tn" => $myTable],'category_id')->where('post_id = ?', $id); 
        $result  = $this->_connection->fetchAll($sql);

        // echo "<pre>";
        // print_r($result);
        // die();

        $catmyTable = $this->_connection->getTableName('mageplaza_blog_category');
        $fields = array('name','category_id','products_category_sku');
        $sql     = $this->_connection->select()->from(["tn" => $catmyTable],$fields)->where('category_id = ?', $result[0]['category_id']); 
        $cate_name  = $this->_connection->fetchAll($sql);

        

        $cat_data = ['name' => $cate_name[0]['name'], 'sku' => $cate_name[0]['products_category_sku']];
        return $cat_data;
    }

    public function getCategoryDataforFaq($categoryname)
    { 
        
        $catmyTable = $this->_connection->getTableName('mageplaza_blog_category');
        $fields = array('name','category_id','products_category_sku');
        $sql     = $this->_connection->select()->from(["tn" => $catmyTable],$fields); 
        $cate_name  = $this->_connection->fetchAll($sql);

        
        $prod_sku = array();

        foreach ($cate_name as $key => $value) {
            $name = strtolower((str_replace(' ', '-', $value['name'])));
            if ($name == $categoryname) {
                
                $prod_sku['productSKU'] = $value['products_category_sku'];            
            }
        }

        // echo "<pre>";
        // print_r($prod_sku);
        
        // die();

       
        return $prod_sku;
    }

    public function getPosts($category_name)
    {
        //echo "string";die();
        $catmyTable = $this->_connection->getTableName('mageplaza_blog_category');
        $fields = array('category_id','name','description');
        $sql     = $this->_connection->select()->from(["tn" => $catmyTable],$fields)->where('name = ?', $category_name); 
        $cate_name  = $this->_connection->fetchAll($sql);

        if (!empty($cate_name)) {
            $cat_id = $cate_name[0]['category_id'];

            $myTable = $this->_connection->getTableName('mageplaza_blog_post_category');
            $sql     = $this->_connection->select()->from(["tn" => $myTable],'post_id')->where('category_id = ?', $cat_id); 
            $result  = $this->_connection->fetchAll($sql);

            

            //$post_ids = array();
            $post_data = array();

            foreach ($result as $key => $value) {
                //$post_ids[] = $value['post_id'];
                $myTable = $this->_connection->getTableName('mageplaza_blog_post');
                $sql     = $this->_connection->select()->from(["tn" => $myTable])->where('post_id = ?', $value['post_id']); 
                $post_data[]  = $this->_connection->fetchAll($sql);
            }

            // echo "<pre>";
            // print_r($post_data);
            // die();
            return $post_data;
        }
        
    }
}