<?php

namespace Carpetcall\Productlisting\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Helper\Output as OutputHelper;
use Magento\Framework\App\ObjectManager;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    protected $_customerSession;
    protected $categoryFactory;

    /**
     * ListProduct constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param Helper $helper
     * @param array $data
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
    \Magento\Catalog\Block\Product\Context $context,
    \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
    \Magento\Catalog\Model\Layer\Resolver $layerResolver,
    \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
    \Magento\Framework\Url\Helper\Data $urlHelper,
    \Magento\Customer\Model\Session $customerSession,
    \Magento\Catalog\Model\CategoryFactory $categoryFactory,
    array $data = [],
    ?OutputHelper $outputHelper = null
    ) {
        $this->_customerSession = $customerSession;
        $this->categoryFactory = $categoryFactory;
        $data['outputHelper'] = $outputHelper ?? ObjectManager::getInstance()->get(OutputHelper::class);
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );

    }

    public function getClearProductCollection()
    {
        $categoryId = $this->getCategoryName(); // YOUR CATEGORY ID
        $category = $this->categoryFactory->create()->load($categoryId);
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 2;
        $_productCollection = $category->getProductCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('online_discount',array('notnull' => true))
            ->setPageSize($pageSize)
            ->setCurPage($page);

        return $_productCollection;
    }


    public function getCategoryName()
    {
        $categoryTitle = 'Hard Flooring';
        $collection = $this->categoryFactory->create()->getCollection()
                      ->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();
        }

        return $categoryId;
    }


}