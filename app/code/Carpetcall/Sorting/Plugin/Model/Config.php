<?php
namespace Carpetcall\Sorting\Plugin\Model;

use Magento\Store\Model\StoreManagerInterface;
class Config
{
    protected $_storeManager;

public function __construct(
    StoreManagerInterface $storeManager
) {
    $this->_storeManager = $storeManager;

}

/**
 * Adding custom options and changing labels
 *
 * @param \Magento\Catalog\Model\Config $catalogConfig
 * @param [] $options
 * @return []
 */
public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
{
    $store = $this->_storeManager->getStore();
        $currencySymbol = $store->getCurrentCurrency()->getCurrencySymbol();

        //echo "string";die();

        //unset($options['position']);
        // unset($options['name']);
        unset($options['price']);

        //Changing label
        //$customOption['position'] = __( 'Relevance' );
        $customOption['name'] = __( 'A - Z' );

        //New sorting options
        $customOption['desc_name'] = __( ' Z - A' );

        $customOption['price'] = __( 'Low To High Price' );

        $options['high_to_low_price'] = __('High To Low Price');

        //$options['created_at'] = __('New');
        //$options['most_viewed'] = __('Most viewed');
        //Merge default sorting options with custom options
        $options = array_merge($customOption, $options);

    return $options;
}
}