<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Model\Job;

use FME\Jobs\Model\ResourceModel\Job\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \FME\Jobs\Model\Job\DataProvider
{

    /**
     * Disabling the following fields
     * @var array
     */
    private $disabledFields = [
        'jobs_open_positions',
        'jobs_job_type',
        'jobs_career_level',
        'jobs_min_qualification',
        'jobs_min_experience',
        'jobs_required_travel',
        'jobs_required_skills',
        'use_config_email',
        'notification_email_receiver',
        'use_config_template',
        'email_notification_temp',
    ];

    /**
     * @var \Carpetcall\JobsEmailer\Helper\EmailLink
     */
    private $emailHelper;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \FME\Jobs\Model\ResourceModel\Job\CollectionFactory $blockCollectionFactory
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Carpetcall\JobsEmailer\Helper\EmailLink $emailHelper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        \FME\Jobs\Model\ResourceModel\Job\CollectionFactory $blockCollectionFactory,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Carpetcall\JobsEmailer\Helper\EmailLink $emailHelper,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $blockCollectionFactory,
            $dataPersistor,
            $storeManager,
            $meta,
            $data
        );
        $this->emailHelper = $emailHelper;
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        $meta = parent::getMeta();
        foreach ($this->disabledFields as $fieldCode) {
            $meta['general']['children'][$fieldCode]['arguments']['data']['config']['visible'] = false;
        }
        return $meta;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = parent::getData();
        if (!$data) {
            return [];
        }
        foreach ($data as &$item) {
            $item['emails'] = $this->emailHelper->loadExistingEmailsForForm($item['jobs_id']);
        }
        return $data;
    }

}
