<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Model\Job\Metadata;

/**
 * Class GetJobService
 * TECH DEBT
 * Writing service to access metadata because \FME\Jobs\Model\Job and \FME\Jobs\Model\ResourceModel\Mdata are not
 * written correctly
 * @see \FME\Jobs\Model\Job
 * @see \FME\Jobs\Model\ResourceModel\Mdata
 */
class GetJobService {

    const TABLE_ENTITY_FME_JOBS = 'fme_jobs';
    const TABLE_ENTITY_FME_META_DATA = 'fme_meta_data';
    const TABLE_ENTITY_FME_META_TYPE = 'fme_meta_type';

    private $metaCodes = [];

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $connection;

    /**
     * GetJobService constructor.
     * @param \Magento\Framework\App\ResourceConnection $connection
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $connection
    ) {
        $this->connection = $connection;

    }

    /**
     * @return array
     */
    private function getMetaTypeId($code) {
        if (!$this->metaCodes) {
            $selectMetaCodes = $this->connection->getConnection()->select();
            $selectMetaCodes->from(
                ['metatype' => self::TABLE_ENTITY_FME_META_TYPE],
                ['id', 'type_code']
            );
            foreach ($this->connection->getConnection()->fetchAll($selectMetaCodes) as $result) {
                $this->metaCodes[$result['type_code']] = $result['id'];
            }
        }
        if (!isset($this->metaCodes[$code])) {
            // no meta data for code
            return null;
        }
        return $this->metaCodes[$code];
    }

    /**
     * @param $jobId
     * @param $metadataCode
     * @return array
     */
    public function getMetadata($jobId, $metadataCode) {
        if (!$this->getMetaTypeId($metadataCode)) {
            return [];
        }
        $dataKey = 'jobs_' . $metadataCode;
        $selectJobData = $this->connection->getConnection()->select()
            ->from(
                ['job' => self::TABLE_ENTITY_FME_JOBS],
                [$dataKey]
            )->where('jobs_id = ?', $jobId);
        $results = $this->connection->getConnection()->fetchOne($selectJobData);
        $jobIds = explode(',', $results);
        $selectJobMeta = $this->connection->getConnection()->select();
        $selectJobMeta
            ->from(
                ['meta' => self::TABLE_ENTITY_FME_META_DATA],
                ['data_code', 'data_name']
            )->where(
                'type_code = ?', $this->getMetaTypeId($metadataCode)
            )->where('data_code IN (?)', $jobIds);
        $data = [];
        foreach ($this->connection->getConnection()->fetchAll($selectJobMeta) as $jobMeta) {
            $data[$jobMeta['data_code']] = $jobMeta['data_name'];
        }
        return $data;
    }

}
