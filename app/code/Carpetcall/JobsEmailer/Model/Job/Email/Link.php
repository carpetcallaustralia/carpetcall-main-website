<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Model\Job\Email;

/**
 * Class Link
 * @package Carpetcall\JobsEmailer\Model\Job\Email
 */
class Link extends \Magento\Framework\Model\AbstractModel implements \Carpetcall\JobsEmailer\Api\Data\Job\Email\LinkInterface
{
    const EMAIL_TYPE_TO = 'TO';
    const EMAIL_TYPE_BCC = 'BCC';

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * Link constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Serialize\Serializer\Json $serializer,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->serializer = $serializer;
    }

    /**
     * @inheridoc
     */
    public function getJobId() {
        $this->_getData(self::JOB_ID);
    }

    /**
     * @inheridoc
     */
    public function setJobId($jobId) {
        $this->setData(self::JOB_ID, $jobId);
    }

    /**
     * Initialize with resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init(\Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link::class);
    }

    /**
     * @return mixed|void
     */
    public function getEmails() {
        $emails = $this->_getData(self::EMAILS);
        if (!$emails) {
            return [];
        }
        return $emails;
    }

    /**
     * Store specific bcc header email data for a given state
     * @param $state
     * @param $emailTo
     * @return mixed|void
     */
    public function setStateEmailTo($state, $emailTo)
    {
        $emails = $this->getEmails();
        if (!isset($emails[$state])) {
            $emails[$state] = [];
        }
        if (!isset($emails[$state][self::EMAIL_TYPE_TO])) {
            $emails[$state][self::EMAIL_TYPE_TO] = [];
        }
        $emails[$state][self::EMAIL_TYPE_TO] = $emailTo;
        $this->setData(self::EMAILS, $emails);
    }

    /**
     * Store specific to header email data for a given state
     * @param $state
     * @param $emailBcc
     * @return mixed|void
     */
    public function setStateEmailBcc($state, $emailBcc)
    {
        $emails = $this->getEmails();
        if (!isset($emails[$state])) {
            $emails[$state] = [];
        }
        if (!isset($emails[$state][self::EMAIL_TYPE_BCC])) {
            $emails[$state][self::EMAIL_TYPE_BCC] = [];
        }
        $emails[$state][self::EMAIL_TYPE_BCC] = $emailBcc;
        $this->setData(self::EMAILS, $emails);
    }

    /**
     * @return \Carpetcall\JobsEmailer\Model\Job\Email\Link|void
     */
    public function beforeSave() {
        $emails = $this->getData(self::EMAILS);
        if (is_array($emails)) {
            $emails = $this->serializer->serialize($emails);
        }
        $this->setData(self::EMAILS_SERIALIZED, $emails);

        parent::beforeSave();
    }

    /**
     * @return \Carpetcall\JobsEmailer\Model\Job\Email\Link|void
     */
    public function afterLoad() {
        $emails = $this->getData(self::EMAILS);
        if (!($emails)) {
            $serializedEmails = $this->getData(self::EMAILS_SERIALIZED);
            $emails = $this->serializer->unserialize($serializedEmails);
        }
        $this->setData(self::EMAILS, $emails);
        parent::afterLoad();
    }


}
