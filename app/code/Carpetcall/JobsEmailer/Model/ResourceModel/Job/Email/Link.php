<?php

/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email;

/**
 * Class Link
 * @package Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email
 */
class Link extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    private $metadataPool;

    /**
     * Link constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\EntityManager\MetadataPool $metadataPool
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool,
        $connectionName = null)
    {
        parent::__construct($context, $connectionName);
        $this->metadataPool = $metadataPool;
        $this->_storeManager = $storeManager;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('carpetcall_jobs_email_link', 'link_id');
    }

    /**
     * @param $jobId
     * @return \Zend_Db_Statement_Interface
     */
    public function getJobEmailLinks($jobId) {
        $select = $this->_getLoadSelect('job_id', $jobId, $this);
        $select->order('link_id ' . \Zend_Db_Select::SQL_DESC);
        return $this->getConnection()->fetchRow($select);
    }

}
