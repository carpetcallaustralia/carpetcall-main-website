<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Helper;

/**
 * Class EmailLink
 * TECH DEBT because the job model is not built correctly
 * @see \FME\Jobs\Model\Job
 */
class EmailLink {

    /**
     * @var \Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link
     */
    private $emailLinkResource;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var \Carpetcall\CareerEmails\Helper\Data
     */
    private $careerEmailHelper;

    /**
     * EmailLink constructor.
     * @param \Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link $emailLinkResource
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param \Carpetcall\CareerEmails\Helper\Data $careerEmailHelper
     */
    public function __construct(
        \Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link $emailLinkResource,
        \Magento\Framework\Serialize\Serializer\Json $serializer,
        \Carpetcall\CareerEmails\Helper\Data $careerEmailHelper
    ) {
        $this->emailLinkResource = $emailLinkResource;
        $this->serializer = $serializer;
        $this->careerEmailHelper = $careerEmailHelper;
    }

    /**
     * @param $jobId
     * @return array|bool|float|int|mixed|string|null
     */
    public function getEmailMap($jobId) {
        $emailsSerialised = $this->emailLinkResource->getJobEmailLinks($jobId)[\Carpetcall\JobsEmailer\Api\Data\Job\Email\LinkInterface::EMAILS_SERIALIZED];
        $emailMap = $this->serializer->unserialize($emailsSerialised);
        return $emailMap;
    }

    /**
     * @param $jobId
     * @param $location
     * @return string[]
     */
    public function getEmails($jobId, $location = null) {

        $emailMap = $this->getEmailMap($jobId);

        if (!$location) {
            return [null];
        }

        if (!isset($emailMap[$location])) {
            return [null];
        }
        $emails = [];
        foreach ([\Carpetcall\JobsEmailer\Model\Job\Email\Link::EMAIL_TYPE_TO, \Carpetcall\JobsEmailer\Model\Job\Email\Link::EMAIL_TYPE_BCC] as $emailType) {
            if (!isset($emailMap[$location][$emailType])) {
                $emails[] = $this->careerEmailHelper->getCareerEmail($location, $emailType);
                continue;
            }
            $emails[] = $emailMap[$location][$emailType];
        }
        return $emails;
    }

    /**
     * Load existing emails for a given job id, in the format for the job form
     * @param $jobId
     * @return array
     */
    public function loadExistingEmailsForForm($jobId) {
        $emailMap = $this->getEmails($jobId);
        $emailsForForm = [];
        foreach ($this->careerEmailHelper->getEmailOptions() as $state => $options) {
            foreach ($options as $emailOption) {
                $emailFormKey = $state . '_' . strtolower($emailOption);
                $emailsForForm[$emailFormKey] = null;
                if (isset($emailMap[$state][strtoupper($emailOption)])) {
                    $emailsForForm[$emailFormKey] = $emailMap[$state][strtoupper($emailOption)];
                }
            }
        }
        return $emailsForForm;
    }

}
