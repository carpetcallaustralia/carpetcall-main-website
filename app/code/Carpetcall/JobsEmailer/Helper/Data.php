<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Helper;

/**
 * Class Data
 * @package Carpetcall\JobsEmailer\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var string[]
     */
    private $locationStateMap = [
        "New South Wales" => 'nsw',
        "Queensland" => 'qld',
        "South Australia" => 'sa',
        "Victoria" => 'vic',
        "Western Australia" => 'wa',
        "Australian Capital Territory" => 'act',
        "Tasmania" => 'tas',
    ];

    /**
     * @param $locationLabel
     * @return string|null
     */
    public function getStateFromLocation($locationLabel) {
        if (!in_array($locationLabel, array_keys($this->locationStateMap))) {
            return null;
        }
        return $this->locationStateMap[$locationLabel];
    }

}
