<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Test\Unit\Model\Job\Email;

/**
 * Class LinkTest
 * @package Carpetcall\JobsEmailer\Test\Unit\Model\Job\Email
 */
class LinkTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Carpetcall\JobsEmailer\Model\Job\Email\Link
     */
    private $_mock;

    /**
     * Do basic setup stuff
     */
    protected function setUp(): void {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $args = [
        ];

        $this->_mock = $this->objectManager->getObject(
            \Carpetcall\JobsEmailer\Model\Job\Email\Link::class,
            $args
        );

    }

    /**
     * Test Get Emails
     */
    public function testGetEmails() {
        $expected = ['unit-test-land' => 'my@test.email'];
        $this->_mock->setData('emails', $expected);
        $this->assertEquals($expected, $this->_mock->getEmails(), 'Email data setting failed');
    }

}
