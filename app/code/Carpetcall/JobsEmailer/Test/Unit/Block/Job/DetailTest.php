<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Test\Unit\Block\Job;

/**
 * Class DetailTest
 * @package Carpetcall\JobsEmailer\Test\Unit\Block\Job
 */
class DetailTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Carpetcall\JobsEmailer\Block\Job\Detail
     */
    private $_block;

    private $_template = 'Carpetcall_JobsEmailer::jobs/detail.phtml';

    private $moduleName = 'Carpetcall_JobsEmailer';

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $collectionStub;

    /**
     * Do basic setup stuff
     */
    protected function setUp(): void {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        /** Block Rendering Stuff */
        $fileResolverStub = $this->getMockBuilder(\Magento\Framework\View\Element\Template\File\Resolver::class)->disableOriginalConstructor()->getMock();
        $fileResolverStub->expects($this->any())->method('getTemplateFileName')->willReturn($this->getTemplateFilename());

        $readerStub = $this->getMockBuilder(\Magento\Framework\Filesystem\Directory\Read::class)->disableOriginalConstructor()->getMock();
        $readerStub->expects($this->any())->method('getRelativePath')->willReturn('phpunit');

        $filesystemStub = $this->getMockBuilder(\Magento\Framework\Filesystem::class)->disableOriginalConstructor()->getMock();
        $filesystemStub->expects($this->any())->method('getDirectoryRead')->willReturn($readerStub);

        $eventManagerStub = $this->createPartialMock(\Magento\Framework\Event\Manager::class, ['dispatch']);
        $eventManagerStub->expects($this->any())->method('dispatch')->willReturnSelf();

        $configStub = $this->getMockBuilder(\Magento\Framework\App\Config::class)->disableOriginalConstructor()->getMock();
        $configStub->expects($this->any())->method('getValue')->willReturn('');

        $appStateStub = $this->getMockBuilder(\Magento\Framework\App\State::class)->disableOriginalConstructor()->getMock();
        $appStateStub->expects($this->any())->method('getAreaCode')->willReturn(1);

        $validatorStub = $this->getMockBuilder(\Magento\Framework\View\Element\Template\File\Validator::class)->disableOriginalConstructor()->getMock();
        $validatorStub->expects($this->any())->method('isValid')->willReturn(true);

        $phpEngine = $this->objectManager->getObject(\Magento\Framework\View\TemplateEngine\Php::class);

        $enginePoolStub = $this->getMockBuilder(\Magento\Framework\View\TemplateEnginePool::class)->disableOriginalConstructor()->getMock();
        $enginePoolStub->expects($this->any())->method('get')->willReturn($phpEngine);

        $contextStub = $this->getMockBuilder(\Magento\Backend\Block\Template\Context::class)->disableOriginalConstructor()->getMock();
        $contextStub->expects($this->any())->method('getResolver')->willReturn($fileResolverStub);
        $contextStub->expects($this->any())->method('getFilesystem')->willReturn($filesystemStub);
        $contextStub->expects($this->any())->method('getEventManager')->willReturn($eventManagerStub);
        $contextStub->expects($this->any())->method('getScopeConfig')->willReturn($configStub);
        $contextStub->expects($this->any())->method('getAppState')->willReturn($appStateStub);
        $contextStub->expects($this->any())->method('getValidator')->willReturn($validatorStub);
        $contextStub->expects($this->any())->method('getEnginePool')->willReturn($enginePoolStub);
        /** Block Rendering Stuff End */

        $requestStub = $this->getMockBuilder(\Magento\Framework\App\Request\Http::class)->disableOriginalConstructor()->getMock();
        $requestStub->expects($this->any())->method('getParams')->willReturn(['id' => 5]);

        $this->collectionStub = $this->createMock(\FME\Jobs\Model\ResourceModel\Job\Collection::class);
        $this->collectionStub->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->collectionStub->expects($this->any())->method('addStoreViewFilter')->willReturnSelf();
        $collectionFactoryStub = $this->getFactoryMock($this->collectionStub, \FME\Jobs\Model\ResourceModel\Job\Collection::class);
        $blockJobStub = $this->getMockBuilder(\FME\Jobs\Block\Job::class)->disableOriginalConstructor()->getMock();
        $blockJobStub->expects($this->any())->method('getDepartmentName')->willReturnMap([
            ['unit-test', 'Unit Testing'],
            ['code-quality', 'Dept of Good Code'],
            ['test', 'Testing'],
        ]);
        $helperStub = $this->getMockBuilder(\FME\Jobs\Helper\Job::class)->disableOriginalConstructor()->getMock();
        $helperStub->expects($this->any())->method('getPopupEnable')->willReturn(true);
        $helperStub->expects($this->any())->method('isJobsCaptchaEnable')->willReturn(false);
        $coreRegistryStub = $this->getMockBuilder(\Magento\Framework\Registry::class)->disableOriginalConstructor()->getMock();
        $objectManagerStub = $this->getMockBuilder(\Magento\Framework\ObjectManagerInterface::class)->disableOriginalConstructor()->getMock();

        $storeStub = $this->createMock(\Magento\Store\Model\Store::class);
        $storeStub->expects($this->any())->method('getBaseUrl')->willReturn('http://unit-test.com/');
        $storeStub->expects($this->any())->method('__call')->willReturnMap([
            ['getStoreId', [], 2]
        ]);
        // Preferable to mock magic method __call instead https://github.com/sebastianbergmann/phpunit/issues/4240#issuecomment-751598912
//        $storeStub->expects($this->any())->method('getStoreId')->willReturn(2);
        $storemanagerStub = $this->getMockBuilder(\Magento\Store\Model\StoreManagerInterface::class)->disableOriginalConstructor()->getMock();
        $storemanagerStub->expects($this->any())->method('getStore')->willReturn($storeStub);

        $jobStub = $this->getMockBuilder(\FME\Jobs\Model\Job::class)->disableOriginalConstructor()->getMock();
        $jobStub->expects($this->any())->method('load')->willReturnSelf();
        $jobStub->expects($this->any())->method('getLocations')->willReturn([
            ['data_code' => 'unit-test', 'data_name' => 'First Place'],
        ]);
        $jobFactoryStub = $this->getFactoryMock($jobStub, \FME\Jobs\Model\Job::class);

        $jobResourceStub = $this->getMockBuilder(\FME\Jobs\Model\ResourceModel\Job::class)->disableOriginalConstructor()->getMock();
        $timezoneStub = $this->getMockBuilder(\Magento\Framework\Stdlib\DateTime\Timezone::class)->disableOriginalConstructor()->getMock();
        $timezoneStub->expects($this->any())->method('date')->willReturn(['date' => 'time is a construct']);
        $contextStub->expects($this->any())->method('getStoreManager')->willReturn($storemanagerStub);
        $contextStub->expects($this->any())->method('getLocaleDate')->willReturn($timezoneStub);
        $contextStub->expects($this->any())->method('getRequest')->willReturn($requestStub);
        $args = [
            'collectionFactory' => $collectionFactoryStub,
            'job' => $blockJobStub,
            'helper' => $helperStub,
            'coreRegistry' => $coreRegistryStub,
            'objectManager' => $objectManagerStub,
            'context' => $contextStub,
            'storemanager' => $storemanagerStub,
            'jobFactory' => $jobFactoryStub,
            'jobResource' => $jobResourceStub,
            'data' => ['template' => $this->_template],
        ];

        $this->_block = $this->objectManager->getObject(
            \Carpetcall\JobsEmailer\Block\Job\Detail::class,
            $args
        );

    }

    /**
     * @param string $area
     */
    private function getTemplateFilename($area = 'frontend') {
        /** @var \Magento\Framework\Component\ComponentRegistrar $componentRegistrar */
        $componentRegistrar = $this->objectManager->getObject(\Magento\Framework\Component\ComponentRegistrar::class);
        $modulePath = $componentRegistrar->getPath(\Magento\Framework\Component\ComponentRegistrar::MODULE, $this->moduleName);
        $templatePath = str_replace($this->moduleName.'::', '', $this->_template);
        $filePath = $modulePath . '/view/'.$area.'/templates/' . $templatePath;
        return $filePath;
    }

    /**
     * @param $path
     */
    protected function getFileContents($path, $withLine = false) {
        $componentRegistrar = $this->objectManager->getObject(\Magento\Framework\Component\ComponentRegistrar::class);
        $modulePath = $componentRegistrar->getPath(\Magento\Framework\Component\ComponentRegistrar::MODULE, $this->moduleName);
        $filesPath = $modulePath . '/Test/Unit/_files' . DIRECTORY_SEPARATOR;
        $fullPath = $filesPath . $path;
        if (file_exists($fullPath)) {
            if ($withLine) {
                return file_get_contents($fullPath);
            }
            return rtrim(file_get_contents($fullPath));
        }
        throw new \Exception('Failed to read file: ' . $fullPath);
    }

    /**
     *
     */
    public function testNoDataHtml() {
        $this->collectionStub->expects($this->any())->method('getData')->willReturn([]);
        $this->assertEquals($this->getFileContents('frontend/job/detail/no_item.html'), $this->_block->toHtml(), 'block testing output failed');
    }

    /**
     * Test to html
     */
    public function testToHtml() {
        $jobData = [
            [
                'jobs_title' => 'Unit Test',
                'jobs_publish_date' => '00000000', //  echo __(new Zend_Date(strtotime($item[0]['jobs_publish_date'])))
                'jobs_select_departments' => 'code-quality',
                'jobs_location' => 'unit-test',
                'jobs_description' => 'Unit Testing provides a great way to clean the code you are working with',
                'jobs_id' => '123',
                'jobs_job_type' => 'test',
            ]
        ];
        $this->collectionStub->expects($this->any())->method('getData')->willReturn($jobData);

        /**
         * Not triggering Failure
         * @see https://youtrack.jetbrains.com/issue/WI-46570
         * // $this->assertEquals($this->getFileContents('frontend/job/detail/test_item.html'), $this->_block->toHtml(), 'block testing output failed');
         */
//        $this->assertEquals(
//            $this->stripSpace(
//                $this->getFileContents('frontend/job/detail/test_item.html')
//            ),
//            $this->stripSpace(
//                $this->_block->toHtml()
//            ),
//            'block testing output failed'
//        );
        $difference = strcasecmp($this->stripSpace($this->getFileContents('frontend/job/detail/test_item.html')), $this->stripSpace($this->_block->toHtml()));
        if ($difference !== 0) {
            $componentRegistrar = $this->objectManager->getObject(\Magento\Framework\Component\ComponentRegistrar::class);
            $modulePath = $componentRegistrar->getPath(\Magento\Framework\Component\ComponentRegistrar::MODULE, $this->moduleName);
            $filesPath = $modulePath . '/Test/Unit/_files' . DIRECTORY_SEPARATOR;
            $fullPath = $filesPath . 'frontend/job/detail/test_item.html';
            $newTmpPath = $fullPath.'.tmp';
            file_put_contents($newTmpPath, $this->_block->toHtml());
            exec("cmd.exe /c 'C:\Program Files\JetBrains\PhpStorm 2021.1\bin\phpstorm.bat' diff '" . $this->windowsifyPath($fullPath) . "' '" . $this->windowsifyPath($newTmpPath) . "'");
        }
        $this->assertEquals(0, $difference, 'block testing output failed');
    }

    /**
     * Get factory and output mocks
     * @param \PHPUnit\Framework\MockObject\MockObject $mock
     * @param string $baseClass
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    private function getFactoryMock(\PHPUnit\Framework\MockObject\MockObject $mock, string $baseClass): \PHPUnit\Framework\MockObject\MockObject
    {
        $factoryStub = $this->getMockBuilder($baseClass . 'Factory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $factoryStub->expects($this->any())->method('create')->willReturn($mock);
        return $factoryStub;
    }

    /**
     * Windowsify path from linux to windows See WSL
     * @param string $linuxPath
     */
    private function windowsifyPath(string $linuxPath)
    {
        $windowsPath = str_replace('/mnt/c/', 'C:\\', $linuxPath);
        $windowsPath = str_replace('/', '\\', $windowsPath);
        return $windowsPath;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function stripSpace($data)
    {
//        return $data;
        return preg_replace('/\s+/', '', $data);
    }

}
