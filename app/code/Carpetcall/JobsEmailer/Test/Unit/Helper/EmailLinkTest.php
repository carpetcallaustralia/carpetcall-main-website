<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Helper;

/**
 * Class EmailLinkTest
 * @package Carpetcall\JobsEmailer\Helper
 */
class EmailLinkTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Carpetcall\JobsEmailer\Helper\EmailLink
     */
    private $_helper;

    /**
     * Do basic setup stuff
     */
    protected function setUp(): void {
        $emailLinkResourceStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link::class)->disableOriginalConstructor()->getMock();
        $emailLinkResourceStub->expects($this->any())->method('getJobEmailLinks')->willReturn([
            'emails_serialized' => 'This doe not matter. It is a unit test'
        ]);
        $serializerStub = $this->getMockBuilder(\Magento\Framework\Serialize\Serializer\Json::class)->disableOriginalConstructor()->getMock();
        $serializerStub->expects($this->any())->method('unserialize')->willReturn([

            'nsw' => ['TO' => 'test1@email.com,test6@email.com', 'BCC' => 'test2@email.com'],
            'qld' => ['TO' => 'test3@email.com', 'BCC' => 'test4@email.com'],
            'vic' => ['TO' => 'test5@email.com', 'BCC' => ''],
        ]);
        $careerEmailHelperStub = $this->getMockBuilder(\Carpetcall\CareerEmails\Helper\Data::class)->disableOriginalConstructor()->getMock();
        $careerEmailHelperStub->expects($this->any())->method('getEmailOptions')->willReturn([
            'nsw' => ['to', 'bcc'],
            'qld' => ['to', 'bcc'],
            'vic' => ['to', 'bcc']
        ]);

        $this->_helper = new \Carpetcall\JobsEmailer\Helper\EmailLink(
            $emailLinkResourceStub,
            $serializerStub,
            $careerEmailHelperStub
        );
    }

    /**
     * Test Load Existing Emails For Form
     */
    public function testLoadExistingEmailsForForm()
    {
        $formEmails = [
            'nsw_to' => 'test1@email.com,test6@email.com',
            'nsw_bcc' => 'test2@email.com',
            'qld_to' => 'test3@email.com',
            'qld_bcc' => 'test4@email.com',
            'vic_to' => 'test5@email.com',
            'vic_bcc' => '',
        ];
        $this->assertEquals($formEmails, $this->_helper->loadExistingEmailsForForm(123), 'Testing email form structure');
    }

}
