<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Test\Unit\Controller\Index;

/**
 * Class ApplicationTest
 * @package Carpetcall\JobsEmailer\Test\Unit\Controller\Index
 */
class ApplicationTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Carpetcall\JobsEmailer\Controller\Index\Application
     */
    private $_controller;

    /**
     * Do basic setup stuff
     */
    protected function setUp(): void
    {
        $uploaderFactoryStub = $this->getMockBuilder(\Magento\MediaStorage\Model\File\UploaderFactory::class)->disableOriginalConstructor()->getMock();
        $adapterFactoryStub = $this->getMockBuilder(\Magento\Framework\Image\AdapterFactory::class)->disableOriginalConstructor()->getMock();
        $filesystemStub = $this->getMockBuilder(\Magento\Framework\Filesystem::class)->disableOriginalConstructor()->getMock();
        $transportBuilderStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Model\Email\TransportBuilder::class)->disableOriginalConstructor()->getMock();
        $jobsHelperStub = $this->getMockBuilder(\FME\Jobs\Helper\Job::class)->disableOriginalConstructor()->getMock();
        $storeManagerStub = $this->getMockBuilder(\Magento\Store\Model\StoreManagerInterface::class)->disableOriginalConstructor()->getMock();
        $contextStub = $this->getMockBuilder(\Magento\Framework\App\Action\Context::class)->disableOriginalConstructor()->getMock();
        $modelFactoryStub = $this->getMockBuilder(\FME\Jobs\Model\ApplicationsFactory::class)->disableOriginalConstructor()->getMock();
        $scopeConfigStub = $this->getMockBuilder(\Magento\Framework\App\Config\ScopeConfigInterface::class)->disableOriginalConstructor()->getMock();
        $scopeConfigStub->expects($this->any())->method('getValue')->willReturnMap([
            ['trans_email/ident_support/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null, 'test@unit.com']
        ]);
        $jobFactoryStub = $this->getMockBuilder(\FME\Jobs\Model\JobFactory::class)->disableOriginalConstructor()->getMock();
        $metaDataServiceStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Model\Job\Metadata\GetJobService::class)->disableOriginalConstructor()->getMock();
        $emailLinkStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Helper\EmailLink::class)->disableOriginalConstructor()->getMock();
        $emailLinkStub->expects($this->any())->method('getEmails')->willReturnMap([
            [123, 'unit', ['test@intenrnal-unit.com', 'secret@unit.com']]
        ]);
        $loggerStub = $this->getMockBuilder(\Psr\Log\LoggerInterface::class)->disableOriginalConstructor()->getMock();
        $jobsEmailerHelperStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Helper\Data::class)->disableOriginalConstructor()->getMock();

        $this->_controller = new \Carpetcall\JobsEmailer\Controller\Index\Application(
            $uploaderFactoryStub,
            $adapterFactoryStub,
            $filesystemStub,
            $transportBuilderStub,
            $jobsHelperStub,
            $storeManagerStub,
            $contextStub,
            $modelFactoryStub,
            $scopeConfigStub,
            $jobFactoryStub,
            $metaDataServiceStub,
            $emailLinkStub,
            $jobsEmailerHelperStub,
            $loggerStub
        );
    }

    /**
     * @return array[]
     */
    public function internalEmailResolveDataProvider() {
        return [
            'no location' => [null, 'test@unit.com'],
            'email link defined' => [
                [
                    'jobs_id' => 123,
                    'locationName' => 'unit'
                ], ['test@intenrnal-unit.com', 'secret@unit.com']]
        ];
    }

    /**
     * @param $location
     * @param $expected
     * @dataProvider internalEmailResolveDataProvider
     */
    public function testInternalEmailResolves($location, $expected) {
        $this->assertEquals($expected, $this->_controller->getInternalEmail($location), 'internal email fails');
    }

}
