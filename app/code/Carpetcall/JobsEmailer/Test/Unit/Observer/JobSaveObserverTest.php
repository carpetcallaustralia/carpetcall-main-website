<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Test\Unit\Observer;

/**
 * Class JobsSaveObserverTest
 * @package Carpetcall\JobsEmailer\Test\Unit\Observer
 */
class JobSaveObserverTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Carpetcall\JobsEmailer\Observer\JobSaveObserver
     */
    private $_mock;

    /**
     * Do basic setup stuff
     */
    protected function setUp(): void {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $careerEmailHelperStub = $this->getMockBuilder(\Carpetcall\CareerEmails\Helper\Data::class)->disableOriginalConstructor()->getMock();
        $emailLinkFactoryStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Model\Job\Email\LinkFactory::class)->disableOriginalConstructor()->getMock();
        $linkResourceStub = $this->getMockBuilder(\Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link::class)->disableOriginalConstructor()->getMock();


        $this->_mock = new \Carpetcall\JobsEmailer\Observer\JobSaveObserver(
            $careerEmailHelperStub, $emailLinkFactoryStub, $linkResourceStub
        );

    }

    /**
     * Test jobs with emails set
     */
    public function testJobsWithEmailsSet() {
        // TODO: mock event data
        $this->markTestSkipped('Better to test as integration test');

        $eventStub = $this->createMock(\Magento\Framework\Event::class);
        $observerStub = $this->getMockBuilder(\Magento\Framework\Event\Observer::class)->disableOriginalConstructor()->getMock();
        $observerStub->expects($this->any())->method('getEvent')->willReturn($eventStub);
        $this->assertEquals(true, $this->_mock->execute($observerStub), 'Email linking failed');
    }

}
