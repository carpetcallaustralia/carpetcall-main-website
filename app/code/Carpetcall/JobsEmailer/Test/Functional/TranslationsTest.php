<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Test\Functional;

use Magento\Framework\App\Bootstrap;

/**
 * Class TranslationsTest
 * @package Carpetcall\JobsEmailer\Test\Functional
 */
class TranslationsTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Carpetcall\JobsEmailer\Model\Job\Email\Link
     */
    private $_mock;

    /**
     * Do basic setup stuff
     */
    protected function setUp(): void {

        require (__DIR__ . '/../../../../../bootstrap.php');
        $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
        $this->objectManager = $bootstrap->getObjectManager();

    }

    /**
     * Test translations
     */
    public function testTrasnslations() {
        $this->assertEquals("Select City", __("Select Gender")->render(), 'Gender is still showing');
    }

}
