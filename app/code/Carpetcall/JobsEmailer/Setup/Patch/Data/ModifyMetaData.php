<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Carpetcall\JobsEmailer\Setup\Patch\Data;

/**
 * Class ModifyMetaData
 * @package Carpetcall\JobsEmailer\Setup\Patch\Data
 * Change Gender to City
 */
class ModifyMetaData implements \Magento\Framework\Setup\Patch\DataPatchInterface, \Magento\Framework\Setup\Patch\PatchVersionInterface {
    const FME_META_TYPE_TABLE = 'fme_meta_type';

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * ModifyMetaData constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        try {
            $bind = ['type_name' => 'City'];
            $where = ['type_code = ?' => 'gender'];
            $connection->update($connection->getTableName(self::FME_META_TYPE_TABLE), $bind, $where);
        } catch (\Exception $exception) {
            die('broke - ' . $exception->getMessage());
            return;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }
}
