<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Carpetcall\JobsEmailer\Setup\Patch\Data;

/**
 * Class CreateAustralianMetaData
 * @package Carpetcall\JobsEmailer\Setup\Patch\Data
 * Create locations as Australian States
 */
class CreateAustralianMetaData implements \Magento\Framework\Setup\Patch\DataPatchInterface, \Magento\Framework\Setup\Patch\PatchVersionInterface {

    const FME_META_DATA_TABLE = 'fme_meta_data';

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var string[]
     */
    protected $_locations = [
        "New South Wales",
        "Queensland",
        "South Australia",
        "Victoria",
        "Western Australia",
        "Australian Capital Territory",
        "Tasmania",
    ];

    protected $_cities = [
        "Sydney",
        "Brisbane",
        "Adelaide",
        "Melbourne",
        "Perth",
        "Canberra",
        "Hobart",
    ];

    /**
     * CreateAustralianMetaData constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $selectLocationTypeCode = $this->moduleDataSetup->getConnection()->select();
        $selectLocationTypeCode->from(
            $this->moduleDataSetup->getConnection()->getTableName(
                \Carpetcall\JobsEmailer\Setup\Patch\Data\ModifyMetaData::FME_META_TYPE_TABLE
            ),
            ['id', 'type_code']
        )->where('type_code IN (?)', ['location', 'gender']);
        $typeCodes = [];
        foreach ($this->moduleDataSetup->getConnection()->fetchAll($selectLocationTypeCode) as $result) {
                $typeCodes[$result['type_code']] = $result['id'];
        }
        $this->createLocations($typeCodes['location']);
        $this->createCities($typeCodes['gender']);
    }

    /**
     * Create locations for each Australian state/territory
     */
    private function createLocations($locationTypeCode) {

        $locationData = [];
        foreach ($this->_locations as $location) {
            $locationData[] = [
                'data_code' => null,
                'data_name' => $location,
                'data_description' => null,
                'data_status' => 1,
                'type_code' => $locationTypeCode
            ];
        }

        if (empty($locationData)) {
            return;
        }

        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getConnection()->getTableName(self::FME_META_DATA_TABLE),
            array_keys($locationData[0]),
            $locationData
        );
    }

    /**
     * Create capital cities
     */
    private function createCities($cityTypeCode)
    {
        $cityData = [];
        foreach ($this->_cities as $city) {
            $cityData[] = [
                'data_code' => null,
                'data_name' => $city,
                'data_description' => null,
                'data_status' => 1,
                'type_code' => $cityTypeCode
            ];
        }

        if (empty($cityData)) {
            return;
        }

        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getConnection()->getTableName(self::FME_META_DATA_TABLE),
            array_keys($cityData[0]),
            $cityData
        );

    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }
}
