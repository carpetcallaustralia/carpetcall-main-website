<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Api\Data\Job\Email;

/**
 * Interface LinkInterface - Used to extend the FME_JobsManager jobs model to have custom emails based on the state
 */
interface LinkInterface {

    const JOB_ID = 'job_id';
    const EMAILS = 'emails';
    const EMAILS_SERIALIZED = 'emails_serialized';
    const STATE = 'state';

    /**
     * Get Job ID
     *
     * @return mixed
     */
    public function getJobId();

    /**
     * Set Job ID
     *
     * @param $jobId
     *
     * @return $this
     */
    public function setJobId($jobId);

    /**
     * Get all emails unserialized
     * @return mixed
     */
    public function getEmails();

    /**
     * @param $state
     * @return mixed
     */
    public function setStateEmailTo($state, $emailTo);

    /**
     * @param $state
     * @return mixed
     */
    public function setStateEmailBcc($state, $emailBcc);

}
