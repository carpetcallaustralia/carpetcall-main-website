<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Block\Job;

/**
 * Class Listing - used to view the jobs collection
 */
class Listing extends \FME\Jobs\Block\Job
{

    /**
     * @var \FME\Jobs\Model\JobFactory
     */
    private $jobFactory;

    /**
     * @var
     */
    private $_metaLocations;

    /**
     * Listing constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \FME\Jobs\Model\ResourceModel\Job\CollectionFactory $collectionFactory
     * @param \FME\Jobs\Model\ResourceModel\Mdata\CollectionFactory $metaCollection
     * @param \FME\Jobs\Helper\Job $jobHelper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Store\Model\StoreManagerInterface $storemanager
     * @param \Magento\Framework\App\Request\Http $request
     * @param \FME\Jobs\Model\JobFactory $jobFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\Jobs\Model\ResourceModel\Job\CollectionFactory $collectionFactory,
        \FME\Jobs\Model\ResourceModel\Mdata\CollectionFactory $metaCollection,
        \FME\Jobs\Helper\Job $jobHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\StoreManagerInterface $storemanager,
        \Magento\Framework\App\Request\Http $request,
        \FME\Jobs\Model\JobFactory $jobFactory
    ) {
        parent::__construct(
            $context,
            $collectionFactory,
            $metaCollection,
            $jobHelper,
            $objectManager,
            $date,
            $messageManager,
            $storemanager,
            $request
        );
        $this->jobFactory = $jobFactory;
    }

    /**
     * @return array
     */
    private function getLocations($selectedLocations)
    {
        $locations = [];
        $allLocations = $this->getMetaLocations();
        foreach ($allLocations as $location) {
            if (!in_array($location['data_code'], explode(',', $selectedLocations))) {
                continue;
            }
            $locations[] = [
                'value' => $location['data_code'],
                'label' => $location['data_name']
            ];
        }
        return $locations;

    }

    /**
     * @param $locationCodes
     * @return string
     */
    public function getAllLocationNames($locations)
    {
        $locationNames = [];
        foreach ($this->getLocations($locations) as $location) {
            $locationNames[] = $location['label'];
        }
        return implode(', ', $locationNames);
    }

    /**
     * @return array
     */
    private function getMetaLocations()
    {
        if ($this->_metaLocations) {
            return $this->_metaLocations;
        }
        // load location meta data for all jobs
        $this->_metaLocations = $this->jobFactory->create()->getLocations();
        return $this->_metaLocations;
    }

}
