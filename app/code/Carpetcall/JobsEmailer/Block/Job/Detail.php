<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Block\Job;

/**
 * Class Detail
 * @package Carpetcall\JobsEmailer\Block\Job
 */
class Detail extends \FME\Jobs\Block\Detail
{

    /**
     * @var \FME\Jobs\Model\JobFactory
     */
    private $jobFactory;

    /**
     * @var array
     */
    private $_locations;

    /**
     * Detail constructor.
     * @param \FME\Jobs\Model\ResourceModel\Job\CollectionFactory $collectionFactory
     * @param \FME\Jobs\Block\Job $job
     * @param \FME\Jobs\Helper\Job $helper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storemanager
     * @param \FME\Jobs\Model\JobFactory $jobFactory
     * @param array $data
     */
    public function __construct(
        \FME\Jobs\Model\ResourceModel\Job\CollectionFactory $collectionFactory,
        \FME\Jobs\Block\Job $job,
        \FME\Jobs\Helper\Job $helper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storemanager,
        \FME\Jobs\Model\JobFactory $jobFactory,
        array $data = []
    ) {
        $this->jobFactory = $jobFactory;
        parent::__construct($collectionFactory, $job, $helper, $coreRegistry, $objectManager, $context, $storemanager, $data);
    }

    /**
     * Check if there are multiple locations or not
     * @return int|void
     */
    public function isMultipleLocations()
    {
        return count($this->getLocations()) > 0;
    }

    /**
     * Get locations as array for populating location selector
     * @return int[]
     */
    public function getLocations()
    {
        if (!$this->_locations) {
            /** @var \FME\Jobs\Model\Job $job */
            $job = $this->getCurrentJob();
            if (!$job) {
                $this->_locations = [];
                return $this->_locations;
            }
            $selectedLocations = '';
            $jobCollection = $this->getJobDetail();
            if (is_array($jobCollection->getData())) {
                $selectedLocations = $jobCollection->getData()[0]['jobs_location'];
            }

            $jobLocations = $job->getLocations();
            if (!$jobLocations) {
                $this->_locations = [];
                return $this->_locations;
            }
            $locations = [];
            foreach ($jobLocations as $location) {
                if (!in_array($location['data_code'], explode(',', $selectedLocations))) {
                    continue;
                }
                $locations[] = [
                    'value' => $location['data_code'],
                    'label' => $location['data_name']
                ];
            }
            $this->_locations = $locations;
        }
        return $this->_locations;
    }

    /**
     * @return string
     */
    public function getAllLocationNames() {
        $locationNames = [];
        $locations = $this->getLocations();
        if (!$locations) {
            return null;
        }
        foreach ($locations as $location) {
            $locationNames[] = $location['label'];
        }
        return implode(', ', $locationNames);
    }

    /**
     * @return mixed|null
     */
    private function getCurrentJob()
    {
        $jobCollection = $this->getJobDetail();
        if (is_array($jobCollection->getData())) {
            $jobId = $jobCollection->getData()[0]['jobs_id'];
            // TODO: refactor to use jobrepository
            $job = $this->jobFactory->create();
            $job->load($jobId);
            return $job;
        }
        return null;
    }

}
