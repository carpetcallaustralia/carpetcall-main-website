<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 * Heavily based off Mageplaza free email attachments module
 * @link https://github.com/mageplaza/magento-2-email-attachments/
 */

namespace Carpetcall\JobsEmailer\Plugin\Email;

/**
 * Class TransportFactoryPlugin
 */
class TransportFactoryPlugin {

    /**
     * @param \Magento\Framework\Mail\TransportInterfaceFactory $subject
     * @param array $data
     *
     * @return mixed
     */
    public function beforeCreate(\Magento\Framework\Mail\TransportInterfaceFactory $subject, array $data = [])
    {
        return [$data];
    }

}
