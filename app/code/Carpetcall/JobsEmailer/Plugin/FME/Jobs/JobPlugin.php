<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Plugin\FME\Jobs;

/**
 * Class JobPlugin
 */
class JobPlugin {

    /**
     * @var array|mixed
     */
    private $allowedMetaDataTypes;

    /**
     * JobPlugin constructor.
     * @param array $allowedMetaDataTypes
     */
    public function __construct($allowedMetaDataTypes = []) {
        $this->allowedMetaDataTypes = $allowedMetaDataTypes;
    }

    /**
     * Limit meta data types to what is defined in di
     * @see \FME\Jobs\Model\Job::getTypes
     */
    public function afterGetTypes(\FME\Jobs\Model\Job $subject, array $types) {
        $allowedTypes = [];
        foreach ($types as $type) {
            if (!in_array($type['type_code'], $this->allowedMetaDataTypes)) {
                continue;
            }
            $allowedTypes[] = $type;
        }
        return $allowedTypes;
    }

}
