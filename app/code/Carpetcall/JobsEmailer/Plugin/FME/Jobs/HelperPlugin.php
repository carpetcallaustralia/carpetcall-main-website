<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Plugin\FME\Jobs;

/**
 * Class HelperPlugin
 */
class HelperPlugin {

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $_timezoneInterface;

    /**
     * HelperPlugin constructor.
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $_timezoneInterface
     */
    public function __construct(\Magento\Framework\Stdlib\DateTime\TimezoneInterface $_timezoneInterface) {
        $this->_timezoneInterface = $_timezoneInterface;
    }

    /**
     * Remove the time from the datetime
     *
     * @param \FME\Jobs\Helper\Job $subject
     * @param $result
     * @param $dateTime
     * @return string
     * @throws \Exception
     * @see \FME\Jobs\Helper\Job::getTimeAccordingToTimeZone
     */
    public function afterGetTimeAccordingToTimeZone(\FME\Jobs\Helper\Job $subject, $result, $dateTime) {
        $dateTimeAsTimeZone = $this->_timezoneInterface
            ->date(new \DateTime($dateTime))
            ->format('m/d/y');
        return $dateTimeAsTimeZone;
    }

}
