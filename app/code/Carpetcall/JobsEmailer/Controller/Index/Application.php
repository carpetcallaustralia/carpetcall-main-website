<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Controller\Index;

use  FME\Jobs\Model\Applications;
use  Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use  Magento\Framework\Exception\LocalizedException;
use  Psr\Log\LoggerInterface;
use  Magento\Framework\App\ObjectManager;
use  Magento\Framework\App\Config\ScopeConfigInterface;
use  Magento\Store\Model\ScopeInterface;

/**
 * Class Application
 * @package Carpetcall\JobsEmailer\Controller\Index
 */
class Application extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    protected $adapterFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Carpetcall\JobsEmailer\Model\Email\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \FME\Jobs\Model\ApplicationsFactory
     */
    protected $modelFactory;
    /**
     * @var \Psr\Log\LoggerInterface|null
     */
    private $logger;

    /**
     * @var \FME\Jobs\Helper\Job
     */
    private $jobsHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \FME\Jobs\Model\JobFactory
     */
    private $jobFactory;

    /**
     * @var \Carpetcall\JobsEmailer\Model\Job\Metadata\GetJobService
     */
    private $metaDataService;

    /**
     * @var \Carpetcall\JobsEmailer\Helper\EmailLink
     */
    private $emailLink;

    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    private $mediaReader;

    /**
     * @var \Carpetcall\JobsEmailer\Helper\Data
     */
    private $jobsEmailerHelper;

    /**
     * Application constructor.
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Image\AdapterFactory $adapterFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Carpetcall\JobsEmailer\Model\Email\TransportBuilder $transportBuilder
     * @param \FME\Jobs\Helper\Job $jobsHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Action\Context $context
     * @param \FME\Jobs\Model\ApplicationsFactory $modelFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \FME\Jobs\Model\JobFactory $jobFactory
     * @param \Carpetcall\JobsEmailer\Model\Job\Metadata\GetJobService $metaDataService
     * @param \Carpetcall\JobsEmailer\Helper\EmailLink $emailLink
     * @param \Carpetcall\JobsEmailer\Helper\Data $jobsEmailerHelper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Carpetcall\JobsEmailer\Model\Email\TransportBuilder $transportBuilder,
        \FME\Jobs\Helper\Job $jobsHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Action\Context $context,
        \FME\Jobs\Model\ApplicationsFactory $modelFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \FME\Jobs\Model\JobFactory $jobFactory,
        \Carpetcall\JobsEmailer\Model\Job\Metadata\GetJobService $metaDataService,
        \Carpetcall\JobsEmailer\Helper\EmailLink $emailLink,
        \Carpetcall\JobsEmailer\Helper\Data $jobsEmailerHelper,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->jobsHelper = $jobsHelper;
        $this->filesystem = $filesystem;
        $this->storeManager = $storeManager;
        $this->modelFactory = $modelFactory;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->jobFactory = $jobFactory;
        $this->metaDataService = $metaDataService;
        $this->emailLink = $emailLink;
        $this->mediaReader = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->jobsEmailerHelper = $jobsEmailerHelper;
        parent::__construct($context);
    }

    /**
     * Send Emails to custom config defined in Carpetcall_ContactusEmails
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute() {

        try {
            $files = $this->getRequest()->getFiles();
            $files = (array)$files;

            $saveData = [];
            if (!$files) {
                $this->messageManager->addErrorMessage(__('No CV attached'));
                $this->_redirect('*/*/');
                return;
            }

            foreach ($files as $key => $value) {

                if ($value['error'] == 1 && $value['size'] == 0) {
                    $this->messageManager->addErrorMessage(__('File size too large to upload'));
                    $this->_redirect('*/*/');
                    return;
                }

                $uploaderFactory = $this->uploaderFactory->create(['fileId' => $key]);

                $uploaderFactory->setAllowedExtensions(['pdf']);

                $file_ext = pathinfo($value['name'], PATHINFO_EXTENSION);


                if (!$uploaderFactory->checkAllowedExtension($file_ext)) {
                    $this->messageManager->addErrorMessage(__('File type ' . $value['name'] . ' not supported'));
                    $this->_redirect('*/*/');
                    return;
                }


                $imageAdapter = $this->adapterFactory->create();
                /* start of validated image */
                // $uploaderFactory->addValidateCallback('custom_image_upload',
                // $imageAdapter,'validateUploadFile');
                $uploaderFactory->setAllowRenameFiles(true);
                $uploaderFactory->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('fme_jobs');

                // Upload cv
                $result = $uploaderFactory->save($destinationPath);
                $imagepath = $result['file'];
                $saveData['cvfile'] = $imagepath;
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('*/*/');
            return;
        }
        $data = $this->getRequest()->getPostValue();
        if (!isset($data['jobs_id'])) {
            $this->messageManager->addErrorMessage("Something went wrong with the application");
            $this->_redirect('*/*/');
            return;
        }

        $locations = $this->metaDataService->getMetadata($data['jobs_id'], 'location');
        $data['locationName'] = $locations[$data['location']];
        $data['cityName'] = '';

        try {
            // TODO: handle applicant saving better
            foreach ($data as $key => $val) {
                $saveData[$key] = $val;
            }
            if (isset($saveData['jobs_location'])) {
                $saveData['jobs_location'] = $data['locationName'];
            }
            $application = $this->modelFactory->create();
            $application->setData($saveData);
            $application->save();
            $this->messageManager->addSuccessMessage(__('Your Application has been submitted.'));
        } catch (\Exception $exception) {
            $this->logger->error('Careers: Failed sending applicant details: ' . $exception->getMessage());
            $this->_redirect('*/*/');
            return;
        }

        try {
            $this->_emailToApplicant($data);
        } catch (\Exception $exception) {
            $this->logger->error('Careers: Applicant email sending failed: ' . $exception->getMessage());
            $this->_redirect('*/*/');
            return;
        }
        try {
            $this->_emailToInternal($data, $application);
        } catch (\Exception $exception) {
            $this->logger->error('Careers: Internal Applicant email sending failed: ' . $exception->getMessage());
            $this->_redirect('*/*/');
            return;
        }
        $this->_redirect('*/*/');
    }

    /**
     * Send email to applicant alerting them the application is received
     * @param $data
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function _emailToApplicant($data)
    {
        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($data);

        if (!isset($data['email'])) {
            return;
        }
        $to = $data['email'];
        $fullName = $data['fullname'] ?: '';

        $transportBuilder = clone $this->_transportBuilder;
        $transportBuilder->setTemplateIdentifier(\FME\Jobs\Helper\Job::JOB_EMAIL_TEMPLATE_APPLICANT_CONFIRMATION);
        $transportBuilder->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->storeManager->getStore()->getId(),
            ]
       );
        $transportBuilder->setTemplateVars(['fieldsData' => $postObject->getData()]);
        $transportBuilder->setFromByScope($this->jobsHelper->getSenderEmail());
        $transportBuilder->addTo($to, $fullName);
        $transport = $transportBuilder->getTransport();
        try {
            $transport->sendMessage();
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

    }

    /**
     * Send email to internal defined emails with applicant details
     * @param $jobData
     * @param $application
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function _emailToInternal($jobData, $application)
    {

        list($to, $bccEmail) = $this->getInternalEmail($jobData);
        if (!$to) {
            $this->messageManager->addErrorMessage('No internal email received');
            return;
        }

        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($jobData);

        $this->_transportBuilder
            ->setTemplateIdentifier(\FME\Jobs\Helper\Job::JOB_EMAIL_TEMPLATE_INTERNAL_CONFIRMATION)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getStore()->getId(),
                ]
            )->setTemplateVars(['fieldsData' => $postObject->getData()])
            ->setFromByScope([
                'name' => 'Carpet Call Careers Portal',
                'email' => 'noreply@carpetcall.com.au'
            ])
            ->addTo($to);
        if ($bccEmail) {
            if (is_array($bccEmail)) {
                $bccEmail = implode(',', $bccEmail);
            }
            $this->_transportBuilder->addBcc($bccEmail);
        }
        $attachmentName = sprintf('%s_%s.%s',
            $this->attachmentiseName($jobData['fullname']),
            $this->attachmentiseName($jobData['jobs_title']),
            'pdf'
        );
        if (!isset($application->getData()['cvfile'])) {
            $this->messageManager->addErrorMessage('No CV attached');
            return;
        }

        $cvContent = $this->getCV($application->getData()['cvfile']);
        $this->_transportBuilder->addAttachment(
            $cvContent,
            $attachmentName
        );
        $transport = $this->_transportBuilder->getTransport();
        try {
            $transport->sendMessage();
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }

    /**
     * @return mixed|null
     */
    public function getInternalEmail($jobData)
    {
        if (!$jobData) {
            return [$this->scopeConfig->getValue('trans_email/ident_support/email', ScopeInterface::SCOPE_STORE)];
        }
        if (!isset($jobData['locationName'])) {
            return [$this->scopeConfig->getValue('trans_email/ident_support/email', ScopeInterface::SCOPE_STORE)];
        }
        $locationName = $jobData['locationName'];
        if (!$locationName) {
            return [$this->scopeConfig->getValue('trans_email/ident_support/email', ScopeInterface::SCOPE_STORE)];
        }
        $internalEmails = $this->emailLink->getEmails($jobData['jobs_id'], $this->jobsEmailerHelper->getStateFromLocation($locationName));
        return $internalEmails;
    }

    /**
     * Clean name for attachment use
     * @param $name
     * @return array|string|string[]
     */
    private function attachmentiseName($name)
    {
        return str_replace(' ', '_', strtolower($name));
    }

    /**
     * Get applicant's CV content
     * @param $cvfile
     * @return string
     */
    private function getCV($cvfile)
    {
        $file = $this->mediaReader->getAbsolutePath('fme_jobs' . DIRECTORY_SEPARATOR . $cvfile);
        if (!$this->mediaReader->isFile($file)) {
            return null;
        }
        try {
            return $this->mediaReader->readFile($file);
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            // Failed reading the applicants CV
            return null;
        }

    }
}
