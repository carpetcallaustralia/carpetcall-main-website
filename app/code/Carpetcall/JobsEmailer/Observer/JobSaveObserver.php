<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Observer;

/**
 * Class JobsSaveObserver
 */
class JobSaveObserver implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @var \Carpetcall\CareerEmails\Helper\Data
     */
    private $careerEmailHelper;

    /**
     * @var \Carpetcall\JobsEmailer\Model\Job\Email\LinkFactory
     */
    private $emailLinkFactory;

    /**
     * @var \Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link
     */
    private $linkResource;

    /**
     * JobSaveObserver constructor.
     * @param \Carpetcall\CareerEmails\Helper\Data $careerEmailHelper
     * @param \Carpetcall\JobsEmailer\Model\Job\Email\LinkFactory $emailLinkFactory
     * @param \Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link $linkResource
     */
    public function __construct(
        \Carpetcall\CareerEmails\Helper\Data $careerEmailHelper,
        \Carpetcall\JobsEmailer\Model\Job\Email\LinkFactory $emailLinkFactory,
        \Carpetcall\JobsEmailer\Model\ResourceModel\Job\Email\Link $linkResource
    ) {
        $this->careerEmailHelper = $careerEmailHelper;
        $this->emailLinkFactory = $emailLinkFactory;
        $this->linkResource = $linkResource;
    }

    /**
     * Store emails defined on jobs object in email link
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$observer->getEvent()) {
            return $this;
        }
        $event = $observer->getEvent();
        /** @var \FME\Jobs\Model\Job $job */
        $job = $event->getData('job');
        $request = $event->getData('request');
        if (!$job) {
            // NOTHING to do
            return $this;
        }
        if (!$request) {
            // NOTHING to do
            return $this;
        }

        $job->unsetData('emails');

        if (is_array($job->getData('jobs_location'))) {
            $locations = implode(',', $job->getData('jobs_location'));
            $job->setData('jobs_location', $locations);
        }

        if (is_array($job->getData('jobs_gender'))) {
            // cities using gender
            $genders = implode(',', $job->getData('jobs_gender'));
            $job->setData('jobs_gender', $genders);
        }

        /** @var \Carpetcall\JobsEmailer\Model\Job\Email\Link $link */
        $link = $this->emailLinkFactory->create();
        $link->setJobId($job->getId());
        try {
            $this->populateEmails($link, $request);
            return $this;
        } catch (\Exception $exception) {
            // TODO: maybe log exceptions
            die($exception->getMessage());
            return $this;
        }

    }

    /**
     * Populate emails from request
     * @param \Carpetcall\JobsEmailer\Model\Job\Email\Link $link
     * @param $request
     */
    private function populateEmails(\Carpetcall\JobsEmailer\Model\Job\Email\Link $link, $request) {
        $data = $request->getPostValue();
        if (!$this->hasAnyEmails($data)) {
            return;
        }
        $emails = $data['emails'];
        foreach ($this->careerEmailHelper->getEmailOptions() as $state => $emailOption) {
            foreach ($emailOption as $sender) {
                $emailKey = $state . '_' . $sender;
                switch ($sender) {
                    case 'to':
                        if (isset($emails[$emailKey]) && $emails[$emailKey] !== '') {
                            $link->setStateEmailTo($state, $emails[$emailKey]);
                        }
                        break;
                    case 'bcc':
                        if (isset($emails[$emailKey]) && $emails[$emailKey] !== '') {
                            $link->setStateEmailBcc($state, $emails[$emailKey]);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        $this->linkResource->save($link);
    }

    /**
     * @param $data
     * @throws \Exception
     */
    private function hasAnyEmails($data) {
        if (!isset($data['emails'])) {
            throw new \Exception('Email config incorrect');
        }
        $emails = $data['emails'];
        $emailOptions = $this->careerEmailHelper->getEmailOptions();
        foreach ($emailOptions as $state => $emailOption) {
            foreach ($emailOption as $emailType) {
                if (isset($emails[$state.'_'.$emailType]) && $emails[$state.'_'.$emailType] !== '') {
                    return true;
                }
            }
        }
        return false;
    }
}
