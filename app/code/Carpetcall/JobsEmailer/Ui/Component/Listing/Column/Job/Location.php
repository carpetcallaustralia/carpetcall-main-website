<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\JobsEmailer\Ui\Component\Listing\Column\Job;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Location
 * Allow for multiple locations in listing
 * @package Carpetcall\JobsEmailer\Ui\Component\Listing\Column\Job
 */
class Location extends Column
{
    protected $mFactory;

    /**
     * Location constructor.
     * @param \FME\Jobs\Model\Job $mFactory
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \FME\Jobs\Model\Job $mFactory,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        $this->mFactory = $mFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (!isset($dataSource['data']['items'])) {
            return;
        }
        foreach ($dataSource['data']['items'] as &$item) {
            $item['jobs_location'] = $this->prepareItemLocation($item);
        }
        return $dataSource;
    }

    /**
     * @param $item
     */
    private function prepareItemLocation($item)
    {
        $locationIds = explode(',', $item['jobs_location']);
        $locationNames = [];
        $mCollection = $this->mFactory->getLocations();
        foreach ($locationIds as $locationId) {
            foreach ($mCollection as $locationData) {
                if ($locationId == $locationData['data_code']) {
                    $locationNames[] = $locationData['data_name'];
                }
            }
        }
        return implode(', ', $locationNames);
    }
}
