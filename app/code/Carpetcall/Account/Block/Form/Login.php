<?php
namespace Carpetcall\Account\Block\Form;
use Magento\Framework\View\Element\Template;
class Login extends \Magento\Customer\Block\Form\Login
{
 
	protected function _prepareLayout()
    {
        
        $this->pageConfig->setDescription('Visit the login for your customer account.');

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle('Customer Login');
        }
        $this->pageConfig->getTitle()->set(__('Login | Customer Account | Carpet Call'));
        return parent::_prepareLayout();
    }
}