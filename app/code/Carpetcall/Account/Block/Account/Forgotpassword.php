<?php
namespace Carpetcall\Account\Block\Account;
use Magento\Framework\View\Element\Template;
class Forgotpassword extends \Magento\Customer\Block\Account\Forgotpassword
{
 
	protected function _prepareLayout()
    {
        
        $this->pageConfig->setDescription('Forgotten your password to your customer account? Visit here.');

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle('Forgot Your Password?');
        }
        $this->pageConfig->getTitle()->set(__('Forgot Password | Customer Account | Carpet Call'));
        return parent::_prepareLayout();
    }
}