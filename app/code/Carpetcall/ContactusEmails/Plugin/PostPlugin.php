<?php
namespace Carpetcall\ContactusEmails\Plugin;

use Magento\Contact\Model\MailInterface;
use Magento\Framework\DataObject;
class PostPlugin
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    protected $request;
    private $mail;

    /**
     * Post constructor.
     * 
     * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Carpetcall\ContactusEmails\Model\ContactFactory $contactFactory,
        \Magento\Framework\App\RequestInterface $request,
        MailInterface $mail
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager = $messageManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->_contactFactory = $contactFactory;
        $this->request = $request;
        $this->mail = $mail;
    }

    /**
     * @param \Magento\Contact\Controller\Index\Post $subject
     * @return $this|void
     */
    public function aroundExecute(
        \Magento\Contact\Controller\Index\Post $subject
    ) {

        $post = $this->request->getPostValue();
        if (!$post) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        //$this->inlineTranslation->suspend();
        try {

            // echo "<pre>";
            // print_r($post['datetime']);die();
            $model = $this->_contactFactory->create();
            $model->setEnquiryType($post['enquiry_type']);
            $model->setFirstname($post['name']);
            $model->setLastname($post['lastname']);
            $model->setEmail($post['email']);
            $model->setPhone($post['telephone']);
            $model->setState($post['state']);
            $model->setStore($post['store']);
            $model->setMessage($post['comment']);
            $model->setCreatedAt($post['datetime']);
            $model->save();
            
            $this->sendEmail($this->validatedParams());
            $this->messageManager->addSuccessMessage(
                 __('Thanks for contacting us with your comments and questions. We\'ll respond to you very soon.')
             );
           return $this->resultRedirectFactory->create()->setPath('contact/index');
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            return $this->resultRedirectFactory->create()->setPath('contact/index');
        }
    }

    private function sendEmail($post)
    {
        $this->mail->send(
            $post['email'],
            ['data' => new DataObject($post)]
        );
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function validatedParams()
    {
        $request = $this->request;

        if (trim($request->getParam('name')) === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if (trim($request->getParam('comment')) === '') {
            throw new LocalizedException(__('Enter the comment and try again.'));
        }
        if (false === \strpos($request->getParam('email'), '@')) {
            throw new LocalizedException(__('The email address is invalid. Verify the email address and try again.'));
        }
        if (trim($request->getParam('hideit')) !== '') {
            throw new \Exception();
        }

        return $request->getParams();
    }
}