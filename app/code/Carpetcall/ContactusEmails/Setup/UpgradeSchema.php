<?php

namespace Carpetcall\ContactusEmails\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
          $table = $installer->getConnection()->newTable(
            $installer->getTable('contact_us')
                )->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [ 'identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, ],
                        'Entity ID'
                    )->addColumn(
                        'enquiry_type',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'Enquiry Type'
                    )->addColumn(
                        'firstname',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'FirstName'
                    )->addColumn(
                        'lastname',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'LastName'
                    )->addColumn(
                        'email',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'Email'
                    )->addColumn(
                        'phone',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'Phone'
                    )->addColumn(
                        'state',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'State'
                    )->addColumn(
                        'postcode',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'Postcode'
                    )->addColumn(
                        'store',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false, ],
                        'Store'
                    )->addColumn(
                        'message',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '2M',
                        [ 'nullable' => false, ],
                        'Message'
                    );
            $installer->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), "1.0.2", "<")) {
            $installer->getConnection()->addColumn(
                $installer->getTable('contact_us'),
                'created_at',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    'nullable' => true,
                    'comment' => 'Created At'
                ]
            );
        }
        $installer->endSetup();
    }
}