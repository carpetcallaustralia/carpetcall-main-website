<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Carpetcall\ContactusEmails\Model;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Area;
use Magento\Contact\Model\ConfigInterface;
use Mageplaza\StoreLocator\Model\LocationFactory;


class Mail extends \Magento\Contact\Model\Mail implements \Magento\Contact\Model\MailInterface
{
    /**
     * @var ConfigInterface
     */
    private $contactsConfig;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StateInterface
     */
    private $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    protected $scopeConfig;
    protected $locationFactory;

    /**
     * Initialize dependencies.
     *
     * @param ConfigInterface $contactsConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface|null $storeManager
     */
    public function __construct(
        ConfigInterface $contactsConfig,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager = null,
        LocationFactory $locationFactory
    ) {
        $this->contactsConfig = $contactsConfig;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
        $this->locationFactory = $locationFactory;
    }

    /**
     * Send email from contact form
     *
     * @param string $replyTo
     * @param array $variables
     * @return void
     */
    public function send($replyTo, array $variables)
{
    /** @see \Magento\Contact\Controller\Index\Post::validatedParams() */
    $replyToName = !empty($variables['data']['name']) ? $variables['data']['name'] : null;

    // echo "<pre>";
    // print_r($variables['data']['store']);die();

    $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    switch ($variables['data']['state']) {
        case "Tasmania":
            if ($variables['data']['enquiry_type'] == 'Sales enquiry') {
                $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_to', $storeScope);
                // $final_bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_bcc', $storeScope);

                $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }

            }else{
                $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_tas_to', $storeScope);
                $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_tas_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }
            break;
        case "New South Wales":
            if ($variables['data']['enquiry_type'] == 'Sales enquiry') {
                $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_to', $storeScope);
                //$final_bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_bcc', $storeScope);
                $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }else{
                $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw_to', $storeScope);
                $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }
            break;
        case "Queensland":
            if ($variables['data']['enquiry_type'] == 'Sales enquiry') {
                $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_to', $storeScope);
                //$final_bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_bcc', $storeScope);
                $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }else{
                $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_qld_to', $storeScope);
                $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_qld_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }
            break;
        case "South Australia":
            if ($variables['data']['enquiry_type'] == 'Sales enquiry') {
                $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_to', $storeScope);
                //$final_bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_bcc', $storeScope);
                $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }else{
                $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_sa_to', $storeScope);
                $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_sa_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }
            break;
        case "Victoria":
            if ($variables['data']['enquiry_type'] == 'Sales enquiry') {
                $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_to', $storeScope);
                //$final_bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_bcc', $storeScope);
                $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }else{
                $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_vic_to', $storeScope);
                $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_vic_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }
            break;
        case "Western Australia":
            if ($variables['data']['enquiry_type'] == 'Sales enquiry') {
                $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_to', $storeScope);
                //$final_bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_bcc', $storeScope);
                $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }
            }else{
                $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_wa_to', $storeScope);
                $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_wa_bcc', $storeScope);
                $bccemails_format = explode(',', $bccemails);
                
                foreach ($bccemails_format as $value) {
                    $final_bccemails[] = trim($value);
                }

            }
            break;
        default:
    }
    //die();

    if ($variables['data']['enquiry_type'] == 'Service enquiry') {

    // Conditions - Service enquiry
    // to: should be the email set in "Service enquiry admin email [name of state] State To"
    // bcc: should be the email set in "Service enquiry admin email [name of state] State Bcc"

    // $emails = 'testmscgoriteeps@gmail.com';
    // $final_bccemails = ['arjun.cmarix@gmail.com', 'testmysite24@gmail.com'];
    //$this->contactsConfig->emailSender();
    $formemail = $this->scopeConfig->getValue('trans_email/ident_general/email', $storeScope);
    $formname = $this->scopeConfig->getValue('trans_email/ident_general/name', $storeScope);
    $from = ['email' => $formemail, 'name' => $formname];
    

    $this->inlineTranslation->suspend();
    try {

        $transport = $this->transportBuilder
            ->setTemplateIdentifier($this->contactsConfig->emailTemplate())
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getStore()->getId()
                ]
            )
            ->setTemplateVars($variables)
            ->setFrom($from)
            ->addTo($emails)
            ->addBcc($final_bccemails)
            //->setReplyTo($replyTo, $replyToName)
            ->getTransport();

        $transport->sendMessage();
    } finally {
        $this->inlineTranslation->resume();
    }

    }else{

    // Conditions - Sales enquiry
    // to: should be the email set in the individual store
    // cc: should be the email set in "Sales enquiry admin email [name of state] State To"
    // bcc: should be the email set in "Sales enquiry admin email [name of state] State Bcc"

    $locations = $this->locationFactory->create()->getCollection();
    foreach ($locations as $key => $value) {
        if (strtoupper($value['name']) == $variables['data']['store']) {
             $store_email_add_for_to = $value['email'];
        }
    }

    // $emails = 'testmscgoriteeps@gmail.com';
    // $final_bccemails = ['arjun.cmarix@gmail.com', 'testmysite24@gmail.com'];
    //$this->contactsConfig->emailSender();
    $formemail = $this->scopeConfig->getValue('trans_email/ident_general/email', $storeScope);
    $formname = $this->scopeConfig->getValue('trans_email/ident_general/name', $storeScope);
    $from = ['email' => $formemail, 'name' => $formname];
    

    $this->inlineTranslation->suspend();
    try {

        $this->transportBuilder
            ->setTemplateIdentifier($this->contactsConfig->emailTemplate())
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getStore()->getId()
                ]
            )
            ->setTemplateVars($variables)
            ->setFrom($from)
            ->addTo($store_email_add_for_to);
            if (!empty($final_bccemails)) {
                $this->transportBuilder->addBcc($final_bccemails);
            }
            if (!empty($emails)) {
                $this->transportBuilder->addCc($emails);
            }
            $transport = $this->transportBuilder->getTransport();

        $transport->sendMessage();
    } finally {
        $this->inlineTranslation->resume();
    }

    }
   
    
}

}