<?php
namespace Carpetcall\ContactusEmails\Model;

class Contact extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Carpetcall\ContactusEmails\Model\ResourceModel\Contact');
    }
}

