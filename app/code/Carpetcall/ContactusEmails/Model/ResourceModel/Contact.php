<?php 
namespace Carpetcall\ContactusEmails\Model\ResourceModel;
class Contact extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

	public function _construct()
	{
		$this->_init("contact_us","id");
	}
}