<?php 
namespace Carpetcall\ContactusEmails\Model\ResourceModel\Contact;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	public function _construct()
	{
		$this->_init("Carpetcall\ContactusEmails\Model\Contact","Carpetcall\ContactusEmails\Model\ResourceModel\Contact");
	}
}