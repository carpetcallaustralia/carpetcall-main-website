<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 22/2/21
 * Time: 4:28 pm
 */

namespace Carpetcall\ProductConfigurator\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class UpdateSalesOrderEmailContent implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Carpetcall\ProductConfigurator\Helper\Option
     */
    protected $helper;

    /**
     * UpdateSalesOrderEmailContent constructor.
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Carpetcall\ProductConfigurator\Helper\Option $helper
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Carpetcall\ProductConfigurator\Helper\Option $helper
    )
    {
        $this->productRepository = $productRepository;
        $this->helper = $helper;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $transport = $observer->getTransport();
        /** @var $order \Magento\Sales\Model\Order */
        $order =  $transport->getOrder();

        $message = '';
        foreach ($order->getAllVisibleItems() as $item) {
            $product = $this->productRepository->getById($item->getProduct()->getId());
            $isBlinds = $this->helper->isBlindsProduct($product);
            $isShutters = $this->helper->isShutterProduct($product);

            if ($isBlinds || $isShutters) {
                $message = <<<EOF
<p>Thank you for shopping with Carpet Call! This email confirms your order has been placed and is being processed.</p>
<p>We will contact you to confirm your order details within 2 business days.  Please find attached our installation guide for your product, this will help you prepare for when your order is ready.</p>
<p>Your order details are below</p>.
EOF;
            }

        }

        if($order && $message) {
            $transport['blinds_shutter_message']  = $message;
            $transport['show_default_message'] = '';
        }else {
            $transport['show_default_message'] = 1;
        }
    }

}
