<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 12/1/21
 * Time: 1:52 pm
 */

namespace Carpetcall\ProductConfigurator\Observer;

class LayoutLoadBefore implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Carpetcall\ProductConfigurator\Helper\Data
     */
    protected $_helper;

    /**
     * LayoutLoadBefore constructor.
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Carpetcall\ProductConfigurator\Helper\Data $helper
    )
    {
        $this->_registry = $registry;
        $this->_helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $this->_registry->registry('current_product');

        if (!$product){
            return $this;
        }

        $categoryId = !empty($product->getCategoryIds()) ? $product->getCategoryIds()[0] : null;
        $allowedCategories = explode(',', $this->_helper->getCategoryLayoutUpdate());
        if ($categoryId && in_array($categoryId, $allowedCategories)){
            $layout = $observer->getLayout();
            $layout->getUpdate()->addHandle('catalog_product_view_product_configurator');
        }

        return $this;
    }
}
