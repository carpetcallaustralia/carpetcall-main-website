<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 12/1/21
 * Time: 2:09 pm
 */

namespace Carpetcall\ProductConfigurator\Model\Config\Source;

class ShutterMaterialType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var string[]
     */
    protected $_options = [ 'PVC', 'Timber', 'Paulownia' ];

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [ 'value' => '', 'label' => 'None' ];
        foreach ($this->_options as $option) {
            $options[] = ['value' => $this->_format($option), 'label' => $option];
        }

        return $options;
    }

    /**
     * @param $string
     * @return string
     */
    private function _format($string)
    {
        return strtolower(str_replace(' ', '-', $string));
    }
}
