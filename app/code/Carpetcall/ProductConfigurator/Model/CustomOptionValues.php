<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 13/1/21
 * Time: 2:04 pm
 */

namespace Carpetcall\ProductConfigurator\Model;


class CustomOptionValues
{
    const SHUTTER_MATERIAL_TYPE_NONE   = 0;

    const SHUTTER_MATERIAL_TYPE_PVC    = 1;

    const SHUTTER_MATERIAL_TYPE_TIMBER = 2;

}
