<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 7/4/21
 * Time: 12:24 pm
 */

namespace Carpetcall\ProductConfigurator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class DeleteCustomOption extends Command
{
    const IDENTIFIER = 'identifier';

    const PRODUCT_ID = 'id';

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('product_configurator:delete')
            ->setDescription('This delete a specific custom option by identifier.');

        $options = [
            new InputOption(
                self::IDENTIFIER,
                '-i',
                InputOption::VALUE_REQUIRED,
                'Identifier'
            ),

            new InputOption(
                self::PRODUCT_ID,
                '-p',
                InputOption::VALUE_REQUIRED,
                'Product Id'
            ),
            ];

        $this->setDefinition($options);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$identifier = $input->getOption(self::IDENTIFIER)) {
            $output->writeln('<error>Please provide a valid custom option identifier.</error>');
        }

        if (!$productId = $input->getOption(self::PRODUCT_ID)) {
            $output->writeln('<error>Please provide a valid product Id.</error>');
        }

        $product = $this->productFactory->create()->load($productId);

        if (!$product->getId()) {
            $output->writeln('<error>Product does not exist.</error>');
        }

        $isValid = 0;
        if ($product->getOptions()) {
            foreach ($product->getOptions() as $opt) {
                if ($opt->getIdentifier() === $identifier) {
                    $isValid++;
                    $opt->delete();
                    $output->writeln('<comment>Successfuly deleted '. $opt->getIdentifier() . ' custom option.</comment>');
                }
            }
        }

        if ($isValid == 0 ) {
            $output->writeln('<error>Identifier does not exist.</error>');
        }

        $output->writeln('<info>Command Finished.</info>');
    }
}
