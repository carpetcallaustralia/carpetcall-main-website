<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 15/2/21
 * Time: 10:34 am
 */

namespace Carpetcall\ProductConfigurator\Helper;


class Option extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Pektsekye\OptionImages\Model\Value
     */
    protected $_oiValue;

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $_mediaConfig;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;

    /**
     * @var \Magento\Catalog\Model\Product\OptionFactory
     */
    protected $_optionFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Pektsekye\OptionImages\Model\Value $oiValue,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    )
    {
        $this->_optionFactory = $optionFactory;
        $this->_oiValue = $oiValue;
        $this->_mediaConfig = $mediaConfig;
        $this->_imageHelper = $imageHelper;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * @param $product
     * @return array[]
     */
    public function getCustomImage($product)
    {
        $config = array('image' => array(), 'thumbnail' => array());

        $images = $this->_oiValue->getImages($product->getId());
        foreach ($images as $id => $image) {
            $valueId = (int) $id;
            $config['image'][$valueId] = $this->_mediaConfig->getMediaUrl($image);
            $config['thumbnail'][$valueId] = $this->_mediaConfig->getMediaUrl($image);
        }

        return $config;
    }

    /**
     * @param $identifier
     * @return \Magento\Framework\Phrase|string
     */
    public function getSubtitle($identifier) {
        $subTitle = '&nbsp;';
        switch ($identifier) {
            case 'width' :
                $subTitle = __('Shutters');
                break;
            case 'mid-rail-size-1' :
                $subTitle = __('Mid Rail Size');
                break;

        }
        return $subTitle;
    }

    public function getOptionToHideOnLoad($optionType, $identifier) {
        $class = '';
        if ($optionType == 'stained') {
            $class = 'hide';
        }elseif ($optionType == 'light-filtering' || $optionType == 'sunscreen') {
            $class = 'hide';
        }elseif ($identifier === 'bracket-covers'){
            if ($optionType == 'bracket-covers-grey' || $optionType == 'bracket-covers-ivory' || $optionType == 'bracket-covers-white') {
                $class = 'hide';
            }
        }
        return $class;
    }

    public function additionalTextHtml($identifier) {
        $html = '';
        if ($identifier == 'width') {
            $html = '<span class="mm">mm</span><p class="range-limit">min 200mm - max 2960mm</p>';
        } elseif ($identifier == 'drop') {
            $html = '<span class="mm">mm</span><p class="range-limit">min 200mm - max 3000mm</p>';
        } elseif ($identifier == 'mid-rail-size-1' || $identifier == 'mid-rail-size-2') {
            $html = '<span class="mm">mm</span>';
        }
        return $html;
    }

    public function includeFalseHeader($product, $identifier) {
        if ($product->getName() == 'Plantation Shutters' && $identifier === 'width') {
            return 'Measurements';
        }elseif ($identifier === 'room-name') {
            return 'Measuring Type';
        }
        return '';
    }

    public function includeSubLabel($product) {
        if ($product->getName() == 'Plantation Shutters') {
            return true;
        }
        return false;
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
     * @param $identifier @string
     * @return int
     */
    public function getOptionIdByIdentifier($product, $identifier)
    {
        $options = $product->getOptions();

        $optionId = 0;
        foreach ($options as $option) {
            if (strtolower($identifier) === $option->getIdentifier()) {
                $optionId = $option->getId();
            }
        }

        return $optionId;
    }

    public function isShutterProduct($product) {
        $categoryIds = $product->getCategoryIds();

        foreach ($categoryIds as $categoryId) {
            $category = $this->_categoryFactory->create()->load($categoryId);
            if ($category->getLevel() == 2) {
                if (strtolower($category->getName()) === 'shutters') {
                    return true;
                }
            }
        }
        return false;
    }

    public function isBlindsProduct($product) {
        $categoryIds = $product->getCategoryIds();

        foreach ($categoryIds as $categoryId) {
            $category = $this->_categoryFactory->create()->load($categoryId);
            if ($category->getLevel() == 2) {
                if (strtolower($category->getName()) === 'blinds') {
                    return true;
                }
            }
        }
        return false;
    }

    public function getDefaultClass($product, $optionTitle) {
        $class = 'inactive';
        if ($product->getUrlKey() == 'roller-blinds' && $optionTitle == 'single-roller-blind') {
            $class = 'active';
        }elseif ($product->getUrlKey() == 'double-roller-blinds' && $optionTitle == 'double-roller-blind') {
            $class = 'active';
        }
        return $class;
    }

}
