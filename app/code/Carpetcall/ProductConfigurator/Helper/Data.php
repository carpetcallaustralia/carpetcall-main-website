<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 12/1/21
 * Time: 11:47 am
 */

namespace Carpetcall\ProductConfigurator\Helper;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ENABLE = 'catalog/product_configurator/enable';

    const XML_PATH_CATEGORY_LAYOUT_UPDATE = 'catalog/product_configurator/category_layout_update';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    /**
     * @return array|mixed
     */
    public function getCategoryLayoutUpdate()
    {
        $categories =  $this->scopeConfig->getValue(
            self::XML_PATH_CATEGORY_LAYOUT_UPDATE,
            ScopeInterface::SCOPE_STORE
        );

        return !empty($categories) ? $categories : [];
    }

    /**
     * @return mixed
     */
    public function isEnable()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLE,
            ScopeInterface::SCOPE_STORE
        );
    }
}
