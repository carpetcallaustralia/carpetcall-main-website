<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 13/1/21
 * Time: 11:16 am
 */

namespace Carpetcall\ProductConfigurator\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('catalog_product_option'),
                'identifier',
                [
                    'type'     => Table::TYPE_TEXT,
                    'nullable' => true,
                    'default'  => '',
                    'comment'  => 'Identifier',
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('catalog_product_option'),
                'tooltip',
                [
                    'type'     => Table::TYPE_TEXT,
                    'nullable' => true,
                    'default'  => '',
                    'comment'  => 'Tooltip',
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('catalog_product_option'),
                'is_feature_option',
                [
                    'type'     => Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'default'  => 0,
                    'comment'  => 'Is Feature Option',
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('catalog_product_option_type_value'),
                'shutter_material',
                [
                    'type'     => Table::TYPE_TEXT,
                    'nullable' => true,
                    'default'  => null,
                    'comment'  => 'Shutter Material',
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            // Recreate missing column
            if (!$setup->getConnection()->tableColumnExists('catalog_product_option_type_value', 'option_type')) {
                $setup->getConnection()->dropColumn('catalog_product_option_type_value', 'colour_type');
                $setup->getConnection()->addColumn(
                    $setup->getTable('catalog_product_option_type_value'),
                    'option_type',
                    [
                        'type'     => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default'  => null,
                        'comment'  => 'Option Type',
                    ]
                );
            }

        }

        $installer->endSetup();
    }

}
