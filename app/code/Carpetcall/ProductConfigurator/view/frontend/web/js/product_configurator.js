define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url'
    ],
    function($, modal, urlBuilder) {
        "use strict";
        //creating jquery widget
        $.widget('carpetcall.productConfigurator', {
            options: {
                radiOoptionId: '',
                textOptionId: '',
                toggleBtnId: '',
                dropdownOptionId: '',
                checkboxOptionId: ''
            },

            _create: function() {
                this._bind();
            },

            //bind observers to elements
            _bind: function() {
                var self = this;

                $(document).ready(function() {
                    self._init();
                    self._initRadioButtons();
                    self._initCheckboxes();
                    self._initTextInputs();
                    self._initCustomInputs();
                    self._initToggleButtons();
                    self._beforeSumit();

                });
            },

            _init: function() {
                var self = this;

                // On page load initialise these elements.
                var midRail = $('.mid-rail'),
                    midRailQty = $('.mid-rail-qty'),
                    midRailSize1 = $('.mid-rail-size-1'),
                    midRailSize2 = $('.mid-rail-size-2'),
                    bladeSize = $('.blade-size'),
                    tiltRod = $('.tilt-rod'),

                    bracketCovers = $('.bracket-covers'),
                    motorType = $('.motor-type'),
                    operationType = $('.operation-type'),
                    springAssist = $('.spring-assist'),
                    colourFront = $('.colour-front'),
                    baserailTypeFront = $('.baserail-type-front'),
                    chainLengthMessage = $('.chain-length-msg'),
                    chainSize = $('.chain-size'),
                    chainSizeStainless = $('.chain-size-stainless'),
                    rollDirectionFront = $('.roll-direction-front'),
                    productConfiguratorWrapper = $('.product-configurator-wrapper');

                // For shutters
                // Hide mid rail, show it only if height is 1500mm - 2400mm
                midRail.hide();
                midRailQty.hide();
                midRailQty.find('.input-text').attr('placeholder', 'Please Enter 1 or 2');
                midRailSize1.hide();
                midRailSize2.hide();
                bladeSize.find('select>option:eq(1)').prop('selected', true).attr('disabled', true);
                bladeSize.find('select').attr('disabled', true);
                tiltRod.find('select >option:eq(1)').prop('selected', true).attr('disabled', true);
                tiltRod.find('select').attr('disabled', true);

                // For blinds
                bracketCovers.hide();
                motorType.find('select').hide();
                motorType.find('select').prop('disabled', true)
                operationType.find("[data-option-title=motorised]").parent().hide();
                springAssist.hide();
                colourFront.hide();
                baserailTypeFront.hide();
                chainLengthMessage.hide();
                chainSize.hide();
                chainSizeStainless.hide();
                rollDirectionFront.hide();
                productConfiguratorWrapper.find('.chain-size-stainless-row').hide();
                productConfiguratorWrapper.find('.mid-rail-item-row').hide();
                productConfiguratorWrapper.find('.bracket-covers-row').hide();
                productConfiguratorWrapper.find('.spring-assist-row').hide();
                productConfiguratorWrapper.find('.chain-size-row').hide();


                // This help us identify which options have invalid identifier
                productConfiguratorWrapper.find('.js-0').text('undefined');

            },

            _initRadioButtons: function() {
                var self = this,
                    selectedRadioOption = self.options.radiOoptionId;

                if (selectedRadioOption) {
                    $("#" + selectedRadioOption).change(function () {
                        var optionId = $(this).attr("data-option-id"),
                            optionTitle = $(this).attr("data-option-title"),
                            optionType = $(this).attr("data-option-type"),
                            optionTitleRaw = $(this).attr("data-option-title-raw"),
                            optionIdentifier = $(this).attr("data-option-identifier"),
                            isFeatureOption = $(this).attr("data-is-feature-option"),
                            productOptionWrapper = $('.product-options-wrapper'),
                            productConfiguratorWrapper =  $('.product-configurator-wrapper'),
                            width = $('.width').find('.input-text');

                        if ($(this).is(':checked')) {
                            var append = '';

                            if (optionIdentifier === 'fabric-type-back') {
                                append = ' / ';
                            }

                            productConfiguratorWrapper.find('.'+optionId).text(optionTitleRaw + append);

                            if (optionIdentifier === 'fabric-type' || optionIdentifier === 'fabric-type-back' || optionIdentifier === 'fabric-type-front') {
                                $("select[name='fabric-filter']").val(optionTitle).trigger("change");
                            }

                            if (optionIdentifier === 'stained-painted') {
                                productConfiguratorWrapper.find('.'+optionId).prev().text(optionType[0].toUpperCase() +  optionType.slice(1));
                            }

                            if (optionIdentifier === 'blind-type') {
                                if (optionTitle.toLowerCase() === 'single-roller-blind') {
                                    window.location.href  = urlBuilder.build('roller-blinds');
                                }else if (optionTitle.toLowerCase() === 'double-roller-blind') {
                                    window.location.href  = urlBuilder.build('double-roller-blinds');
                                }
                            }

                            if (optionIdentifier === 'shutter-material') {
                                var colourElem = $('.field.' + optionIdentifier).next(),
                                    selectedOption = optionTitle.toLowerCase(),
                                    stainBtn = $('#stained-btn'),
                                    paintedBtn = $('#painted-btn'),
                                    stainPaintedElem = $('.field.stained-painted');

                                colourElem.find('.field.choice').not('.'+selectedOption).hide();
                                colourElem.find('.field.choice.'+selectedOption).show();

                                if (optionTitle.toLowerCase() === 'paulownia') {
                                    // only show stained options for paulownia
                                    stainBtn.removeClass('hide').addClass('active');
                                    paintedBtn.removeClass('active').addClass('inactive');
                                    stainPaintedElem.find('.field.choice.stained').removeClass('hide');
                                    stainPaintedElem.find('.field.choice.painted').addClass('hide');
                                }else {
                                    stainBtn.addClass('hide').removeClass('active');
                                    stainPaintedElem.find('.field.choice.painted').removeClass('hide');
                                    stainPaintedElem.find('.field.choice.stained').addClass('hide');
                                }
                            }

                            if (optionIdentifier === 'mount') {
                                if (optionTitle.toLowerCase() === 'face-fit') {
                                    productOptionWrapper.find('.bracket-covers').show();
                                    productConfiguratorWrapper.find('.bracket-covers-row').show();
                                }else {
                                    productOptionWrapper.find('.bracket-covers').hide();
                                    productConfiguratorWrapper.find('.bracket-covers .radio').prop('checked', false);
                                    productConfiguratorWrapper.find('.bracket-covers-row').hide();
                                }
                            }

                            if (optionIdentifier === 'component-colour') {
                                productOptionWrapper.find('.bracket-covers').find('.field.choice:not(:last)').addClass('hide');
                                productOptionWrapper.find('.bracket-covers').find('.field.choice.'+optionType).removeClass('hide');
                            }

                            if (optionIdentifier === 'mid-rail') {
                                if (optionTitle.toLowerCase() === 'uneven') {
                                    productOptionWrapper.find('.mid-rail-qty').show();
                                    productOptionWrapper.find('.mid-rail-qty .input-text').val('');
                                    productConfiguratorWrapper.find('.mid-rail-item-row').show();
                                    productConfiguratorWrapper.find('.mid-rail-item-row-1').show();
                                    productConfiguratorWrapper.find('.mid-rail-item-row-2').show();
                                }else {
                                    productOptionWrapper.find('.mid-rail-size-1').hide();
                                    productOptionWrapper.find('.mid-rail-size-2').hide();
                                    productOptionWrapper.find('.mid-rail-qty').hide();
                                    productConfiguratorWrapper.find('.mid-rail-item-row-1').hide();
                                    productConfiguratorWrapper.find('.mid-rail-item-row-2').hide();

                                    self._resetMidRailValues();
                                }
                            }

                            if (optionIdentifier === 'operation-type') {
                                if (optionTitle === 'motorised') {
                                    self._showMotorisedOptions();
                                    self._hideChainDrivenOptions();
                                    self._resetPriceOfChainDrivenOptions();
                                    productConfiguratorWrapper.find('.chain-driven-option-row').hide();
                                    productConfiguratorWrapper.find('.motorised-option-row').show();

                                    if (width.val() >= 500 && width.val() <= 2099) {
                                        productConfiguratorWrapper.find('.motor-type select').prop('selectedIndex', 1).trigger("change");
                                    }

                                }else {
                                    self._showChainDrivenOptions();
                                    self._hideMotorisedOptions();
                                    self._resetPriceOfMotorisedOptions();
                                    productConfiguratorWrapper.find('.chain-driven-option-row').show();
                                    productConfiguratorWrapper.find('.motorised-option-row').hide();

                                    if (width.val() > 2100) {
                                        productConfiguratorWrapper.find('.motor-type select').prop('selectedIndex', 2).trigger("change");
                                    }
                                }
                            }

                            if (optionIdentifier === 'control-chain') {
                                var value = $('.drop').find('.input-text').val(),
                                    chainSize = '';

                                if ($(".control-chain input[type='radio']:checked").data('option-title') === 'stainless-steel') {
                                    chainSize = '.chain-size-stainless';
                                    productOptionWrapper.find('.chain-size select').prop('selectedIndex',0).trigger("change");
                                }else {
                                    chainSize = '.chain-size';
                                    productOptionWrapper.find('.chain-size-stainless select').prop('selectedIndex',0).trigger("change");
                                }

                                if (value <= 900) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 1).trigger("change");
                                }else if (value >= 901 && value <= 1200) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 2).trigger("change");
                                }else if (value >= 1201 && value <= 1500) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 3).trigger("change");
                                }else if (value >= 1501 && value <= 1800) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 4).trigger("change");
                                }else if (value >= 1801 && value <= 2100) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 5).trigger("change");
                                }else if (value >= 2101 && value <= 2400) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 6).trigger("change");
                                }else if (value >= 2401 && value <= 2700) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 7).trigger("change");
                                }else if (value >= 2701 && value <= 3000) {
                                    productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 8).trigger("change");
                                }

                                if (optionTitle === 'stainless-steel' && productOptionWrapper.find('.chain-length .checkbox').is(":checked")) {
                                    productOptionWrapper.find('.chain-size-stainless').show();
                                    productOptionWrapper.find('.chain-size').hide();
                                }else if (optionTitle !== 'stainless-steel' && productOptionWrapper.find('.chain-length .checkbox').is(":checked")) {
                                    productOptionWrapper.find('.chain-size-stainless').hide();
                                    productOptionWrapper.find('.chain-size').show();
                                }
                            }
                        }

                        var radio = $(this).parent().parent().find('.radio');
                        if (radio.is(':checked')) {
                            var radioId = $(this).attr('id');
                            radio.not('#'+ radioId).next().addClass('inactive').removeClass('active');
                            $(this).next().addClass('active').removeClass('inactive');
                            //radio.filter(":not([data-option-title= " + "'"+optionId+ "'"+ "])").next().addClass('inactive').removeClass('active');
                        }

                        if (isFeatureOption) {
                            var imgSrc = $(this).next().find('.option-image').attr('src');
                            productConfiguratorWrapper.find('.'+optionId).prev().attr('src', imgSrc);
                        }

                    });
                }

            },

            _initCheckboxes: function() {
                var selectedCheckbox = this.options.radiOoptionId,
                    productOptionWrapper = $('.product-options-wrapper'),
                    productConfiguratorWrapper = $('.product-configurator-wrapper');

                $("#" + selectedCheckbox).change(function () {
                    var optionIdentifier = $(this).attr("data-option-identifier"),
                        optionId = $(this).attr("data-option-id");
                    if (optionIdentifier === 'chain-length') {
                        if (this.checked) {
                            if ($(".control-chain input[type='radio']:checked").data('option-title') === 'stainless-steel') {
                                productOptionWrapper.find('.chain-size-stainless').show();
                                productOptionWrapper.find('.chain-size').hide();
                                productConfiguratorWrapper.find('.'+optionId).text('Yes');
                                productConfiguratorWrapper.find('.chain-size-row').hide();
                                productConfiguratorWrapper.find('.chain-size-stainless-row').show();
                            }else {
                                productOptionWrapper.find('.chain-size').show();
                                productOptionWrapper.find('.chain-size-stainless').hide();
                                productConfiguratorWrapper.find('.'+optionId).text('Yes');
                                productConfiguratorWrapper.find('.chain-size-row').show();
                                productConfiguratorWrapper.find('.chain-size-stainless-row').hide();
                            }
                        }else {
                            productOptionWrapper.find('.chain-size').hide();
                            productOptionWrapper.find('.chain-size-stainless').hide();
                            productOptionWrapper.find('.chain-size select').prop('selectedIndex',0).trigger("change");
                            productOptionWrapper.find('.chain-size-stainless select').prop('selectedIndex',0).trigger("change");
                            productConfiguratorWrapper.find('.'+optionId).text('No');
                            productConfiguratorWrapper.find('.chain-size-row').hide();
                            productConfiguratorWrapper.find('.chain-size-stainless-row').hide();
                        }
                    }
                });
            },

            _initTextInputs: function() {
                var self = this,
                    selectedTextInput   = self.options.textOptionId;

                if (selectedTextInput) {
                    $('#' + selectedTextInput).on('input', function(e){
                        var value = $(this).val(), //this.value,
                            optionId = $(this).attr("data-option-id"),
                            optionIdentifier = $(this).attr("data-option-identifier"),
                            width = $('.width').find('.input-text'),
                            productConfiguratorWrapper = $('.product-configurator-wrapper'),
                            productOptionWrapper = $('.product-options-wrapper'),
                            productConfiguratorSummary = $('.product-configurator-summary'),
                            //motorType = $('*[data-identifier="motor-type"]'),
                            motorType = $('.motor-type'),
                            operationType = $('.operation-type'),
                            metrics = ['width', 'drop', 'mid-rail-size-1', 'mid-rail-size-2'],
                            append = '',
                            chainLengthMessage = $('.chain-length-msg'),
                            recommendedSize = '';

                        if (metrics.includes(optionIdentifier) && value > 0) {
                            append = 'mm';
                        }

                        productConfiguratorWrapper.find('.'+optionId).text(value + append);

                        if (optionIdentifier.toLowerCase() === 'drop') {
                            if (value >= 1500 && value <= 2400) {
                                productOptionWrapper.find('.mid-rail').show();
                                productConfiguratorWrapper.find('.mid-rail-item-row').show();
                                $('.mid-rail').find('.radio').next().removeClass('active').addClass('inactive');
                            }else {
                                 $('.mid-rail').find('.radio').prop('checked', false)
                                productOptionWrapper.find('.mid-rail').hide();
                                productOptionWrapper.find('.mid-rail-size-1').hide();
                                productOptionWrapper.find('.mid-rail-size-2').hide();
                                productOptionWrapper.find('.mid-rail-qty').hide();
                                productConfiguratorWrapper.find('.mid-rail-item').hide();

                                self._resetMidRailValues();
                            }

                            // Recommended chain size
                            var chainSize = '.chain-size';
                            if ($(".control-chain input[type='radio']:checked").data('option-title') === 'stainless-steel') {
                                chainSize = '.chain-size-stainless';
                            }

                            if (value <= 900) {
                                recommendedSize = 500;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 1).trigger("change");
                            }else if (value >= 901 && value <= 1200) {
                                recommendedSize = 750;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 2).trigger("change");
                            }else if (value >= 1201 && value <= 1500) {
                                recommendedSize = 1000;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 3).trigger("change");
                            }else if (value >= 1501 && value <= 1800) {
                                recommendedSize = 1250;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 4).trigger("change");
                            }else if (value >= 1801 && value <= 2100) {
                                recommendedSize = 1500;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 5).trigger("change");
                            }else if (value >= 2101 && value <= 2400) {
                                recommendedSize = 1750;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 6).trigger("change");
                            }else if (value >= 2401 && value <= 2700) {
                                recommendedSize = 2000;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 7).trigger("change");
                            }else if (value >= 2701 && value <= 3000) {
                                recommendedSize = 2250;
                                productConfiguratorWrapper.find(chainSize + ' select').prop('selectedIndex', 8).trigger("change");
                            }

                            // Recommended Message
                            chainLengthMessage.show();
                            chainLengthMessage.find('.js-selected-drop').text(value);
                            chainLengthMessage.find('.js-recommended-chain-size').text(recommendedSize);
                        }

                        if (optionIdentifier.toLowerCase() === 'width') {
                            if (value >= 500) {
                                operationType.find("[data-option-title=motorised]").parent().show();
                            }else {
                                operationType.find("[data-option-title=motorised]").parent().hide();
                            }

                            if (value >= 550) {
                                productOptionWrapper.find('.spring-assist').show();
                                productConfiguratorWrapper.find('.spring-assist-row').show();
                            } else {
                                productOptionWrapper.find('.spring-assist').hide();
                                productConfiguratorWrapper.find('.spring-assist .radio').prop('checked', false);
                                productConfiguratorWrapper.find('.spring-assist-row').hide();
                            }
                        }

                        var motorTypeDropdown = $('.motor-type select');
                        if (motorTypeDropdown.length) {
                            var res = motorTypeDropdown.attr('id').split('_'),
                                motorTypeOptionId = res[1];
                            if (width.val() >= 500 && width.val() <= 2099) {
                                $('.motor-type select>option:eq(1)').attr('selected', true);
                                motorTypeDropdown.trigger("change");
                                motorTypeDropdown.show();
                                productConfiguratorWrapper.find('.js-'+motorTypeOptionId).text(motorType.find('select option:selected').text());
                            }else if (width.val() > 2100) {
                                $('.motor-type select>option:eq(2)').attr('selected', true);
                                motorTypeDropdown.trigger("change");
                                motorTypeDropdown.show();
                                productConfiguratorWrapper.find('.js-'+motorTypeOptionId).text(motorType.find('select option:selected').text());
                            }else {
                                $('.motor-type select>option:eq(0)').attr('selected', true);
                                motorTypeDropdown.trigger("change");
                                motorTypeDropdown.hide();
                                productConfiguratorWrapper.find('.js-'+motorTypeOptionId).text('');
                            }
                        }

                        if (optionIdentifier.toLowerCase() === 'mid-rail-qty') {
                            if (value == 1) {
                                productOptionWrapper.find('.mid-rail-size-1').show();
                                productOptionWrapper.find('.mid-rail-size-2').hide();
                                productConfiguratorWrapper.find('.mid-rail-item-row-1').show();
                                productConfiguratorWrapper.find('.mid-rail-item-row-2').hide();
                            }else if (value == 2) {
                                productOptionWrapper.find('.mid-rail-size-1').show();
                                productOptionWrapper.find('.mid-rail-size-2').show();
                                productConfiguratorWrapper.find('.mid-rail-item-row-1').show();
                                productConfiguratorWrapper.find('.mid-rail-item-row-2').show();
                            }else {
                                productOptionWrapper.find('.mid-rail-size-1').hide();
                                productOptionWrapper.find('.mid-rail-size-2').hide();
                                productConfiguratorWrapper.find('.mid-rail-item-row-1').hide();
                                productConfiguratorWrapper.find('.mid-rail-item-row-2').hide();
                                productConfiguratorWrapper.find('.mid-rail-item-row-1 [data-option-identifier=mid-rail-size-1]').text('');
                                productConfiguratorWrapper.find('.mid-rail-item-row-2 [data-option-identifier=mid-rail-size-2]').text('');
                                productOptionWrapper.find("[data-option-identifier=mid-rail-size-1]").val('');
                                productOptionWrapper.find("[data-option-identifier=mid-rail-size-2]").val('');
                            }
                        }

                    });
                }

                $('#duplicate-qty').on('input', function(e){
                    $('#qty').val($(this).val());
                });

            },

            _initCustomInputs: function() {
                var optionTypeElem = $('.field.choice.admin__field.admin__field-option'),
                    paintedColourElem = $('.field.choice.admin__field.admin__field-option.painted'),
                    stainedColourElem = $('.field.choice.admin__field.admin__field-option.stained');

                $('#stained-btn').click(function(event) {
                    event.preventDefault();
                    if (optionTypeElem.hasClass('stained')) {
                        $(this).addClass('active').removeClass('inactive');
                        $(this).siblings().addClass('inactive').removeClass('active');
                        optionTypeElem.removeClass('hide');
                        paintedColourElem.addClass('hide');
                    }
                });

                $('#painted-btn').click(function(event) {
                    event.preventDefault();
                    if (optionTypeElem.hasClass('painted')) {
                        $(this).addClass('active').removeClass('inactive');
                        $(this).siblings().addClass('inactive').removeClass('active');
                        optionTypeElem.removeClass('hide');
                        stainedColourElem.addClass('hide');
                    }
                });

                var selectedDropdown = this.options.dropdownOptionId;
                $("#" + selectedDropdown).change(function () {
                    var selectedOption = $(this).val(),
                        identifier = $(this).attr("data-select-identifier"),
                        elem = $('.product-options-wrapper .field.' + identifier);

                    elem.find('.field.choice').not('.'+selectedOption).hide();
                    elem.find('.field.choice.'+selectedOption).removeClass('hide').show();

                });

                $("select[data-option-identifier=chain-size], select[data-option-identifier=chain-size-stainless]").change(function () {
                    var optionId = $(this).attr("data-option-id"),
                        optionIdentifier = $(this).attr("data-option-identifier"),
                        productConfiguratorWrapper =  $('.product-configurator-wrapper'),
                        selectedText = $(this).find("option:selected").text();

                    if (optionIdentifier === 'chain-size') {
                        productConfiguratorWrapper.find('.chain-size-row').show();
                        productConfiguratorWrapper.find('.chain-size-stainless-row').hide();
                        productConfiguratorWrapper.find('.'+optionId).text(selectedText);
                    }else if (optionIdentifier === 'chain-size-stainless') {
                        productConfiguratorWrapper.find('.chain-size-row').hide();
                        productConfiguratorWrapper.find('.chain-size-stainless-row').show();
                        productConfiguratorWrapper.find('.'+optionId).text(selectedText);
                    }

                });
            },

            _initToggleButtons: function() {
                var selectedToggleBtn = this.options.toggleBtnId,
                    productOptionWrapper = $('.product-options-wrapper');

                $('#' + selectedToggleBtn + '-back').click(function(event) {
                    event.preventDefault();

                    var optionIdentifier = $(this).attr("data-option-identifier"),
                        target = $(this).attr("data-target");

                    if (optionIdentifier === target) {
                        $(this).addClass('active').removeClass('inactive');
                        $(this).next().addClass('inactive');
                        return;
                    }
                    productOptionWrapper.find('.field.' + optionIdentifier).hide();
                    productOptionWrapper.find('.field.' + target).show();
                    productOptionWrapper.find('.field.' + target).find(("[data-target='" + target + "']")).addClass('active').removeClass('inactive');
                });

                $('#' + selectedToggleBtn + '-front').click(function(event) {
                    event.preventDefault();

                    var optionIdentifier = $(this).attr("data-option-identifier"),
                        target = $(this).attr("data-target");

                    if (optionIdentifier === target) {
                        $(this).addClass('active').removeClass('inactive');
                        $(this).prev().addClass('inactive');
                        return;
                    }
                    productOptionWrapper.find('.field.' + target).find(("[data-target='" + target + "']")).addClass('active').removeClass('inactive');
                    productOptionWrapper.find('.field.' + optionIdentifier).hide();
                    productOptionWrapper.find('.field.' + target).show();
                });
            },

            _beforeSumit: function() {
                $('#product_addtocart_form').on('submit', function(event) {
                    $.each($('.product-options-wrapper .fieldset > .field'), function() {
                        // Make sure all hidden required custom options have values selected.
                        if($(this).is(":hidden")) {
                            var cssClass = $(this).attr('class');
                            if ($(this).find('.toggle-btn').length) {
                                if ($(this).find('.option-image-wrapper.active').length === 0) {
                                    if (cssClass.includes('back')) {
                                        $(this).show();
                                        $(this).next().hide();
                                        $(this).find('.toggle-btn').children('a').eq(0).addClass('active').removeClass('inactive');
                                    }else if (cssClass.includes('front')) {
                                        $(this).show();
                                        $(this).prev().hide();
                                        $(this).find('.toggle-btn').children('a').eq(1).addClass('active').removeClass('inactive');
                                    }
                                }
                            }
                        }

                    });

                });

            },

            _resetMidRailValues: function () {
                var productConfiguratorSummary = $('.product-configurator-summary'),
                    productConfiguratorWrapper = $('.product-configurator-wrapper');

                productConfiguratorSummary.find("[data-option-identifier=mid-rail-size-1]").text('mm');
                productConfiguratorSummary.find("[data-option-identifier=mid-rail-size-2]").text('mm');
                productConfiguratorWrapper.find("[data-option-identifier=mid-rail-size-1]").val('');
                productConfiguratorWrapper.find("[data-option-identifier=mid-rail-size-2]").val('');
                productConfiguratorWrapper.find('.mid-rail-qty .input-text').val('');
            },

            _showMotorisedOptions: function() {
                $('.field.motor-type').show();
                $('.field.remote').show();
                $('.field.charger-cable').show();
                $('.field.wifi-bridge').show();
                $('.field.motor-side').show();
            },

            _hideMotorisedOptions: function() {
                $('.field.motor-type').hide();
                $('.field.remote').hide();
                $('.field.charger-cable').hide();
                $('.field.wifi-bridge').hide();
                $('.field.motor-side').hide();
            },

            _showChainDrivenOptions: function() {
                var productOptionWrapper = $('.product-options-wrapper');
                $('.field.control-chain').show();
                $('.field.control-side').show();
                $('.field.chain-length').show();

                if ($(".control-chain input[type='radio']:checked").data('option-title') === 'stainless-steel' && productOptionWrapper.find('.chain-length .checkbox').is(":checked")) {
                    $('.field.chain-size-stainless').show();
                }else if ($(".control-chain input[type='radio']:checked").data('option-title') !== 'stainless-steel' && productOptionWrapper.find('.chain-length .checkbox').is(":checked")) {
                    $('.field.chain-size').show();
                }
            },

            _hideChainDrivenOptions: function() {
                $('.field.control-chain').hide();
                $('.field.control-side').hide();
                $('.field.chain-length').hide();
                $('.field.chain-size').hide();
                $('.field.chain-size-stainless').hide();
                $('.field.spring-assist').hide();
            },

            _resetPriceOfMotorisedOptions: function () {
                var productOptionWrapper = $('.product-options-wrapper'),
                    remote = $('.field.remote'),
                    chargerCable = $('.field.charger-cable'),
                    wifiBridge = $('.field.wifi-bridge'),
                    motorSide = $('.field.motor-side');
                productOptionWrapper.find('.motor-type select').prop('selectedIndex',0).trigger("change");
                remote.find("[data-option-title=no]").trigger('click').prop('checked', false);
                remote.find("[data-option-title=no]").next().removeClass('active').addClass('inactive');
                chargerCable.find("[data-option-title=no]").trigger('click').prop('checked', false);
                chargerCable.find("[data-option-title=no]").next().removeClass('active').addClass('inactive');
                wifiBridge.find("[data-option-title=no]").trigger('click').prop('checked', false);
                wifiBridge.find("[data-option-title=no]").next().removeClass('active').addClass('inactive');
                motorSide.find(".radio").prop('checked', false);
                motorSide.find(".radio").next().removeClass('active').addClass('inactive');
            },

            _resetPriceOfChainDrivenOptions: function () {
                var productOptionWrapper = $('.product-options-wrapper'),
                    springAssist = $('.field.spring-assist'),
                    controlChain = $('.field.control-chain'),
                    controlSide = $('.field.control-side');
                springAssist.find("[data-option-title=no]").trigger('click').prop('checked', false);
                springAssist.find("[data-option-title=no]").next().removeClass('active').addClass('inactive');
                controlChain.find(".radio").prop('checked', false);
                controlChain.find(".radio").next().removeClass('active').addClass('inactive');
                controlSide.find(".radio").prop('checked', false);
                controlSide.find(".radio").next().removeClass('active').addClass('inactive');
                productOptionWrapper.find('.chain-size select').prop('selectedIndex',0).trigger("change");
                productOptionWrapper.find('.chain-size-stainless select').prop('selectedIndex',0).trigger("change");
            }

        });

        return $.carpetcall.productConfigurator;
    }
);
