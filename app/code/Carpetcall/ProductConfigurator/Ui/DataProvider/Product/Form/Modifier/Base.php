<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 13/1/21
 * Time: 11:21 am
 */

namespace Carpetcall\ProductConfigurator\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Field;
use Carpetcall\ProductConfigurator\Model\CustomOptionValues;

class Base extends AbstractModifier
{
    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var \Carpetcall\ProductConfigurator\Model\Config\Source\OptionType
     */
    protected $optionType;

    /**
     * @var \Carpetcall\ProductConfigurator\Model\Config\Source\ShutterMaterialType
     */
    protected $shutterMaterialType;

    /**
     * Base constructor.
     * @param \Carpetcall\ProductConfigurator\Model\Config\Source\OptionType $optionType
     * @param \Carpetcall\ProductConfigurator\Model\Config\Source\ShutterMaterialType $shutterMaterialType
     */
    public function __construct(
        \Carpetcall\ProductConfigurator\Model\Config\Source\OptionType $optionType,
        \Carpetcall\ProductConfigurator\Model\Config\Source\ShutterMaterialType $shutterMaterialType
    )
    {
        $this->optionType = $optionType;
        $this->shutterMaterialType = $shutterMaterialType;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;

        $this->addFields();

        return $this->meta;
    }

    /**
     * Adds fields to the meta-data
     */
    protected function addFields()
    {
        $groupCustomOptionsName    = CustomOptions::GROUP_CUSTOM_OPTIONS_NAME;
        $optionContainerName       = CustomOptions::CONTAINER_OPTION;
        $commonOptionContainerName = CustomOptions::CONTAINER_COMMON_NAME;

        // Add fields to the option
        $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
        [$optionContainerName]['children'][$commonOptionContainerName]['children'] = array_replace_recursive(
            $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
            [$optionContainerName]['children'][$commonOptionContainerName]['children'],
            $this->getOptionFieldsConfig()
        );

        // Add fields to the values
        $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
        [$optionContainerName]['children']['values']['children']['record']['children'] = array_replace_recursive(
            $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
            [$optionContainerName]['children']['values']['children']['record']['children'],
            $this->getValueFieldsConfig()
        );
    }

    /**
     * The custom option fields config
     *
     * @return array
     */
    protected function getOptionFieldsConfig()
    {
        $fields['identifier'] = $this->getIdentifierFieldConfig();
        $fields['tooltip'] = $this->getTooltipFieldConfig();
        $fields['is_feature_option'] = $this->getIsFeatureOptionFieldConfig();

        return $fields;
    }

    /**
     * The custom option fields config
     *
     * @return array
     */
    protected function getValueFieldsConfig()
    {
        $fields['option_type'] = $this->getOptionTypeFieldConfig();
        $fields['shutter_material'] = $this->getShutterMaterialFieldConfig();

        return $fields;
    }

    /**
     * Get special offer field config
     *
     * @return array
     */
    protected function getIdentifierFieldConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label'         => __('Identifier'),
                        'componentType' => Field::NAME,
                        'formElement'   => Input::NAME,
                        'dataScope'     => 'identifier',
                        'dataType'      => Text::NAME,
                        'sortOrder'     => 65
                    ],
                ],
            ],
        ];
    }

    /**
     * Get special offer field config
     *
     * @return array
     */
    protected function getTooltipFieldConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label'         => __('Tooltip'),
                        'componentType' => Field::NAME,
                        'formElement'   => Input::NAME,
                        'dataScope'     => 'tooltip',
                        'dataType'      => Text::NAME,
                        'sortOrder'     => 66
                    ],
                ],
            ],
        ];
    }

    /**
     * Get special offer field config
     *
     * @return array
     */
    protected function getIsFeatureOptionFieldConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label'         => __('Is Feature Option'),
                        'componentType' => Field::NAME,
                        'formElement'   => Checkbox::NAME,
                        'dataScope'     => 'is_feature_option',
                        'dataType'      => Text::NAME,
                        'sortOrder'     => 67,
                        'valueMap'      => [
                            'true'  => '1',
                            'false' => '0'
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Get description field config
     *
     * @return array
     */
    protected function getOptionTypeFieldConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Option Type'),
                        'componentType' => Field::NAME,
                        'formElement' => \Magento\Ui\Component\Form\Element\Select::NAME,
                        'dataType' =>   \Magento\Ui\Component\Form\Element\DataType\Boolean::NAME,
                        'dataScope'     => 'option_type',
                        'value' => '0',
                        'options' => $this->optionType->toOptionArray(),
                        'sortOrder'     => 41
                    ],
                ],
            ],
        ];
    }

    /**
     * Get description field config
     *
     * @return array
     */
    protected function getShutterMaterialFieldConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Shutter Material'),
                        'componentType' => Field::NAME,
                        'formElement' => \Magento\Ui\Component\Form\Element\Select::NAME,
                        'dataType' =>   \Magento\Ui\Component\Form\Element\DataType\Boolean::NAME,
                        'dataScope'     => 'shutter_material',
                        'value' => '0',
                        'options' => $this->shutterMaterialType->toOptionArray(),
                        'sortOrder'     => 42
                    ],
                ],
            ],
        ];
    }
}
