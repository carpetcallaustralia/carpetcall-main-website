<?php
namespace Carpetcall\Freehomemeasure\Api;

interface StatelistInterface
{

    /**
     * GET for test api
     * @param mixed $data
	 * @return array
     */

  public function getStatelist();

}