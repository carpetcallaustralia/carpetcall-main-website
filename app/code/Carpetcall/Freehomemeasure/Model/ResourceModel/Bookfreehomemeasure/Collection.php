<?php 
namespace Carpetcall\Freehomemeasure\Model\ResourceModel\Bookfreehomemeasure;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("Carpetcall\Freehomemeasure\Model\Bookfreehomemeasure","Carpetcall\Freehomemeasure\Model\ResourceModel\Bookfreehomemeasure");
	}
}