<?php
namespace Carpetcall\Freehomemeasure\Model;

class Bookfreehomemeasure extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Carpetcall\Freehomemeasure\Model\ResourceModel\Bookfreehomemeasure');
    }
}

