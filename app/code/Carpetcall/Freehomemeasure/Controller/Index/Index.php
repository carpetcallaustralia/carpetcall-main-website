<?php

namespace Carpetcall\Freehomemeasure\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneFactory;
use Mageplaza\StoreLocator\Model\LocationFactory;
use Mageplaza\StoreLocator\Model\ResourceModel\Location as LocationResource;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_storeInfo;
    protected $_storeManagerInterface;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */    
    protected $scopeConfig;

    protected $timezoneFactory;

    /**
     * @var LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var LocationResource
     */
    protected $_locationResource;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Carpetcall\Freehomemeasure\Model\BookfreehomemeasureFactory  $bookfreehomemeasure,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        LocationResource $locationResource,
        LocationFactory $locationFactory,
        ScopeConfigInterface $scopeConfig,
        TimezoneFactory $timezoneFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->_pageFactory = $pageFactory;
        $this->_bookfreehomemeasure = $bookfreehomemeasure;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->timezoneFactory = $timezoneFactory;
        $this->_locationResource = $locationResource;
        $this->_locationFactory  = $locationFactory;
        return parent::__construct($context);
    }

    /**
     * Booking action
     *
     * @return void
     */
    public function execute()
    {
        // 1. POST request : Get booking data
        $data = (array) $this->getRequest()->getPost();
        if (!empty($data)) {
            $selected_store = ($data['store-checkbox'] != '') ? explode(' / ', $data['store-checkbox']) : $data['store-checkbox'];
            // Retrieve your form data
            $firstname = $data['firstname'];
            $lastname = $data['lastname'];
            $email = $data['email'];
            $phone = $data['phone'];
            $streetaddress = $data['address'];
            $suburb = $data['suburb'];
            $state = $data['state'];
            $postcode = $data['postcode'];
            // $store_postcode = $data['store_postcode'];
            $store_postcode = (count($selected_store) > 1) ? $selected_store[1] : '';
            $storeSelected = (count($selected_store) > 0) ? $selected_store[0] : '';
            $comment = $data['comment'];
            $interested_in = $data['interested_in'];
            $interested_in_other = $data['interested_in_other'];
            $datetime = $data['datetime'];

            //echo'<pre>'; print_r($storeSelected); die('____!!');

            $model = $this->_bookfreehomemeasure->create();
            $model->addData([
                "firstname" => $firstname,
                "lastname" => $lastname,
                "email" => $email,
                "phone" => $phone,
                "streetaddress" => $streetaddress,
                "suburb" => $suburb,
                "state" => $state,
                "postcode" => $postcode,
                "store_postcode" => $store_postcode,
                "store" => $storeSelected,
                "message" => $comment,
                "interested_in" => $interested_in,
                "interested_in_other" => $interested_in_other,
                "created_at" => $datetime,
                ]);
            $saveData = $model->save();
            if($saveData){
                $this->sendEmail($data, $selected_store);
                $this->CustomerEmail($data, $selected_store);
                $this->messageManager->addSuccessMessage('Thank you for submitting details. Our team will get back to you with more details !');
            }else{
                $this->messageManager->addErrorMessage('Not inserted!');
            }
            

            // $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            // $resultRedirect->setUrl('free-home-measure');
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setRefererOrBaseUrl();
            return $resultRedirect;

        }
        // 2. GET request : Render the booking page 
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }


    private function sendEmail($data, $selected_store)
    {
        $timezoneModel = $this->timezoneFactory->create();
        $current_time = $timezoneModel->formatDate(null, \IntlDateFormatter::SHORT, true);
        $current_time = explode(', ', $current_time);
        $firstname = $data['firstname'];
        $email = $data['email'];
        $phone = $data['phone'];
        $address = $data['address'];
        $suburb = $data['suburb'];
        $state = $data['state'];
        $postcode = $data['postcode'];
        // $store_postcode = $data['store_postcode'];
        $store_postcode = (count($selected_store) > 1) ? $selected_store[1] : '';
        $storeSelected = (count($selected_store) > 0) ? $selected_store[0] : '';
        $currentdate = $timezoneModel->formatDate(null, \IntlDateFormatter::LONG);
        $currenttime = $current_time[1];
        $comment = $data['comment'];
        $interested_in = $data['interested_in'];
        $interested_in_other = $data['interested_in_other'];

        //echo "<pre>"; print_r($storeSelected); die();

         $templateVars = [
            'store' => $this->storeManager->getStore(),
            'lastname' => $data['lastname'],
            'firstname' => $firstname,
            'email'   => $email,
            'telephone' => $phone,
            'address' => $address,
            'suburb' => $suburb,
            'state' => $state,
            'postcode' => $postcode,
            'store_postcode' => $store_postcode,
            'storecheckbox' => $storeSelected,
            'currentdate'    => $currentdate,
            'currenttime'    => $currenttime,
            'comment' => $comment,
            'interested_in' => $interested_in
        ];

        if ('Other' == $interested_in) {
            $templateVars['interested_in_other'] = $interested_in_other;
        }
        
        $templateOptions = [
            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
            'store' => $this->storeManager->getStore()->getId()
        ];

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        $cc_emails = '';
        $bcc_emails = '';
        switch ($data['state']) {
            case "Tasmania":
                $cc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_to', $storeScope);
                $bcc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_bcc', $storeScope);
            break;
            case "New South Wales":
                $cc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_to', $storeScope);
                $bcc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_bcc', $storeScope);
            break;
            case "Queensland":
                $cc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_to', $storeScope);
                $bcc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_bcc', $storeScope);
            break;
            case "South Australia":
                $cc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_to', $storeScope);
                $bcc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_bcc', $storeScope);
            break;
            case "Victoria":
                $cc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_to', $storeScope);
                $bcc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_bcc', $storeScope);
            break;
            case "Western Australia":
                $cc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_to', $storeScope);
                $bcc_emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_bcc', $storeScope);
            break;
            default:
        }

        $final_cc_emails = [];
        $cc_emails = explode(',', $cc_emails);    
        foreach ($cc_emails as $cc_email) {
            $final_cc_emails[] = trim($cc_email);
        }

        $final_bcc_emails = [];
        $bcc_emails = explode(',', $bcc_emails);    
        foreach ($bcc_emails as $bcc_email) {
            $final_bcc_emails[] = trim($bcc_email);
        }

        // //
        // $cc_emails = 'testmscgoriteeps@gmail.com';
        // $final_bccemails = ['arjun.cmarix@gmail.com', 'testmysite24@gmail.com'];

        $email_Template = 'hello_template';  
        $formemail = $this->storeManager->getStore()->getConfig('trans_email/ident_general/email');
        $formname = $this->storeManager->getStore()->getConfig('trans_email/ident_general/name');

        /*$to_email = (count($selected_store) > 1) ? $selected_store[1] : (strtolower($selected_store[0]) . '.store@carpetcall.com.au');*/

        if(!empty($selected_store)){
            if(isset($selected_store[2]) && !empty($selected_store[2])){
                $to_email = $selected_store[2];
            } else {
                $to_email = (strtolower($selected_store[0]) . '.store@carpetcall.com.au');
            }
        }
        
        $this->transportBuilder
            ->setTemplateIdentifier($email_Template)    
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom(['name' => $formname,'email' => $formemail])
            ->setReplyTo($email, $data['firstname'] . ' ' . $data['lastname'])
            ->addTo($to_email);
            if (!empty($final_cc_emails)) {
                $this->transportBuilder->addCc($final_cc_emails);
            }
            if (!empty($final_bcc_emails)) {
                $this->transportBuilder->addBcc($final_bcc_emails);
            }
            $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }

    private function CustomerEmail($data, $selected_store)
    {
        $timezoneModel = $this->timezoneFactory->create();
        $current_time = $timezoneModel->formatDate(null, \IntlDateFormatter::SHORT, true);
        $current_time = explode(', ', $current_time);
        $firstname = $data['firstname'];
        $email = $data['email'];
        $phone = $data['phone'];
        $address = $data['address'];
        $suburb = $data['suburb'];
        $state = $data['state'];
        $postcode = $data['postcode'];
        // $store_postcode = $data['store_postcode'];
        $store_postcode = (count($selected_store) > 1) ? $selected_store[1] : '';
        $storeSelected = (count($selected_store) > 0) ? $selected_store[0] : '';
        $currentdate = $timezoneModel->formatDate(null, \IntlDateFormatter::LONG);
        $currenttime = $current_time[1];
        $comment = $data['comment'];
        $interested_in = $data['interested_in'];
        $interested_in_other = $data['interested_in_other'];

        //echo "<pre>"; print_r($storeSelected); die();

        $templateVars = [
            'store' => $this->storeManager->getStore(),
            'lastname' => $data['lastname'],
            'firstname' => $firstname,
            'email'   => $email,
            'telephone' => $phone,
            'address' => $address,
            'suburb' => $suburb,
            'state' => $state,
            'postcode' => $postcode,
            'store_postcode' => $store_postcode,
            'storecheckbox' => $storeSelected,
            'currentdate'    => $currentdate,
            'currenttime'    => $currenttime,
            'comment' => $comment,
            'interested_in' => $interested_in
        ];
        if ('Other' == $interested_in) {
            $templateVars['interested_in_other'] = $interested_in_other;
        }

        $templateOptions = [
            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
            'store' => $this->storeManager->getStore()->getId()
        ];

        $email_Template = 'customer_template';  
        $formemail = $this->storeManager->getStore()->getConfig('trans_email/ident_general/email');
        $formname = $this->storeManager->getStore()->getConfig('trans_email/ident_general/name');

        $transport = $this->transportBuilder
            ->setTemplateIdentifier($email_Template)
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom(['name' => $formname,'email' => $formemail])
            ->setReplyTo('info@carpetcall.com.au', $formname)
            ->addTo($email)
            ->getTransport();
        $transport->sendMessage();
    }
}