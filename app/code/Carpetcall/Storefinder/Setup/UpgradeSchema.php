<?php
namespace Carpetcall\Storefinder\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		if (version_compare($context->getVersion(), '1.0.1') < 0) 
		{
			$connection = $setup->getConnection();
			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'head_office',
			[
			'type' => Table::TYPE_SMALLINT,
			'length' => 1,
			'nullable' => true,
			'default' => 0,
			'comment' => 'Head office'
			]
			);
		}

		if (version_compare($context->getVersion(), '1.0.2') < 0) 
		{
			$connection = $setup->getConnection();
			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'meta_description',
			[
			'type' => Table::TYPE_TEXT,
			'nullable' => true,
			'comment' => 'Meta Description'
			]
			);

			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'meta_title',
			[
			'type' => Table::TYPE_TEXT,
			'nullable' => true,
			'comment' => 'Meta Title'
			]
			);

			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'h1_tag',
			[
			'type' => Table::TYPE_TEXT,
			'nullable' => true,
			'comment' => 'H1 tag'
			]
			);
		}

		if (version_compare($context->getVersion(), '1.0.3') < 0) 
		{
			$connection = $setup->getConnection();
			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'store_unique_id',
			[
				'type' => Table::TYPE_TEXT,
				'nullable' => true,
				'comment' => 'Store Unique ID'
			]
			);
		}

		if (version_compare($context->getVersion(), '1.0.4') < 0) 
		{
			$connection = $setup->getConnection();
			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'store_type',
			[
			'type' => Table::TYPE_SMALLINT,
			'length' => 1,
			'nullable' => true,
			'default' => 0,
			'comment' => 'Store Type'
			]
			);
		}

		if (version_compare($context->getVersion(), '1.0.5') < 0) 
		{
			$connection = $setup->getConnection();
			$connection->addColumn(
				$setup->getTable('mageplaza_storelocator_location'),
				'description_field_2',
				[
				'type' => Table::TYPE_TEXT,
				'length' => NULL,
				'nullable' => true,
				'comment' => 'Description field 2'
				]
			);
			$connection->addColumn(
				$setup->getTable('mageplaza_storelocator_location'),
				'description_field_1',
				[
				'type' => Table::TYPE_TEXT,
				'length' => NULL,
				'nullable' => true,
				'comment' => 'Description field 1'
				]
			);
		}

		if (version_compare($context->getVersion(), '1.0.6') < 0) 
		{
			$connection = $setup->getConnection();
			$connection->addColumn(
			$setup->getTable('mageplaza_storelocator_location'),
			'embed_url',
			[
			'type' => Table::TYPE_TEXT,
			'nullable' => true,
			'comment' => 'Embed URL'
			]
			);
		}
	}
}