<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Block\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Contact extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    protected $request;
    protected $date;
    protected $locationFactory;
    protected $timezone;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        LocationFactory $locationFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        array $data = []
    ) {
        $this->locationFactory = $locationFactory;
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->date = $date;
        $this->timezone = $timezone;
        parent::__construct($context, $data);
    }

    public function getSelectedStoreData()
    {

        $params = $this->getRequest()->getParams();
        // echo "<pre>";
        // print_r($params);die();

        if (!empty($params['id'])) {
            $location_id = $params['id'];

            $locations = $this->locationFactory->create()->getCollection();
            $locations->addFieldToFilter('location_id',$location_id);
            $store_data = $locations->getData();
            // echo "<pre>";
            // print_r($store_data);die();
            if (!empty($store_data)) {
                $state = $store_data[0]['state_province'];
                $store_name = $store_data[0]['name'];

                $selected_store_details = ['state' => $state, 'store_name' => $store_name];
                // echo "<pre>";
                //print_r($selected_store_details);
                //die;
                return $selected_store_details;
            }else{
                return null;
            }
            
        }
        return null;
         
    }

    public function gmtDate()
    {
        //return $this->date->gmtDate();
        $date = $this->timezone->date();
        return $date;

    }

}

