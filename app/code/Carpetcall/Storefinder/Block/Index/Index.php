<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Block\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Index extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    protected $request;
    protected $locationFactory;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        LocationFactory $locationFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->locationFactory = $locationFactory;
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getLocations()
    {
        $locations = $this->locationFactory->create()->getCollection();
        return $locations;
    }

    public function getCurrentUrl()
    {

        $params = $this->getRequest()->getParams();
        if (!empty($params)) {
            return $params['param']['state'];
        }
        return null;

        // echo "<pre>";
        // print_r($params['param']['state']);die;
         
    }

    public function getNoOfStoresByState()
    {
        $locations = $this->locationFactory->create()->getCollection();
        // echo "<pre>";
        // print_r($locations->getData());die();
        $count = array();
        $count_nsw = 0;
        $count_sa = 0;
        $count_wa = 0;
        $count_qld = 0;
        $count_tas = 0;
        $count_vic = 0;
        foreach ($locations as $value) {
            if (ucwords(strtolower($value['state_province'])) == 'New South Wales') {
                $count_nsw = $count_nsw + 1;
            }elseif (ucwords(strtolower($value['state_province'])) == 'South Australia') {
                $count_sa = $count_sa + 1;
            }elseif (ucwords(strtolower($value['state_province'])) == 'Western Australia') {
                $count_wa = $count_wa + 1;
            }elseif (ucwords(strtolower($value['state_province'])) == 'Queensland') {
                $count_qld = $count_qld + 1;
            }elseif (ucwords(strtolower($value['state_province'])) == 'Tasmania') {
                $count_tas = $count_tas + 1;
            }elseif (ucwords(strtolower($value['state_province'])) == 'Victoria') {
                $count_vic = $count_vic + 1;
            }
        }

        $count[] = ['count_nsw' => $count_nsw,
                        'count_sa' => $count_sa,
                        'count_wa' => $count_wa,
                        'count_qld' => $count_qld,
                        'count_tas' => $count_tas,
                        'count_vic' => $count_vic,
                       ];

        return $count;
        //echo $count_nsw;die();
    }

    protected function _prepareLayout() {

        $params = $this->getRequest()->getParams();
        
        if (!empty($params)){
            $count = count($params['param']);
        }
        

        // echo "<pre>";
        // print_r($params);die;

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');

        if (empty($params) || ($params['param']['state'] == 'head_offices')) {

            $this->pageConfig->getTitle()->set(__($this->scopeConfig->getValue('storelocator/seo/meta_title', $storeScope)));
            $this->pageConfig->setDescription(__($this->scopeConfig->getValue('storelocator/seo/meta_description', $storeScope)));
            if ($pageMainTitle) {
                $pageMainTitle->setPageTitle('Find a Carpet Call Store near you');
            }
        }elseif ($count == 1) {

            switch ($params['param']['state']) {
                case 'nsw':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in NSW | Carpets, Rugs & Shutters NSW'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across NSW. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in New South Wales');
                    }
                break;
                case 'qld':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in QLD | Shutters and Flooring Queensland'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across QLD. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Queensland');
                    }
                break;
                case 'tas':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in Tasmania | Rugs and Blinds Tasmania'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Tasmania. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Tasmania');
                    }
                break;
                case 'sa':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in SA | Rugs and Flooring South Australia'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across SA. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in South Australia');
                    }
                break;
                case 'wa':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in WA | Blinds and Rugs Western Australia'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Western Australia. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Western Australia');
                    }
                break;
                case 'vic':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in Victoria | Carpets and Blinds Victoria'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Victoria. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Victoria');
                    }
                break;
                case 'adelaide':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores Adelaide | Blinds, Flooring & Rugs Adelaide'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Adelaide. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Adelaide');
                    }
                break;
                case 'sydney':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in Sydney | Carpet, Flooring & Rugs Sydney'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Sydney. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Sydney');
                    }
                break;
                case 'hobart':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in Hobart | Carpet, Rugs & Blinds Hobart'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Hobart. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Hobart');
                    }
                break;
                case 'melbourne':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores Melbourne | Carpet, Blinds & Rugs Melbourne'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Melbourne. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Melbourne');
                    }
                break;
                case 'brisbane':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores Brisbane | Flooring, Blinds & Carpet Brisbane'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Brisbane. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Brisbane');
                    }
                break;
                case 'perth':
                    $this->pageConfig->getTitle()->set(__('Carpet Call Stores in Perth | Carpet, Blinds & Rugs Perth'));
                    $this->pageConfig->setDescription(__('Discover the collection of carpets, rugs, flooring options, and more offered by our local stores across Perth. Find a store near you!'));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Carpet Call Stores in Perth');
                    }
                break;
                
                default:
                    $this->pageConfig->getTitle()->set(__($this->scopeConfig->getValue('storelocator/seo/meta_title', $storeScope)));
                    $this->pageConfig->setDescription(__($this->scopeConfig->getValue('storelocator/seo/meta_description', $storeScope)));
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle('Find a Carpet Call Store near you');
                    }
                    break;
            }
            
        }elseif ($count == 2) {
            
            $locations = $this->getLocations();
            // echo "<pre>";
            // print_r($locations->getData());die;
            foreach ($locations->getData() as $key => $value) {
                // echo $value['city'];
                //echo $params['param']['store'];
                //$city = str_replace(' ', '-', strtolower($value['city']));
                $city = $value['url_key'];
                if ($city == $params['param']['store']) {
                    $this->pageConfig->getTitle()->set(__($value['meta_title']));
                    
                    if ($pageMainTitle) {
                        $pageMainTitle->setPageTitle($value['h1_tag']);
                    }
                    $this->pageConfig->setDescription(__($value['meta_description']));
                }
            }
        }

            return parent::_prepareLayout();
        }


        public function getCurrentUrlForCityContent()
        {

            $params = $this->getRequest()->getParams();
            if (!empty($params)) {

                
                $count = count($params['param']);
                

                if ($count == 1) {

                    switch ($params['param']['state']) {
                        case 'adelaide':
                          echo $this->getLayout()
                          ->createBlock('Magento\Cms\Block\Block')
                          ->setBlockId('adelaide')
                          ->toHtml();
                        break;
                        case 'sydney':
                         echo $this->getLayout()
                          ->createBlock('Magento\Cms\Block\Block')
                          ->setBlockId('sydney')
                          ->toHtml();
                        break;
                        case 'hobart':
                        echo $this->getLayout()
                          ->createBlock('Magento\Cms\Block\Block')
                          ->setBlockId('hobart')
                          ->toHtml();
                        break;
                        case 'melbourne':
                        echo $this->getLayout()
                          ->createBlock('Magento\Cms\Block\Block')
                          ->setBlockId('melbourne')
                          ->toHtml();
                         
                        break;
                        case 'brisbane':
                        echo $this->getLayout()
                          ->createBlock('Magento\Cms\Block\Block')
                          ->setBlockId('brisbane')
                          ->toHtml();
                          
                        break;
                        case 'perth':
                        echo $this->getLayout()
                          ->createBlock('Magento\Cms\Block\Block')
                          ->setBlockId('perth')
                          ->toHtml();
                            
                        break;
                        
                        default:

                    }
            
        
                }

                //return $params['param']['state'];
            }
            return null;
        }

}

