<?php

namespace Carpetcall\Storefinder\Block;

class Locations extends \Magento\Framework\View\Element\Template
{

        /**
      * @var \Magento\Framework\Registry
      */
     
     protected $_registry;
     
     /**
     * ...
     * ...
     * @param \Magento\Framework\Registry $registry,
     */

      private $layerResolver;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Mageplaza\StoreLocator\Block\Frontend $frontend,
        \Mageplaza\StoreLocator\Model\ResourceModel\Location\CollectionFactory $collectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
         \Magento\Framework\Registry $registry,
          \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        array $data = []
    )
    {        
        $this->_registry = $registry;
        $this->layerResolver = $layerResolver;
        $this->_frontend         = $frontend;
        $this->collectionFactory = $collectionFactory;
        $this->messageManager    = $messageManager;
        parent::__construct($context, $data);
    }

    public function getLocationsData($locationIds,$currentcategory)
    {
        
        // Case 1: If only Rugs category or Rugs & Accessories then Owned stores
        // Case 2: If only HF category or HF & Accessories then Head office stores
        // Case 3: If only Accessories then Owned stores
        
        $data       = [];

        if($currentcategory != null){
            if ($currentcategory == 'Hard Flooring') {
                $collection = $this->collectionFactory->create()
                ->addFieldToFilter('location_id', $locationIds)
                ->addFieldToFilter('store_type', 2)
                ->addFieldToFilter('status', 1);
            }
            if ($currentcategory == 'Rugs') {
                $collection = $this->collectionFactory->create()
                ->addFieldToFilter('location_id', $locationIds)
                ->addFieldToFilter('store_type', 0)
                ->addFieldToFilter('status', 1);
            }
        }else{
            $collection = $this->collectionFactory->create()
            ->addFieldToFilter('location_id', $locationIds)
            ->addFieldToFilter('status', 1);
        }

        

        try {
            $locations = $this->_frontend->filterLocation($collection);
            foreach ($locations as $location) {
                //if ($location->getIsShowProductPage()) {
                    $data[] = [
                        'name'    => $location->getName(),
                        'address' => $location->getStreet() . ' ' .
                            $location->getStateProvince() . ', ' .
                            $location->getCity() . ', ' .
                            $location->getCountry()
                    ];
                //}
            }
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $data;
    }

}
