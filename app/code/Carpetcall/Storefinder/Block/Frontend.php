<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 29/1/21
 * Time: 2:38 pm
 */

namespace Carpetcall\Storefinder\Block;

use DateTimeZone as DateTimeZoneAlias;
use Exception as ExceptionAlias;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Phrase;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Quote\Model\Quote\Item;
use Magento\Widget\Block\BlockInterface;
use Mageplaza\StoreLocator\Helper\Data as HelperData;
use Mageplaza\StoreLocator\Helper\Image as HelperImage;
use Mageplaza\StoreLocator\Model\Config\Source\System\MapStyle;
use Mageplaza\StoreLocator\Model\Holiday;
use Mageplaza\StoreLocator\Model\HolidayFactory;
use Mageplaza\StoreLocator\Model\Location;
use Mageplaza\StoreLocator\Model\LocationFactory;
use Mageplaza\StoreLocator\Model\ResourceModel\Holiday as HolidayResource;
use Mageplaza\StoreLocator\Model\ResourceModel\Location as LocationResource;
use Mageplaza\StoreLocator\Model\ResourceModel\Location\Collection;
use Mageplaza\StoreLocator\Model\ResourceModel\Location\CollectionFactory;
use Magento\Catalog\Model\CategoryFactory;

/**
 * Class Frontend
 * @package Carpetcall\Storefinder\Block
 */
class Frontend extends \Mageplaza\StoreLocator\Block\Frontend
{
    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * Frontend constructor.
     * @param Context $context
     * @param DateTime $dateTime
     * @param HelperData $helperData
     * @param LocationFactory $locationFactory
     * @param HolidayFactory $holidayFactory
     * @param HelperImage $helperImage
     * @param BlockFactory $blockFactory
     * @param Cart $cart
     * @param ProductRepositoryInterface $productRepository
     * @param CollectionFactory $locationColFactory
     * @param LocationResource $locationResource
     * @param HolidayResource $holidayResource
     * @param ManagerInterface $messageManager
     * @param CategoryFactory $categoryFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        HelperData $helperData,
        LocationFactory $locationFactory,
        HolidayFactory $holidayFactory,
        HelperImage $helperImage,
        BlockFactory $blockFactory,
        Cart $cart,
        ProductRepositoryInterface $productRepository,
        CollectionFactory $locationColFactory,
        LocationResource $locationResource,
        HolidayResource $holidayResource,
        ManagerInterface $messageManager,
        CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context, $dateTime, $helperData, $locationFactory, $holidayFactory, $helperImage, $blockFactory, $cart, $productRepository, $locationColFactory, $locationResource, $holidayResource, $messageManager, $data);
    }

    /**
     * Update logic on how to determine which stores is available for pickup.
     * @return array
     */
    public function getPickupLocationList()
    {
        try {
            $locations   = $this->_locationColFactory->create()->addFieldToFilter('status', 1);
            $isOwnedStore = $this->isHeadOfficeorOwnedstores();

            if (!$isOwnedStore) {
                $locations->addFieldToFilter('store_type', 2);
            }else{
                $locations->addFieldToFilter('store_type', 0);
            }
            
            return $this->filterLocation($locations);
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return [];
    }

    /**
     * Check whether cart items is available for store pick depending on the categories
     *
     *   Case 1: If only Rugs category or Rugs & Accessories then Owned stores
     *   Case 2: If only HF category or HF & Accessories then Head office stores
     *   Case 3: If only Accessories then Owned stores
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isHeadOfficeorOwnedstores()
    {
        $OwnedStores = [];
        $headOfficeStoresOnly = [];
        $allItems = $this->_cart->getQuote()->getAllItems();
        /** @var $item Item */

        foreach ($allItems as $item) {
            $productId = $item->getProduct()->getId();
            $product = $this->productRepository->getById($productId);
            $categoryIds = $product->getCategoryIds();

           foreach ($categoryIds as $categoryId){
               $category = $this->categoryFactory->create()->load($categoryId);
               if ($category->getLevel() == 2) {

                   if (strtolower($category->getName()) == 'rugs' || strtolower($category->getName()) == 'accessories') {
                       $OwnedStores[] = $category->getId();
                   }else {
                       $headOfficeStoresOnly[] = $category->getId();
                   }

               }
            }

        }

        return count($OwnedStores) > 0 && count($headOfficeStoresOnly) == 0;
    }

}
