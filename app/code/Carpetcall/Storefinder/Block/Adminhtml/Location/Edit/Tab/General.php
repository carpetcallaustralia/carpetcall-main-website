<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreLocator
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Carpetcall\Storefinder\Block\Adminhtml\Location\Edit\Tab;

use Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\Data\Form;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

/**
 * Class General
 * @package Mageplaza\StoreLocator\Block\Adminhtml\Location\Edit\Tab
 */
class General extends Generic implements TabInterface
{
    /**
     * @var Store
     */
    public $systemStore;

    /**
     * @var Yesno
     */
    protected $_yesNo;

    /**
     * Location constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Yesno $yesNo
     * @param Store $systemStore
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Yesno $yesNo,
        Store $systemStore,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_yesNo      = $yesNo;
        $this->systemStore = $systemStore;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @inheritdoc
     * @return Generic
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        /** @var \Mageplaza\StoreLocator\Model\Location $location */
        $location = $this->_coreRegistry->registry('mageplaza_storelocator_location');

        /** @var Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('general_');
        $form->setFieldNameSuffix('general');

        $fieldset = $form->addFieldset('base_fieldset', [
            'legend' => __('General'),
            'class'  => 'fieldset-wide'
        ]);

        $fieldset->addField('name', 'text', [
            'name'     => 'name',
            'label'    => __('Name'),
            'title'    => __('Name'),
            'required' => true
        ]);

        $fieldset->addField('status', 'select', [
            'name'   => 'status',
            'label'  => __('Visibility'),
            'title'  => __('Visibility'),
            'values' => $this->_yesNo->toOptionArray()
        ]);
        if (!$location->hasData('status')) {
            $location->setStatus(1);
        }

        // $fieldset->addField('head_office', 'select', [
        //     'name'   => 'head_office',
        //     'label'  => __('Head office'),
        //     'title'  => __('Head office'),
        //     'values' => $this->_yesNo->toOptionArray()
        // ]);
        // if (!$location->hasData('status')) {
        //     $location->setStatus(1);
        // }

        $fieldset->addField('store_type', 'select', [
            'name'   => 'store_type',
            'label'  => __('Store Type'),
            'title'  => __('Store Type'),
            'values' => $this->toOptionArray()
        ]);

        $fieldset->addField('store_unique_id', 'text', [
            'name'  => 'store_unique_id',
            'label' => __('Store Unique ID'),
            'title' => __('Store Unique ID'),
        ]);

        $fieldset->addField('description', 'textarea', [
            'name'  => 'description',
            'label' => __('Description'),
            'title' => __('Description'),
            'note'  => __('For internal use, this field is visible to admins.'),
        ]);

        $fieldset->addField('url_key', 'text', [
            'name'  => 'url_key',
            'label' => __('URL Key'),
            'title' => __('URL Key')
        ]);

        if ($this->_storeManager->isSingleStoreMode()) {
            $fieldset->addField('store_ids', 'hidden', [
                'name'  => 'store_ids',
                'value' => $this->_storeManager->getStore()->getId()
            ]);
        } else {
            /** @var RendererInterface $rendererBlock */
            $rendererBlock = $this->getLayout()
                ->createBlock(Element::class);
            $fieldset->addField('store_ids', 'multiselect', [
                'name'   => 'store_ids',
                'label'  => __('Store Views'),
                'title'  => __('Store Views'),
                'values' => $this->systemStore->getStoreValuesForForm(false, true)
            ])->setRenderer($rendererBlock);

            if (!$location->hasData('store_ids')) {
                $location->setStoreIds(0);
            }
        }

        $fieldset->addField('sort_order', 'text', [
            'name'  => 'sort_order',
            'label' => __('Sort Order'),
            'title' => __('Sort Order'),
            'class' => 'validate-digits',
            'value' => '0',
            'note'  => __('The sort order of the location.'),
        ]);

        $fieldset->addField('is_default_store', 'checkbox', [
            'name'     => 'is_default_store',
            'label'    => __('Is Default Location'),
            'title'    => __('Is Default Location'),
            'checked'  => $location->getIsDefaultStore() ? true : false,
            'onchange' => 'this.value = this.checked ? 1 : 0;',
            'value'    => $location->getIsDefaultStore()
        ]);

        $fieldset->addField('meta_description', 'textarea', [
            'name'  => 'meta_description',
            'label' => __('Meta Description'),
            'title' => __('Meta Description'),
        ]);

        $fieldset->addField('meta_title', 'text', [
            'name'  => 'meta_title',
            'label' => __('Meta Title'),
            'title' => __('Meta Title'),
        ]);

        $fieldset->addField('h1_tag', 'text', [
            'name'  => 'h1_tag',
            'label' => __('H1 Tag'),
            'title' => __('H1 Tag'),
        ]);

        $fieldset->addField('embed_url', 'text', [
            'name'  => 'embed_url',
            'label' => __('Embed URL'),
            'title' => __('Embed URL'),
        ]);

        $fieldset->addField(
            'description_field_1',
            'editor',
            [
                'name' => 'description_field_1',
                'label' => __('Description Field 1'),
                'title' => __('Description Field 1'),
                'rows' => '5',
                'cols' => '30',
                'wysiwyg' => true,
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        $fieldset->addField(
            'description_field_2',
            'editor',
            [
                'name' => 'description_field_2',
                'label' => __('Description Field 2'),
                'title' => __('Description Field 2'),
                'rows' => '5',
                'cols' => '30',
                'wysiwyg' => true,
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        $form->addValues($location->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => 'Owned Store'],
            ['value' => 1, 'label' => 'Franchise Store'],
            ['value' => 2, 'label' => 'Head Office']
        ];
    }
}
