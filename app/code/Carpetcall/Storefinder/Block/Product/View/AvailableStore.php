<?php
 
namespace Carpetcall\Storefinder\Block\Product\View;

 
class AvailableStore extends \Mageplaza\StoreLocator\Block\Product\View\AvailableStore
{
        /**
      * @var \Magento\Framework\Registry
      */
     
     protected $_registry;
     
     /**
     * ...
     * ...
     * @param \Magento\Framework\Registry $registry,
     */

      private $layerResolver;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Mageplaza\StoreLocator\Block\Frontend $frontend,
        \Mageplaza\StoreLocator\Model\ResourceModel\Location\CollectionFactory $collectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
         \Magento\Framework\Registry $registry,
          \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        array $data = []
    )
    {        
        $this->_registry = $registry;
        $this->layerResolver = $layerResolver;
        $this->_frontend         = $frontend;
        $this->collectionFactory = $collectionFactory;
        $this->messageManager    = $messageManager;
        parent::__construct($context, $frontend, $collectionFactory, $messageManager, $data);
    }

    public function getLocationsData($locationIds)
    {
        // Case 1: If only Rugs category or Rugs & Accessories then Owned stores
        // Case 2: If only HF category or HF & Accessories then Head office stores
        // Case 3: If only Accessories then Owned stores
    
        // $category = $this->_registry->registry('current_category');//get current category
        // $current_category = $this->layerResolver->get()->getCurrentCategory();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');//get current category
        /*$current_category = $category->getName();
        

        $data       = [];

        if ($current_category == 'Hard Flooring') {
            $collection = $this->collectionFactory->create()
            ->addFieldToFilter('location_id', $locationIds)
            ->addFieldToFilter('store_type', 2)
            ->addFieldToFilter('status', 1);
        }elseif ($current_category == 'Rugs') {
            $collection = $this->collectionFactory->create()
            ->addFieldToFilter('location_id', $locationIds)
            ->addFieldToFilter('store_type', 0)
            ->addFieldToFilter('status', 1);
        }else{
            $collection = $this->collectionFactory->create()
            ->addFieldToFilter('location_id', $locationIds)
            ->addFieldToFilter('status', 1);
        }*/

        if($category){
            $current_category = $category->getName();
        }else{
            $current_category = null;
        }
        
        $data       = [];

        if($current_category != null){
            if ($current_category == 'Hard Flooring') {
                $collection = $this->collectionFactory->create()
                ->addFieldToFilter('location_id', $locationIds)
                ->addFieldToFilter('store_type', 2)
                ->addFieldToFilter('status', 1);
            }
            if ($current_category == 'Rugs') {
                $collection = $this->collectionFactory->create()
                ->addFieldToFilter('location_id', $locationIds)
                ->addFieldToFilter('store_type', 0)
                ->addFieldToFilter('status', 1);
            }
        }else{
            $collection = $this->collectionFactory->create()
            ->addFieldToFilter('location_id', $locationIds)
            ->addFieldToFilter('status', 1);
        }

        

        try {
            $locations = $this->_frontend->filterLocation($collection);
            foreach ($locations as $location) {
                if ($location->getIsShowProductPage()) {
                    $data[] = [
                        'name'    => $location->getName(),
                        'address' => $location->getStreet() . ' ' .
                            $location->getStateProvince() . ', ' .
                            $location->getCity() . ', ' .
                            $location->getCountry()
                    ];
                }
            }
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $data;
    }
}
?>