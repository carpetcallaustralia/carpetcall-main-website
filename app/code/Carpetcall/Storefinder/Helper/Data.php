<?php
/**
 * Created by PhpStorm.
 * User: seth
 * Date: 4/2/21
 * Time: 5:53 pm
 */

namespace Carpetcall\Storefinder\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param $state
     * @return string
     */
    public function getAbbreviatedState($state)
    {
        $state_int = ucwords(strtolower($state));
        switch ($state_int) {
            case "New South Wales":
                $state_init_url_text = 'nsw';
                break;
            case "South Australia":
                $state_init_url_text = 'sa';
                break;
            case "Western Australia":
                $state_init_url_text = 'wa';
                break;
            case "Queensland":
                $state_init_url_text = 'qld';
                break;
            case "Tasmania":
                $state_init_url_text = 'tas';
                break;
            case "Victoria":
                $state_init_url_text = 'vic';
                break;
            default:
                $state_init_url_text = '';
        }

        return $state_init_url_text;
    }
}
