<?php
namespace Carpetcall\Storefinder\Controller;
class Router implements \Magento\Framework\App\RouterInterface
    {

     /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Event manager
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    /**
     * Config primary
     *
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * Url
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * Response
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;


  public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\UrlInterface $url,        
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->_eventManager = $eventManager;
        $this->_url = $url;        
        $this->_storeManager = $storeManager;
        $this->_response = $response;
    }

     public function match(\Magento\Framework\App\RequestInterface $request)
       {
        $identifier = trim($request->getPathInfo(), '/');
        $url_parts = explode("/", $identifier, 4);
        // echo "<pre>";
        // print_r($url_parts);die();

        $condition = new \Magento\Framework\DataObject(['identifier' => $identifier, 'continue' => true]);

        $identifier = $condition->getIdentifier();

        if ($condition->getRedirectUrl()) {
            $this->_response->setRedirect($condition->getRedirectUrl());
            $request->setDispatched(true);
            return $this->actionFactory->create('Magento\Framework\App\Action\Redirect');
        }

        if (!$condition->getContinue()) {
            return null;
        }

        // check your custom condition here if its satisfy they go ahed othrwise set return null
        $satisfy=true;
        if (!$satisfy) {
            return null;
        }

        if ($url_parts[0] == 'find-a-store') {
            if (!empty($url_parts[2])) {
            $params = array('state' => $url_parts[1], 'store' => $url_parts[2]);
            }else{
                $params = array('state' => $url_parts[1]);
            }
            
            $request->setModuleName($url_parts[0])->setControllerName('index')->setActionName('index')->setParam('param',$params);
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);

            return $this->actionFactory->create('Magento\Framework\App\Action\Forward');
        }else{
          return;
        }
        

        
        }
    }