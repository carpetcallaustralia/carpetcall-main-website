<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Controller\Index;

use DateTimeZone as DateTimeZoneAlias;
use Mageplaza\StoreLocator\Model\LocationFactory;
use Mageplaza\StoreLocator\Model\ResourceModel\Location as LocationResource;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Mageplaza\StoreLocator\Model\HolidayFactory;


class Storedetail extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;
    protected $_locationResource;
    protected $_dateTime;    
    protected $holidayFactory;                                                       

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LocationFactory $locationFactory,
        \Carpetcall\Storefinder\Controller\Index\Currentlocationforstores $coordinatesData,
        LocationResource $locationResource,
        HolidayFactory $holidayFactory,
        DateTime $dateTime
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
        $this->locationFactory = $locationFactory;
        $this->coordinatesData = $coordinatesData;
        $this->_locationResource   = $locationResource;
        $this->_dateTime           = $dateTime;
        $this->holidayFactory      = $holidayFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    { 
        $param = $this->getRequest()->getParams();
        
        $result = $this->resultJsonFactory->create();
        $tophtml='';
        $html='';
        $locations = $this->locationFactory->create()->getCollection();
         //echo "<pre>";
        // // // print_r($param);                        
         //print_r($locations->getData());
         //die();

        
        // End - Code to get current location address

        $base_url = $this->storeManager->getStore()->getBaseUrl();
        $media_url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $coordinates = array();
        $bottom_description = array();
        $i = 0;

        foreach ($locations->getData() as $key => $value) {
            

            $dateTime = new \DateTime($this->_dateTime->date(), new DateTimeZoneAlias('UTC'));
            $dateTime->setTimezone(new DateTimeZoneAlias('Australia/Brisbane'));
            $currentTime      = strtotime($dateTime->format('H:i'));
            $holidayIds       = $this->_locationResource->getHolidayIdsByLocation($value['location_id']);
            
            
            if (str_replace(' ', '-', strtolower($value['url_key'])) == $param['store']) {

                // timings data
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                if ($value['operation_mon'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/monday', $storeScope);
                    $json = json_decode($data, true);
                    $mon_open_close = ($json['monday']['value'] == 1) ? 'open' : 'close';
                    $mon_from = $json['monday']['from'][0].':'.$json['monday']['from'][1];
                    $mon_to = $json['monday']['to'][0].':'.$json['monday']['to'][1];
                    $final_mon = ($mon_open_close == "open") ? '<label>Monday </label><span>'.date("g:i A", strtotime($mon_from)).' - '.date("g:i A", strtotime($mon_to)).'</span>' : '<label>Monday </label><span>Close</span>';

                }else{
                    $json = json_decode($value['operation_mon'], true);
                    $mon_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $mon_from = $json['from'][0].':'.$json['from'][1];
                    $mon_to = $json['to'][0].':'.$json['to'][1];
                    $final_mon = ($mon_open_close == "open") ? '<label>Monday </label><span>'.date("g:i A", strtotime($mon_from)).' - '.date("g:i A", strtotime($mon_to)).'</span>' : '<label>Monday </label><span>Close</span>';
                }

                if ($value['operation_tue'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/tuesday', $storeScope);
                    $json = json_decode($data, true);
                    $tue_open_close = ($json['tuesday']['value'] == 1) ? 'open' : 'close';
                    $tue_from = $json['tuesday']['from'][0].':'.$json['tuesday']['from'][1];
                    $tue_to = $json['tuesday']['to'][0].':'.$json['tuesday']['to'][1];
                    $final_tue = ($tue_open_close == "open") ? '<label>Tuesday </label><span>'.date("g:i A", strtotime($tue_from)).' - '.date("g:i A", strtotime($tue_to)).'</span>' : '<label>Tuesday </label><span>Close</span>';   
                }else{
                    $json = json_decode($value['operation_tue'], true);
                    $tue_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $tue_from = $json['from'][0].':'.$json['from'][1];
                    $tue_to = $json['to'][0].':'.$json['to'][1];
                    $final_tue = ($tue_open_close == "open") ? '<label>Tuesday </label><span>'.date("g:i A", strtotime($tue_from)).' - '.date("g:i A", strtotime($tue_to)).'</span>' : '<label>Tuesday </label><span>Close</span>';   
                }

                if ($value['operation_tue'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/wednesday', $storeScope);
                    $json = json_decode($data, true);
                    $wed_open_close = ($json['wednesday']['value'] == 1) ? 'open' : 'close';
                    $wed_from = $json['wednesday']['from'][0].':'.$json['wednesday']['from'][1];
                    $wed_to = $json['wednesday']['to'][0].':'.$json['wednesday']['to'][1];
                    $final_wed = ($wed_open_close == "open") ? '<label>Wednesday </label><span>'.date("g:i A", strtotime($wed_from)).' - '.date("g:i A", strtotime($wed_to)).'</span>' : '<label>Wednesday </label><span>Close</span>';   
                }else{
                    $json = json_decode($value['operation_wed'], true);
                    $wed_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $wed_from = $json['from'][0].':'.$json['from'][1];
                    $wed_to = $json['to'][0].':'.$json['to'][1];
                    $final_wed = ($wed_open_close == "open") ? '<label>Wednesday </label><span>'.date("g:i A", strtotime($wed_from)).' - '.date("g:i A", strtotime($wed_to)).'</span>' : '<label>Wednesday </label><span>Close</span>';   
                }

                if ($value['operation_thu'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/thursday', $storeScope);
                    $json = json_decode($data, true);
                    $thu_open_close = ($json['thursday']['value'] == 1) ? 'open' : 'close';
                    $thu_from = $json['thursday']['from'][0].':'.$json['thursday']['from'][1];
                    $thu_to = $json['thursday']['to'][0].':'.$json['thursday']['to'][1];
                    $final_thu = ($thu_open_close == "open") ? '<label>Thursday </label><span>'.date("g:i A", strtotime($thu_from)).' - '.date("g:i A", strtotime($thu_to)).'</span>' : '<label>Thursday </label><span>Close</span>';   
                }else{
                    $json = json_decode($value['operation_thu'], true);
                    $thu_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $thu_from = $json['from'][0].':'.$json['from'][1];
                    $thu_to = $json['to'][0].':'.$json['to'][1];
                    $final_thu = ($thu_open_close == "open") ? '<label>Thursday </label><span>'.date("g:i A", strtotime($thu_from)).' - '.date("g:i A", strtotime($thu_to)).'</span>' : '<label>Thursday </label><span>Close</span>';   
                }

                if ($value['operation_fri'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/friday', $storeScope);
                    $json = json_decode($data, true);
                    $fri_open_close = ($json['friday']['value'] == 1) ? 'open' : 'close';
                    $fri_from = $json['friday']['from'][0].':'.$json['friday']['from'][1];
                    $fri_to = $json['friday']['to'][0].':'.$json['friday']['to'][1];
                    $final_fri = ($fri_open_close == "open") ? '<label>Friday </label><span>'.date("g:i A", strtotime($fri_from)).' - '.date("g:i A", strtotime($fri_to)).'</span>' : '<label>Friday </label><span>Close</span>';   
                }else{
                    $json = json_decode($value['operation_fri'], true);
                    $fri_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $fri_from = $json['from'][0].':'.$json['from'][1];
                    $fri_to = $json['to'][0].':'.$json['to'][1];
                    $final_fri = ($fri_open_close == "open") ? '<label>Friday </label><span>'.date("g:i A", strtotime($fri_from)).' - '.date("g:i A", strtotime($fri_to)).'</span>' : '<label>Friday </label><span>Close</span>';   
                }

                if ($value['operation_sat'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/saturday', $storeScope);
                    $json = json_decode($data, true);
                    $sat_open_close = ($json['saturday']['value'] == 1) ? 'open' : 'close';
                    $sat_from = $json['saturday']['from'][0].':'.$json['saturday']['from'][1];
                    $sat_to = $json['saturday']['to'][0].':'.$json['saturday']['to'][1];
                    $final_sat = ($sat_open_close == "open") ? '<label>Saturday </label><span>'.date("g:i A", strtotime($sat_from)).' - '.date("g:i A", strtotime($sat_to)).'</span>' : '<label>Saturday </label><span>Close</span>';   
                }else{
                    $json = json_decode($value['operation_sat'], true);
                    $sat_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $sat_from = $json['from'][0].':'.$json['from'][1];
                    $sat_to = $json['to'][0].':'.$json['to'][1];
                    $final_sat = ($sat_open_close == "open") ? '<label>Saturday </label><span>'.date("g:i A", strtotime($sat_from)).' - '.date("g:i A", strtotime($sat_to)).'</span>' : '<label>Saturday </label><span>Close</span>';   
                }

                if ($value['operation_sun'] == 'use_config') {
                    $data = $this->scopeConfig->getValue('storelocator/time_default/sunday', $storeScope);
                    $json = json_decode($data, true);
                    $sun_open_close = ($json['sunday']['value'] == 1) ? 'open' : 'close';
                    $sun_from = $json['sunday']['from'][0].':'.$json['sunday']['from'][1];
                    $sun_to = $json['sunday']['to'][0].':'.$json['sunday']['to'][1];
                    $final_sun = ($sun_open_close == "open") ? '<label>Sunday </label><span>'.date("g:i A", strtotime($sun_from)).' - '.date("g:i A", strtotime($sun_to)).'</span>' : '<label>Sunday </label><span>Close</span>';   
                }else{
                    $json = json_decode($value['operation_sun'], true);
                    $sun_open_close = ($json['value'] == 1) ? 'open' : 'close';
                    $sun_from = $json['from'][0].':'.$json['from'][1];
                    $sun_to = $json['to'][0].':'.$json['to'][1];
                    $final_sun = ($sun_open_close == "open") ? '<label>Sunday </label><span>'.date("g:i A", strtotime($sun_from)).' - '.date("g:i A", strtotime($sun_to)).'</span>' : '<label>Sunday </label><span>Close</span>';   
                }

                // timings data

                // <h1 class="mp-dark">'.$value['name'].'</h1>
                if (!empty($value['description'])) {
                    $tophtml.='<div class="mp_storelocator_head">
                                <span class="mp_storelocator_description">'.$value['description'].'</span>
                            </div>';    
                }
                
                $html.='<div class="mp-store-view-info" >
                        <div class="mp-row-store-img" id="store-image-gallery">
                            <div class="map-pin">
                            <img class="responsive-img" src="'.$media_url.'main-map-icon.png" alt="'.$value['name'].'">
                            </div>
                            <div class="mp-store-name">
                                <span>'.$value['city'].'</span>
                            </div>
                        </div>
                        <div class="store-info store-info-details">
                            <div class="row-store-info">
                            <div class="store-addr">
                            <a href="'.$value['embed_url'].'" target="_blank">
                                <span class="mp-main-addr">
                                    '.$value['street'].', '.$value['city'].' '.$value['state_province'].' '.$value['postal_code'].'
                                </span>
                            </a><br>
                            <div class="loc-web">
                                <span id="store-view-phone">Call Us: '.$value['phone_one'].'</span>
                            </div>
                            </div>
                            </div>
                            <div class="store-contact-options">
                                <a href="'.$base_url.'contact?id='.$value['location_id'].'" class="view">CONTACT STORE</a>
                                <a onclick="getLocationdata('.$value["location_id"].')" target="_blank" class="directions">GET DIRECTIONS</a>
                            </div>
                        </div>';

                // Check if store is not in Holiday list
                if ($this->checkHoliday($holidayIds, $currentTime)) {
                $html.='<span style="color: #DB4437; margin-right: 10px">Closed</span>';
                } else {
                    $html.='<div class="store-timing-list">
                            <label>OPENING HOURS </label>    
                            <ul>
                                <li>'.$final_mon.'</li>
                                <li>'.$final_tue.'</li>
                                <li>'.$final_wed.'</li>
                                <li>'.$final_thu.'</li>
                                <li>'.$final_fri.'</li>
                                <li>'.$final_sat.'</li>
                                <li>'.$final_sun.'</li>
                            </ul>
                        </div>';
                }
                
            $html.='</div>';

            $coordinates[] = ['latitude' => $value['latitude'],'longitude' => $value['longitude'],'name' => $value['name'],'state' => $value['state_province'],'api_key' => $this->scopeConfig->getValue('storelocator/map_setting/api_key', $storeScope)];
            $bottom_description[] = ['bottom_description1' => $value['description_field_1'],'bottom_description2' => $value['description_field_2']];
            $i++;
        }
        }

        // echo "<pre>";
        // print_r($bottom_description);                        
        // die();
        return $result->setData(['success' => true,'value'=>$html,'coordinates'=>$coordinates,'tophtml' => $tophtml,'bottom_description' => $bottom_description]);
    }

    public function checkHoliday($holidayIds, $currentTime)
    {
        foreach ($holidayIds as $holidayId) {
            $holiday = $this->holidayFactory->create()->load($holidayId);

            if ($holiday->getStatus() &&
                $currentTime >= strtotime($holiday->getFrom()) && $currentTime <= strtotime($holiday->getTo())) {
                return true;
            }
        }

        return false;
    }

    
}
?>


