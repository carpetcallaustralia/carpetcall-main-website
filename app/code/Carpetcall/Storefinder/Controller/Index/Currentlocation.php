<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Controller\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Currentlocation extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;
    protected $_resource;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LocationFactory $locationFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
        $this->locationFactory = $locationFactory;
        $this->_resource = $resource;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    { 
        $param = $this->getRequest()->getParams();
        
        $lat = $param['lat'];
        $lng = $param['lng'];
        $result = $this->resultJsonFactory->create();

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $key = $this->scopeConfig->getValue('storelocator/map_setting/api_key', $storeScope);

        $url = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=".$key."&latlng=$lat,$lng");

        $json = json_decode($url);
        // echo "<pre>";
        // print_r($json);die();
        foreach($json->results[0]->address_components as $adr_node) {
            if($adr_node->types[0] == 'postal_code') {
                $post_code =  $adr_node->long_name;
                //$post_code =  '2064';
            }
            if($adr_node->types[0] == 'locality') {
                $city =  $adr_node->long_name;
            }
        }

        $html='';
        $locations = $this->locationFactory->create()->getCollection();
        // echo "<pre>";
        // print_r($param);                        
        // print_r($locations->getData());
        //  die();
        $base_url = $this->storeManager->getStore()->getBaseUrl();
        $media_url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        // Code to get lat, long form zip code
        $address = $post_code.", Australia";
        $url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&key=".$key."";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
        $responseJson = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($responseJson);

        if ($response->status == 'OK') {
            $latitude = $response->results[0]->geometry->location->lat;
            $longitude = $response->results[0]->geometry->location->lng;

            $connection = $this->_resource->getConnection();
            $tableName = $this->_resource->getTableName('mageplaza_storelocator_location'); //gives table name with prefix
            
            //Select Data from table
            $sql = " SELECT * FROM (
                SELECT *, 
                    (
                        (
                            (
                                acos(
                                    sin(( ".$latitude." * pi() / 180))
                                    *
                                    sin(( latitude * pi() / 180)) + cos(( ".$latitude." * pi() /180 ))
                                    *
                                    cos(( latitude * pi() / 180)) * cos((( ".$longitude." - longitude) * pi()/180)))
                            ) * 180/pi()
                        ) * 60 * 1.1515 * 1.609344
                    )
                as distance FROM ". $tableName."
            ) ". $tableName."
            WHERE distance <= 100 
            ORDER BY distance 
            LIMIT 15";
            $store_colection = $connection->fetchAll($sql); // gives associated array, table fields as key in array.

            // Code to fetch stores from database based on zip code lat, long

            $coordinates = array();
            $i = 0;
            $html.='
            <h5>Showing stores nearest to your specified location.</h5>
            <ul class="list mp-storelocator-list-location">';
            foreach ($store_colection as $key => $value) {
                $state_init_url = $this->getStateInitUrl($value['state_province']);
                    $html.='<li class="mpstorelocator-location-4 mp store-view store-view-4">
                        <div class="mp-store-view-info">
                        <div class="mp-row-store-img" id="store-image-gallery">
                            <div class="map-pin">
                                <img class="responsive-img" src="'.$media_url.'main-map-icon.png" alt="'.$value['name'].'">
                            </div>
                            <div class="mp-store-name">
                                <span>'.$value['name'].'</span>
                            </div>
                        </div>
                        <div class="row-store-info">
                            <div class="store-addr">
                                <a href="'.$value['embed_url'].'" target="_blank">
                                    <span class="mp-main-addr">
                                        '.$value['street'].', '.$value['city'].' '.$value['state_province'].' '.$value['postal_code'].'
                                    </span>
                                    </a>
                                    <br>
                                <div class="loc-web">
                                    <span id="store-view-phone">Call Us:<a href="tel:'.$value['phone_one'].'"> '.$value['phone_one'].'</a></span>
                                </div>
                            </div>
                            <div class="store-contact-options">
                                <a href="'.$base_url.'find-a-store/'.$state_init_url.'/'.str_replace(' ', '-', strtolower($value['url_key'])).'" class="view">VIEW STORE DETAILS</a>
                                <a onclick="getLocationdata('.$value["location_id"].')" class="directions">GET DIRECTIONS</a>
                            </div>
                        </div>
                    </div>
                        </li>';
                        $coordinates[] = ['latitude' => $value['latitude'],'longitude' => $value['longitude'],'name' => $value['name']];
                $i++;
            }
            $html.='</ul">';

            return $result->setData(['success' => true,'value'=>$html,'coordinates'=>$coordinates,'postcode' => $post_code,'lat' => $lat,'long' => $lng, 'city' => $city]);
        }

    }

    public function getStateInitUrl($state_int)
    {
        $state_int = ucwords(strtolower($state_int));
        switch ($state_int) {
            case "New South Wales":
            $state_init_url_text = 'nsw';
            break;
            case "South Australia":
            $state_init_url_text = 'sa';
            break;
            case "Western Australia":
            $state_init_url_text = 'wa';
            break;
            case "Queensland":
            $state_init_url_text = 'qld';
            break;
            case "Tasmania":
            $state_init_url_text = 'tas';
            break;
            case "Victoria":
            $state_init_url_text = 'vic';
            break;
            default:
            $state_init_url_text = '';
        }

        return $state_init_url_text;
    }

    
}


