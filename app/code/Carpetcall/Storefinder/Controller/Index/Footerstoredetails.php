<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Controller\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Footerstoredetails extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;
    protected $request;
    protected $_resource;
    protected $_helper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LocationFactory $locationFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\ResourceConnection $resource,
        \Carpetcall\Storefinder\Helper\Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
        $this->locationFactory = $locationFactory;
        $this->request = $request;
        $this->_resource = $resource;
        $this->_helper = $helper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        // echo "<pre>";
        // print_r($params);die;
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $key = $this->scopeConfig->getValue('storelocator/map_setting/api_key', $storeScope);
        // Code to get lat, long form zip code
        $address = $params['store'].", Australia";
        $url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&key=".$key."";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $responseJson = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($responseJson);

        if ($response->status == 'OK') {
            $latitude = $response->results[0]->geometry->location->lat;
            $longitude = $response->results[0]->geometry->location->lng;

            $connection = $this->_resource->getConnection();
            $tableName = $this->_resource->getTableName('mageplaza_storelocator_location'); //gives table name with prefix

            //Select Data from table
            $sql = " SELECT * FROM (
                SELECT *,
                    (
                        (
                            (
                                acos(
                                    sin(( ".$latitude." * pi() / 180))
                                    *
                                    sin(( latitude * pi() / 180)) + cos(( ".$latitude." * pi() /180 ))
                                    *
                                    cos(( latitude * pi() / 180)) * cos((( ".$longitude." - longitude) * pi()/180)))
                            ) * 180/pi()
                        ) * 60 * 1.1515 * 1.609344
                    )
                as distance FROM ". $tableName."
            ) ". $tableName."
            WHERE distance <= 100 
            ORDER BY distance 
            LIMIT 6";
            $store_colection = $connection->fetchAll($sql);

        if (!empty($params)) {

                $result = $this->resultJsonFactory->create();
                $html='';
                $locations = $this->locationFactory->create()->getCollection();
                //echo "<pre>";

                //print_r(count($store_colection));
                // //print_r($locations->getData());
                //die();
                $html.='<h5>Showing stores nearest to your specified location.</h5>';
                $i = 0;
                foreach ($store_colection as $key => $value) {

                    $storeUrl = '/find-a-store/'. $this->_helper->getAbbreviatedState($value['state_province']) . '/'. $value['url_key'];
                    $html.='<a href="javascript:void(0);"><ul class="store_list">
                            <label for="store-checkbox'.$i.'">
                                <li>
                                    <span class="store_name">'.$value['name'].'</span>
                                    <p class="store_address">
                                    '.$value['street'].', '.$value['city'].' '.$value['state_province'].' '.$value['postal_code'].'
                                    </p>
                                    <div class="field choice">
                                        <input class="radio required-entry" data-validate="{'."'".'validate-one-required-by-name'."'".':true}" type="radio" name="store-checkbox" id="store-checkbox'.$i.'" value="' . $value['name'] . ' / ' . $value['postal_code'] . ' / ' . $value['email'] . '" '.((!empty($params['urlparam']) && ($params['store'] == $value['postal_code'])) ? 'checked' : '').'>
                                        <label for="store-checkbox'.$i.'">
                                            Select store
                                        </label>
                                    </div>
                                </li>
                            </label>
                            </ul></a>';

                $i++;
                }
                return $result->setData(['success' => true,'value'=>$html]);

        }else{
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        }

    }
}


}


