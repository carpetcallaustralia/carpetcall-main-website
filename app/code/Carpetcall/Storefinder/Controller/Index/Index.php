<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Controller\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;
    protected $request;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LocationFactory $locationFactory,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
        $this->locationFactory = $locationFactory;
        $this->request = $request;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    { //        echo "string";die();
        // $route      = $this->request->getRouteName();

        // $moduleName = $this->request->getModuleName();
        // $controller = $this->request->getControllerName();
        // $action     = $this->request->getActionName();
        // $route      = $this->request->getRouteName();
        // // echo $moduleName."<br/>";
        // // echo $controller."<br/>";
        // // echo $action."<br/>";
        // // echo $route."<br/>";die;
        $params = $this->getRequest()->getParams();
        // echo "<pre>";
        // print_r($params);die();

        $state_city_arr = array('nsw','sa','tas','wa','vic','qld','adelaide','sydney','hobart','melbourne','brisbane','perth');
        if (!empty($params)) {

            if (in_array($params['param']['state'], $state_city_arr)) {
                
                $this->_view->loadLayout();
                $this->_view->renderLayout(); 
                
            }elseif ($params['param']['state'] == 'head_offices') {
                
                $this->_view->loadLayout();
                $this->_view->renderLayout(); 

            }else {

                $param = $this->getRequest()->getParams();

                $result = $this->resultJsonFactory->create();
                $html='';
                $locations = $this->locationFactory->create()->getCollection();
                // echo "<pre>";
                // print_r($param);
                // //print_r($locations->getData());
                // die();
                $i = 0;
                foreach ($locations as $key => $value) {
                if ($value['postal_code'] == $param['store']) {
                    $html.='<ul class="store_list">
                            <label for="store-checkbox'.$i.'">
                            <li>
                            <span class="store_name">'.$value['name'].'</span>
                            <p class="store_address">
                            '.$value['street'].', '.$value['city'].' '.$value['state_province'].' '.$value['postal_code'].'
                            </p>
                            <div class="field choice">
                            <input class="radio" type="radio" name="store-checkbox" id="store-checkbox'.$i.'" value="'.$value['name'].'">
                            <label for="store-checkbox'.$i.'">
                            Select store
                            </label>
                            </div>
                            </li>
                            </label>
                            </ul>';
                }
                $i++;
                }
                return $result->setData(['success' => true,'value'=>$html]);
            }
        }else{
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        }
        
    }

    
}


