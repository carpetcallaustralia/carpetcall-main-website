<?php
declare(strict_types=1);

namespace Carpetcall\Storefinder\Controller\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Currentlocationforstores extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;
    protected $_resource;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LocationFactory $locationFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
        $this->locationFactory = $locationFactory;
        $this->_resource = $resource;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    { 
        $result = $this->resultJsonFactory->create();
        $html='';
        $param = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        $lat = $param['lat'];
        $lng = $param['lng'];
        $locationid = $param['street'];

        $locations = $this->locationFactory->create()->getCollection();

        //Start - Code to get current location address

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $key = $this->scopeConfig->getValue('storelocator/map_setting/api_key', $storeScope);

        $url = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=".$key."&latlng=$lat,$lng");

        $json = json_decode($url);
        // echo "<pre>";
        // print_r($json);die();
        foreach($json->results[0]->address_components as $adr_node) {
            if($adr_node->types[0] == 'postal_code') {
                //$post_code =  $adr_node->long_name;
                $post_code =  '2250';
            }
            if($adr_node->types[0] == 'locality') {
                $city =  $adr_node->long_name;
            }
        }
        $address = $post_code.", Australia";

        foreach ($locations->getData() as $key => $value) {
            
            if ($value['location_id'] == $locationid) {
                //$html.='https://maps.google.com?saddr='.urlencode($address).'&daddr='.$value['street'].', '.$value['city'].' '.$value['state_province'].' '.$value['postal_code'].'';
                $html.= $value['embed_url'];
            }
        }
        
        return $result->setData(['value'=>$html]);
        
    }

}


