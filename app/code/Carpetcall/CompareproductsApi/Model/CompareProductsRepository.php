<?php
namespace Carpetcall\CompareproductsApi\Model;

use Carpetcall\CompareproductsApi\Api\CompareProductsRepositoryInterface;
use Magento\Framework\ObjectManagerInterface;
 
class CompareProductsRepository implements CompareProductsRepositoryInterface
{
    const ORDER_TABLE = 'catalog_compare_item';
    protected $resource;
    protected $productdata;

    /**
     *
     * @var \Magento\Authorization\Model\CompositeUserContext
     */
    protected $userContext;
    
    /**
     * 
     * @param \Magento\Authorization\Model\CompositeUserContext $userContext
     */

    public function __construct(
        \Magento\Authorization\Model\CompositeUserContext $userContext,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Model\ProductFactory $productdata,
        \Magento\Catalog\Model\Product\Attribute\Repository $productrepo,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configProduct,
        ObjectManagerInterface $objectManager
    ) {
        $this->resource = $resource;
        $this->productdata = $productdata;
        $this->productrepo = $productrepo;
        $this->userContext = $userContext;
        $this->configProduct = $configProduct;
        $this->objectManager = $objectManager;
    }
        
    /**
     * @inheritDoc
     */
    public function getList($customerId)
    {  //echo $customerId;die();
        $connection = $this->resource->getConnection();
        if ($customerId == 'undefined') {
            // Guest User
            
            $customerId = null;
            $product_details = array();
            $phpsessionid = $_COOKIE['PHPSESSID']; //PHPSESSID value from cookie
            //$phpsessionid = 'acn3c4u04be9tfdv3s2n0qvkkb';

            $tableName = $connection->getTableName(self::ORDER_TABLE);

            $query = $connection->select()
                ->from($tableName)
                ->where('visitor_phpsession_id = ?', $phpsessionid);

            $fetchData = $connection->fetchAll($query);
            
            //$result1 = $connection->fetchAll("SELECT * FROM catalog_compare_item Where (customer_id IS NULL OR customer_id = '') AND visitor_phpsession_id = '".$phpsessionid."'");
            // echo "<pre>";
            //     print_r($fetchData);die();
            foreach ($fetchData as $data) {
                
                $product_features = array();
                $product_colours = array();
                $product_style = array();
                $product_features_val = array();
                $product_features_img = array();
                $product_id = $data['product_id'];

                $product = $this->productdata->create()->load($product_id);

                $selectOptions = $this->productrepo->get('carpet_features')->getOptions();
                foreach ($selectOptions as $selectOption) {
                    $values = explode(',',$product->getData('carpet_features'));
                    if (in_array($selectOption->getValue(), $values)) {
                    $mediaPath = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                        
                        $feat_img = $mediaPath.'carpet_features/'.preg_replace('/\s+/', '-', strtolower($selectOption->getLabel())).'.png';
                        $product_features[] = ['label' => $selectOption->getLabel(), 'img' => $feat_img ];
                    }   
                    
                }

                //echo "<pre>";print_r($product_style_label);die();
                
                $productAttributeOptions = $this->configProduct->getConfigurableAttributesAsArray($product);
            
                $_children = $product->getTypeInstance()->getUsedProducts($product);

                foreach ($productAttributeOptions as $key) {
                    foreach ($key['values'] as $key) {
                        $product_colours[] = ['value_index' => $key['value_index'],
                                      'label' => $key['label']];
                    }
                }
                

                $selectOptionsstyle = $this->productrepo->get('carpet_style')->getOptions();
                foreach ($selectOptionsstyle as $selectOption) {
                    $stylevalues = explode(',',$product->getData('carpet_style'));
                    if (in_array($selectOption->getValue(), $stylevalues)) {

                        $mediaPath_sty = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                        ->getStore()
                        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                        
                        $feat_img_sty = $mediaPath_sty.'carpet_features/'.preg_replace('/\s+/', '-', strtolower($selectOption->getLabel())).'.png';

                        $product_style = ['label' => $selectOption->getLabel(),'img' => $feat_img_sty];
                    }   
                }


                $product_details[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'image' => $product->getData('image'),
                'features' => $product_features,
                'style' => $product_style,
                'colours' => $product_colours,
                'fibre' => $product->getData('carpet_fibre'),
                'env' => $product->getData('carpet_environment'),
                'warranty' => $product->getData('carpet_warranty'),
                 ];
            }
            //echo "<pre>";print_r($product_details);die();
            return $product_details;
            
            // Guest User
        }else{
            // Logged In User
            
            $connection = $this->resource->getConnection();
            $customerId = $this->getCustomerId();
            //$customer_id = 4;
            $product_details = array();

            $result1 = $connection->fetchAll("SELECT * FROM catalog_compare_item Where customer_id = ".$customerId);
            foreach ($result1 as $data) {
                // echo "<pre>";
                // print_r($data);die();

                $product_features = array();
                $product_colours = array();
                $product_id = $data['product_id'];

                $product = $this->productdata->create()->load($product_id);

                $selectOptions = $this->productrepo->get('carpet_features')->getOptions();
                foreach ($selectOptions as $selectOption) {
                    $values = explode(',',$product->getData('carpet_features'));
                    if (in_array($selectOption->getValue(), $values)) {
                        $product_features[] = $selectOption->getLabel();
                    }   
                }

                $selectOptionscolors = $this->productrepo->get('carpet_colour')->getOptions();
                foreach ($selectOptionscolors as $selectOption) {
                    $coloursvalues = explode(',',$product->getData('carpet_colour'));
                    if (in_array($selectOption->getValue(), $coloursvalues)) {
                        $product_colours[] = $selectOption->getLabel();
                    }   
                }
                
                $product_details[] = [
                    'id' => $product->getId(),                              
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'image' => $product->getData('image'),
                'features' => $product->getData('carpet_features'),
                'style' => $product->getData('carpet_style'),
                'colours' => $product->getData('carpet_colour'),
                'fibre' => $product->getData('carpet_fibre'),
                'env' => $product->getData('carpet_environment'),
                'warranty' => $product->getData('carpet_warranty'),
                 ];
            }

            return $product_details;
            // Logged In User
    }
    }

     /**
     * 
     * @return int
     */
    public function getCustomerId()
    {
        return $customerId = $this->userContext->getUserId();
    } 
}
