<?php
declare(strict_types=1);

namespace Carpetcall\CompareproductsApi\Model;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Carpetcall\CompareproductsApi\Api\CompareproductsApiManagementInterface;

/**
 * Class ContactusManagement
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CompareproductsApiManagement implements CompareproductsApiManagementInterface
{ 
    
    const ORDER_TABLE = 'catalog_compare_item';
    protected $_catalogProductCompareList;
    /**
     *
     * @var \Magento\Authorization\Model\CompositeUserContext
     */
    protected $userContext;
    
    /**
     * 
     * @param \Magento\Authorization\Model\CompositeUserContext $userContext
     */

    public function __construct(
        \Magento\Authorization\Model\CompositeUserContext $userContext,
        \Magento\Catalog\Model\Product\Compare\Item $catalogProductCompareList,
        \Magento\Reports\Model\Product\Index\ComparedFactory $ComparedFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->userContext = $userContext;
        $this->_catalogProductCompareList = $catalogProductCompareList;
        $this->_comparedfactory = $ComparedFactory;
        $this->resource = $resource;
    }

    public function addtocompare($productIds,$customerId)
    {
        $connection = $this->resource->getConnection();

        if (!empty($customerId)) {
            // Login User
            $customerId = $this->getCustomerId();
            // check if guest user has already added products to compare then transfer guest user to logged in user i.e. update customer id from Null to id

            // $result1 = $connection->fetchAll("SELECT * FROM catalog_compare_item Where (customer_id IS NULL OR customer_id = '') AND visitor_phpsession_id = '".$phpsessionid."'");
            // //echo '<pre>';print_r($result1[0]['catalog_compare_item_id']);die();
            // if (!empty($result1)) {
            //     // Customer Id needs to updated
            //     // $catalog_compare_item_id = $result1[0]['catalog_compare_item_id'];
            //     // $collection = $this->_catalogProductCompareList->getCollection()->getData();
            //     // foreach ($collection as $data) {
            //     //     print_r($data);
            //     // }
            //     // die();
            //     // //echo '<pre>';print_r($collection);die();
            // }

            // Check if login user has already added product to cart
            //$result1 = $connection->fetchAll("SELECT * FROM catalog_compare_item Where customer_id = ".$customerId . " AND product_id = ".$productIds);
            $tableName = $connection->getTableName(self::ORDER_TABLE);

            $query = $connection->select()
                ->from($tableName)
                ->where('customer_id = ?', $customerId)
                ->where('product_id = ?', $productIds);

            $fetchData = $connection->fetchAll($query);
            //echo '<pre>';print_r($fetchData);die();
            if (empty($fetchData)) {

                // If products > 3 stop from adding
                //$count_no_of_products_added_qry = $connection->fetchAll("SELECT * FROM catalog_compare_item Where  customer_id = '".$customerId."'");

                $tableName = $connection->getTableName(self::ORDER_TABLE);

                $query = $connection->select()
                    ->from($tableName)
                    ->where('customer_id = ?', $customerId);

                $count_no_of_products_added_qry = $connection->fetchAll($query);

                $count_no_of_products_added = count($count_no_of_products_added_qry);
                if ($count_no_of_products_added == 3) {
                    return "Maximum 3 product allowed in comparison list.";
                }else{

                    $this->_catalogProductCompareList->setCustomerId($customerId);
                    $this->_catalogProductCompareList->addProductData($productIds);
                    $this->_catalogProductCompareList->save();
                    $this->_catalogProductCompareList->unsetData();

                    $viewData = [
                        'product_id' => $productIds,
                        'customer_id' => $customerId
                    ];
                    $this->_comparedfactory->create()->setData($viewData)->save();

                return "Product added in compare list.";
                }
            }else{
                return "Product already exists in compare list.";
            }

            // Login User
        }else{
            // Guest User

            $customerId = NULL;
            $phpsessionid = $_COOKIE['PHPSESSID']; //PHPSESSID value from cookie

            //$result1 = $connection->fetchAll("SELECT * FROM catalog_compare_item Where (customer_id IS NULL OR customer_id = '') AND product_id = ".$productIds . " AND visitor_phpsession_id = '".$phpsessionid."'");
            $tableName = $connection->getTableName(self::ORDER_TABLE);

            $query = $connection->select()
                ->from($tableName)
                ->where('visitor_phpsession_id = ?', $phpsessionid)
                ->where('product_id = ?', $productIds);

            $fetchData = $connection->fetchAll($query);
            ///echo '<pre>';print_r($result1);die();
            if (empty($fetchData)) {
            // If products > 3 stop from adding
            //$count_no_of_products_added_qry = $connection->fetchAll("SELECT * FROM catalog_compare_item Where  visitor_phpsession_id = '".$phpsessionid."'");
            $tableName = $connection->getTableName(self::ORDER_TABLE);

                $query = $connection->select()
                    ->from($tableName)
                    ->where('visitor_phpsession_id = ?', $phpsessionid);

            $count_no_of_products_added_qry = $connection->fetchAll($query);
            $count_no_of_products_added = count($count_no_of_products_added_qry);
            if ($count_no_of_products_added == 3) {
                return "Maximum 3 product allowed in comparison list.";
            }else{
            $this->_catalogProductCompareList->setCustomerId($customerId);
            $this->_catalogProductCompareList->setVisitorPhpsessionId($phpsessionid);
            $this->_catalogProductCompareList->addProductData($productIds);
            $this->_catalogProductCompareList->save();
            $this->_catalogProductCompareList->unsetData();

            $viewData = [
                'product_id' => $productIds,
                'customer_id' => $customerId
            ];
            $this->_comparedfactory->create()->setData($viewData)->save();

                return "Product added in compare list.";
            }
            }else{
                return "Product already exists in compare list.";
            }

            
            // Guest User
        }
    }

    /**
     * 
     * @return int
     */
    public function getCustomerId()
    {
        return $customerId = $this->userContext->getUserId();
    }    

}