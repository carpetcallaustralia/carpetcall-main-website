<?php
declare(strict_types=1);

namespace Carpetcall\CompareproductsApi\Model;

use Magento\Contact\Model\MailInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\DataObject;
use Carpetcall\CompareproductsApi\Api\RemoveCompareproductsApiManagementInterface;

/**
 * Class ContactusManagement
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RemoveCompareproductsApiManagement implements RemoveCompareproductsApiManagementInterface
{ 
    const ORDER_TABLE = 'catalog_compare_item';
    protected  $_productloader;
    protected $_compareItemFactory;
    protected $_catalogProductCompareList;

    /**
     *
     * @var \Magento\Authorization\Model\CompositeUserContext
     */
    protected $userContext;
    
    /**
     * 
     * @param \Magento\Authorization\Model\CompositeUserContext $userContext
     */


    public function __construct(
        \Magento\Authorization\Model\CompositeUserContext $userContext,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Model\Product\Compare\ItemFactory $compareItemFactory,
        \Magento\Catalog\Model\Product\Compare\ListCompare $catalogProductCompareList,
        \Magento\Catalog\Model\ProductFactory $productloader
    ) {
        $this->_productloader = $productloader;
        $this->_compareItemFactory = $compareItemFactory;
        $this->_catalogProductCompareList = $catalogProductCompareList;
        $this->userContext = $userContext;
        $this->resource = $resource;
    }

    public function removefromcomparelist($productId,$customerId)
    {
        $connection = $this->resource->getConnection();
        if (empty($customerId)) {
            // Guest Customer

            $phpsessionid = $_COOKIE['PHPSESSID'];
            //$phpsessionid = '48s369sop86lmv964j0j93ae5n';

            $tableName = $connection->getTableName(self::ORDER_TABLE);
            
            $whereConditions = [
              $connection->quoteInto('visitor_phpsession_id = ?', $phpsessionid),
              $connection->quoteInto('product_id = ?', $productId),
            ];

            $deleteRows = $connection->delete($tableName, $whereConditions);
            return "Product has been removed successfully";

            // $result1 = $connection->fetchAll("SELECT * FROM catalog_compare_item Where (customer_id IS NULL OR customer_id = '') AND visitor_phpsession_id = '".$phpsessionid."'");
            // foreach ($result1 as $data) {

            //     $product=$this->_productloader->create()->load($productId);
                
            //     //$this->_catalogProductCompareList->removeProduct($product);

            //     $item = $this->_compareItemFactory->create();
            //     //$this->_addVisitorToItem($item);
            //     //$item->addAttributeToFilter('visitor_phpsession_id',$phpsessionid);
            //     $item->loadByProduct($product);

            //     echo "<pre>";
            //     print_r($item->getCollection()->getData());die;

            //     if ($item->getId()) {
            //         $item->delete();
            //     }
            //     return "Product has been removed successfully";
            //}
            // Guest Customer
        }else{
            // Login Customer
            $customerId = $this->getCustomerId();
            //$customerId = 10;
            $tableName = $connection->getTableName(self::ORDER_TABLE);

            $whereConditions = [
              $connection->quoteInto('customer_id = ?', $customerId),
              $connection->quoteInto('product_id = ?', $productId),
            ];

            $deleteRows = $connection->delete($tableName, $whereConditions);
            return "Product has been removed successfully";

            // Login Customer
        }
        
    }    

     /**
     * 
     * @return int
     */
    public function getCustomerId()
    {
        return $customerId = $this->userContext->getUserId();
    } 
}