<?php
namespace Carpetcall\CompareproductsApi\Api;

/**
 * Interface CompareproductsApiManagementInterface
 *
 * @package Carpetcall\CompareproductsApi\Api
 */
interface CompareproductsApiManagementInterface
{
    /**
     * Contact us form.
     *
     * @param mixed $productIds
     * @param mixed $customerId
     * @param mixed $phpsessionid
     *
     * @return string
     */
    public function addtocompare($productIds,$customerId);
}