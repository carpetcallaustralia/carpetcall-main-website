<?php
namespace Carpetcall\CompareproductsApi\Api;

interface CompareProductsRepositoryInterface
{
    /**
     * Get compare products
     *
     * @param mixed $customerId
     * @param int $limit
     * @param mixed $phpsessionid
     * @return array
     */
    public function getList($customerId);
}
