<?php
namespace Carpetcall\CompareproductsApi\Api;

/**
 * Interface RemoveCompareproductsApiManagementInterface
 *
 * @package Carpetcall\CompareproductsApi\Api
 */
interface RemoveCompareproductsApiManagementInterface
{
    /**
     * Contact us form.
     *
     * @param mixed $productId
     * @param mixed $customerId
     *
     * @return string
     */
    public function removefromcomparelist($productId,$customerId);
}