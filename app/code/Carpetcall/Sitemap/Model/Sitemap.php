<?php
namespace Carpetcall\Sitemap\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Sitemap extends \Mageplaza\Blog\Model\Sitemap
{

     protected function _construct()
    {
        parent::_construct();

        $this->scopeConfig = ObjectManager::getInstance()->get(ScopeConfigInterface::class);
    }

    public function getCustomurlCollection()
    {
    $siteMapcollection = array();
    /* if want to add multiple url then load your custom collection and set  */
    $incl_urls_bunch = $this->scopeConfig->getValue('excludeurls/includeurls/includeurls', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    $incl_urls = explode(",",str_replace(' ','',$incl_urls_bunch));

    // echo "<pre>";
    //     print_r($incl_urls);die();

     foreach ($incl_urls as $urls) {

            $siteMapcollection[] = new \Magento\Framework\DataObject([
                'id'         => $urls,
                'url'        => $urls,
                'updated_at' => date("Y-m-d h:i:s"),
            ]);
    }
    return $siteMapcollection;

    }

   public function _initSitemapItems()
    {
        $this->_sitemapItems[] = new \Magento\Framework\DataObject([
            'collection' => $this->getCustomurlCollection(),
        ]);

        parent::_initSitemapItems(); 
    }

    protected function _getSitemapRow($url, $lastmod = null, $changefreq = null, $priority = null, $images = null)
    { 
        $url = $this->_getUrl($url);
        
        $excl_urls_bunch = $this->scopeConfig->getValue('excludeurls/excludeurls/excludeurls', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $excl_urls = explode(",",str_replace(' ','',$excl_urls_bunch));
        // echo "<pre>";
        // print_r($excl_urls);die();
        
        if (in_array($url, $excl_urls)) {
            return '';
        }
        $row = '<loc>' . $this->_escaper->escapeUrl($url) . '</loc>';
        if ($lastmod) {
            $row .= '<lastmod>' . $this->_getFormattedLastmodDate($lastmod) . '</lastmod>';
        }
        if ($changefreq) {
            $row .= '<changefreq>' . $this->_escaper->escapeHtml($changefreq) . '</changefreq>';
        }
        if ($priority) {
            $row .= sprintf('<priority>%.1f</priority>', $this->_escaper->escapeHtml($priority));
        }
        if ($images) {
            // Add Images to sitemap
            foreach ($images->getCollection() as $image) {
                $row .= '<image:image>';
                $row .= '<image:loc>' . $this->_escaper->escapeUrl($image->getUrl()) . '</image:loc>';
                $row .= '<image:title>' . $this->_escaper->escapeHtml($images->getTitle()) . '</image:title>';
                if ($image->getCaption()) {
                    $row .= '<image:caption>' . $this->_escaper->escapeHtml($image->getCaption()) . '</image:caption>';
                }
                $row .= '</image:image>';
            }
            // Add PageMap image for Google web search
            $row .= '<PageMap xmlns="http://www.google.com/schemas/sitemap-pagemap/1.0"><DataObject type="thumbnail">';
            $row .= '<Attribute name="name" value="' . $this->_escaper->escapeHtml($images->getTitle()) . '"/>';
            $row .= '<Attribute name="src" value="' . $this->_escaper->escapeUrl($images->getThumbnail()) . '"/>';
            $row .= '</DataObject></PageMap>';
        }

        return '<url>' . $row . '</url>';
    }
}