<?php
/**
 * Hello Rewrite Product View Controller
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace Carpetcall\Sitemap\Controller\Index;

class Index extends \Bss\HtmlSiteMap\Controller\Index\Index
{

	/**
	 * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		
        $title = $this->helper->getTitle();
        $meta_title = $this->helper->getTitleSiteMap();
        $description = $this->helper->getDescriptionSitemap();
        $keywords = $this->helper->getKeywordsSitemap();
        //$metaTitle = $this->helper->getMetaTitleSitemap();
        
        $resultPage = $this->resultPageFactory->create();
 
        $resultPage->getConfig()->getTitle()->set(__($meta_title)); // browser tab title
        $resultPage->getConfig()->setKeywords(__($description)); // meta keywords
        $resultPage->getConfig()->setDescription(__($description)); //meta description
 
        $pageMainTitle = $resultPage->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle(__($title)); // Page H1 title
        }
        return $resultPage;

		//return parent::execute();
	}
}