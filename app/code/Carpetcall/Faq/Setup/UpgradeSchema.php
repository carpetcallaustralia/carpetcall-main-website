<?php 
namespace Carpetcall\Faq\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{ 
	public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
	{
		$setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.6') < 0) {

            // Get module table
            $tableName = $setup->getTable('prince_faqgroup');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data

                $columns = [
                    'meta_title' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '256',
                        'nullable' => true,
                        'comment' => 'Meta Title',
                    ],
                    'meta_description' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '2M',
                        'nullable' => true,
                        'comment' => 'Meta Description',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }

            }
        }
	}
}	