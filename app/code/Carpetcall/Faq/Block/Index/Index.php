<?php
/**
 * Hello Rewrite Product ListProduct Block
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace Carpetcall\Faq\Block\Index;

class Index extends \Mageprince\Faq\Block\Index\Index
{
	public function getCurrentUrl()
    {
        $currentUrl = $this->_urlInterface->getCurrentUrl();
        $word = "faqs";
        if(strpos($currentUrl, $word) !== false){
            $arr = explode('faqs/', $currentUrl);
            if (!empty($arr[1])) {
                $category = $arr[1];
                return $category;
            }else{
                return "";    
            }
        } else{
            return "";
        }
    }

    public function _prepareLayout()
        {
            $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
            $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
    
            if ($breadcrumbsBlock) {
    
                $breadcrumbsBlock->addCrumb(
                    'home',
                    [
                    'label' => __('Home'), //lable on breadCrumbes
                    'title' => __('Home'),
                    'link' => $baseUrl,
                    ]
                );
                $breadcrumbsBlock->addCrumb(
                    'ideas-advice',
                    [
                    'label' => __('Ideas & Advice'), //lable on breadCrumbes
                    'title' => __('Ideas & Advice'),
                    'link' => $baseUrl.'ideas-advice',
                    ]
                );
                if (!empty($this->getCurrentUrl())) {
                    $breadcrumbsBlock->addCrumb(
                        'faq',
                        [
                        'label' => __('FAQs'),
                        'title' => __('FAQs'),
                        'link' => $baseUrl.'faqs',
                        ]
                    );
                }else{
                    $breadcrumbsBlock->addCrumb(
                        'faq',
                        [
                        'label' => __('FAQs'),
                        'title' => __('FAQs'),
                        'link' => ''
                        ]
                    );
                }

                if (!empty($this->getCurrentUrl())) {
                    $breadcrumbsBlock->addCrumb(
                        'faq '.$this->getCurrentUrl(),
                        [
                        'label' => __(ucwords(str_replace('-', ' ', $this->getCurrentUrl())) .' FAQs'),
                        'title' => __(ucwords(str_replace('-', ' ', $this->getCurrentUrl())) .' FAQs'),
                        'link' => ''
                        ]
                    );
                }
                
            }
            $this->pageConfig->getTitle()->set(__('FAQs')); // set page name
            return parent::_prepareLayout();
        }
}