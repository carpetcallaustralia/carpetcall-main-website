<?php
namespace Carpetcall\Faq\Block;
use Magento\Framework\View\Element\Template;

class Faq extends \Magento\Framework\View\Element\Template
{   
    protected $categoryFactory;
    protected $category;

    public function __construct(
        Template\Context $context, 
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\Category $category,
        array $data = array())
    {
        $this->categoryFactory = $categoryFactory;
        $this->category = $category;

        parent::__construct($context, $data);
    }

    public function getCategoryDetailsByName($passing_category_for_faq_section)
    {
        $collection = $this->categoryFactory->create()->getCollection()->addFieldToFilter('name',$passing_category_for_faq_section);
        return $collection;
    }

    public function getCategoryName($second_level_cat_id)
    {
        $category = $this->categoryFactory->create()->load($second_level_cat_id);
        $categoryName = $category->getName();
        return $categoryName;
    }
}