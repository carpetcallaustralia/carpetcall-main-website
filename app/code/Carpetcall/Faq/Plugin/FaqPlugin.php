<?php
namespace Carpetcall\Faq\Plugin;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Theme\Block\Html\Title as HtmlTitle;
use Mageprince\Faq\Helper\Data;
 
class FaqPlugin
{
    protected $_resultPage;
    protected $_urlInterface;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\UrlInterface $urlInterface,
        Data $helper,
        \Mageprince\Faq\Model\FaqGroupFactory $faqgroup
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_urlInterface = $urlInterface;
        $this->helper = $helper;
        $this->faqgroup = $faqgroup;
    }

    public function afterExecute(
    \Mageprince\Faq\Controller\Index\Index $cartIndex,
    $resultPage
    )
    {

        $current_url = $this->_urlInterface->getCurrentUrl();
        $parse = parse_url($current_url);
        $explode = explode('/', $parse['path']);

        // Start - code to display meta data category vice
        $arr = explode('/faqs', $current_url);
        $category_name = substr($arr[1], strrpos($arr[1], '/') + 1);
        if ((!empty($arr[1])) && (!empty($category_name))) {

            $meta_data = $this->getMetaData($category_name);
            if (!empty($meta_data)) {
                $metaTitleConfig = $meta_data['meta_title'];
                $metaDescriptionConfig = $meta_data['meta_description'];
            }else{
                $metaTitleConfig = '';
                $metaDescriptionConfig = '';
            }

        }else{
            // without category page
            $metaTitleConfig = $this->helper->getConfig('faqtab/seo/meta_title');
            $metaDescriptionConfig = $this->helper->getConfig('faqtab/seo/meta_description');
        }

        // End - code to display meta data category vice
        

        $resultPage = $this->resultPageFactory->create();
        $pageMainTitle = $resultPage->getLayout()->getBlock('page.main.title');
        $pageTitle = $this->helper->getConfig('faqtab/general/page_title');

        if ($pageMainTitle && $pageMainTitle instanceof HtmlTitle) {
            //$pageMainTitle->setPageTitle($pageTitle);
        }

        if (!$this->helper->getConfig('faqtab/general/enable')) {
            $pageMainTitle->setPageTitle('FAQ Disabled');
            return $resultPage;
        }
        $metaKeywordsConfig = $this->helper->getConfig('faqtab/seo/meta_keywords');
        $resultPage->getConfig()->getTitle()->set($metaTitleConfig);
        $resultPage->getConfig()->setDescription($metaDescriptionConfig);
        $resultPage->getConfig()->setKeywords($metaKeywordsConfig);

        return $resultPage;
        
    
    }

    public function getMetaData($category)
    {
        $coll = $this->faqgroup->create()->getCollection();
        // echo '<pre>';
        // print_r($category);die();
        $meta_data = array();
        foreach ($coll->getData() as $value) {
            $grpname = strtolower(str_replace(' ', '-', $value['groupname']));
            if ($grpname == $category) {
                $meta_data = ['meta_title' => $value['meta_title'],'meta_description' => $value['meta_description']];
                return $meta_data;
            }
        }
        //die();
    }

    public function getFaqCategories($value='')
    {
       $coll = $this->faqgroup->create()->getCollection();
        
        $category_arr = array();
        foreach ($coll->getData() as $value) {
            if ($value['groupname'] == $category) {
                return $meta_data = ['meta_title' => $value['meta_title'],'meta_description' => $value['meta_description']];
            }
        }
    }

}
?>