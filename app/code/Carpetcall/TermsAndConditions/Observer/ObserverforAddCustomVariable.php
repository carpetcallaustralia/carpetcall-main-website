<?php
namespace Carpetcall\TermsAndConditions\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class ObserverforAddCustomVariable implements ObserverInterface
{

    public function __construct(
    ) {
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Framework\App\Action\Action $controller */
        $transport = $observer->getTransport();

        $order = &$transport['order'];
        
        if ($order->getShippingMethod() == 'flatrate_flatrate') {
            if ((!empty($order->getTermsAndConditions())) && ($order->getTermsAndConditions() == 1))
            { 
            $transport['permission_tot_leave_items_if_unattended'] = 'User has given permission to leave items if unattended';
            }
        }
        
        

        // $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/email.log');
        // $logger = new \Zend\Log\Logger();
        // $logger->addWriter($writer);
        // $logger->info('Your text message1234');
        // $logger->info($order->getId());
        // $logger->info($order->getShippingMethod());die();
    }

}
