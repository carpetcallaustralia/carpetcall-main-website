<?php
declare(strict_types=1);

namespace Carpetcall\TermsAndConditions\Observer;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class TransferValueToOrder implements ObserverInterface
{
    /**
     * @param Observer $observer
     */
    public function execute(EventObserver $observer)
    {
        $event = $observer->getEvent();
        // Get Order Object
        /* @var $order \Magento\Sales\Model\Order */
        $order = $event->getOrder();
        // Get Quote Object
        /** @var $quote \Magento\Quote\Model\Quote $quote */
        $quote = $event->getQuote();

        $quoteShippingAdress = $observer->getQuote()->getShippingAddress();
        $streetShippingAdress = $quoteShippingAdress->getData('street');
        $cityShippingAdress = $quoteShippingAdress->getData('city');
        $countryCodeShippingAdress = $quoteShippingAdress->getData('country_id');
        $postCodeShippingAdress = $quoteShippingAdress->getData('postcode');
        $regionShippingAdress = $quoteShippingAdress->getData('region');
        $telephoneShippingAdress = $quoteShippingAdress->getData('telephone');

        $quoteBillingAddress = $observer->getQuote()->getBillingAddress();
        $streetBillingAddress = $quoteBillingAddress->getData('street');
        $cityBillingAddress = $quoteBillingAddress->getData('city');
        $countryCodeBillingAddress = $quoteBillingAddress->getData('country_id');
        $postCodeBillingAddress = $quoteBillingAddress->getData('postcode');
        $regionBillingAddress = $quoteBillingAddress->getData('region');
        $telephoneBillingAddress = $quoteBillingAddress->getData('telephone');

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/order-shipping-billing-address.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info("-------ADDRESS DETAIL-------");
        $logger->info("ORDER ID : ". $order->getIncrementId());
        $logger->info("ShippingAdress :  ". $streetShippingAdress.",".$cityShippingAdress.",".$countryCodeShippingAdress.",".$postCodeShippingAdress.",".$regionShippingAdress.",".$telephoneShippingAdress);
        $logger->info("BillingAddress :  ". $streetBillingAddress.",".$cityBillingAddress.",".$countryCodeBillingAddress.",".$postCodeBillingAddress.",".$regionBillingAddress.",".$telephoneBillingAddress);

        $order = $observer->getOrder();
        if ($order->getShippingMethod() == 'flatrate_flatrate') {
            $order->setTermsAndConditions($quote->getTermsAndConditions());
        }
        return $this;
    }
}
