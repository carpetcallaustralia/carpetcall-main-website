<?php
namespace Carpetcall\TermsAndConditions\Controller\Ajax;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Cmarix\Customadmingrid\Model\CustominfoFactory;
use Magento\Store\Api\StoreResolverInterface;

class Getemailinfo extends \Magento\Framework\App\Action\Action {
    protected $request;
    private $quote;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
    */
    
	protected $_cartModel;

    protected $session;

    protected $resultJsonFactory;

    protected $subscriberFactory;
    
    protected $customerSession;

    protected $storeResolver;

    protected $resource;
    protected $checkoutSession;
    protected $quoteRepository;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $helper
     
     */
    public function __construct(
	\Magento\Framework\App\Action\Context $context, 
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
	\Magento\Checkout\Model\Cart $cartModel,
    \Magento\Framework\Session\SessionManagerInterface $session,
    \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
    \Magento\Customer\Model\Session $customerSession,
    StoreResolverInterface $storeResolver,
    \Magento\Framework\App\ResourceConnection $resource,
    \Magento\Quote\Model\Quote $quote,
    \Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cartModel = $cartModel;
        $this->session = $session;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerSession = $customerSession;
        $this->storeResolver = $storeResolver;
        $this->resource = $resource;
        $this->quote = $quote;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
    }

    public function execute() {

        $currentStoreId = $this->storeResolver->getCurrentStoreId();
        $status = $this->getRequest()->getPost('status');
        
        $quoteId = $this->getQuoteId();
        
        // die();
        if($status == 1) {
            $val = 1;
            $this->updateQuoteData($quoteId,$val);
        }else {
            $val = 0;
            $this->updateQuoteData($quoteId,$val);
        }
    }


    public function getQuotes()
    {
        
        return $this->checkoutSession->getQuote();
    }

    public function getQuoteId()
    {
        return  $this->getQuotes()->getId();
        // $allItems = $this->getQuotes()->getAllVisibleItems();
        // foreach ($allItems as $item) {
        //     echo $item->getProductId();
        // }die();
    }

    public function updateQuoteData($quoteId, int $customData)
    {
        $quote = $this->quoteRepository->get($quoteId); // Get quote by id
        $quote->setData('terms_and_conditions', $customData); // Fill data
        $this->quoteRepository->save($quote); // Save quote
    }
}
