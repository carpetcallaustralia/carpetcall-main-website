<?php
namespace Carpetcall\HFPrice\Controller\Ajax;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Cmarix\Customadmingrid\Model\CustominfoFactory;
use Magento\Store\Api\StoreResolverInterface;

class HFPrice extends \Magento\Framework\App\Action\Action {
    protected $request;
    private $quote;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
    */
    
	protected $_cartModel;

    protected $session;

    protected $resultJsonFactory;

    protected $subscriberFactory;
    
    protected $customerSession;

    protected $storeResolver;

    protected $resource;
    protected $checkoutSession;
    protected $quoteRepository;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $helper
     
     */
    public function __construct(
	\Magento\Framework\App\Action\Context $context, 
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
	\Magento\Checkout\Model\Cart $cartModel,
    \Magento\Framework\Session\SessionManagerInterface $session,
    \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
    \Magento\Customer\Model\Session $customerSession,
    StoreResolverInterface $storeResolver,
    \Magento\Framework\App\ResourceConnection $resource,
    \Magento\Quote\Model\Quote $quote,
    \Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cartModel = $cartModel;
        $this->session = $session;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerSession = $customerSession;
        $this->storeResolver = $storeResolver;
        $this->resource = $resource;
        $this->quote = $quote;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
    }

    public function execute() 
    {
        $totalperpackprice = $this->getRequest()->getPost('totalperpackprice');
        $id = $this->getRequest()->getPost('id');

        // echo "<pre>";
        // print_r($name);
        // die();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $array_product = [$id]; //product Ids
        $productActionObject = $objectManager->create('Magento\Catalog\Model\Product\Action');
        $productActionObject->updateAttributes($array_product, array('hftotal_price' => $totalperpackprice), 0);
    }

}
