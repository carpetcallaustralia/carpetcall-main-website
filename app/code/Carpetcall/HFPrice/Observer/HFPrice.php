<?php
namespace Carpetcall\HFPrice\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class HFPrice implements ObserverInterface
{
    public function __construct(\Magento\Framework\View\LayoutInterface $layout)
    {
      $this->layout = $layout;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {

        
        $_product = $observer->getProduct();
        $blockobj = $this->layout->createBlock('Carpetcall\Productdetailspage\Block\Categoriesdetails');
        $productId = $_product->getId();
        $product = $blockobj->getProductById($productId);

        $categoryIds = $product->getCategoryIds();

        $categories = $blockobj->getCategoryCollection()
        ->addAttributeToFilter('entity_id', $categoryIds);
        $cate_arr = array();
        foreach ($categories as $category) {
        $cate_arr[] = $category->getName();
        }


        // $product = $observer->getProduct();
        // $catIds = $product->getCategoryIds();
        //var_dump($catIds);
        if (in_array("Hard Flooring", $cate_arr, TRUE)) :
        $spec_pri =  $_product->getSpecialPrice();
          if (!empty($spec_pri)) {
            $price_per_pack = $spec_pri * $_product->getCoveragePerPack(); 
          }else{
            $price_per_pack = $_product->getPrice() * $_product->getCoveragePerPack(); 
          }

        $item = $observer->getEvent()->getData('quote_item');           
        $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
        //$price = $_product->getHftotalPrice(); //set your price here
        $item->setCustomPrice((int)$price_per_pack);
        $item->setOriginalCustomPrice((int)$price_per_pack);
        $item->getProduct()->setIsSuperMode(true);

        // $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/price.log');
        // $logger = new \Zend\Log\Logger();
        // $logger->addWriter($writer);
        // $logger->info(print_r($cate_arr,true));
        // $logger->info($_product->getHftotalPrice());
        // die();
        endif;
        
    }

}