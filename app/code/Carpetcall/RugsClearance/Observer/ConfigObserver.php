<?php
namespace Carpetcall\RugsClearance\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\ProductFactory;

class ConfigObserver implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $logger;

    private $request;
    protected $categoryLinkManagement;
    protected $categorydetails;
    protected $_productFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;


    /**
     * @param Logger $logger
     */
    public function __construct(
    	RequestInterface $request,
    	\Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagementInterface,
    	\Magento\Catalog\Model\CategoryFactory $categoryFactory,
    	\Magento\Catalog\Model\Category $categorydetails,
    	\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        Logger $logger,
        ProductFactory $productFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
    	$this->request = $request;
        $this->logger = $logger;
        $this->categoryLinkManagement = $categoryLinkManagementInterface;
        $this->_categoryFactory = $categoryFactory;
        $this->categorydetails = $categorydetails;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->resource = $resource;
    }

    public function execute(EventObserver $observer)
    {
    	$rugs_clearance_category = $this->request->getParam('groups');
        $rugs_clearance_category_status = $rugs_clearance_category['rugs_clearance_category']['fields']['rugs_clearance_category']['value']; //Current faq_url value, Here you can filter current value
        
        // $rugs_clearance_category_status is the backend switch status 1 = Yes & 0 = No
        //$entity_id = '';
    	if ($rugs_clearance_category_status == 1) {

    		// Assign Rugs clearance category to Rugs product

			$categoryTitle = 'Rugs Clearance';
			$collection = $this->_categoryFactory->create()->getCollection()
			              ->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
			//echo 'fdfdfs'.$collection->getSize();die();
			if ($collection->getSize()) {
			   	$rug_clearance_categoryId = $collection->getFirstItem()->getId();

			    $rugs_cate = 'Rugs';
			    $collection = $this->_categoryFactory->create()->getCollection()
			              ->addAttributeToFilter('name',$rugs_cate)->setPageSize(1);
			    $rugs_cate_ids = $collection->getFirstItem()->getId();
				$ids = [$rugs_cate_ids];
				// Get Product collection by category id for Sku
				$collection = $this->_productCollectionFactory->create();
		        $collection->addAttributeToSelect('*');
		        $collection->addCategoriesFilter(['in' => $ids]);

		        // Using following function get the list of SKU which already had clearance category & ignore them
		        $sku = $this->getSkusOfClearanceAlready();
		        $collection->addAttributeToFilter('sku', array('nin' => array($sku)));
		        $collection->addAttributeToFilter('type_id', array('eq' => 'configurable'));
		   		
		        //$sku = array();
		        foreach ($collection->getData() as $key) {
		        	$sku = $key['sku'];
		        	$product = $this->_productFactory->create()->load($key['entity_id']);
					$already_assign_category = $product->getCategoryIds();

		        	//
		        	array_push($already_assign_category,$rug_clearance_categoryId);

		        	$this->categoryLinkManagement->assignProductToCategories($sku, $already_assign_category);
		        }

			}else{
				echo "Rugs Clearance category not assigned";
			}
			 
    	}else{


    		// Assign Rugs clearance category to Rugs product

			$categoryTitle = 'Rugs Clearance';
			$collection = $this->_categoryFactory->create()->getCollection()
			              ->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
			//echo 'fdfdfs'.$collection->getSize();die();
			if ($collection->getSize()) {
			   	$rug_clearance_categoryId = $collection->getFirstItem()->getId();

			    $rugs_cate = 'Rugs';
			    $collection = $this->_categoryFactory->create()->getCollection()
			              ->addAttributeToFilter('name',$rugs_cate)->setPageSize(1);
			    $rugs_cate_ids = $collection->getFirstItem()->getId();
				$ids = [$rugs_cate_ids];
				// Get Product collection by category id for Sku
				$collection = $this->_productCollectionFactory->create();
		        $collection->addAttributeToSelect('*');
		        $collection->addCategoriesFilter(['in' => $ids]);
	        	
		        // Using following function get the list of SKU which already had clearance category & ignore them
		        $sku = $this->getSkusOfClearanceAlready();
		        $collection->addAttributeToFilter('sku', array('nin' => array($sku)));
		        $collection->addAttributeToFilter('type_id', array('eq' => 'configurable'));
				//echo '<pre>';print_r($collection->getData());die();
		        //$sku = array();
		        foreach ($collection->getData() as $key) {
		        	$sku = $key['sku'];
		        	$product = $this->_productFactory->create()->load($key['entity_id']);
					$already_assign_category = $product->getCategoryIds();

					if (in_array($rug_clearance_categoryId, $already_assign_category)) {
						$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

						$CategoryLinkRepository = $objectManager->get('\Magento\Catalog\Model\CategoryLinkRepository');

						$CategoryLinkRepository->deleteByIds($rug_clearance_categoryId,$sku);
					}

		        }

			}else{
				echo "Rugs Clearance category not removed";
			}

    	}
        
        //$this->logger->info($observer->getStore());
    }

    public function getSkusOfClearanceAlready()
    {
    	// Remove those category Ids from switching to Rugs Clearance which are already in Rugs Clearance category
    	$categoryTitle = "Rugs Clearance";
		$sku = array();

    	$connection  = $this->resource->getConnection();
		$tableName = $connection->getTableName('rugs_import_data');

        $query = "SELECT configurable_sku FROM " . $tableName . " WHERE category = '".$categoryTitle."'";
        $fetchData = $connection->fetchAll($query);

        foreach ($fetchData as $data) {
			$sku[] = $data['configurable_sku'];
		}
		//echo '<pre>';print_r($sku);die();
		return $sku;
		
    }
}
