# Magento2 Module Carpetcall Career Emails

    carpetcall/module-career-emails

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)

## Main Functionalities
Add career emails to system config

## Installation
\* = in production please use the `--keep-generated` option

### Composer
 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require carpetcall/module-career-emails`
 - enable the module by running `php bin/magento module:enable Carpetcall_CareerEmails`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration
 - careers_admin_email (admin defined internal career email processing)

