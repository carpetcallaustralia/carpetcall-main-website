<?php
/**
 * Conversion Digital code
 * @author Nathaniel Rogers <nathaniel@conversiondigital.com.au>
 */

namespace Carpetcall\CareerEmails\Helper;

/**
 * Class Data
 * @package Carpetcall\CareerEmails\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const CAREER_ADMIN_EMAIL_PREFIX = 'careers_admin_email/careers_admin_email/';

    /**
     * TODO: load list of states from state directory list
     * @var string[]
     */
    private $_allowedStates = [
        'nsw',
        'qld',
        'sa',
        'vic',
        'wa',
        'act',
        'tas',
    ];

    private $_allowedEmailTypes = ['to', 'bcc'];

    /**
     * @var array
     */
    private $_allEmails = [];

    /**
     * Get list of states and email types
     * @return array
     */
    public function getEmailOptions() {
        $options = [];
        foreach ($this->_allowedStates as $state) {
            $options[$state] = $this->_allowedEmailTypes;
        }
        return $options;
    }

    /**
     * Get career email
     *
     * @param $state
     * @param string $type
     * @return mixed
     */
    public function getCareerEmail($state, $type = 'to') {
        if (!in_array($this->_allowedStates, $state)) {
            return '';
        }
        $areaKey = $state.'_'.strtolower($type);
        $allEmails = $this->getAllCareerEmails();
        if (!isset($allEmails[$areaKey])) {
            return '';
        }
        return $allEmails[$areaKey];
    }

    /**
     * Get career email
     *
     * @param $state
     * @param string $type
     * @return mixed
     */
    public function getAllCareerEmails() {
        if (!$this->_allEmails) {
            $this->_allEmails = $this->scopeConfig->getValue(self::CAREER_ADMIN_EMAIL_PREFIX,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }
        return $this->_allEmails;
    }
}
