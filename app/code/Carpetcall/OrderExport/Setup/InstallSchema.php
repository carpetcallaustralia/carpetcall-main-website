<?php

namespace Carpetcall\OrderExport\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();


        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'custom_order_category',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'default' => NULL,
                'length' => 255,
                'comment' => 'Custom Order Category'
            ] 
        );

    $setup->endSetup();    
    }    
}


