<?php

namespace Carpetcall\OrderExport\Cron;

class GugsCategoryData
{

	protected $filesystem;
	protected $directoryList;
	protected $csvProcessor;

    protected $_orderCollectionFactory;
    protected $_orderCollection;

	public function __construct(
	    \Magento\Framework\File\Csv $csvProcessor,
	    \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
	    \Magento\Framework\Filesystem $filesystem,
	    \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
	    \Magento\Sales\Model\Order $orderCollection
	    )
	{
	    $this->filesystem = $filesystem;  
	    $this->directoryList = $directoryList;
	    $this->csvProcessor = $csvProcessor;
	    $this->_orderCollectionFactory = $orderCollectionFactory;
	    $this->_orderCollection = $orderCollection;
	}

	public function execute()
	{

        $this->writeToCSV();
		$this->writeToProductCsv();

		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Order_Export_Cron_Log.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('Cron run Successfully');  
	}

	private function writeToCsv()
	{
		$orderData = $this->getOrderCollection();
				    
	    //$fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR).'/carpetcall-log/export';
        $fileDirectoryPath = 'carpetcall-log/export';

	    if(!is_dir($fileDirectoryPath))
	        mkdir($fileDirectoryPath, 0777, true);


        $fileName = 'ORRUGS'.date("Ymdis").'.csv';
	    $filePath =  $fileDirectoryPath . '/' . $fileName;


        $data[] = array('Increment Id','order id','business name','first name','last name','email','phone','delivery address 1','delivery address 2','delivery zip','delivery state','delivery city','delivery country','billing business name','billing first name','billing last name','billing email','billing phone','billing address 1','billing address 2','billing zip','billing state','billing city','billing country','delivery option','store id','store_state','store address','grand total','code','shipping cost','payment method','status','order status','cc name','cc type','cc date','cc num','cc ccv','time stamp','payment number','bank reference','summary code','response code','response text','payment time','payment reference','invoice','payer id','payment date','payment status','payer status','txn id','payment type','receiver id','receipt id','status code','status description','response description','atl','comment');


		foreach($orderData as $orders) {

			$order_collection = $this->_orderCollection->load($orders->getEntityId());
            $shipping_address = $order_collection->getShippingAddress();
            $billing_address = $order_collection->getBillingAddress();            
            $payment_data = $order_collection->getPayment();
            
            $ccExpMonth = $payment_data->getAdditionalInformation('cc_exp_month');
            $ccExpDate = $payment_data->getAdditionalInformation('cc_exp_year');
            $ccMonthDate = $ccExpMonth.'/'.$ccExpDate;

            $ccNumber = $payment_data->getAdditionalInformation('cc_number');
            $ccLastFourNumber = substr($ccNumber, -4);            

			
	    $data[] = array(
                'Increment Id' => $orders->getIncrementId(),
                'order id' => $orders->getEntityId(),
                'business name' => '',
                'first name' => $orders->getCustomerFirstname(),
                'last name' => $orders->getCustomerLastname(),
                'email' => $orders->getCustomerEmail(),
                'phone' => $shipping_address->getData('telephone'),
                'delivery address 1' => $shipping_address->getData('street'),
                'delivery address 2' => '',
                'delivery zip' => $shipping_address->getData('postcode'),
                'delivery state' => $shipping_address->getData('region'),
                'delivery city' => $shipping_address->getData('city'),
                'delivery country' => 'Australia',
                'billing business name' => '',
                'billing first name' => $billing_address->getFirstname(),
                'billing last name' => $billing_address->getLasttname(),
                'billing email' => $billing_address->getEmail(),
                'billing phone' => $billing_address->getData('telephone'),
                'billing address 1' => $billing_address->getData('street'),
                'billing address 2' => '',
                'billing zip' => $billing_address->getData('postcode'),
                'billing state' => $billing_address->getData('region'),
                'billing city' => $billing_address->getData('city'),
                'billing country' => 'Australia',
                'delivery option' => $orders->getData('shipping_method'),
                'store id' => $orders->getData('store_id'),
                'store_state' => $shipping_address->getData('region'),
                'store address' => $shipping_address->getData('street'),
                'grand total' => $orders->getData('grand_total'),
                'code' => '',
                'shipping cost' => '',
                'payment method' => $payment_data->getMethodInstance()->getTitle(),
                'status' => '',
                'order status' => $orders->getData('status'),
                'cc name' => '',
                'cc type' => $payment_data->getAdditionalInformation('cc_type'),
                'cc date' => $ccMonthDate,
                'cc num' => $ccLastFourNumber,
                'cc ccv' => $payment_data->getAdditionalInformation('cc_cid'),
                'time stamp' => $orders->getData('created_at'),
                'payment number' => '',
                'bank reference' => '',
                'summary code' => '',
                'response code' => '',
                'response text' => '',
                'payment time' => '',
                'payment reference' => '',
                'invoice' => '',
                'payer id' => '',
                'payment date' => '',
                'payment status' => '',
                'payer status' => '',
                'txn id' => $payment_data->getAdditionalInformation('txn_id'),
                'payment type' => '',
                'receiver id' => '',
                'receipt id' => '',
                'status code' => '',
                'status description' => '',
                'response description' => '',
                'atl' => '',
                'comment' => ''
                );

		}

	    $this->csvProcessor
	        ->setEnclosure('"')
	        ->setDelimiter(',')
	        ->saveData($filePath, $data);

	    return true;
	}

    private function writeToProductCsv()
    {
        $orderData = $this->getOrderCollection();
                    
        //$fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR).'/carpetcall-log/export';
        $fileDirectoryPath = 'carpetcall-log/export';

        if(!is_dir($fileDirectoryPath))
            mkdir($fileDirectoryPath, 0777, true);


        $fileName = 'OLRUGS'.date("Ymdis").'.csv';
        $filePath =  $fileDirectoryPath . '/' . $fileName;


        $data[] = array('Increment Id','order id','product code','product name','price','quantity');


        foreach($orderData as $orders) {

            $order_collection = $this->_orderCollection->load($orders->getEntityId());

            //$order_collection->getAllItems();
            foreach ($order_collection->getAllVisibleItems() as $_items) {

                $data[] = array(
                        'Increment Id' => $orders->getIncrementId(),
                        'order id' => $orders->getEntityId(),
                        'product code' => $_items->getSku(),
                        'product name' => $_items->getName(),
                        'price' => $_items->getPrice(),
                        'quantity' => $_items->getQtyOrdered()
                        );

                }
            }
        

        $this->csvProcessor
            ->setEnclosure('"')
            ->setDelimiter(',')
            ->saveData($filePath, $data);

        return true;
    }

	public function getOrderCollection()
	{
		$from = date('Y-m-d H:i:s', strtotime('-1 hour')); // past 1 hour;
		$to = date('Y-m-d H:i:s');

		$orderCollection = $this->_orderCollectionFactory->create()->addFieldToSelect(array('*'));
		$orderCollection->addFieldToFilter('order_cat', ['eq' => 'RUGS'])->addFieldToFilter('created_at', ['gteq' => $from])
        		->addFieldToFilter('created_at', ['lteq' => $to]);

    	return $orderCollection;
	}
}