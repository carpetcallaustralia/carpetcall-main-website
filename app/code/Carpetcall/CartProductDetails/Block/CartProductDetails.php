<?php
namespace Carpetcall\CartProductDetails\Block;
use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Model\Product;

class CartProductDetails extends \Magento\Framework\View\Element\Template
{
	protected $objectManager;
    public function __construct( 
        ObjectManagerInterface $objectManager,
         \Magento\Catalog\Model\ProductFactory $_productloader
    ) {
                $this->objectManager = $objectManager;
                $this->product = $_productloader;
    }
    
    public function getProductData($item_id){

        $product_data = $this->objectManager->get('Magento\Quote\Model\Quote\Item')->load($item_id);

        return $product_data;
    }

    public function getProductSku($item_id){

        $product_sku = $this->product->create()->load($item_id);

        return $product_sku;
    }


}