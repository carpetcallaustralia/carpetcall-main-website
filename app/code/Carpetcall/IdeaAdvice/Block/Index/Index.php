<?php
namespace Carpetcall\IdeaAdvice\Block\Index;

use Mageplaza\Blog\Block\Adminhtml\Category\Tree;
use Magento\Framework\App\ObjectManager;
use Mageplaza\Blog\Block\Frontend;
use Magento\Store\Model\StoreManagerInterface;

class Index extends \Magento\Framework\View\Element\Template 
{
	protected $storeManager;
	protected $helperData;
    public function __construct(
    	\Magento\Catalog\Block\Product\Context $context,
    	StoreManagerInterface $storeManager,
    	array $data = []) 
    {
    	$this->store = $storeManager;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getbuyingguidecategories()
    {
    	$tree = ObjectManager::getInstance()->create(Tree::class);
        $tree = $tree->getTree(null, $this->store->getStore()->getId());
        
        return $tree;
    }

    public function getBaseUrl()
    {
        $store = $this->store->getStore();
        $baseUrl = $store->getBaseUrl();
        return $baseUrl;
    }

}	