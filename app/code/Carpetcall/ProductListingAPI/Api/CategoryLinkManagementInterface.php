<?php
namespace Carpetcall\ProductListingAPI\Api;

interface CategoryLinkManagementInterface
{

  /**
     * Get products assigned to a category
     *
     * @param int $categoryId
     * @return \Carpetcall\ProductListingAPI\Api\Data\CategoryProductLinkInterface[]
     */
    public function getAssignedProducts($categoryId);

}