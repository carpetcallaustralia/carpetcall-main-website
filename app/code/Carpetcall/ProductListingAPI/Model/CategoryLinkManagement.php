<?php
namespace Carpetcall\ProductListingAPI\Model;

/**
 * Class CategoryLinkManagement
 */
class CategoryLinkManagement implements \Carpetcall\ProductListingAPI\Api\CategoryLinkManagementInterface
{
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var \Carpetcall\ProductListingAPI\Api\Data\CategoryProductLinkInterfaceFactory
     */
    protected $productLinkFactory;
    protected $categoryFactory;
    protected $_productRepositoryFactory;
    protected $request;

    /**
     * CategoryLinkManagement constructor.
     *
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Carpetcall\ProductListingAPI\Api\Data\CategoryProductLinkInterfaceFactory $productLinkFactory
     */
    public function __construct(
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Carpetcall\ProductListingAPI\Api\Data\CategoryProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configProduct,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->productLinkFactory = $productLinkFactory;
        $this->categoryFactory = $categoryFactory;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->configProduct = $configProduct;
        $this->request = $request;
    }


    /**
     * {@inheritdoc}
     */
    public function getAssignedProducts($categoryId)
    {
        $allParameters=$this->request->getParams();
        if (array_key_exists("carpet_colours",$allParameters))
        {
            $carpet_colours = explode(',',$allParameters['carpet_colours']);
        }
        if (array_key_exists("carpet_features",$allParameters))
        {
            $carpet_features = explode(',',$allParameters['carpet_features']);
        }
        // echo "<pre>";
        // print_r($carpet_features);
        // die();
        $category = $this->categoryRepository->get($categoryId);
        if (!$category->getIsActive()) {
            return [[
                'error' => true,
                'error_desc' => 'Category is disabled'
            ]];
        }

        $product_data_arr = array();

        $category = $this->categoryFactory->create()->load($categoryId);
         
        $categoryProducts = $category->getProductCollection()
            ->addAttributeToSelect('*');

        // Get final product IDs based on filter option
        $final_prd_ids = array();
        if (!empty($carpet_colours)) {
            foreach ($categoryProducts as $product) {
                $productAttributeOptions = $this->configProduct->getConfigurableAttributesAsArray($product);
                foreach ($productAttributeOptions as $key) {
                    foreach ($key['values'] as $key) {
                        if (in_array($key['value_index'], $carpet_colours)) {
                            $final_prd_ids[] = $product->getEntityId();
                        }
                    }
                }
            }

        }elseif (!empty($carpet_features)) {
            
        }{

        }
        // Get final product IDs based on filter option
        
         // echo "<pre>";
         // print_r($final_prd_ids);die();
        foreach ($categoryProducts as $product) {
            //echo "string";die();
            if (in_array($product->getEntityId(), $final_prd_ids)) {
            // Product collection when filter is applied starts

            $colours = array();
            $colour_img = array();
            
            $productAttributeOptions = $this->configProduct->getConfigurableAttributesAsArray($product);
            
            $_children = $product->getTypeInstance()->getUsedProducts($product);

            foreach ($_children as $child){
                //echo $child->getID();
                $childproduct = $this->_productRepositoryFactory->create()
                            ->getById($child->getID());

                $objectManager =\Magento\Framework\App\ObjectManager::getInstance();
                $helperImport = $objectManager->get('\Magento\Catalog\Helper\Image');

                $imageUrl = $helperImport->init($childproduct, 'product_page_image_small')
                                ->setImageFile($childproduct->getSmallImage()) // image,small_image,thumbnail
                                ->resize(380)
                                ->getUrl();
                        
                $colour_img[] = ['colour_img' => $imageUrl,
                                 'carpet_colours_value_index' => $child->getCarpetColours()];
                
                //echo $childproduct->getData('image');
                //echo $childproduct->getData('small_image');
                //echo "<pre>";
                //print_r();
                //die();
            }

             // echo "<pre>";
             //    print_r($productAttributeOptions);
             //    die();
            foreach ($productAttributeOptions as $key) {
                foreach ($key['values'] as $key) {
                    $colours[] = ['value_index' => $key['value_index'],
                                  'label' => $key['label']];
                }
            }

            $product_data_arr[] = ['id'=> $product->getId(),'name' => $product->getName(),
                                    'url_key' => $product->getUrlKey(),
                                    'image' => $product->getImage(),
                                    'carpet_colours' => $colours,
                                    'colour_img' => $colour_img
                                  ];
            // Product collection when filter is applied ends
        }elseif (!empty($final_prd_ids)) {
            //echo "string11";die;
            // Empty return for products not in array
        }else{
            // Product collection when filter is not applied start
            $colours = array();
            $colour_img = array();
            
            $productAttributeOptions = $this->configProduct->getConfigurableAttributesAsArray($product);
            
            $_children = $product->getTypeInstance()->getUsedProducts($product);

            foreach ($_children as $child){
                //echo $child->getID();
                $childproduct = $this->_productRepositoryFactory->create()
                            ->getById($child->getID());

                $objectManager =\Magento\Framework\App\ObjectManager::getInstance();
                $helperImport = $objectManager->get('\Magento\Catalog\Helper\Image');

                $imageUrl = $helperImport->init($childproduct, 'product_page_image_small')
                                ->setImageFile($childproduct->getSmallImage()) // image,small_image,thumbnail
                                ->resize(380)
                                ->getUrl();
                        
                $colour_img[] = ['colour_img' => $imageUrl,
                                 'carpet_colours_value_index' => $child->getCarpetColours()];
                
                //echo $childproduct->getData('image');
                //echo $childproduct->getData('small_image');
                //echo "<pre>";
                //print_r();
                //die();
            }

             // echo "<pre>";
             //    print_r($productAttributeOptions);
             //    die();
            foreach ($productAttributeOptions as $key) {
                foreach ($key['values'] as $key) {
                    $colours[] = ['value_index' => $key['value_index'],
                                  'label' => $key['label']];
                }
            }

            $product_data_arr[] = ['id'=> $product->getId(),'name' => $product->getName(),
                                    'url_key' => $product->getUrlKey(),
                                    'image' => $product->getImage(),
                                    'carpet_colours' => $colours,
                                    'colour_img' => $colour_img
                                  ];
            // Product collection when filter is not applied ends
        }
    }

        return $product_data_arr;
    }
}