<?php
namespace Carpetcall\OrderSucess\Block;

class Success  extends \Magento\Framework\View\Element\Template
{
	protected $_scopeConfig;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $orderFactory;
    protected $_storeManager;
    protected $_product;

 
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Sales\Model\OrderRepository $orderRepository,
    \Magento\Sales\Model\OrderFactory $orderFactory,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Catalog\Model\ProductFactory $productFactory,
    array $data = []
    ) {
    parent::__construct($context, $data);
    $this->_scopeConfig = $scopeConfig;
    $this->_checkoutSession = $checkoutSession;
    $this->orderFactory = $orderFactory;
    $this->_storeManager = $storeManager;
    $this->productFactory = $productFactory;
    }

    public function getRealOrderId(){
         $lastorderId = $this->_checkoutSession->getLastOrderId();
        return $lastorderId;
    }

    public function getOrder(){
        if ($this->_checkoutSession->getLastRealOrderId()) {
             $order = $this->orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
             return $order;
        }
    }

    public function getCurrentCurrencyCode()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }    
    
    public function getPriceById($id)
    {
        $product = $this->productFactory->create();
        $productPriceById = $product->load($id)->getFinalPrice();
        return $productPriceById;
    }
}