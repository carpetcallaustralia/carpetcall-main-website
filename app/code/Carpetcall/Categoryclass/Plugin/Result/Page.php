<?php
namespace Carpetcall\Categoryclass\Plugin\Result;

use Magento\Framework\App\ResponseInterface;

class Page
{
  private $context;
  private $registry;

  public function __construct(
  \Magento\Framework\View\Element\Context $context,
  \Magento\Framework\Registry $registry,
  \Magento\Framework\App\Cache\Manager $cacheManager
  ) {
     $this->context = $context;
     $this->registry = $registry;
     $this->cacheManager = $cacheManager;
  }

  public function beforeRenderResult(
  \Magento\Framework\View\Result\Page $subject,
  ResponseInterface $response
  ){

  //   $this->cacheManager->flush($this->cacheManager->getAvailableTypes());
  //  $this->cacheManager->clean($this->cacheManager->getAvailableTypes());

  // $category = $this->registry->registry('current_category');

  // if($this->context->getRequest()->getFullActionName() == 'catalog_product_view'){
  //    $subject->getConfig()->addBodyClass('current-category-'.$category->getName());
  // }

  // return [$response];
  }
}