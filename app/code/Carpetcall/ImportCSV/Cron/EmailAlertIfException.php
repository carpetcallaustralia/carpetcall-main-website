<?php
namespace Carpetcall\ImportCSV\Cron;
 
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Carpetcall\ImportCSV\Helper\Data;

class EmailAlertIfException
{   
    protected $_resources;
    protected $_logger;
    protected $_helper;
    protected $_storeManager;
    protected $_scopeConfig;
    protected $_objectManager;
    protected $_transportBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resources,
        \Carpetcall\ImportCSV\Logger\Logger $logger,
        \Carpetcall\ImportCSV\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) 
    {
        $this->_resources = $resources;
        $this->_logger = $logger;
        $this->_helper = $helper;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_objectManager = $objectManager;
        $this->_transportBuilder = $transportBuilder;
    }

    public function execute()
    {
        $connection = $this->_resources->getConnection();
        $cronTable = $this->_resources->getTableName('cron_schedule');

        $today = date('Y-m-d',time());
        //echo $today = date('Y-m-d',strtotime("yesterday"));

        $templateVars = array();
        $templateVars['name'] = 'Carpetcall Team';

        //daily_LOLS_product_import
        $cronLOLSQuery = "SELECT * FROM " . $cronTable . " WHERE job_code = 'daily_LOLS_product_import' AND status = 'error' AND executed_at >= "."'".$today."' LIMIT 1";

        $cronQueryLOLSCollection = $connection->fetchAll($cronLOLSQuery);

        if(count($cronQueryLOLSCollection) > 0){
            foreach ($cronQueryLOLSCollection as $keyLols => $valueLols) {
                $templateVars['lols_name'] = 'Carpetcall Team';
                $templateVars['lols_job_code'] = $valueLols['job_code'];
                $templateVars['lols_status'] = $valueLols['status'];
                $templateVars['lols_messages'] = $valueLols['messages'];
                $templateVars['lols_executed_at'] = $valueLols['executed_at'];
            }
        }

        //daily_GOLS_product_import_custom_table
        $cronGOLSQuery = "SELECT * FROM " . $cronTable . " WHERE job_code = 'daily_GOLS_product_import_custom_table' AND status = 'error' AND executed_at >= "."'".$today."' LIMIT 1";

        $cronQueryGOLSTableCollection = $connection->fetchAll($cronGOLSQuery);

        if(count($cronQueryGOLSTableCollection) > 0){
            foreach ($cronQueryGOLSTableCollection as $keyGolsTable => $valueGolsTable) {
                $templateVars['gols_table_name'] = 'Carpetcall Team';
                $templateVars['gols_table_job_code'] = $valueGolsTable['job_code'];
                $templateVars['gols_table_status'] = $valueGolsTable['status'];
                $templateVars['gols_table_messages'] = $valueGolsTable['messages'];
                $templateVars['gols_table_executed_at'] = $valueGolsTable['executed_at'];
            }
        }

        //daily_GOLS_associated_product_import
        $cronGOLSQuery = "SELECT * FROM " . $cronTable . " WHERE job_code = 'daily_GOLS_associated_product_import' AND status = 'error' AND executed_at >= "."'".$today."' LIMIT 1";

        $cronQueryGOLSCollection = $connection->fetchAll($cronGOLSQuery);

        if(count($cronQueryGOLSCollection) > 0){
            foreach ($cronQueryGOLSCollection as $keyGols => $valueGols) {
                $templateVars['gols_name'] = 'Carpetcall Team';
                $templateVars['gols_job_code'] = $valueGols['job_code'];
                $templateVars['gols_status'] = $valueGols['status'];
                $templateVars['gols_messages'] = $valueGols['messages'];
                $templateVars['gols_executed_at'] = $valueGols['executed_at'];
            }
        }

        if(count($templateVars) > 1){
            $this->sendEmailToAdmin($templateVars);
        }
    }

    public function sendEmailToAdmin($templateVars){
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $admin_emailTo = $this->_scopeConfig->getValue('cron_execption/import_cav_cron_execption/exception_admin_mail_to', $storeScope);
        if(!isset($admin_emailTo)){
            $admin_emailTo = 'dev-notify@conversiondigital.com.au';
        }
        $admin_emailToName = 'Carpetcall';

        $admin_emailCc = $this->_scopeConfig->getValue('cron_execption/import_cav_cron_execption/exception_admin_mail_cc', $storeScope);
        $admin_emailBcc = $this->_scopeConfig->getValue('cron_execption/import_cav_cron_execption/exception_admin_mail_bcc', $storeScope);

        $final_cc_emails = array();
        $cc_emails = explode(',', $admin_emailCc);    
        foreach ($cc_emails as $cc_email) {
            $final_cc_emails[] = trim($cc_email);
        }

        $final_bcc_emails = array();
        $bcc_emails = explode(',', $admin_emailBcc);    
        foreach ($bcc_emails as $bcc_email) {
            $final_bcc_emails[] = trim($bcc_email);
        }

        $email_Template = 'product_import_cron_template';  
        $formemail = $this->_storeManager->getStore()->getConfig('trans_email/ident_general/email');
        $formname = $this->_storeManager->getStore()->getConfig('trans_email/ident_general/name');

        $templateOptions = [
            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
            'store' => $this->_storeManager->getStore()->getId()
        ];

        $this->_transportBuilder
            ->setTemplateIdentifier($email_Template)    
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom(['name' => $formname,'email' => $formemail])
            ->setReplyTo($admin_emailTo, $admin_emailToName)
            ->addTo($admin_emailTo);
            if (!empty($final_cc_emails)) {
                $this->_transportBuilder->addCc($final_cc_emails);
            }
            if (!empty($final_bcc_emails)) {
                $this->_transportBuilder->addBcc($final_bcc_emails);
            }
        $transport = $this->_transportBuilder->getTransport();
        try{
            $transport->sendMessage();
            $this->_logger->addInfo("Emil Sent sucessfully.....");
        }catch(Exception $e){
            $this->_logger->addInfo("Error:". $e);
        }
    }
}
