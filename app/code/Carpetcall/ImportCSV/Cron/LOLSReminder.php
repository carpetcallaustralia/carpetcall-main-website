<?php
namespace Carpetcall\ImportCSV\Cron;
 
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Carpetcall\ImportCSV\Helper\Data;

class LOLSReminder
{
    protected $_storeManager;
    protected $_customerRepository;
    protected $_customerFactory;
    protected $_customer;
    protected $_scopeConfig;
    protected $resource;
    protected $_dir;
    protected $_fileCsv;
    protected $_product;
    protected $_productRepository;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_logger;
    protected $_stockRegistry;
    protected $_helper;
    private $_objectManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Customer $customers,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\File\Csv $fileCsv,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Carpetcall\ImportCSV\Logger\Logger $logger,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Carpetcall\ImportCSV\Helper\Data $helper,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) 
    {
        $this->_storeManager = $storeManager;
        $this->_customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
        $this->_scopeConfig = $scopeConfig;
        $this->resource = $resource;
        $this->_dir = $dir;
        $this->_moduleReader = $moduleReader;
        $this->_fileCsv = $fileCsv;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_logger = $logger;
        $this->_stockRegistry = $stockRegistry;
        $this->_helper = $helper;
        $this->_objectManager = $objectManager;
    }

    public function execute()
    {
        if(!empty($this->_helper->getBothCategoryFilesArray()['LOLS'])){

            $LOLSfileForReadData = $this->_dir->getRoot(). '/importcsv/'.$this->_helper->getBothCategoryFilesArray()['LOLS'][0];
            $QtyColumnDataArray = $this->_helper->getFinalQtyColumnData();

            if (file_exists($LOLSfileForReadData)) {
                $fileData = $this->_fileCsv->getData($LOLSfileForReadData);

                $this->_logger->addInfo($this->_helper->getBothCategoryFilesArray()['LOLS'][0].'...Started....');

                foreach ($fileData as $key => $value) {      
                    if($key != '0'){
                        $check_product = $this->_objectManager->get('\Magento\Catalog\Model\Product');

                        if($check_product->getIdBySku($value[1])) {
                            $oldProduct = $this->_productRepository->get($value[1]);

                            foreach ($QtyColumnDataArray as $sku => $qty) {
                                if($sku == $value[1]){
                                    $isInStock = 1;
                                    if($qty > 0){
                                        $isInStock = 1;
                                    }else{
                                        $isInStock = 0;
                                    }

                                    $oldProduct->setStockData(
                                        array(
                                            'use_config_manage_stock' => 0,
                                            'manage_stock' => 1,
                                            'is_in_stock' => $isInStock,
                                            'qty' => $qty
                                        )
                                    );
                                }
                            }
                            
                            $categoryTitle = $value[0];
                            if($categoryTitle != null){
                                $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                                $categoryIdsArray = array();
                                if ($collection->getSize() > 0) {
                                    $collection1 = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name','Clearance')->setPageSize(1);
                                    $categoryIdsArray['extra_child_category'] = $collection1->getFirstItem()->getId();
                                    $categoryIdsArray['child_category'] = $collection->getFirstItem()->getId();
                                    if($collection->getFirstItem()->getParentId()){
                                        $categoryIdsArray['parent_category'] = $collection->getFirstItem()->getParentId();

                                        //color group category assign
                                        $parent_category_id = $collection->getFirstItem()->getParentId();
                                        if(trim($value[50]) != ""){
                                            $categoryObj = $this->_objectManager->get('\Magento\Catalog\Model\CategoryRepository')->get($parent_category_id);
                                            $subcategories = $categoryObj->getChildrenCategories();
                                            foreach($subcategories as $subcategorie) {
                                                if($subcategorie->getName() == trim($value[50])){
                                                    $categoryIdsArray[] = $subcategorie->getId();
                                                }
                                            }
                                        }

                                        $baseURL = $this->_storeManager->getStore()->getBaseUrl();
                                        $category = $this->_categoryRepository->get($categoryIdsArray['parent_category'], $this->_storeManager->getStore()->getId());

                                        $categoryName = str_replace(" ", "-", strtolower($category->getName()));
                                        $productName = str_replace(" ", "-", strtolower($value[5]));

                                        $URLkey = $categoryName."-".$value[1]."-".$productName;
                                    }
                                    
                                    $oldProduct->setStoreId(1);
                                    $oldProduct->setWebsiteIds(array(1));
                                    $oldProduct->setCategoryIds($categoryIdsArray);
                                    $oldProduct->setPrice($value[44]);
                                    $oldProduct->setAttributeSetId(4);
                                    $oldProduct->setName($value[5]);
                                    $oldProduct->setBrands($value[3]);
                                    $oldProduct->setBoardType($value[4]);
                                    $oldProduct->setUrlKey($URLkey);
                                    $oldProduct->setSupplier($value[6]);
                                    $oldProduct->setParagraph1($value[7]);
                                    $oldProduct->setParagraph2($value[8]);
                                    $oldProduct->setParagraph3($value[9]);
                                    $oldProduct->setParagraph4($value[10]);

                                    $CountryOfOriginManufacture = $this->_helper->getCountryCode(trim($value[11]));
                                    
                                    if(isset($CountryOfOriginManufacture)){
                                        $oldProduct->setCountryOfManufacture($CountryOfOriginManufacture);
                                    }
                                    
                                    $oldProduct->setProductLength($value[12]);
                                    $oldProduct->setProductWidth($value[13]);
                                    if($value[14] == 'n/a' || $value[14] == 'na' || $value[14] == 'NA'){
                                        $oldProduct->setProductThickness("");
                                    }else{
                                        $oldProduct->setProductThickness($value[14]);
                                    }
                                    $oldProduct->setProductThicknessTotal($value[15]);
                                    $oldProduct->setPackLength($value[16]);
                                    $oldProduct->setPackWidth($value[17]);
                                    $oldProduct->setPackThickness($value[18]);
                                    $oldProduct->setPackWeight($value[19]);
                                    $oldProduct->setBoardsPerPack($value[20]);
                                    $oldProduct->setInstallationOptions($value[21]);
                                    if($value[22] == 'n/a' || $value[22] == 'na' || $value[22] == 'NA'){
                                        $oldProduct->setUnderlayOptions("");
                                    }else{
                                        $oldProduct->setUnderlayOptions($value[22]);
                                    }
                                    if($value[23] == 'n/a' || $value[23] == 'na' || $value[23] == 'NA'){
                                        $oldProduct->setGlueOptions("");
                                    }else{
                                        $oldProduct->setGlueOptions($value[23]);
                                    }
                                    if($value[24] == 'n/a' || $value[24] == 'na' || $value[24] == 'NA'){
                                        $oldProduct->setScotiaOptions("");
                                    }else{
                                        $oldProduct->setScotiaOptions($value[24]);
                                    }
                                    if($value[25] == 'n/a' || $value[25] == 'na' || $value[25] == 'NA'){
                                        $oldProduct->setAccessories("");
                                    }else{
                                        $oldProduct->setAccessories($value[25]);
                                    }
                                    $oldProduct->setCoveragePerPack($value[26]);
                                    $oldProduct->setEdgeType($value[27]);
                                    if($value[28] == 'n/a' || $value[28] == 'na' || $value[28] == 'NA'){
                                        $oldProduct->setJointSystem("");
                                    }else{
                                        $oldProduct->setJointSystem($value[28]);
                                    }
                                    $oldProduct->setSurfaceFinish($value[29]);
                                    if($value[30] == 'n/a' || $value[30] == 'na' || $value[30] == 'NA'){
                                        $oldProduct->setJankaRating("");
                                    }else{
                                        $oldProduct->setJankaRating($value[30]);
                                    }
                                    $oldProduct->setStructuralWarranty($value[31]);
                                    $oldProduct->setWearLayerWarranty($value[32]);
                                    $oldProduct->setConstructionStyle($value[33]);
                                    $oldProduct->setRecommendedUse($value[34]);
                                    $oldProduct->setRecommendedAreasOfUse($value[35]);
                                    $oldProduct->setCareInstructions($value[36]);
                                    $oldProduct->setStandardsCoating($value[37]);
                                    $oldProduct->setStandardsAcRating($value[38]);
                                    $oldProduct->setStandardsCoreType($value[39]);
                                    $oldProduct->setStandardsAntiSlipTest($value[40]);
                                    $oldProduct->setIsoCertification($value[41]);
                                    $oldProduct->setStandardsBase($value[42]);
                                    if($value[43] == 'n/a' || $value[43] == 'na' || $value[43] == 'NA'){
                                        $oldProduct->setTrimOptions("");
                                    }else{
                                        $oldProduct->setTrimOptions($value[43]);
                                    }
                                    if($value[45] == ""){
                                        $oldProduct->setOnlineDiscount($value[45]);
                                        $oldProduct->setSpecialPrice("");
                                    }else{
                                        $oldProduct->setOnlineDiscount($value[45]);
                                        $disPer = (100 - (int)str_replace("%", "", $value[45]));
                                        $disAmt = (trim($value[44]) * $disPer / 100);
                                        $oldProduct->setSpecialPrice($disAmt);
                                    }

                                    if($value[44] != "" && $value[26] != ""){
                                        $pricePerPack = intval($value[44] * $value[26]);
                                        $oldProduct->setPricePerPack($pricePerPack);
                                    }
                                    $storeId = 1;

                                    if($value[51] != ""){
                                        $roomsValue = explode(",",$value[51]);
                                        $roomAttIds = array();
                                        foreach ($roomsValue as $room) {
                                            $roomAttIds[] = $this->_helper->createOrGetId('hf_room',trim($room));
                                        }
                                        
                                        $oldProduct->addAttributeUpdate('hf_room', implode(",", $roomAttIds), $storeId);
                                    }
                                    

                                    if($value[50] != ""){
                                        $colorGroupAttId = $this->_helper->createOrGetId('hf_color_group',trim($value[50]));
                                        $oldProduct->addAttributeUpdate('hf_color_group',$colorGroupAttId,$storeId);
                                    }

                                    if(trim($value[0]) == "WALL COVERINGS"){
                                        $hfType = 'Wall Coverings';
                                    }else{
                                        $hfTypeString = explode(" ",trim($value[0]));
                                        $hfType = $hfTypeString[0];
                                    }

                                    if($hfType != ""){
                                        $hfTypeAttId = $this->_helper->createOrGetId('hf_type',trim($hfType));
                                        $oldProduct->addAttributeUpdate('hf_type',$hfTypeAttId,$storeId);
                                    }

                                    $oldProduct->addAttributeUpdate('pickup_only',1,$storeId);
                                    
                                    try{
                                        $oldProduct->save();
                                        $this->_logger->addInfo('Update product save with ID: '. $oldProduct->getSku());   
                                    } catch(Exception $e){
                                        $this->_logger->addInfo($oldProduct->getSku().' Product can not create. Error '.$e->getMessage());
                                    }

                                    /*if($value[46] != null  || $value[47] != null || $value[48] != null){

                                        //Remove images
                                        $productRepository = $this->_objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
                                        $existingMediaGalleryEntries = $oldProduct->getMediaGalleryEntries();
                                        foreach ($existingMediaGalleryEntries as $key => $entry) {
                                            unset($existingMediaGalleryEntries[$key]);
                                        }
                                        $oldProduct->setMediaGalleryEntries($existingMediaGalleryEntries);
                                        $productRepository->save($oldProduct);
                                        $this->_logger->addInfo('Remove image sucessfully for : '.$oldProduct->getSku());

                                        //Add media
                                        $importDir = $this->_dir->getRoot().'/pub/media/import/*';

                                        foreach(glob($importDir) as $img) { 

                                            $imgStringArray = explode('/', $img);
                                            $imageName = end($imgStringArray);

                                            
                                            if(strpos($imageName, $value[48]) !== false){
                                                if(file_exists($img)){
                                                    $oldProduct->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                                }
                                            }
                                            if(strpos($imageName, $value[47]) !== false){
                                                if(file_exists($img)){
                                                    $oldProduct->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                                }
                                            }
                                            if(strpos($imageName, $value[46]) !== false){
                                                if(file_exists($img)){
                                                    $oldProduct->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                                }
                                            }
                                            
                                        }
                                        try{
                                            $oldProduct->save();
                                            $this->_logger->addInfo('Add image sucessfully for : '.$oldProduct->getSku());
                                        }catch(Exception $e){
                                            $this->_logger->addInfo('Image not added: '.$oldProduct->getSku());
                                        }
                                    }else{
                                        $this->_logger->addInfo('Image name not there in CSV  : '.$oldProduct->getSku());
                                    }*/
                                }else{
                                    $this->_logger->addInfo($value[0] .'Category does not exist for SKU:'.$value[1]);
                                    continue;
                                }  
                            }else{
                                $this->_logger->addInfo('Category name does not exist...');
                                    continue;
                            }
                        }else{
                            $newProduct = $this->_objectManager->create('\Magento\Catalog\Model\Product');
                            $newProduct->setSku($value[1]);
                            foreach ($QtyColumnDataArray as $sku => $qty) {
                                if($sku == $value[1]){
                                    $isInStock = 1;
                                    if($qty > 0){
                                        $isInStock = 1;
                                    }else{
                                        $isInStock = 0;
                                    }

                                    $newProduct->setStockData(
                                        array(
                                            'use_config_manage_stock' => 0,
                                            'manage_stock' => 1,
                                            'is_in_stock' => $isInStock,
                                            'qty' => $qty
                                        )
                                    );
                                }
                            }

                            $categoryTitle = $value[0];
                            if($categoryTitle != null){
                                $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                                $categoryIdsArray = array();

                                if($collection->getSize() > 0) {
                                    $collection1 = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name','Clearance')->setPageSize(1);
                                    $categoryIdsArray['extra_child_category'] = $collection1->getFirstItem()->getId();
                                    $categoryIdsArray['child_category'] = $collection->getFirstItem()->getId();
                                    if($collection->getFirstItem()->getParentId()){
                                        $categoryIdsArray['parent_category'] = $collection->getFirstItem()->getParentId();

                                        //color group category assign
                                        $parent_category_id = $collection->getFirstItem()->getParentId();

                                        $categoryObj = $this->_objectManager->get('\Magento\Catalog\Model\CategoryRepository')->get($parent_category_id);
                                        if(trim($value[50]) != ""){
                                            $subcategories = $categoryObj->getChildrenCategories();
                                            foreach($subcategories as $subcategorie) {
                                                if($subcategorie->getName() == trim($value[50])){
                                                    $categoryIdsArray[] = $subcategorie->getId();
                                                }
                                            }
                                        }

                                        $baseURL = $this->_storeManager->getStore()->getBaseUrl();
                                        $category = $this->_categoryRepository->get($categoryIdsArray['parent_category'], $this->_storeManager->getStore()->getId());

                                        $categoryName = str_replace(" ", "-", strtolower($category->getName()));
                                        $productName = str_replace(" ", "-", strtolower($value[5]));

                                        $URLkey = $categoryName."-".$value[1]."-".$productName;
                                    }

                                    $newProduct->setStoreId(1);
                                    $newProduct->setWebsiteIds(array(1));
                                    $newProduct->setCategoryIds($categoryIdsArray);
                                    $newProduct->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
                                    $newProduct->setData('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                                    $newProduct->setData('visibility', 4);
                                    $newProduct->setPrice($value[44]);
                                    $newProduct->setAttributeSetId(4);
                                    $newProduct->setName($value[5]);
                                    $newProduct->setBrands($value[3]);
                                    $newProduct->setBoardType($value[4]);
                                    $newProduct->setUrlKey($URLkey);
                                    $newProduct->setSupplier($value[6]);
                                    $newProduct->setParagraph1($value[7]);
                                    $newProduct->setParagraph2($value[8]);
                                    $newProduct->setParagraph3($value[9]);
                                    $newProduct->setParagraph4($value[10]);
                                    
                                    $CountryOfOriginManufacture = $this->_helper->getCountryCode(trim($value[11]));
                                    
                                    if(isset($CountryOfOriginManufacture)){
                                        $newProduct->setCountryOfManufacture($CountryOfOriginManufacture);
                                    }

                                    $newProduct->setProductLength($value[12]);
                                    $newProduct->setProductWidth($value[13]);
                                    if($value[14] == 'n/a' || $value[14] == 'na' || $value[14] == 'NA'){
                                        $newProduct->setProductThickness("");
                                    }else{
                                        $newProduct->setProductThickness($value[14]);
                                    }
                                    $newProduct->setProductThicknessTotal($value[15]);
                                    $newProduct->setPackLength($value[16]);
                                    $newProduct->setPackWidth($value[17]);
                                    $newProduct->setPackThickness($value[18]);
                                    $newProduct->setPackWeight($value[19]);
                                    $newProduct->setBoardsPerPack($value[20]);
                                    $newProduct->setInstallationOptions($value[21]);
                                    if($value[22] == 'n/a' || $value[22] == 'na' || $value[22] == 'NA'){
                                        $newProduct->setUnderlayOptions("");
                                    }else{
                                        $newProduct->setUnderlayOptions($value[22]);
                                    }
                                    if($value[23] == 'n/a' || $value[23] == 'na' || $value[23] == 'NA'){
                                        $newProduct->setGlueOptions("");
                                    }else{
                                        $newProduct->setGlueOptions($value[23]);
                                    }
                                    if($value[24] == 'n/a' || $value[24] == 'na' || $value[24] == 'NA'){
                                        $newProduct->setScotiaOptions("");
                                    }else{
                                        $newProduct->setScotiaOptions($value[24]);
                                    }
                                    if($value[25] == 'n/a' || $value[25] == 'na' || $value[25] == 'NA'){
                                        $newProduct->setAccessories("");
                                    }else{
                                        $newProduct->setAccessories($value[25]);
                                    }
                                    $newProduct->setCoveragePerPack($value[26]);
                                    $newProduct->setEdgeType($value[27]);
                                    if($value[28] == 'n/a' || $value[28] == 'na' || $value[28] == 'NA'){
                                        $newProduct->setJointSystem("");
                                    }else{
                                        $newProduct->setJointSystem($value[28]);
                                    }
                                    $newProduct->setSurfaceFinish($value[29]);
                                    if($value[30] == 'n/a' || $value[30] == 'na' || $value[30] == 'NA'){
                                        $newProduct->setJankaRating("");
                                    }else{
                                        $newProduct->setJankaRating($value[30]);
                                    }
                                    $newProduct->setStructuralWarranty($value[31]);
                                    $newProduct->setWearLayerWarranty($value[32]);
                                    $newProduct->setConstructionStyle($value[33]);
                                    $newProduct->setRecommendedUse($value[34]);
                                    $newProduct->setRecommendedAreasOfUse($value[35]);
                                    $newProduct->setCareInstructions($value[36]);
                                    $newProduct->setStandardsCoating($value[37]);
                                    $newProduct->setStandardsAcRating($value[38]);
                                    $newProduct->setStandardsCoreType($value[39]);
                                    $newProduct->setStandardsAntiSlipTest($value[40]);
                                    $newProduct->setIsoCertification($value[41]);
                                    $newProduct->setStandardsBase($value[42]);

                                    if($value[43] == 'n/a' || $value[43] == 'na' || $value[43] == 'NA'){
                                        $newProduct->setTrimOptions("");
                                    }else{
                                        $newProduct->setTrimOptions($value[43]);
                                    }
                                    if($value[45] == ""){
                                            $newProduct->setOnlineDiscount($value[45]);
                                            $newProduct->setSpecialPrice("");
                                    }else{
                                        $newProduct->setOnlineDiscount($value[45]);
                                        $disPer = (100 - (int)str_replace("%", "", $value[45]));
                                        $disAmt = (trim($value[44]) * $disPer / 100);
                                        $newProduct->setSpecialPrice($disAmt);
                                    }

                                    if($value[44] != "" && $value[26] != ""){
                                        $pricePerPack = intval($value[44] * $value[26]);
                                        $newProduct->setPricePerPack($pricePerPack);
                                    }

                                    if($value[51] != ""){
                                        $roomsValue = explode(",",$value[51]);
                                        $roomAttIds = array();
                                        foreach ($roomsValue as $room) {
                                            $roomAttIds[] = $this->_helper->createOrGetId('hf_room',trim($room));
                                        }
                                        $newProduct->setData('hf_room', implode(",", $roomAttIds));
                                    }

                                    if($value[50] != ""){
                                        $colorGroupAttId = $this->_helper->createOrGetId('hf_color_group',trim($value[50]));
                                        $newProduct->setData('hf_color_group',$colorGroupAttId);
                                    }

                                    if(trim($value[0]) == "WALL COVERINGS"){
                                        $hfType = 'Wall Coverings';
                                    }else{
                                        $hfTypeString = explode(" ",trim($value[0]));
                                        $hfType = $hfTypeString[0];
                                    }

                                    if($hfType != ""){
                                        $hfTypeAttId = $this->_helper->createOrGetId('hf_type',trim($hfType));
                                        $newProduct->setData('hf_type',$hfTypeAttId);
                                    }

                                    $newProduct->setData('pickup_only',1);

                                    try{
                                        $newProduct->save();
                                        $this->_logger->addInfo('New product save with ID: '. $newProduct->getSku());   
                                    } catch(Exception $e){
                                        $this->_logger->addInfo($newProduct->getSku().' Product can not create. Error '.$e->getMessage());
                                    } 

                                    $importDir = $this->_dir->getRoot().'/pub/media/import/*';

                                    //if(($value[46] != "")  && ($value[47] != "") && ($value[48] != "")){
                                    if($value[46] != null  || $value[47] != null || $value[48] != null){
                                        foreach(glob($importDir) as $img) { 

                                            $imgStringArray = explode('/', $img);
                                            $imageName = end($imgStringArray);
                                        
                                            
                                            if(strpos($imageName, $value[48]) !== false){
                                                if(file_exists($img)){
                                                    $newProduct->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                                }
                                            }
                                            if(strpos($imageName, $value[47]) !== false){
                                                if(file_exists($img)){
                                                    $newProduct->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                                }
                                            }
                                            if(strpos($imageName, $value[46]) !== false){
                                                if(file_exists($img)){
                                                    $newProduct->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                                }
                                            }
                                        }
                                        try{
                                            $newProduct->save();
                                            $this->_logger->addInfo('Add image sucessfully for : '.$newProduct->getSku());
                                        }catch(Exception $e){
                                            $this->_logger->addInfo('Image not added: '.$newProduct->getSku());
                                        }
                                    }else{
                                        $this->_logger->addInfo('Image name not there in CSV  : '.$newProduct->getSku());
                                    }
                                }else{
                                    $this->_logger->addInfo($value[0] .'Category does not exist for SKU:'.$value[1]);
                                    continue;
                                }
                            }else{
                                $this->_logger->addInfo('Category name does not exist in file...');
                                continue;
                            }
                        }
                    }
                }
                
                $this->_logger->addInfo($this->_helper->getBothCategoryFilesArray()['LOLS'][0].'....Completed....');
            }
        }else{
            $this->_logger->addInfo("File does not exist in folder..");
        }

        $filesArray =  $this->_helper->getBothCategoryFilesArray()['LOLS'];
        
        if($filesArray > 0 ){
            foreach ($filesArray as $file) {
                $filename = $this->_dir->getRoot(). '/importcsv/'.$file;
                $this->_helper->fileMovedAndDelete($filename);
            }
        }
    }
}