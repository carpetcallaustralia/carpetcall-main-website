<?php
namespace Carpetcall\ImportCSV\Cron;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Carpetcall\ImportCSV\Helper\Data;
use Magento\Catalog\Model\CategoryLinkRepository;

class GOLSReminder
{
    protected $_storeManager;
    protected $_customerRepository;
    protected $_customerFactory;
    protected $_customer;
    protected $_scopeConfig;
    protected $_resources;
    protected $_dir;
    protected $_fileCsv;
    protected $_product;
    protected $_productRepository;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_logger;
    protected $_stockRegistry;
    protected $_helper;
    private $_objectManager;
    protected $_productCollectionFactory;
    private $categoryLinkRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Customer $customers,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resources,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\File\Csv $fileCsv,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Carpetcall\ImportCSV\Logger\Logger $logger,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Carpetcall\ImportCSV\Helper\Data $helper,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        CategoryLinkRepository $categoryLinkRepository
    )
    {
        $this->_storeManager = $storeManager;
        $this->_customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
        $this->_scopeConfig = $scopeConfig;
        $this->_resources = $resources;
        $this->_dir = $dir;
        $this->_moduleReader = $moduleReader;
        $this->_fileCsv = $fileCsv;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_logger = $logger;
        $this->_stockRegistry = $stockRegistry;
        $this->_helper = $helper;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_objectManager = $objectManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
    }

    public function execute()
    {
        $connection = $this->_resources->getConnection();
        $rugConfigTable = $this->_resources->getTableName('rugs_config');
        $rugAssociatedTable = $this->_resources->getTableName('rugs_import_data');

        $query = "SELECT * FROM " . $rugConfigTable . " WHERE flag = '0' LIMIT 0,50";
        $GOLSConfigdata = $connection->fetchAll($query);

        if(count($GOLSConfigdata) > 0){
            $this->_logger->addInfo('****** GOLS Started *****');

            foreach ($GOLSConfigdata as $config) {
               $configSKU = $config['config_sku'];

               $query = "SELECT * FROM `" . $rugAssociatedTable . "` LEFT JOIN `".$rugConfigTable."` ON `".$rugConfigTable."`.`config_sku` = `".$rugAssociatedTable."`.`configurable_sku` WHERE `".$rugConfigTable."`.`config_sku` = '".$configSKU."'";

               $GOLSData = $connection->fetchAll($query);

                $preDataCheckVariant = array();
                if(count($GOLSData) > 0){
                    foreach ($GOLSData as $keyPre => $dataPre) {

                        $widthString = explode("        ",$dataPre['length_width_height']);

                        if(count($widthString) > 1){
                            $availableSize = trim($dataPre['available_size']);
                            $preDataCheckVariant[$dataPre['sku']] = $availableSize;
                        }
                    }
                }

                if(count($preDataCheckVariant) > 0){

                    $flipArray = array_flip($preDataCheckVariant);

                    foreach ($preDataCheckVariant as $prekey => $preData) {
                       if(!in_array($prekey, $flipArray)){
                            $lastDegitArray = explode(".",$prekey);
                            $lastDegit = end($lastDegitArray);

                            $availableSizeString = $lastDegit.' - '.$preData;
                            $queryVariant = "UPDATE " . $rugAssociatedTable . " SET available_size = '".$availableSizeString."' WHERE sku = '".$prekey."'";
                            $connection->query($queryVariant);

                            $this->_logger->addInfo("Same variant in same bunch of Product so variant option change for ".$prekey." : ".$availableSizeString);
                       }
                    }
                }

                $queryDataToProcess = "SELECT * FROM `" . $rugAssociatedTable . "` LEFT JOIN `".$rugConfigTable."` ON `".$rugConfigTable."`.`config_sku` = `".$rugAssociatedTable."`.`configurable_sku` WHERE `".$rugConfigTable."`.`config_sku` = '".$configSKU."'";

                $GOLSDataToProcess = $connection->fetchAll($queryDataToProcess);

               $preConfSKU = 0;
               $assProductSKUs = [];

               foreach ($GOLSDataToProcess as $key => $data) {

                    /*add or update associated product */

                    $finalConfigSKU = $data['configurable_sku'];

                    $check_product = $this->_objectManager->get('\Magento\Catalog\Model\Product');
                    if($check_product->getIdBySku($data['sku'])) {
                        $oldAssProduct = $this->_productRepository->get($data['sku']);

                        $isInStock = 1;
                        if($data['stock'] > 2){
                            $isInStock = 1;
                        }else{
                            $isInStock = 0;
                        }

                        $oldAssProduct->setStockData(
                            array(
                                'use_config_manage_stock' => 0,
                                'manage_stock' => 1,
                                'min_sale_qty'=> 0,
                                'min_qty' => 2,
                                'is_in_stock' => $isInStock,
                                'qty' => $data['stock']
                            )
                        );

                        $categoryTitle = $data['category'];
                        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                        $categoryIdsArray = array();
                        if($collection->getSize() > 0) {
                            $categoryIdsArray['child_category'] = $collection->getFirstItem()->getId();

                            if($collection->getFirstItem()->getParentId()){
                                $categoryIdsArray['parent_category'] = $collection->getFirstItem()->getParentId();

                                $baseURL = $this->_storeManager->getStore()->getBaseUrl();
                                $category = $this->_categoryRepository->get($categoryIdsArray['parent_category'], $this->_storeManager->getStore()->getId());

                                $categoryName = str_replace(" ", "-", strtolower($category->getName()));
                                $productName = strtolower($data['range_name']);
                                $sku = str_replace(".", "-", strtolower($data['sku']));

                                $URLkey = $categoryName."-".$sku;
                            }

                            $widthString = explode("        ",$data['length_width_height']);

                            if(count($widthString) > 1){

                                $availableSize = $data['available_size'];

                                if($availableSize != null){
                                    $optionId = $this->_helper->createOrGetId('size', $availableSize);

                                    if($optionId){
                                        $this->_logger->addInfo('Option created sucessfully with option ID :'.$optionId);
                                        $oldAssProduct->setSize($optionId);
                                    }
                                }

                                $width = trim(strtolower($widthString[0]));
                                $lengthWidthArray = explode("x",$width);
                                $length = trim(str_replace("cm", " ", $lengthWidthArray[0]));
                                $width = trim(str_replace("cm", " ", $lengthWidthArray[1]));
                                $weight = trim($data['weight']);
                                $WeigthCalculation = ($length / 100) * ($width / 100) * $weight;

                                $oldAssProduct->setWeight(trim($WeigthCalculation));
                            }else{
                                $this->_logger->addInfo('length_width_height does not have proper value in product SKU: '.$data['sku'] .': '.$data['length_width_height']);
                                continue;
                            }

                            $oldAssProduct->setStoreId(1);
                            $oldAssProduct->setWebsiteIds(array(1));
                            $oldAssProduct->setCategoryIds($categoryIdsArray);
                            $oldAssProduct->setAttributeSetId(4);
                            $oldAssProduct->setPrice(floor($data['price']));
                            $oldAssProduct->setSpecialPrice(floor($data['online_price']));
                            $oldAssProduct->setUrlKey($URLkey);
                            $oldAssProduct->setData('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                            $oldAssProduct->setData('visibility', 1);

                            $storeId = 0;
                            if($data['rugs_fiber'] != ""){
                                $fiberAttOptId = $this->_helper->createOrGetId('rugs_fiber',trim($data['rugs_fiber']));
                                $oldAssProduct->addAttributeUpdate('rugs_fiber',$fiberAttOptId,$storeId);
                            }

                            try{
                                // reset value for carpet_colours attribute
                                // trello issue: Carpet Call - disable default carpet colour for rugs
                                $oldAssProduct->setCarpetColours('');
                                $oldAssProduct->save();
                                $assProductSKUs[$oldAssProduct->getId()] = $oldAssProduct->getId();
                                $this->_logger->addInfo('Updated associated product save with ID: '. $oldAssProduct->getId().' / SKU:'.$data['sku'].' / Price:'.'$'.$data['price'].' / Qty: '.$data['stock']);

                            } catch(Exception $e){
                                $this->_logger->addInfo($oldAssProduct->getSku().' Associated Product can not update. Error '.$e->getMessage());
                            }
                        }else{
                            $this->_logger->addInfo('Product SKU : ('.$oldAssProduct->getSku().')Category "'.$data['category'].'" does not exist.');
                            continue;
                        }

                    }else{
                        $newAssProduct = $this->_objectManager->create('\Magento\Catalog\Model\Product');
                        $newAssProduct->setSku($data['sku']);

                        $isInStock = 1;
                        if($data['stock'] > 2){
                            $isInStock = 1;
                        }else{
                            $isInStock = 0;
                        }

                        $newAssProduct->setStockData(
                            array(
                                'use_config_manage_stock' => 0,
                                'manage_stock' => 1,
                                'min_sale_qty'=> 0,
                                'min_qty' => 2,
                                'is_in_stock' => $isInStock,
                                'qty' => $data['stock']
                            )
                        );

                        $categoryTitle = $data['category'];
                        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                        $categoryIdsArray = array();
                        if($collection->getSize() > 0) {
                            $categoryIdsArray['child_category'] = $collection->getFirstItem()->getId();

                            if($collection->getFirstItem()->getParentId()){
                                $categoryIdsArray['parent_category'] = $collection->getFirstItem()->getParentId();

                                $baseURL = $this->_storeManager->getStore()->getBaseUrl();
                                $category = $this->_categoryRepository->get($categoryIdsArray['parent_category'], $this->_storeManager->getStore()->getId());

                                $categoryName = str_replace(" ", "-", strtolower($category->getName()));
                                $productName = strtolower($data['range_name']);
                                $sku = str_replace(".", "-", strtolower($data['sku']));

                                $URLkey = $categoryName."-".$sku;
                            }

                            $newAssProduct->setStoreId(1);
                            $newAssProduct->setWebsiteIds(array(1));
                            $newAssProduct->setCategoryIds($categoryIdsArray);
                            $newAssProduct->setBrands($data['range_name']);
                            $newAssProduct->setAttributeSetId(4);
                            $newAssProduct->setName($data['range_name']);
                            $newAssProduct->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);

                            $widthString = explode("        ",$data['length_width_height']);

                            if(count($widthString) > 1){

                                $availableSize = $data['available_size'];

                                if($availableSize != null){
                                    $optionId = $this->_helper->createOrGetId('size', $availableSize);

                                    if($optionId){
                                        $this->_logger->addInfo('Option created sucessfully with option ID :'.$optionId);
                                        $newAssProduct->setSize($optionId);
                                    }
                                }

                                $width = trim(strtolower($widthString[0]));
                                $lengthWidthArray = explode("x",$width);
                                $length = trim(str_replace("cm", " ", $lengthWidthArray[0]));
                                $width = trim(str_replace("cm", " ", $lengthWidthArray[1]));
                                $weight = trim($data['weight']);
                                $WeigthCalculation = ($length / 100) * ($width / 100) * $weight;

                                $newAssProduct->setWeight(trim($WeigthCalculation));
                            }else{
                                $this->_logger->addInfo('Product does not create due to length_width_height does not have proper value in product SKU: '.$data['sku'] .': '.$data['length_width_height']);
                                continue;
                            }

                            $newAssProduct->setPrice(floor($data['price']));
                            $newAssProduct->setSpecialPrice(floor($data['online_price']));
                            $newAssProduct->setUrlKey($URLkey);
                            $newAssProduct->setData('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                            $newAssProduct->setData('visibility', 1);

                            if($data['rugs_fiber'] != ""){
                                $fiberAttOptId = $this->_helper->createOrGetId('rugs_fiber',trim($data['rugs_fiber']));
                                $newAssProduct->setData('rugs_fiber',$fiberAttOptId);
                            }

                            try{
                                $newAssProduct->save();
                                $assProductSKUs[$newAssProduct->getId()] = $newAssProduct->getId();
                                $this->_logger->addInfo('New associated product save with ID: '. $newAssProduct->getId().'/ SKU: '.$data['sku']);
                            } catch(Exception $e){
                                $this->_logger->addInfo($newAssProduct->getSku().' Associated Product can not create. Error '.$e->getMessage());
                            }
                        }else{
                            $this->_logger->addInfo('Product SKU : ('.$newAssProduct->getSku().')Category "'.$data['category'].'" does not exist.');
                            continue;
                        }
                    }

                    $preConfSKU = $finalConfigSKU;
                }

                $query = "SELECT * FROM `" . $rugAssociatedTable . "` LEFT JOIN `".$rugConfigTable."` ON `".$rugConfigTable."`.`config_sku` = `".$rugAssociatedTable."`.`configurable_sku` WHERE `".$rugConfigTable."`.`config_sku` = '".$preConfSKU."' ORDER BY `".$rugAssociatedTable."`.`id` DESC LIMIT 1";

                $configData = $connection->fetchAll($query);

                if(count($configData) > 0){
                    $check_product1 = $this->_objectManager->get('\Magento\Catalog\Model\Product');

                    if($check_product1->getIdBySku($preConfSKU)) {
                        $oldConfig = $this->_productRepository->get($preConfSKU);
                        /*$isInStock = 1;
                        if($data['stock'] > 0){
                            $isInStock = 1;
                        }else{
                            $isInStock = 0;
                        }

                        $oldConfig->setStockData(
                            array(
                                'use_config_manage_stock' => 0,
                                'manage_stock' => 1,
                                'is_in_stock' => $isInStock,
                                'qty' => $data['stock']
                            )
                        );*/

                        $oldConfig->setStockData(array(
                            'use_config_manage_stock' => 0, //'Use config settings' checkbox
                            'manage_stock' => 1, //manage stock
                            'is_in_stock' => 1, //Stock Availability
                            )
                        );

                        $sizeOptionArray = array();
                        $sizeTextFilterArray = array();
                        $sizeOptionArray = explode(",", $oldConfig->getData('rugs_size_filter'));

                        foreach ($sizeOptionArray as $sizeOptionId) {
                            $attribute = $oldConfig->getResource()->getAttribute('rugs_size_filter');
                            if ($attribute->usesSource()) {
                                $sizeTextFilterArray[$sizeOptionId] = $attribute->getSource()->getOptionText($sizeOptionId);
                            }
                        }

                        $colorOptionArray = array();
                        $colorTextFilterArray = array();
                        $colorOptionArray = explode(",", $oldConfig->getData('rugs_color_filter'));

                        foreach ($colorOptionArray as $colorOptionId) {
                            $attribute = $oldConfig->getResource()->getAttribute('rugs_color_filter');
                            if ($attribute->usesSource()) {
                                $colorTextFilterArray[$colorOptionId] = $attribute->getSource()->getOptionText($colorOptionId);
                            }
                        }

                        $sizeAndColorCatArray = array_merge($sizeTextFilterArray,$colorTextFilterArray);
                        $sizeAndColorCatArray[] = $data['rugs_fiber'];

                        $categoryTitle = $data['category'];
                        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                        $categoryIdsArray = array();
                        if($collection->getSize() > 0) {

                            // main category & sub category
                            $categoryIdsArray['child_category'] = $collection->getFirstItem()->getId();

                            if($collection->getFirstItem()->getParentId()){
                                $categoryIdsArray['parent_category'] = $collection->getFirstItem()->getParentId();

                                //fiber & color & size category assign
                                $parent_category_id = $collection->getFirstItem()->getParentId();

                                $categoryObj = $this->_objectManager->get('\Magento\Catalog\Model\CategoryRepository')->get($parent_category_id);
                                $subcategories = $categoryObj->getChildrenCategories();
                                if($sizeAndColorCatArray > 0 ){
                                    foreach($subcategories as $subcategorie) {
                                        foreach ($sizeAndColorCatArray as $catName) {
                                            if($subcategorie->getName() == $catName){
                                                $categoryIdsArray[] = $subcategorie->getId();
                                            }
                                        }
                                    }
                                }

                                $baseURL = $this->_storeManager->getStore()->getBaseUrl();
                                $category = $this->_categoryRepository->get($categoryIdsArray['parent_category'], $this->_storeManager->getStore()->getId());

                                $categoryName = str_replace(" ", "-", strtolower($category->getName()));
                                $productName = strtolower($data['range_name']);
                                $sku = str_replace(".", "-", strtolower($preConfSKU));

                                $URLkey = $categoryName."-".$sku;
                            }

                            $existCategoryIdsArray = $oldConfig->getCategoryIds();
                            $not = array_diff($existCategoryIdsArray,$categoryIdsArray);

                            if(count($not) > 0){
                                $query = "UPDATE " . $rugConfigTable . " SET cat_not_in = '".implode(",", $not)."' WHERE config_sku = '".$preConfSKU."'";
                                $connection->query($query);
                            }

                            $oldConfig->setStoreId(1);
                            $oldConfig->setWebsiteIds(array(1));
                            $oldConfig->setCategoryIds($categoryIdsArray);
                            $oldConfig->setBrands($data['range_name']);
                            $oldConfig->setAttributeSetId(4);
                            $oldConfig->setName($data['range_name']);
                            $oldConfig->setParagraph1($data['des_1']);
                            $oldConfig->setParagraph2($data['des_2']);
                            $oldConfig->setParagraph3($data['des_3']);
                            $oldConfig->setParagraph4($data['des_4']);

                            $CountryOfOriginManufacture = $this->_helper->getCountryCode(trim($data['country_of_origin']));

                            if(isset($CountryOfOriginManufacture)){
                                $oldConfig->setCountryOfManufacture($CountryOfOriginManufacture);
                            }

                            $storeId = 0;
                            if($data['rugs_fiber'] != ""){
                                $fiberAttOptId = $this->_helper->createOrGetId('rugs_fiber',trim($data['rugs_fiber']));
                                $oldConfig->addAttributeUpdate('rugs_fiber',$fiberAttOptId,$storeId);
                            }

                            $oldConfig->setYarnType($data['yarn_type']);
                            $oldConfig->setCareInstructions($data['care_instruction']);
                            $oldConfig->setTypeId('configurable');

                            $widthString = explode("        ",$data['length_width_height']);

                            if(count($widthString) > 1){
                                $width = trim(strtolower($widthString[0]));
                                $lengthWidthArray = explode("x",$width);
                                $length = trim(str_replace("cm", " ", $lengthWidthArray[0]));
                                $width = trim(str_replace("cm", " ", $lengthWidthArray[1]));
                                $weight = trim($data['weight']);
                                $WeigthCalculation = ($length / 100) * ($width / 100) * $weight;

                                $oldConfig->setWeight(trim($WeigthCalculation));
                            }else{
                                $this->_logger->addInfo('length_width_height does not have proper value in product SKU: '.$data['sku'] .': '.$data['length_width_height']);
                                continue;
                            }

                            $oldConfig->setPrice('0.00');
                            //$oldConfig->setSpecialPrice($data['online_price']);
                            $oldConfig->setUrlKey($URLkey);
                            $oldConfig->setData('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                            $oldConfig->setData(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);

                            $size_attr_id = $oldConfig->getResource()->getAttribute('size')->getId();
                            $oldConfig->getTypeInstance()->setUsedProductAttributeIds([$size_attr_id], $oldConfig);
                            $configurableAttributesData = $oldConfig->getTypeInstance()->getConfigurableAttributesAsArray($oldConfig);
                            $oldConfig->setCanSaveConfigurableAttributes(true);
                            $oldConfig->setConfigurableAttributesData($configurableAttributesData);
                            $configurableProductsData = [];
                            $oldConfig->setConfigurableProductsData($configurableProductsData);

                            try{
                                // reset value for carpet_colours attribute
                                // trello issue: Carpet Call - disable default carpet colour for rugs
                                $oldConfig->setCarpetColours('');
                                $oldConfig->save();
                                $configId = $oldConfig->getId();
                                $this->_logger->addInfo('Update Config product save with ID: '. $oldConfig->getId().' / SKU: '.$oldConfig->getSku());
                            } catch(Exception $e){
                                $this->_logger->addInfo($oldConfig->getSku().' Config Product can not update. Error '.$e->getMessage());
                            }

                            if($data['image_1'] != null  && $data['image_2'] != null && $data['image_3'] != null){

                                /*Remove images*/
                                $productRepository = $this->_objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
                                $existingMediaGalleryEntries = $oldConfig->getMediaGalleryEntries();
                                foreach ($existingMediaGalleryEntries as $key => $entry) {
                                    if (strpos($entry->getData('file'),$data['image_1']) !== false || strpos($entry->getData('file'),$data['image_2']) !== false || strpos($entry->getData('file'),$data['image_3']) !== false) {
                                        unset($existingMediaGalleryEntries[$key]);
                                    }
                                }
                                $oldConfig->setMediaGalleryEntries($existingMediaGalleryEntries);
                                $productRepository->save($oldConfig);
                                $this->_logger->addInfo('Remove image sucessfully for : '.$oldConfig->getSku());

                                /*Add media*/
                                $importDir = $this->_dir->getRoot().'/pub/media/import/*';

                                foreach(glob($importDir) as $img) {

                                    $imgStringArray = explode('/', $img);
                                    $imageName = end($imgStringArray);

                                    if(strpos($imageName, $data['image_3']) !== false){
                                        if (file_exists($img)) {
                                            $oldConfig->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                        }
                                    }
                                    if(strpos($imageName, $data['image_2']) !== false){
                                        if (file_exists($img)) {
                                            $oldConfig->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                        }
                                    }
                                    if(strpos($imageName, $data['image_1']) !== false){
                                        if (file_exists($img)) {
                                            $oldConfig->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                        }
                                    }
                                }
                                try{
                                    $oldConfig->save();
                                    $this->_logger->addInfo('Add image sucessfully for : '.$oldConfig->getSku());
                                }catch(Exception $e){
                                    $this->_logger->addInfo('Image not added: '.$oldConfig->getSku());
                                }
                            }else{
                                $this->_logger->addInfo('Image not there in CSV  : '.$oldConfig->getSku());
                            }

                        }else{
                            $this->_logger->addInfo('Product SKU : ('.$oldConfig->getSku().')Category "'.$data['category'].'" does not exist.');
                            continue;
                        }

                    }else{
                        $newConfig = $this->_objectManager->create('\Magento\Catalog\Model\Product');

                        $newConfig->setSku($preConfSKU);

                        $newConfig->setStockData(array(
                            'use_config_manage_stock' => 0, //'Use config settings' checkbox
                            'manage_stock' => 1, //manage stock
                            'is_in_stock' => 1, //Stock Availability
                            )
                        );

                        $categoryTitle = $data['category'];
                        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                        $categoryIdsArray = array();
                        if($collection->getSize() > 0) {
                            $categoryIdsArray['child_category'] = $collection->getFirstItem()->getId();

                            if($collection->getFirstItem()->getParentId()){
                                $categoryIdsArray['parent_category'] = $collection->getFirstItem()->getParentId();

                                $baseURL = $this->_storeManager->getStore()->getBaseUrl();
                                $category = $this->_categoryRepository->get($categoryIdsArray['parent_category'], $this->_storeManager->getStore()->getId());

                                $categoryName = str_replace(" ", "-", strtolower($category->getName()));
                                $productName = strtolower($data['range_name']);
                                $sku = str_replace(".", "-", strtolower($preConfSKU));

                                $URLkey = $categoryName."-".$sku;
                            }

                            $newConfig->setStoreId(1);
                            $newConfig->setWebsiteIds(array(1));
                            $newConfig->setCategoryIds($categoryIdsArray);
                            $newConfig->setBrands($data['range_name']);
                            $newConfig->setAttributeSetId(4);
                            $newConfig->setName($data['range_name']);
                            $newConfig->setParagraph1($data['des_1']);
                            $newConfig->setParagraph2($data['des_2']);
                            $newConfig->setParagraph3($data['des_3']);
                            $newConfig->setParagraph4($data['des_4']);

                            $CountryOfOriginManufacture = $this->_helper->getCountryCode(trim($data['country_of_origin']));

                            if(isset($CountryOfOriginManufacture)){
                                $newConfig->setCountryOfManufacture($CountryOfOriginManufacture);
                            }

                            if($data['rugs_fiber'] != ""){
                                $fiberAttOptId = $this->_helper->createOrGetId('rugs_fiber',trim($data['rugs_fiber']));
                                $newConfig->setData('rugs_fiber',$fiberAttOptId);
                            }

                            $newConfig->setYarnType($data['yarn_type']);
                            $newConfig->setCareInstructions($data['care_instruction']);
                            $newConfig->setTypeId(\Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE);

                            $widthString = explode("        ",$data['length_width_height']);

                            if(count($widthString) > 1){
                                $width = trim(strtolower($widthString[0]));
                                $lengthWidthArray = explode("x",$width);
                                $length = trim(str_replace("cm", " ", $lengthWidthArray[0]));
                                $width = trim(str_replace("cm", " ", $lengthWidthArray[1]));
                                $weight = trim($data['weight']);
                                $WeigthCalculation = ($length / 100) * ($width / 100) * $weight;

                                $newConfig->setWeight(trim($WeigthCalculation));
                            }else{
                                $this->_logger->addInfo('Product does not create due to length_width_height does not have proper value in product SKU: '.$data['sku'] .': '.$data['length_width_height']);
                                continue;
                            }


                            $newConfig->setPrice('0.00');
                            //$newConfig->setSpecialPrice($data['online_price']);
                            $newConfig->setUrlKey($URLkey);
                            $newConfig->setData('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                            $newConfig->setData(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);

                            $size_attr_id = $newConfig->getResource()->getAttribute('size')->getId();
                            $newConfig->getTypeInstance()->setUsedProductAttributeIds([$size_attr_id], $newConfig);
                            $configurableAttributesData = $newConfig->getTypeInstance()->getConfigurableAttributesAsArray($newConfig);
                            $newConfig->setCanSaveConfigurableAttributes(true);
                            $newConfig->setConfigurableAttributesData($configurableAttributesData);
                            $configurableProductsData = [];
                            $newConfig->setConfigurableProductsData($configurableProductsData);

                            try{
                                $newConfig->save();
                                $configId = $newConfig->getId();
                                $this->_logger->addInfo('New Config product save with ID: '. $newConfig->getId().' / SKU: '.$newConfig->getSku());
                            } catch(Exception $e){
                                $this->_logger->addInfo($newConfig->getSku().' Config Product can not create. Error '.$e->getMessage());
                            }

                            $importDir = $this->_dir->getRoot().'/pub/media/import/*';

                            if(($data['image_1'] != "")  && ($data['image_2'] != "") && ($data['image_3'] != "")){
                                foreach(glob($importDir) as $img) {

                                    $imgStringArray = explode('/', $img);
                                    $imageName = end($imgStringArray);

                                    if(strpos($imageName, $data['image_3']) !== false){
                                        if (file_exists($img)) {
                                            $newConfig->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                        }
                                    }
                                    if(strpos($imageName, $data['image_2']) !== false){
                                        if (file_exists($img)) {
                                            $newConfig->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                        }
                                    }
                                    if(strpos($imageName, $data['image_1']) !== false){
                                        if (file_exists($img)) {
                                            $newConfig->addImageToMediaGallery($img, array('image', 'small_image', 'thumbnail','swatch_image'), false, false);
                                        }
                                    }
                                }
                                try{
                                    $newConfig->save();
                                    $this->_logger->addInfo('Add image sucessfully for : '.$newConfig->getSku());
                                }catch(Exception $e){
                                    $this->_logger->addInfo('Image not added: '.$newConfig->getSku());
                                }
                            }else{
                                $this->_logger->addInfo('Image not there in CSV  : '.$newAssProduct->getSku());
                            }
                        }else{
                            $this->_logger->addInfo('Product SKU : ('.$newConfig->getSku().')Category "'.$data['category'].'" does not exist.');
                            continue;
                        }
                    }
                }

                /*Assign Associated product to configurable product...*/
                if(isset($configId)){
                    try {
                        $configurableProduct = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($configId);

                        $_children = $configurableProduct->getTypeInstance()->getUsedProducts($configurableProduct);

                        foreach ($_children as $child){
                            $assProductSKUs[$child->getId()] = $child->getId();
                        }

                        $configurableProduct->setAssociatedProductIds($assProductSKUs);
                        $configurableProduct->setCanSaveConfigurableAttributes(true);
                        $configurableProduct->setNewVariationsAttributeSetId(4);
                        $configurableProduct->setAffectConfigurableProductAttributes(4);

                        try{
                            $configurableProduct->save();
                            $this->_logger->addInfo('Assign '.implode(",",$assProductSKUs).' product sucessfully to ID: '. $configurableProduct->getId().' /SKU: '.$configurableProduct->getSku());
                        } catch(Exception $e){
                            $this->_logger->addInfo('Assign product to '.$configurableProduct->getSku().'. Error '.$e->getMessage());
                        }
                    } catch (Exception $e) {
                        $this->_logger->addInfo($e->getMessage());
                    }
                }

                $queryCat = "SELECT * FROM " . $rugConfigTable . " WHERE cat_not_in IS NOT NULL AND config_sku = '".$configSKU."'";
                $catNotExistdata = $connection->fetchAll($queryCat);

                if(count($catNotExistdata) > 0){
                    foreach ($catNotExistdata as $keyCat => $valueCat) {
                        if($valueCat != NULL){
                            $catArray = explode(",", $valueCat['cat_not_in']);
                        }
                    }

                    if(count($catArray) > 0){
                        foreach ($catArray as $keyNot => $valueNot) {
                            $CategoryLinkRepository = $this->_objectManager->get('\Magento\Catalog\Model\CategoryLinkRepository');
                            $CategoryLinkRepository->deleteByIds($valueNot, $configSKU);
                        }
                        $this->_logger->addInfo('Remove category IDs ('.implode(",", $catArray).') from SKU: '.$configSKU);
                    }
                }

                $query = "UPDATE " . $rugConfigTable . " SET flag = '1' WHERE config_sku = '".$configSKU."'";
                $connection->query($query);
            }

            /*foreach ($GOLSConfigdata as $value) {
               $query = "UPDATE " . $rugConfigTable . " SET flag = '1' WHERE config_sku = '".$value['config_sku']."'";
               $connection->query($query);
            }*/

            $this->_logger->addInfo('****** GOLS Completed *****');
        }else{
            $this->_logger->addInfo('****** No record found.. *****');
        }
    }
}
