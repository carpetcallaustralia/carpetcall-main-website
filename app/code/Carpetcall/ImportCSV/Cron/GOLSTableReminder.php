<?php
namespace Carpetcall\ImportCSV\Cron;
 
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Carpetcall\ImportCSV\Helper\Data;

class GOLSTableReminder
{   
    protected $_resources;
    protected $_logger;
    protected $_helper;
    protected $_fileCsv;
    protected $_dir;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resources,
        \Carpetcall\ImportCSV\Logger\Logger $logger,
        \Carpetcall\ImportCSV\Helper\Data $helper,
        \Magento\Framework\File\Csv $fileCsv,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) 
    {
        $this->_resources = $resources;
        $this->_logger = $logger;
        $this->_helper = $helper;
        $this->_fileCsv = $fileCsv;
        $this->_dir = $dir;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_objectManager = $objectManager;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function execute()
    {
        $GOLSCatFilesArray = $this->_helper->getBothCategoryFilesArray()['GOLS'];

        if(count($GOLSCatFilesArray) > 0){
            $this->truncateTable();
            $ConfigSKUArray = array();
            foreach ($GOLSCatFilesArray as $GOLSCatFile) {
                $GOLSfileForReadData = $this->_dir->getRoot(). '/importcsv/'.$GOLSCatFile;
                if(file_exists($GOLSfileForReadData)){
                    $GOLSdata = $this->_fileCsv->getData($GOLSfileForReadData);

                    foreach ($GOLSdata as $key => $data) {
                        $connection = $this->_resources->getConnection();

                        $rugProductTable = $this->_resources->getTableName('rugs_import_data');

                        $SKU = trim($data[1]);
                        $categoryName = ucfirst(strtolower(trim($data[3])));
                        $rugFiber = ucfirst(strtolower(trim($data[10])));

                        if($categoryName == 'Clearance'){
                            $categoryName = "Rugs Clearance";
                        }

                        $skuArray = explode('.', $SKU);
                        $lastEle = array_pop($skuArray);
                        $configSKU = implode(".", $skuArray);

                        $ConfigSKUArray[$configSKU] = $configSKU;

                        $query = "SELECT * FROM " . $rugProductTable . " WHERE sku = '$SKU'";
                        $result = $connection->fetchAll($query);

                        if(count($result) > 0){
                            $stock = ($result[0]['stock']) + (trim($data[13]));
                            $price = trim($data[16]);
                            $onlinePrice = trim($data[18]);
                            
                            $sizeString = explode("        ",$data[14]);
                            if(count($sizeString) > 1){
                                $SizesString = explode("        ",$data[14]);
                                $Size = trim($SizesString[0]);
                                $availableSize = $Size." - "."$".floor($data[18]);
                            }else{
                                $availableSize = null;
                            }

                            $sql = "UPDATE " . $rugProductTable . " SET configurable_sku = '".$configSKU."', stock = '".$stock."', price = '".$price."', online_price = '".$onlinePrice."', available_size = '".$availableSize."' WHERE sku = '".$SKU."'";
                            $connection->query($sql);

                        }else{

                            $sizeString = explode("        ",$data[14]);
                            if(count($sizeString) > 1){
                                $SizesString = explode("        ",$data[14]);
                                $Size = trim($SizesString[0]);
                                $availableSize = $Size." - "."$".floor($data[18]);
                            }else{
                                $availableSize = null;
                            }
                            
                            $sql = 'INSERT INTO ' . $rugProductTable . '(sku, configurable_sku, range_name, category, des_1, des_2, des_3, des_4, country_of_origin, yarn_type, rugs_fiber, care_instruction, stock, length_width_height, weight, price, online_price, image_1, image_2, image_3,available_size) VALUES ('.'"'.$SKU.'"'.','.'"'.$configSKU.'"'.','.'"'.trim($data[2]).'"'.','.'"'.$categoryName.'"'.','.'"'.trim($data[4]).'"'.','.'"'.trim($data[5]).'"'.','.'"'.trim($data[6]).'"'.','.'"'.trim($data[7]).'"'.','.'"'.trim($data[8]).'"'.','.'"'.trim($data[9]).'"'.','.'"'.$rugFiber.'"'.','.'"'.trim($data[12]).'"'.','.'"'.trim($data[13]).'"'.','.'"'.trim($data[14]).'"'.','.'"'.trim($data[15]).'"'.','.'"'.trim($data[16]).'"'.','.'"'.trim($data[18]).'"'.','.'"'.trim($data[19]).'"'.','.'"'.trim($data[20]).'"'.','.'"'.trim($data[21]).'"'.','.'"'.trim($availableSize).'"'.')';
                            $connection->query($sql);
                        }
                    }
                }
            }

            $this->_logger->addInfo("Insert/Update record to ".$rugProductTable." table sucessfully....");
            $this->addDataintoRugsConfigTable($ConfigSKUArray);

            $this->extraProductOOS();

            $filesArray =  $this->_helper->getBothCategoryFilesArray()['GOLS'];
            
            foreach ($filesArray as $file) {
                $filename = $this->_dir->getRoot(). '/importcsv/'.$file;
                $this->_helper->fileMovedAndDelete($filename);
            }
        }else{
            $this->_logger->addInfo("File does not exist in folder..");
        } 
    }

    public function addDataintoRugsConfigTable($ConfigSKUArray){

        $connection = $this->_resources->getConnection();

        $rugConfigTable = $this->_resources->getTableName('rugs_config');

        foreach ($ConfigSKUArray as $cSKU) {
           $sql = 'INSERT INTO ' . $rugConfigTable . '(config_sku) VALUES ('.'"'.$cSKU.'"'.')';
           $connection->query($sql);
        }

        $this->_logger->addInfo("Insert config SKU's to ".$rugConfigTable." table sucessfully....");
    }

    public function truncateTable(){

        $connection = $this->_resources->getConnection();
        $rugProductTable = $this->_resources->getTableName('rugs_import_data');
        $rugConfigTable = $this->_resources->getTableName('rugs_config');

        $query = "TRUNCATE TABLE " . $rugProductTable . "";
        $connection->query($query);

        $this->_logger->addInfo("TRUNCATE ".$rugProductTable." sucessfully....");

        $query = "TRUNCATE TABLE " . $rugConfigTable . "";
        $connection->query($query);

        $this->_logger->addInfo("TRUNCATE ".$rugConfigTable." sucessfully...."); 
    }

    public function extraProductOOS(){
        $letestCSVSKU = $this->getLetestCSVData();
        $existCSVProductSKU = $this->getExistingProductsSKU();

        if(count($letestCSVSKU) > 0 && count($existCSVProductSKU) > 0){
            $finalResult = array_diff($existCSVProductSKU ,$letestCSVSKU);
        }

        if(count($finalResult) > 0){
            $this->_logger->addInfo('******* Set stock 0 Start (Extra Product)*******');
            foreach ($finalResult as $finalResultProduct) {

                $checkProductData = $this->_objectManager->get('\Magento\Catalog\Model\Product');
                if($checkProductData->getIdBySku($finalResultProduct)) {
                    $oldAssExistProduct = $this->_productRepository->get($finalResultProduct);

                    $oldAssExistProduct->setStockData(
                        array(
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'min_sale_qty'=> 0,
                            'min_qty' => 2,
                            'is_in_stock' => 0,
                            'qty' => 0
                        )
                    );

                    try{
                        $oldAssExistProduct->save();
                        $this->_logger->addInfo('Set stock 0 to SKU: '.$finalResultProduct);  
                    } catch(Exception $e){
                        $this->_logger->addInfo('Not set stock 0 to SKU: '.$finalResultProduct);
                        $this->_logger->addInfo($e->getMessage());
                    }
                }
            }
            $this->_logger->addInfo('******* Set stock 0 Start (Extra Product)*******');
        }else{
            $this->_logger->addInfo('Not found any product for set stock 0.');
        }
    }

    public function getExistingProductsSKU(){
        $categoryTitle = 'Rugs';
        $categoryArray = array();

        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name','Rugs')->setPageSize(1);
        $categoryId = array();
        if($collection->getSize() > 0) {
            $categoryArray[] = $collection->getFirstItem()->getId();
        }

        $collection = $this->_categoryFactory->create()->getCollection()
              ->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();
        }

        if($categoryId){
            $category = $this->_categoryRepository->get($categoryId);
            $subCategories = $category->getChildrenCategories();
            foreach($subCategories as $subCategory) {
                $categoryArray[] = $subCategory->getId();
            }
        }

        if(count($categoryArray) > 0){
            $existProductData = $this->getProductCollectionByCategories($categoryArray);

            $existProductDataArray = array();
            
            foreach ($existProductData as $existData) {
                $existProductDataArray[] = $existData->getSku();
            }

            return $existProductDataArray;
        }else{
            $this->_logger->addInfo('Rugs category does not exist');
        }
    }

    public function getLetestCSVData(){

        $connection = $this->_resources->getConnection();
        $rugAssociatedTable = $this->_resources->getTableName('rugs_import_data');
        $productQuery = "SELECT * FROM " . $rugAssociatedTable . "";
        $existproductQuery = $connection->fetchAll($productQuery);

        $letestCSVProductData = array();
        foreach ($existproductQuery as $key => $productData) {
            $letestCSVProductData[] = $productData['sku'];
        }

        return $letestCSVProductData;
    }

    public function getProductCollectionByCategories($categoryArray)
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
        $collection->addCategoriesFilter(['in' => $categoryArray]);
        return $collection;
    }
}
