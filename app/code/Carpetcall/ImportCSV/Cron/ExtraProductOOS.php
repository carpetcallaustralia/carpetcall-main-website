<?php
namespace Carpetcall\ImportCSV\Cron;
 
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Carpetcall\ImportCSV\Helper\Data;

class ExtraProductOOS
{
    protected $_storeManager;
    protected $_scopeConfig;
    protected $_resources;
    protected $_productRepository;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_logger;
    protected $_stockRegistry;
    protected $_helper;
    private $_objectManager;
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resources,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Carpetcall\ImportCSV\Logger\Logger $logger,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Carpetcall\ImportCSV\Helper\Data $helper,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) 
    {
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_resources = $resources;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_logger = $logger;
        $this->_stockRegistry = $stockRegistry;
        $this->_helper = $helper;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_objectManager = $objectManager;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function execute()
    {
        $letestCSVSKU = $this->getLetestCSVData();
        $existCSVProductSKU = $this->getExistingProductsSKU();

        if(count($letestCSVSKU) > 0 && count($existCSVProductSKU) > 0){
            $finalResult = array_diff($existCSVProductSKU ,$letestCSVSKU);
        }

        if(count($finalResult) > 0){
            $this->_logger->addInfo('******* Set stock 0 Start (Extra Product)*******');
            foreach ($finalResult as $finalResultProduct) {

                $checkProductData = $this->_objectManager->get('\Magento\Catalog\Model\Product');
                if($checkProductData->getIdBySku($finalResultProduct)) {
                    $oldAssExistProduct = $this->_productRepository->get($finalResultProduct);

                    $oldAssExistProduct->setStockData(
                        array(
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'min_sale_qty'=> 0,
                            'min_qty' => 2,
                            'is_in_stock' => 0,
                            'qty' => 0
                        )
                    );

                    try{
                        $oldAssExistProduct->save();
                        $this->_logger->addInfo('Set stock 0 to SKU: '.$finalResultProduct);  
                    } catch(Exception $e){
                        $this->_logger->addInfo('Not set stock 0 to SKU: '.$finalResultProduct);
                        $this->_logger->addInfo($e->getMessage());
                    }
                }
            }
            $this->_logger->addInfo('******* Set stock 0 Start (Extra Product)*******');
        }else{
            $this->_logger->addInfo('Not found any product for set stock 0.');
        }
    }

    public function getExistingProductsSKU(){
        $categoryTitle = 'Rugs';
        $categoryArray = array();

        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name','Rugs')->setPageSize(1);
        $categoryId = array();
        if($collection->getSize() > 0) {
            $categoryArray[] = $collection->getFirstItem()->getId();
        }

        $collection = $this->_categoryFactory->create()->getCollection()
              ->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();
        }

        if($categoryId){
            $category = $this->_categoryRepository->get($categoryId);
            $subCategories = $category->getChildrenCategories();
            foreach($subCategories as $subCategory) {
                $categoryArray[] = $subCategory->getId();
            }
        }

        if(count($categoryArray) > 0){
            $existProductData = $this->getProductCollectionByCategories($categoryArray);

            $existProductDataArray = array();
            
            foreach ($existProductData as $existData) {
                $existProductDataArray[] = $existData->getSku();
            }

            return $existProductDataArray;
        }else{
            $this->_logger->addInfo('Rugs category does not exist');
        }
    }

    public function getLetestCSVData(){

        $connection = $this->_resources->getConnection();
        $rugAssociatedTable = $this->_resources->getTableName('rugs_import_data');
        $productQuery = "SELECT * FROM " . $rugAssociatedTable . "";
        $existproductQuery = $connection->fetchAll($productQuery);

        $letestCSVProductData = array();
        foreach ($existproductQuery as $key => $productData) {
            $letestCSVProductData[] = $productData['sku'];
        }

        return $letestCSVProductData;
    }

    public function getProductCollectionByCategories($categoryArray)
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
        $collection->addCategoriesFilter(['in' => $categoryArray]);
        return $collection;
    }
}
