<?php
namespace Carpetcall\ImportCSV\Helper;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Carpetcall\ImportCSV\Helper\Data;
use Magento\Eav\Model\AttributeRepository;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Store\Model\Store;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $attributeRepository;
    protected $attributeValues;
    protected $tableFactory;
    protected $attributeOptionManagement;
    protected $optionLabelFactory;
    protected $optionFactory;
    protected $_storeManager;
    protected $_customerRepository;
    protected $_customerFactory;
    protected $_customer;
    protected $_scopeConfig;
    protected $resource;
    protected $_dir;
    protected $_fileCsv;
    protected $_product;
    protected $_productRepository;
    protected $_categoryFactory;
    protected $_categoryRepository;
    protected $_logger;
    protected $_stockRegistry;
    protected $attributesRepository;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Customer $customers,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\File\Csv $fileCsv,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Carpetcall\ImportCSV\Logger\Logger $logger,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        AttributeRepository $attributesRepository
    ) {
        parent::__construct($context);

        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
        $this->_storeManager = $storeManager;
        $this->_customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
        $this->_scopeConfig = $scopeConfig;
        $this->resource = $resource;
        $this->_dir = $dir;
        $this->_moduleReader = $moduleReader;
        $this->_fileCsv = $fileCsv;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;
        $this->_logger = $logger;
        $this->_stockRegistry = $stockRegistry;
        $this->attributesRepository = $attributesRepository;
    }

    public function getAttribute($attributeCode)
    {
        return $this->attributeRepository->get($attributeCode);
    }

    public function createOrGetId($attributeCode, $label)
    {
        if (strlen($label) < 1) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Label for %1 must not be empty.', $attributeCode)
            );
        }

        // Does it already exist?
        $optionId = $this->getOptionId($attributeCode, $label);
        $optionsValue = "";

        if (!$optionId) {
            // If no, add it.
            /** @var \Magento\Eav\Model\Entity\Attribute\OptionLabel $optionLabel */
            $optionLabel = $this->optionLabelFactory->create();
            $optionLabel->setStoreId(1);
            $optionLabel->setLabel($label);

            $option = $this->optionFactory->create();
            $option->setValue($optionLabel->getLabel());
            $option->setLabel($optionLabel->getLabel());
            $option->setStoreLabels([$optionLabel]);
            $option->setSortOrder(0);
            $option->setIsDefault(false);

            $optionsValue = $option->getValue();

            $this->attributeOptionManagement->add(
                \Magento\Catalog\Model\Product::ENTITY,
                $this->getAttribute($attributeCode)->getAttributeId(),
                $option
            );

            // Get the inserted ID. Should be returned from the installer, but it isn't.
            $optionId = $this->getOptionId($attributeCode, $label, true);

            //swatchtext value
            /*if($attributeCode == 'size'){
                $attribute = $this->attributesRepository->get(
                    ProductAttributeInterface::ENTITY_TYPE_CODE,
                    $attributeCode
                );

                $options = [];
                $options['value'][$optionId][Store::DEFAULT_STORE_ID] = $optionsValue;
                if (is_array($option->getStoreLabels())) {
                    foreach ($option->getStoreLabels() as $label) {
                        if (!isset($options['value'][$optionId][$label->getStoreId()])) {
                            $options['value'][$optionId][$label->getStoreId()] = null;
                        }
                    }
                }
                $attribute->setData('swatchtext', $options);
            }*/
        }

        return $optionId;
    }

    public function getOptionId($attributeCode, $label, $force = false)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);

        // Build option array if necessary
        if ($force === true || !isset($this->attributeValues[ $attribute->getAttributeId() ])) {
            $this->attributeValues[ $attribute->getAttributeId() ] = [];

            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[ $attribute->getAttributeId() ][ $option['label'] ] = $option['value'];
            }
        }

        // Return option ID if exists
        if (isset($this->attributeValues[ $attribute->getAttributeId() ][ $label ])) {
            return $this->attributeValues[ $attribute->getAttributeId() ][ $label ];
        }

        // Return false if does not exist
        return false;
    }

    public function getFinalQtyColumnData(){
        $QtyData = $this->getQtyArray();
        
        $QtyColumnData = array();

        foreach ($QtyData as $key => $sub) {
            foreach ($sub as $id => $value1) {
                if($id !== 'Code'){
                    $QtyColumnData[$id] = (isset($QtyColumnData[$id]) ? $QtyColumnData[$id] + $value1 : $value1);
                }
            }
        }

        return $QtyColumnData;
    }

    public function getQtyArray(){
        $filesData = $this->getBothCategoryFilesArray();

        $qtyArray = array();
        $codeArray = array();
        $QtyData = array();

        foreach($filesData['LOLS'] as $files) { 
            $File = $this->_dir->getRoot(). '/importcsv/'.$files;
        
            if (file_exists($File)) {
                $data = $this->_fileCsv->getData($File);
                
                for($i = 0; $i < count($data); $i++) {
                    foreach ($data[$i] as $key => $value) {
                        if($key == '1' || $value == 'Code'){
                            $codeArray[] = $value;
                        }
                        if($key == '2' || $value == 'Quantity Available'){
                            $qtyArray[] = (int)str_replace(",", "", $value);
                        }
                    }
                }

                $QtyData[] = array_combine($codeArray,$qtyArray);
            }
        }

        return $QtyData;
    }

    public function getBothCategoryFilesArray(){
        $directory = $this->_dir->getRoot()."/importcsv/*";// CSV Files Diectory Path in root

        $hardfloringtWord = "LOLS";
        $rugsWord = "GOLS";

        $hardfloringFiles[$hardfloringtWord] = array();
        $rugFiles[$rugsWord] = array();

        foreach(glob($directory) as $file) { 

            $fileStringArray = explode('/', $file);
            $filename = end($fileStringArray);

            if(strpos($filename, $hardfloringtWord) !== false){
                $hardfloringFiles[$hardfloringtWord][] = $filename;
            }

            if(strpos($filename, $rugsWord) !== false){   
                $rugFiles[$rugsWord][] = $filename;
            }
        }

        $bothFilesArray = array();
        $bothFilesArray = array_merge($hardfloringFiles,$rugFiles);

        return $bothFilesArray;
    }

    public function getCountryCode($countryName){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $countryHelper = $objectManager->get('Magento\Directory\Model\Config\Source\Country');
        $countries = $countryHelper->toOptionArray(); 
        $countriesArray = array();

        foreach ($countries as $k => $coun) {
            if($k != 0){
                $countriesArray[$coun['label']] = $coun['value'];
            }
        }

        $CountryOfOriginManufacture = '';
        foreach ($countriesArray as $country => $code) {
            if($country == $countryName){
                $CountryOfOriginManufacture = $code;
            }
        }
        
        return $CountryOfOriginManufacture;
    }

    public function fileMovedAndDelete($file)
    {
        $fileEleArray = explode("/", $file);
        $fileName = end($fileEleArray);

        $source = $file; 
        $destination = $this->_dir->getRoot(). '/archive/'.$fileName;  

        if(!rename($source,$destination)){
            $this->_logger->addInfo('Permission Issue...');
        }else{
            $this->_logger->addInfo($fileName.' CSV files moved to archive folder sucessfully...');
        }
    }
}


