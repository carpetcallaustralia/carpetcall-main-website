<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Csv;
use Magento\Framework\App\Response\Http\FileFactory;
use Carpetcall\Export\Logger\Logger;

class CsvHelper extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

    /**
     * @var
     */
    protected $fileFactory;

    /**
     * @var protected $logger;
     */
    protected $logger;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList,
        Csv $csvProcessor,
        FileFactory $fileFactory,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->csvProcessor = $csvProcessor;
        $this->fileFactory = $fileFactory;
        $this->logger = $logger; 
    }

    public function createCsvFile($fileName, $data){

        $this->logger->addInfo("createCsvFile fileName: " . $fileName);
        // $this->logger->addInfo("createCsvFile data: " . print_r($data, true));
        $filePath = $this->directoryList->getPath(DirectoryList::VAR_EXPORT)
            . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . $fileName;
        $this->logger->addInfo("filePath: " . $filePath);

        $this->csvProcessor
            ->saveData(
                $filePath,
                $data
            );

        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }
}