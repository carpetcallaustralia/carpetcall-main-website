<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Config;
use Carpetcall\Export\Model\OrderExportFactory;
use Carpetcall\Export\Logger\Logger;

class Data extends AbstractHelper
{

    /**
     * @var protected $orderCollectionFactory;
     */
    protected $orderCollectionFactory;

    /**
     * @var protected $order;
     */
    protected $order;

    /**
     * @var Config
     */
    protected $_orderConfig;

    /**
     * @var protected $orderExport;
     */
    protected $orderExport;

    /**
     * @var protected $logger;
     */
    protected $logger;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param OrderInterface $order
     * @param Config $orderConfig
     * @param OrderExportFactory $orderExport
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        CollectionFactory $orderCollectionFactory,
        OrderInterface $order,
        Config $orderConfig,
        OrderExportFactory $orderExport,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->order = $order;
        $this->orderConfig = $orderConfig;
        $this->orderExport = $orderExport;
        $this->logger = $logger;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }

    public function getOrderCollection ($forShipment = false)
    {
        $orderCollection = $this->orderCollectionFactory->create()
        ->addAttributeToSelect(
            '*'
        )->addFieldToFilter(
            'status',
            ['in' => \Magento\Sales\Model\Order::STATE_PROCESSING]
        )->setOrder(
            'entity_id',
        );
        if ($forShipment) {
//            $from_date = date('Y-m-d 00:00:00', strtotime("-5 day", strtotime($to_date)));
            $to_date = date('Y-m-d 00:00:00');
            $orderCollection->addFieldToFilter(
                'created_at',
                ['lt' => $to_date]
            )->setPageSize(50);
        }
        return $orderCollection;
    }

    public function getOrder($incrementId)
    {
        $_order = $this->order->loadByIncrementId($incrementId);
        return $_order;
    }

    public function getCsvHeader ($type = 'all')
    {
        $csv_header = [
            'order' => [
                'order id',
                'business name',
                'first name',
                'last name',
                'email',
                'phone',
                'delivery address 1',
                'delivery address 2',
                'delivery zip',
                'delivery state',
                'delivery city',
                'delivery country',
                'billing business name',
                'billing first name',
                'billing last name',
                'billing email',
                'billing phone',
                'billing address 1',
                'billing address 2',
                'billing zip',
                'billing state',
                'billing city',
                'billing country',
                'delivery option',
                'store id',
                'store_state',
                'store address',
                'grand total',
                'code',
                'shipping cost',
                'payment method',
                'status',
                'order status',
                'cc name',
                'cc type',
                'cc date',
                'cc num',
                'cc ccv',
                'time stamp',
                'payment number',
                'bank reference',
                'summary code',
                'response code',
                'response text',
                'payment time',
                'payment reference',
                'invoice',
                'payer id',
                'payment date',
                'payment status',
                'payer status',
                'txn id',
                'payment type',
                'receiver id',
                'receipt id',
                'status code',
                'status description',
                'response description',
                'atl',
                'comment'
            ], 'line_item' => [
                'order id',
                'product code',
                'product name',
                'price',
                'quantity'
            ]
        ];

        if ($type != 'all' && array_key_exists($type, $csv_header)) {
            return $csv_header[$type];
        } else {
            return $csv_header;
        }
    }

    public function getRegionCode ($region = '', $character = false)
    {
        $code = '';
        if ($region != '') {
            $region = ucwords(strtolower($region));
            if ($region == 'Australian Capital Territory') {
                $code = 'ACT';
            } else if ($region == 'New South Wales') {
                $code = 'NSW';
            } else if ($region == 'Northern Territory') {
                $code = 'NT';
            } else if ($region == 'Queensland') {
                $code = 'QLD';
            } else if ($region == 'South Australia') {
                $code = 'SA';
            } else if ($region == 'Tasmania') {
                $code = 'TAS';
            } else if ($region == 'Victoria') {
                $code = 'VIC';
            } else if ($region == 'Western Australia') {
                $code = 'WA';
            }

            if ($character && $code != '') {
                $code = substr($code, 0, 1);
            }
        }
        return $code;
    }

    public function getCCType ($type = '')
    {
        $fullString = '';
        if ($type != '') {
            $type = strtoupper($type);
            if ($type == 'AE') {
                $fullString = 'American Express';
            } else if ($type == 'VI') {
                $fullString = 'Visa';
            } else if ($type == 'MC') {
                $fullString = 'MasterCard';
            } else if ($type == 'JCB') {
                $fullString = 'JCB';
            } else if ($type == 'DN') {
                $fullString = 'Diners';
            }
        }
        return $fullString;
    }

    public function getOrderCategory ($order_cat = '')
    {
        $order_category = '';
        if ($order_cat == 'RUGS' || $order_cat == 'ACCESSORIES') {
            // Accessory products need to be within the Rugs CSV, and no on their own CSV
            $order_category = 'rug';
        } else if ($order_cat == 'HARD') {
            $order_category = 'hard_flooring';
        } else if ($order_cat == 'BLINDS') {
            $order_category = 'blind';
        } else if ($order_cat == 'SHUTTERS') {
            $order_category = 'shutter';
        // } else if ($order_cat == 'ACCESSORIES') {
        //     $order_category = 'accessories';
        }

        return $order_category;
    }

    public function getCsvFileNames ($type, $report_export_time = '')
    {
        if ($report_export_time == '') {
            $report_export_time = date('YmdHi', strtotime());
        }

        $csvFileNames = [
            'detail' => 'OR',
            'item' => 'OL'
        ];

        if ($type == 'rug') {
            $csvFileNames['detail'] .= 'RUGS' . $report_export_time . '.csv';
            $csvFileNames['item'] .= 'RUGS' . $report_export_time . '.csv';
        } else if ($type == 'hard_flooring') {
            $csvFileNames['detail'] .= 'HARD' . $report_export_time . '.csv';
            $csvFileNames['item'] .= 'HARD' . $report_export_time . '.csv';
        } else if ($type == 'blind') {
            $csvFileNames['detail'] .= 'BLINDS' . $report_export_time . '.csv';
            $csvFileNames['item'] .= 'BLINDS' . $report_export_time . '.csv';
        } else if ($type == 'shutter') {
            $csvFileNames['detail'] .= 'SHUTTERS' . $report_export_time . '.csv';
            $csvFileNames['item'] .= 'SHUTTERS' . $report_export_time . '.csv';
        } else if ($type == 'accessories') {
            $csvFileNames['detail'] .= 'ACCESSORIES' . $report_export_time . '.csv';
            $csvFileNames['item'] .= 'ACCESSORIES' . $report_export_time . '.csv';
        } else {
            $csvFileNames['detail'] .= $report_export_time . '.csv';
            $csvFileNames['item'] .= $report_export_time . '.csv';
        }

        return $csvFileNames;
    }
}
