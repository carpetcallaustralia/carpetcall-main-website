<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface OrderExportRepositoryInterface
{

    /**
     * Save OrderExport
     * @param \Carpetcall\Export\Api\Data\OrderExportInterface $orderExport
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Carpetcall\Export\Api\Data\OrderExportInterface $orderExport
    );

    /**
     * Retrieve OrderExport
     * @param string $orderexportId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($orderexportId);

    /**
     * Retrieve OrderExport matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Carpetcall\Export\Api\Data\OrderExportSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete OrderExport
     * @param \Carpetcall\Export\Api\Data\OrderExportInterface $orderExport
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Carpetcall\Export\Api\Data\OrderExportInterface $orderExport
    );

    /**
     * Delete OrderExport by ID
     * @param string $orderexportId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($orderexportId);
}

