<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Api\Data;

interface OrderExportSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get OrderExport list.
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface[]
     */
    public function getItems();

    /**
     * Set order_id list.
     * @param \Carpetcall\Export\Api\Data\OrderExportInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

