<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Api\Data;

interface OrderExportInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PROCESSED = 'processed';
    const ORDEREXPORT_ID = 'orderexport_id';
    const ORDER_ID = 'order_id';
    const INCREMENT_ID = 'increment_id';

    /**
     * Get orderexport_id
     * @return string|null
     */
    public function getOrderexportId();

    /**
     * Set orderexport_id
     * @param string $orderexportId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setOrderexportId($orderexportId);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setOrderId($orderId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Carpetcall\Export\Api\Data\OrderExportExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Carpetcall\Export\Api\Data\OrderExportExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Carpetcall\Export\Api\Data\OrderExportExtensionInterface $extensionAttributes
    );

    /**
     * Get increment_id
     * @return string|null
     */
    public function getIncrementId();

    /**
     * Set increment_id
     * @param string $incrementId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setIncrementId($incrementId);

    /**
     * Get processed
     * @return string|null
     */
    public function getProcessed();

    /**
     * Set processed
     * @param string $processed
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setProcessed($processed);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setCreatedAt($createdAt);
}

