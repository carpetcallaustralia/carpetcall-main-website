<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Model\ResourceModel\OrderExport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'orderexport_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Carpetcall\Export\Model\OrderExport::class,
            \Carpetcall\Export\Model\ResourceModel\OrderExport::class
        );
    }
}

