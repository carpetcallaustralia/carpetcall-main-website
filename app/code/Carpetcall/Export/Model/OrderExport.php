<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Model;

use Carpetcall\Export\Api\Data\OrderExportInterface;
use Carpetcall\Export\Api\Data\OrderExportInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class OrderExport extends \Magento\Framework\Model\AbstractModel
{

    protected $orderexportDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'carpetcall_export_orderexport';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param OrderExportInterfaceFactory $orderexportDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Carpetcall\Export\Model\ResourceModel\OrderExport $resource
     * @param \Carpetcall\Export\Model\ResourceModel\OrderExport\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        OrderExportInterfaceFactory $orderexportDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Carpetcall\Export\Model\ResourceModel\OrderExport $resource,
        \Carpetcall\Export\Model\ResourceModel\OrderExport\Collection $resourceCollection,
        array $data = []
    ) {
        $this->orderexportDataFactory = $orderexportDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve orderexport model with orderexport data
     * @return OrderExportInterface
     */
    public function getDataModel()
    {
        $orderexportData = $this->getData();
        
        $orderexportDataObject = $this->orderexportDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $orderexportDataObject,
            $orderexportData,
            OrderExportInterface::class
        );
        
        return $orderexportDataObject;
    }
}

