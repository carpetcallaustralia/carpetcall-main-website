<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Model\Data;

use Carpetcall\Export\Api\Data\OrderExportInterface;

class OrderExport extends \Magento\Framework\Api\AbstractExtensibleObject implements OrderExportInterface
{

    /**
     * Get orderexport_id
     * @return string|null
     */
    public function getOrderexportId()
    {
        return $this->_get(self::ORDEREXPORT_ID);
    }

    /**
     * Set orderexport_id
     * @param string $orderexportId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setOrderexportId($orderexportId)
    {
        return $this->setData(self::ORDEREXPORT_ID, $orderexportId);
    }

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * Set order_id
     * @param string $orderId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Carpetcall\Export\Api\Data\OrderExportExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Carpetcall\Export\Api\Data\OrderExportExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Carpetcall\Export\Api\Data\OrderExportExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get increment_id
     * @return string|null
     */
    public function getIncrementId()
    {
        return $this->_get(self::INCREMENT_ID);
    }

    /**
     * Set increment_id
     * @param string $incrementId
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * Get processed
     * @return string|null
     */
    public function getProcessed()
    {
        return $this->_get(self::PROCESSED);
    }

    /**
     * Set processed
     * @param string $processed
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setProcessed($processed)
    {
        return $this->setData(self::PROCESSED, $processed);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Carpetcall\Export\Api\Data\OrderExportInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}

