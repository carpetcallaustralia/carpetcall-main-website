<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Model;

use Carpetcall\Export\Api\Data\OrderExportInterfaceFactory;
use Carpetcall\Export\Api\Data\OrderExportSearchResultsInterfaceFactory;
use Carpetcall\Export\Api\OrderExportRepositoryInterface;
use Carpetcall\Export\Model\ResourceModel\OrderExport as ResourceOrderExport;
use Carpetcall\Export\Model\ResourceModel\OrderExport\CollectionFactory as OrderExportCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class OrderExportRepository implements OrderExportRepositoryInterface
{

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $orderExportCollectionFactory;

    protected $dataOrderExportFactory;

    protected $orderExportFactory;

    private $storeManager;


    /**
     * @param ResourceOrderExport $resource
     * @param OrderExportFactory $orderExportFactory
     * @param OrderExportInterfaceFactory $dataOrderExportFactory
     * @param OrderExportCollectionFactory $orderExportCollectionFactory
     * @param OrderExportSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceOrderExport $resource,
        OrderExportFactory $orderExportFactory,
        OrderExportInterfaceFactory $dataOrderExportFactory,
        OrderExportCollectionFactory $orderExportCollectionFactory,
        OrderExportSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->orderExportFactory = $orderExportFactory;
        $this->orderExportCollectionFactory = $orderExportCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataOrderExportFactory = $dataOrderExportFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Carpetcall\Export\Api\Data\OrderExportInterface $orderExport
    ) {
        /* if (empty($orderExport->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $orderExport->setStoreId($storeId);
        } */
        
        $orderExportData = $this->extensibleDataObjectConverter->toNestedArray(
            $orderExport,
            [],
            \Carpetcall\Export\Api\Data\OrderExportInterface::class
        );
        
        $orderExportModel = $this->orderExportFactory->create()->setData($orderExportData);
        
        try {
            $this->resource->save($orderExportModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the orderExport: %1',
                $exception->getMessage()
            ));
        }
        return $orderExportModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($orderExportId)
    {
        $orderExport = $this->orderExportFactory->create();
        $this->resource->load($orderExport, $orderExportId);
        if (!$orderExport->getId()) {
            throw new NoSuchEntityException(__('OrderExport with id "%1" does not exist.', $orderExportId));
        }
        return $orderExport->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->orderExportCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Carpetcall\Export\Api\Data\OrderExportInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Carpetcall\Export\Api\Data\OrderExportInterface $orderExport
    ) {
        try {
            $orderExportModel = $this->orderExportFactory->create();
            $this->resource->load($orderExportModel, $orderExport->getOrderexportId());
            $this->resource->delete($orderExportModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the OrderExport: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($orderExportId)
    {
        return $this->delete($this->get($orderExportId));
    }
}

