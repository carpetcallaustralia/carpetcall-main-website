<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Cron;

use Carpetcall\Export\Model\OrderExportFactory;
use Carpetcall\Export\Logger\Logger;
use Carpetcall\Export\Helper\Data;
use Carpetcall\Export\Helper\CsvHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Model\Convert\Order as ConvertOrder;

class ShipOrders
{
    protected const ACCESSORIES_CATEGORY_ID = 11;
    protected const HARD_FLOORING_CATEGORY_ID = 8;
    protected const ORDER_CUSTOM_COMMENT_TEXT = 'Order shipped successfully';


    /**
     * @var protected $logger;
     */
    protected $logger;

    /**
     * @var protected $helper;
     */
    protected $helper;

    /**
     * @var protected $csvHelper;
     */
    protected $csvHelper;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ConvertOrder
     */
    protected $convertOrder;

    /**
     * Constructor
     *
     * @param Logger $logger
     * @param Data $helper
     * @param CsvHelper $csvHelper
     * @param OrderExportFactory $orderExport
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Logger $logger,
        Data $helper,
        CsvHelper $csvHelper,
        ConvertOrder $convertOrder
    ) {
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->csvHelper = $csvHelper;
        $this->convertOrder = $convertOrder;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        try {
            $this->logger->addInfo("Order Ship Started at: " . date('Y-m-d H:i:s a'));
            // order collection
            $orders = $this->helper->getOrderCollection(true);

            $total_orders = count($orders);
            $this->logger->addInfo('Found ' . $total_orders . ' orders for ship.');
            if ($total_orders > 0) {
                foreach($orders as $order) {
                    $this->logger->addInfo('Creating Shipment for  Order# ' . $order->getIncrementId() . ' (' . $order->getId() . ')');
                    // $this->logger->addInfo('Order Log: ' . print_r($order->getData(), true));
                    $orderObj = $this->helper->getOrder($order->getIncrementId());
                    if ($orderObj === null) {
                        $this->logger->addInfo('Order not loaded from the system: ' . $order->getIncrementId());
                        return;
                    }
                    // create shipment
                    $this->createShipment($orderObj);
                    // add comment
                    $orderObj->addStatusToHistory($orderObj->getStatus(),  self::ORDER_CUSTOM_COMMENT_TEXT);
                    $orderObj->save();
                }
            }
            $this->logger->addInfo("Order Shipment Finished at: " . date('Y-m-d H:i:s a'));
            return true;
        } catch (Exception $e) {
            $this->logger->addCritical("Error occurred: " . $e->getMessage());
            return false;
        }
    }

    /**
     * Creates a new shipment for the specified order.
     *
     * @param \Magento\Sales\Model\Order $order
     */
    protected function createShipment($order)
    {
        // check if it's possible to ship the items
        if ($order->canShip()) {
            $shipment = $this->convertOrder->toShipment($order);
            foreach ($order->getAllItems() as $orderItem) {
                if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                    continue;
                }
                $qtyShipped = $orderItem->getQtyToShip();
                $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
                $shipment->addItem($shipmentItem);
            }
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            try {
                $shipment->save();
                $shipment->getOrder()->save();
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
        }
    }
}

