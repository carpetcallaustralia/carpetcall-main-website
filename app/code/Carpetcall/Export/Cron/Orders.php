<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\Export\Cron;

use Carpetcall\Export\Model\OrderExportFactory;
use Carpetcall\Export\Logger\Logger;
use Carpetcall\Export\Helper\Data;
use Carpetcall\Export\Helper\CsvHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Model\Convert\Order as ConvertOrder;

class Orders
{
    protected const ACCESSORIES_CATEGORY_ID = 11;
    protected const HARD_FLOORING_CATEGORY_ID = 8;
    protected const ORDER_CUSTOM_COMMENT_TEXT = 'Order exported successfully';


    /**
     * @var protected $logger;
     */
    protected $logger;

    /**
     * @var protected $helper;
     */
    protected $helper;

    /**
     * @var protected $csvHelper;
     */
    protected $csvHelper;

    /**
     * @var protected $orderExport;
     */
    protected $orderExport;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var ConvertOrder
     */
    protected $convertOrder;

    /**
     * Constructor
     *
     * @param Logger $logger
     * @param Data $helper
     * @param CsvHelper $csvHelper
     * @param OrderExportFactory $orderExport
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Logger $logger,
        Data $helper,
        CsvHelper $csvHelper,
        OrderExportFactory $orderExport,
        ConvertOrder $convertOrder
    ) {
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->csvHelper = $csvHelper;
        $this->orderExport = $orderExport;
        $this->convertOrder = $convertOrder;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        try {
            $this->logger->addInfo("Order Export Started at: " . date('Y-m-d H:i:s a'));
            $orderExportModel = $this->orderExport->create();
            $orderExportDetails = [];
            // order collection
            $orders = $this->helper->getOrderCollection();

            $total_orders = count($orders);
            $this->logger->addInfo('Found ' . $total_orders . ' orders for export.');
            if ($total_orders > 0) {
                $order_export_rows = [
                    'rug' => [],
                    'hard_flooring' => [],
                    'blind' => [],
                    'shutter' => [],
                    'accessories' => []
                ];
                $order_export_item_rows = [
                    'rug' => [],
                    'hard_flooring' => [],
                    'blind' => [],
                    'shutter' => [],
                    'accessories' => []
                ];
                $csvHeader = $this->helper->getCsvHeader();
                $order_export_rows['rug'][] = $csvHeader['order'];
                $order_export_rows['hard_flooring'][] = $csvHeader['order'];
                $order_export_rows['blind'][] = $csvHeader['order'];
                $order_export_rows['shutter'][] = $csvHeader['order'];
                $order_export_rows['accessories'][] = $csvHeader['order'];
                $order_export_item_rows['rug'][] = $csvHeader['line_item'];
                $order_export_item_rows['hard_flooring'][] = $csvHeader['line_item'];
                $order_export_item_rows['blind'][] = $csvHeader['line_item'];
                $order_export_item_rows['shutter'][] = $csvHeader['line_item'];
                $order_export_item_rows['accessories'][] = $csvHeader['line_item'];

                foreach($orders as $order) {
                    $this->logger->addInfo('Exporting Order# ' . $order->getIncrementId() . ' (' . $order->getId() . ')');
                    // $this->logger->addInfo('Order Log: ' . print_r($order->getData(), true));
                    $payment = $order->getPayment();
                    $paymentMethod = $payment->getMethodInstance()->getTitle();
                    $paymentAdditionalInfo = $payment->getAdditionalInformation();
                    // $this->logger->addInfo('Order payment Log: ' . print_r($payment->getMethodInstance()->getTitle(), true));
                    // $this->logger->addInfo('Order paymentMethod Log: ' . json_encode($payment->getData()));
                    // $this->logger->addInfo('Order paymentAdditionalInfo Log: ' . print_r($paymentAdditionalInfo, true));
                    $shippingAddress = $order->getShippingAddress();
                    $billingAddress = $order->getBillingAddress();
                    $billingAddressStreet = $billingAddress->getStreet();
                    $shippingAddressStreet = $shippingAddress->getStreet();
                    // $this->logger->addInfo('Order shippingAddress Log: ' . print_r($shippingAddress->getData(), true));
                    // $this->logger->addInfo('Order billingAddress Log: ' . print_r($billingAddress->getData(), true));
                    $shipping_method = 'pickup';
                    if ($order->getShippingMethod() == 'flatrate_flatrate') {
                        $shipping_method = 'deliver';
                    }
                    $items = $order->getAllVisibleItems();
                    $this->logger->addInfo('Order Items Count: ' . count($items));
                    $order_cat = $order->getOrderCat();
                    $order_category = $this->helper->getOrderCategory($order_cat);
                    $shipping_amount = $order->getShippingAmount();
                    $this->logger->addInfo('order_category: ' . $order_category);

                    // payment info
                    $cc_name = '';
                    $cc_type = '';
                    $cc_expiry_date = '';
                    $cc_card_last4 = '';
                    $cc_cvv = '';
                    $payment_number = '';
                    $bank_reference = '';
                    $summary_code = '';
                    $response_code = '';
                    $response_text = '';
                    $auth_id = '';
                    $payment_status = '';
                    $payer_status = '';
                    $txn_id = $payment->getLastTransId();
                    $payment_type = '';
                    $receiver_id = '';
                    $receipt_id = '';
                    $status_code = '';
                    $status_description = '';
                    $response_description = '';
                    // store
                    $store_id = '';
                    $store_state = '';
                    $store_address = '';
                    if ($shipping_method == 'pickup') {
                        $shipping_region_full = ($shippingAddress->getRegion() != null) ? explode(' (Store ID: ', $shippingAddress->getRegion()) : [];
                        if (count($shipping_region_full) > 0) {
                            $shipping_region = $shipping_region_full[0];
                            // $this->logger->addInfo('Order shipping_region_full Log: ' . print_r($shipping_region_full, true));
                            $store_locator_id = (count($shipping_region_full) > 1) ? (int) str_replace(')', '', $shipping_region_full[1]) : 0;
                            $store_id = ($order_cat != 'HARD' && $store_locator_id > 0) ? $store_locator_id : '';
                            $store_state = $this->helper->getRegionCode($shipping_region, true);
                            $store_address = str_replace(' (Store ID: ' . $store_locator_id . ')', '', implode(', ', $shippingAddressStreet));
                        }
                    }

                    if (stripos($paymentMethod, 'SecurePay') !== false) {
                        $cc_type = $this->helper->getCCType($paymentAdditionalInfo['cc_type']);
                        $cc_expiry_date = $paymentAdditionalInfo['mpsecurepay_expiry_date'];
                        $cc_card_last4 = $paymentAdditionalInfo['mpsecurepay_card_last4'];
                        $auth_id = (array_key_exists('mpsecurepay_auth_id', $paymentAdditionalInfo)) ? $paymentAdditionalInfo['mpsecurepay_auth_id'] : '';
                        $receipt_id = (array_key_exists('mpsecurepay_order_no', $paymentAdditionalInfo)) ? $paymentAdditionalInfo['mpsecurepay_order_no'] : '';
                    } else if (stripos($paymentMethod, 'PayPal') !== false) {
                        $auth_id = $paymentAdditionalInfo['paypal_payer_id'];
                        $payment_status = isset($paymentAdditionalInfo['paypal_payment_status']) ? $paymentAdditionalInfo['paypal_payment_status'] : '';
                        $payer_status = $paymentAdditionalInfo['paypal_payer_status'];
                        $receipt_id = $paymentAdditionalInfo['paypal_express_checkout_token'];
                    }
                    $order_export_rows[$order_category][] = [
                        $order->getIncrementId(),
                        $shippingAddress->getCompany(),
                        $shippingAddress->getFirstname(),
                        $shippingAddress->getLastname(),
                        $billingAddress->getEmail(),
                        $billingAddress->getTelephone(),
                        (isset($shippingAddressStreet[0])) ? $shippingAddressStreet[0] : '',
                        (isset($shippingAddressStreet[1])) ? $shippingAddressStreet[1] : '',
                        $shippingAddress->getPostcode(),
                        $this->helper->getRegionCode($shippingAddress->getRegion()),
                        $shippingAddress->getCity(),
                        'Australia',
                        $billingAddress->getCompany(),
                        $billingAddress->getFirstname(),
                        $billingAddress->getLastname(),
                        $billingAddress->getEmail(),
                        $billingAddress->getTelephone(),
                        (isset($billingAddressStreet[0])) ? $billingAddressStreet[0] : '',
                        (isset($billingAddressStreet[1])) ? $billingAddressStreet[1] : '',
                        $billingAddress->getPostcode(),
                        $this->helper->getRegionCode($billingAddress->getRegion()),
                        $billingAddress->getCity(),
                        'Australia',
                        $shipping_method,
                        $store_id,
                        $store_state,
                        $store_address,
                        $order->getGrandTotal(),
                        '',
                        $shipping_amount,
                        $paymentMethod,
                        '',
                        $order->getStatus(),
                        $cc_name,
                        $cc_type,
                        $cc_expiry_date,
                        $cc_card_last4,
                        $cc_cvv,
                        date('d/M/Y, H:i:s',strtotime($order->getCreatedAt())),
                        $payment_number,
                        $bank_reference,
                        $summary_code,
                        $response_code,
                        $response_text,
                        date('Y-m-d, H:i:s',strtotime($order->getCreatedAt())),
                        $order->getId(),
                        '',
                        $auth_id,
                        date('d/M/Y, H:i:s',strtotime($order->getCreatedAt())),
                        $payment_status,
                        $payer_status,
                        $txn_id,
                        $payment_type,
                        $receiver_id,
                        $receipt_id,
                        $status_code,
                        $status_description,
                        $response_description,
                        ($order->getTermsAndConditions()) ? 1 : 0,
                        $order->getOscOrderComment() // COMMENT
                    ];
                    foreach ($items as $item) {
                        // $this->logger->addInfo('Order Items Log: ' . json_encode($item->getData()));
                        // $this->logger->addInfo('Order product options Log: ' . $item->getProductOptions());
                        $itemProductId = $item->getProductId();
                        $product =  $this->productRepository->getById($itemProductId);
                        $categoriesIds = $product->getCategoryIds();
                        // if (array_intersect($categoriesIds, [$this->RUGS_CATEGORY_ID, $this->ACCESSORIES_CATEGORY_ID])) {
                            // line item
                            $item_sku = $item->getSku();
                            // if($item_sku == "STN.0002.01.001"){
                            //     $item_sku = "STN.0001.01.005";
                            // }
                            // if($item_sku == "CLK.0005.01.005"){
                            //     $item_sku = "CLK.0001.01.005";
                            // }

                            $item_name = $item->getName();
                            if ($order_cat == 'HARD') {
                                $item_name = $item_sku;
                            // } else if ($order_cat == 'ACCESSORIES') {
                            } else if (in_array(self::ACCESSORIES_CATEGORY_ID, $categoriesIds)) {
                                $item_name = '';
                            }

                            $order_export_item_rows[$order_category][] = [
                                $order->getIncrementId(),
                                $item_sku,
                                $item_name,
                                $item->getRowTotalInclTax(),
                                $item->getQtyOrdered()
                            ];
                        // }
                    }

                    // shipping line item
                    if ($shipping_amount == 60) {
                        $order_export_item_rows[$order_category][] = [
                            $order->getIncrementId(),
                            'ROL.0001.01.001',
                            'Shipping',
                            $shipping_amount,
                            1
                        ];
                    } else if ($shipping_amount == 14.95) {
                        $order_export_item_rows[$order_category][] = [
                            $order->getIncrementId(),
                            'ROL.0001.01.008',
                            'Shipping',
                            $shipping_amount,
                            1
                        ];
                    } else if ($shipping_amount == 4.95) {
                        $order_export_item_rows[$order_category][] = [
                            $order->getIncrementId(),
                            'ROL.0001.01.009',
                            'Shipping',
                            $shipping_amount,
                            1
                        ];
                    }

                    $orderExportDetails[$order->getIncrementId()] = [
                        'order_id' => $order->getId(),
                        'increment_id' => $order->getIncrementId(),
                        'processed' => 1
                    ];
                }
                // $this->logger->addInfo('Order Log: ' . print_r($order_export_rows, true));
                // $this->logger->addInfo('Order Items Log: ' . print_r($order_export_item_rows, true));


                // csv
                $this->logger->addInfo('Start creating Report CSV files.');
                $report_export_time = date('YmdHi');
                $this->logger->addInfo('report_export_time: ' . $report_export_time);
                foreach ($order_export_rows as $type => $_orders) {
                    $this->logger->addInfo('order_export_rows (type): ' . $type);
                    $this->logger->addInfo('order_export_rows (_orders): ' . (count($_orders) - 1));
                    if (count($_orders) > 1) {
                        $export_file_names = $this->helper->getCsvFileNames($type, $report_export_time);
                        $this->logger->addInfo('export_file_names: ' . print_r($export_file_names, true));
                        // OR
                        $this->logger->addInfo('Order CSV Log: ' . print_r($export_file_names['detail'], true));
                        $this->logger->addInfo('Creating ' . strtoupper($type) . ' Order Details CSV report (' . $export_file_names['detail'] . ')');
                        $this->csvHelper->createCsvFile($export_file_names['detail'], $_orders);
                        $this->logger->addInfo('Finished creating ' . strtoupper($type) . ' Order Details CSV report.');

                        // OL
                        $this->logger->addInfo('Order items CSV Log: ' . print_r($export_file_names['item'], true));
                        $this->logger->addInfo('Creating ' . strtoupper($type) . ' Order List CSV report (' . $export_file_names['item'] . ')');
                        $this->csvHelper->createCsvFile($export_file_names['item'], $order_export_item_rows[$type]);
                        $this->logger->addInfo('Finished creating ' . strtoupper($type) . ' Order List CSV report.');

                        foreach ($_orders as $_order) {
                            if ((int) $_order[0] > 0 && array_key_exists($_order[0], $orderExportDetails)) {
                                $orderObj = $this->helper->getOrder($_order[0]);
                                if ($orderObj === null) {
                                    $this->logger->addInfo('Order not loaded from the system: ' . $_order[0]);
                                    return;
                                }
                                // create shipment
                                $this->createShipment($orderObj);
                                // add comment
                                $orderObj->addStatusToHistory($orderObj->getStatus(),  self::ORDER_CUSTOM_COMMENT_TEXT);
                                $orderObj->save();
                                $orderExportModel->setData($orderExportDetails[$_order[0]]);
                                $orderExportModel->save();
                            }
                        }
                    }
                }
                $this->logger->addInfo("Order Export Finished at: " . date('Y-m-d H:i:s a'));
            }
            return true;
        } catch (Exception $e) {
            $this->logger->addCritical("Error occurred: " . $e->getMessage());
            return false;
        }
    }

    /**
     * Creates a new shipment for the specified order.
     *
     * @param \Magento\Sales\Model\Order $order
     */
    protected function createShipment($order)
    {
        // check if it's possible to ship the items
        if ($order->canShip()) {
            $shipment = $this->convertOrder->toShipment($order);
            foreach ($order->getAllItems() as $orderItem) {
                if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                    continue;
                }
                $qtyShipped = $orderItem->getQtyToShip();
                $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
                $shipment->addItem($shipmentItem);
            }
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            try {
                $shipment->save();
                $shipment->getOrder()->save();
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
        }
    }
}

