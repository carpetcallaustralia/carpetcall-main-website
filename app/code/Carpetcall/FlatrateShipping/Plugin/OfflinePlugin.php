<?php
namespace Carpetcall\FlatrateShipping\Plugin;

use Magento\OfflineShipping\Model\Carrier\Flatrate as Subject;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;


class OfflinePlugin
{

	/**
    * @var string
    */
   protected $_code = 'flatrate';

   /**
    * @var bool
    */
   protected $_isFixed = true;

   /**
    * @var \Magento\Shipping\Model\Rate\ResultFactory
    */
   protected $_rateResultFactory;

   /**
    * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
    */
   protected $_rateMethodFactory;
    /**
    * @var ItemPriceCalculator
    */
   private $itemPriceCalculator;

   /** @var ProductRepository */
    private $productRepository;

    /** @var CategoryRepository */
    private $categoryRepository;

    /** @var StoreManager */
    private $storeManager;

   public function __construct(
       \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
       \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
       \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
       \Magento\OfflineShipping\Model\Carrier\Flatrate\ItemPriceCalculator $itemPriceCalculator,
       ProductRepositoryInterface $productRepository,
       CategoryRepositoryInterface $categoryRepository,
       StoreManagerInterface $storeManager
   ) {
       $this->_rateResultFactory = $rateResultFactory;
       $this->_rateMethodFactory = $rateMethodFactory;
       $this->_rateErrorFactory=$rateErrorFactory;
       $this->itemPriceCalculator=$itemPriceCalculator;
       $this->productRepository = $productRepository;
       $this->categoryRepository = $categoryRepository;
       $this->storeManager = $storeManager;
   }

    public function getCategoryRepository() {
        $category = $this->categoryRepository;
        return $category;
    }

    public function getStoreId() {
        $storeId = $this->storeManager->getStore()->getId();
        return $storeId;
    }

    public function getProductRepository() {
        $product = $this->productRepository;
        return $product;
    }

   public function aroundCollectRates(Subject $subject,\Closure $proceed,RateRequest $request)
   {
        if (!$subject->getConfigFlag('active')) {
           return false;
        }
        $freeBoxes = $subject->getFreeBoxesCount($request);
        $subject->setFreeBoxes($freeBoxes);

        /** @var Result $result */
        $result = $this->_rateResultFactory->create();

        $shippingPrice = $this->getShippingPrice($subject,$request, $freeBoxes);
        //  $categories = [4, 6];
        $cat_name = [];
        $item_wise_categories = [];
        // $count = 0;
        $item_rugs = 0;
        $item_accessories = 0;
        $accessories_delivery_charge = 0;
        $item_blinds = 0;
        $item_shutters = 0;
        $item_accessories_with_other_category = 0;
        $item_accessories_price = [];
        $cart_items = $request->getAllItems();
        $oversized_rug = false;
        if (count($cart_items) > 0) {
          foreach ($cart_items as $item) {
            $itemProductId = $item->getProductId();
            $product =  $this->getProductRepository()->getById($itemProductId);
            $categoriesIds = $product->getCategoryIds();
            $rugs_item = false;
            foreach ($categoriesIds as $category_id) {
              $_cat = $this->getCategoryRepository()->get($category_id, $this->getStoreId());
              $cat_name[] = $_cat->getName();
              $item_wise_categories[$item->getId()][] = $_cat->getName();
              // if (($_cat->getName() == 'Rugs') || ($_cat->getName() == 'Rugs Clearance')) {
              // 	$count = $count + $item->getQty();
              // }
              //echo $_cat->getName();
              // Restricted flat rate for HF & carpet
              if ($_cat->getName() == 'Hard Flooring' || $_cat->getName() == 'Carpet') {
                return false;
              }
              if ($_cat->getName() == 'Accessories') {
                $item_accessories += 1;
                $item_accessories_price[$item->getId()] = explode(' | $', $product->getAttributeText('accessories_delivery_sku'));
                $accessories_delivery_charge = (is_numeric($item_accessories_price[$item->getId()][1]) && floatval($item_accessories_price[$item->getId()][1]) > $accessories_delivery_charge) ? floatval($item_accessories_price[$item->getId()][1]) : $accessories_delivery_charge;
              }
              if ($_cat->getName() == 'Blinds') {
                $item_accessories_with_other_category += 1;
                $item_blinds += 1;
              } 
              if ($_cat->getName() == 'Shutters') {
                $item_accessories_with_other_category += 1;
                $item_shutters += 1;
              } 
              if ($_cat->getName() == 'Rugs' || $_cat->getName() == 'Rugs Clearance') {
                $rugs_item = true; // check if current item is Rugs
                $item_accessories_with_other_category += 1;
                $item_rugs += 1;
              }
            }

            if ($rugs_item && $product->getTypeId() == 'simple') {
              // $simp_productId = $option->getProduct()->getId();
              // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
              // $simp_product = $objectManager->create('Magento\Catalog\Model\Product')->load($simp_productId);
              // //echo $simp_product->getCustomAttribute('size')->getValue();
              // $attr = $simp_product->getResource()->getAttribute('size');
              // if ($attr->usesSource()) {
              //   $optionText = $attr->getSource()->getOptionText($simp_product->getCustomAttribute('size')->getValue());
              //   // Start - Code to get value of size before x (e.g. 250cm x 270 cm)
              //   $first = strtok($optionText, 'x');
              //   $width = filter_var($first, FILTER_SANITIZE_NUMBER_INT);
              //   if ($width >= 200) {
              //     $rug_width_grt_then_200 = 1;
              //   }
              // }
              $size = $product->getAttributeText('size');
              $size_tokenize_width = strtok($size, 'x');
              $width = filter_var($size_tokenize_width, FILTER_SANITIZE_NUMBER_INT);
              if ($width >= 220) {
                $oversized_rug = true;
              }
            }
            //$productIds[] = $productId;
          }
        }
          
        // Shipping charge logic
        // ===================
        // Rugs ---
        // Free for rugs under 2.2m
        // If rug over 2.2m wide, it is $60, (shipping code ROL.0001.01.001)
        // IF Rugs width > 200 cm then $60(Default charge from Admin)
        // 
        // Accessories ---
        // each accessory has a charge attached in the form of a code (delivery SKU)
        // ROL.0001.01.009 | $4.95
        // ROL.0001.01.008 | $14.95
        // If there are multiple accessories, select the more expensive charge
        // 
        // If Acc + Rugs then FREE, Acc + HF then FREE
        // 
        // Custom Blinds and Shutters
        // $29 per order
        // Free if the order is over $450

        $applied_shipping = '';
        $order_subtotal_after_discount = $request->getPackageValueWithDiscount();
        if ($item_rugs > 0) {
          if (!$oversized_rug) {
            $shippingPrice = 0.00;
          } else {
            $applied_shipping = 'Rugs';
          }
        } else if ($item_blinds > 0) {
          if ($order_subtotal_after_discount >= 450) {
            $shippingPrice = 0.00;
          } else {
            $applied_shipping = 'Blinds';
            $shippingPrice = 29.00;
          }
        } else if ($item_shutters > 0) {
          if ($order_subtotal_after_discount >= 450) {
            $shippingPrice = 0.00;
          } else {
            $applied_shipping = 'Shutters';
            $shippingPrice = 29.00;
          }
        } else if ($item_accessories > 0 && $item_accessories_with_other_category == 0) {
          $applied_shipping = 'Accessories';
          $shippingPrice = $accessories_delivery_charge;
        } else {
          $shippingPrice = 0.00;
        }

        // if ((count(array_unique($cat_name)) === 1 && end($cat_name) === 'Accessories') && (count($request->getAllItems()) == 1)) {
        //   //echo "same";
        //   $shippingPrice = '4.95';
        // }else{
        //   //echo "diff";
        //   if (in_array("Rugs", $cat_name, TRUE) || in_array("Rugs Clearance", $cat_name, TRUE)) :
        //   //$shippingPrice = $shippingPrice * $count;
            
        //     if (!empty($rug_width_grt_then_200)): 
        //       //echo "string";  die();
        //     else: 
        //       $shippingPrice = '0.00';  
        //     endif;
        //   else: 
        //     $shippingPrice = '0.00';
        //   endif;
        // }
        // die();
        //echo $count;die();

       	if ($shippingPrice !== false) {
           $method = $this->createResultMethod($shippingPrice,$subject,$request,$applied_shipping);
           $result->append($method);
       	}

        $rate = $result->getRatesByCarrier('flatrate')[0];
        if ($rate->getPrice() && $request->getFreeShipping()) {
            $rate->setPrice(0);
        }

       	return $result;

   }


   private function createResultMethod($shippingPrice,$subject,$request,$applied_shipping)
   {
       /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
       $method = $this->_rateMethodFactory->create();

       $method->setCarrier('flatrate');
       $method->setCarrierTitle($subject->getConfigData('title') . ($applied_shipping != '' ? ' (' . $applied_shipping . ')' : ''));

       if ($shippingPrice == 60) {
          $method->setMethod('flatrate');
          $method->setMethodTitle('Oversized Shipping Australia Wide');
       } else {
          $method->setMethod('flatrate');
          $method->setMethodTitle($subject->getConfigData('name'));
       }

       $method->setPrice($shippingPrice);
       $method->setCost($shippingPrice);
       return $method;
   } 

   private function getShippingPrice($subject,$request, $freeBoxes)
   {
       $shippingPrice = false;

       $configPrice = $subject->getConfigData('price');
       if ($subject->getConfigData('type') === 'O') {
           // per order
           $shippingPrice = $this->itemPriceCalculator->getShippingPricePerOrder($request, $configPrice, $freeBoxes);
       } elseif ($subject->getConfigData('type') === 'I') {
           // per item
           $shippingPrice = $this->itemPriceCalculator->getShippingPricePerItem($request, $configPrice, $freeBoxes);
         // per vendor
       }

       $shippingPrice = $subject->getFinalPriceWithHandlingFee($shippingPrice);

       if ($shippingPrice !== false && $request->getPackageQty() == $freeBoxes) {
           $shippingPrice = '0.00';
       }
       return $shippingPrice;
   }

}