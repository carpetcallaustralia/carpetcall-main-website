<?php
namespace Carpetcall\CompareProductsLimit\Block;
use Magento\Framework\ObjectManagerInterface;
class Compareproduct extends \Magento\Framework\View\Element\Template
{
	protected $objectManager;
    public function __construct( 
        ObjectManagerInterface $objectManager
    ) {
                $this->objectManager = $objectManager;
    }
    
    public function getMediaUrl(){

        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return $media_dir;
    }
}