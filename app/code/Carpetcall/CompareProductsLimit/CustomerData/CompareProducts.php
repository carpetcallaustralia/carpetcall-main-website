<?php

namespace Carpetcall\CompareProductsLimit\CustomerData;

class CompareProducts extends \Magento\Catalog\CustomerData\CompareProducts
{   
    public function getSectionData()
    {
        $count = $this->helper->getItemCount();
        return [
            'count' => $count,
            'countCaption' => $count == 1 ? __('You have 1 item in your Carpet Comparison list.') : __('You have %1 items in your Carpet Comparison list.', $count),
            'listUrl' => $this->helper->getListUrl(),
            'items' => $count ? $this->getItems() : [],
        ];
    }
}