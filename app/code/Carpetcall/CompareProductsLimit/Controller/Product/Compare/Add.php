<?php

namespace Carpetcall\CompareProductsLimit\Controller\Product\Compare;

class Add extends \Magento\Catalog\Controller\Product\Compare\Add
{
	/**
	 * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		$count = $this->_objectManager->get(\Magento\Catalog\Helper\Product\Compare::class)->getItemCount();
        if($count >= 3) {
        $this->messageManager->addErrorMessage(
            'You can add the compared products under 3 item(s)'
        );

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setRefererOrBaseUrl();
      }
		// Do your stuff here
		return parent::execute();
	}
}