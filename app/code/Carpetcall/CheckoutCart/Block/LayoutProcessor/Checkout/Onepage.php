<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Carpetcall\CheckoutCart\Block\LayoutProcessor\Checkout;

/**
 * Checkout layout processor
 */
class Onepage implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        if ($this->isCaptchaEnabledCheckout()) {
            $recaptchaConfig = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']
            ['children']['payments-list']['children']['before-place-order']['children']['recaptcha'];
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']
            ['children']['payments-list']['children']['before-place-order']['children']['recaptcha'] = $this->getRecaptchaConfig($recaptchaConfig);
        }
        return $jsLayout;
    }

    /**
     * @return bool
     */
    private function isCaptchaEnabledCheckout() {
        return $this->scopeConfig->getValue('recaptcha_frontend/type_for/checkout') ?? false;
    }

    /**
     * @return array
     */
    private function getRecaptchaConfig($recaptchaConfig) {
        $recaptchaConfig['siteId'] = $this->scopeConfig->getValue('recaptcha_frontend/type_recaptcha/public_key');
        $recaptchaConfig['theme'] = $this->scopeConfig->getValue('recaptcha_frontend/type_recaptcha/theme');
        $recaptchaConfig['captchaSize'] = $this->scopeConfig->getValue('recaptcha_frontend/type_recaptcha/size');
        return $recaptchaConfig;
    }
}
