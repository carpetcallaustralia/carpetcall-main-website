define([
    'Magento_Checkout/js/action/select-shipping-method'
], function (selectShippingMethodAction) {
        'use strict';
        return function (target) {
            target.resolveShippingRates = function(ratesData) {
                selectShippingMethodAction(null);
            };

            return target;
        }
    }
);