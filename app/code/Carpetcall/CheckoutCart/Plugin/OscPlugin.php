<?php
namespace Carpetcall\CheckoutCart\Plugin;

use Mageplaza\Osc\Helper\Data;
 
class OscPlugin
{
    protected $_resultPage;
    private $helper;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
    }

    public function afterExecute(
    \Mageplaza\Osc\Controller\Index\Index $cartIndex,
    $resultPage
    )
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $this->_resultPage = $this->resultPageFactory->create();
        //$resultPage = $this->resultPageFactory->create();
        $this->_resultPage->getConfig()->getTitle()->set(__('One Step Checkout | Carpet Call'));
        $this->_resultPage->getConfig()->setDescription('Our simple one step checkout that makes ordering our products easy.');
        $checkoutTitle = $this->helper->getCheckoutTitle();
        $titleBlock = $this->_resultPage->getLayout()->getBlock('page.main.title');
        if ($titleBlock) {
            $titleBlock->setPageTitle(
                __($checkoutTitle)
            );
        }
        return $this->_resultPage;
    }
}
?>