<?php
namespace Carpetcall\CheckoutCart\Plugin;
 
class CheckoutCartPlugin
{
    protected $_resultPage;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
    }

    public function afterExecute(
    \Magento\Checkout\Controller\Cart\Index $cartIndex,
    $resultPage
    )
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $this->_resultPage = $this->resultPageFactory->create();
        //$resultPage = $this->resultPageFactory->create();
        $this->_resultPage->getConfig()->getTitle()->set(__('My Cart | Checkout | Carpet Call'));
        $this->_resultPage->getConfig()->setDescription('View your shopping cart when items added to your basket.');

        $titleBlock = $this->_resultPage->getLayout()->getBlock('page.main.title');
        if ($titleBlock) {
            $titleBlock->setPageTitle(
                __('Shopping Cart')
            );
        }
        return $this->_resultPage;
    }
}
?>