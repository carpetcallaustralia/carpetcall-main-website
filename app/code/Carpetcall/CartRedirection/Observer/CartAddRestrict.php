<?php
namespace Carpetcall\CartRedirection\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Http\Context as customerSession;

class CartAddRestrict implements ObserverInterface
{
    protected $cart;
    protected $messageManager;
    protected $redirect;
    protected $request;
    protected $product;
    protected $customerSession;
    protected $_categoryCollectionFactory;
    protected $_responseFactory;
    protected $_url;

    public function __construct(
        RedirectInterface $redirect,
        Cart $cart,
        ManagerInterface $messageManager,
        RequestInterface $request,
        Product $product,
        ProductRepository $productRepository,
       customerSession $session,
       \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
       \Magento\Framework\App\ResponseFactory $responseFactory,
       \Magento\Framework\UrlInterface $url
	)
    {    
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->redirect = $redirect;
        $this->cart = $cart;
        $this->messageManager = $messageManager;
        $this->request = $request;
        $this->product = $product;
        $this->_productRepository = $productRepository;
        $this->customerSession = $session;
    }
  
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        // $cartUrl = $this->_url->getUrl('checkout/cart/index');
        // $this->_responseFactory->create()->setRedirect($cartUrl)->sendResponse();            
        // die();

        $cartItemsCount = $this->cart->getQuote()->getItemsCount();

        // Adding product details
        $postValues = $this->request->getPostValue();
        $product_data = $this->_productRepository->getById($postValues['product']);

        $categoryIds = $product_data->getCategoryIds();
        $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
        $new_cate_arr = array();
        foreach ($categories as $category) {
            $new_cate_arr[] = $category->getName();
        }
        // echo "<pre>";
        // print_r($new_cate_arr);die();
        // Adding product details

        // Already added product to cart details
        $message = '';
        $productInfo = $this->cart->getQuote()->getItemsCollection();
        //$productInfo = $this->_cart->getQuote()->getAllItems(); /*****For All items *****/
        foreach ($productInfo as $item)
        {
            $product_data = $this->_productRepository->getById($item->getProductId());

            $categoryIds = $product_data->getCategoryIds();
            $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
            $cate_arr = array();

            foreach ($categories as $category) {
                $cate_arr[] = $category->getName();
            }

        // Already added product to cart details
        
        if (!empty($cate_arr)) {
            if ((($cate_arr[0] == 'Rugs') && ($new_cate_arr[0] == 'Rugs')) || (($cate_arr[0] == 'Rugs Clearance') && ($new_cate_arr[0] == 'Rugs Clearance'))) {
                // Add to cart as both are Rugs
            }elseif ((($cate_arr[0] == 'Hard Flooring') || ($cate_arr[0] == 'Underlay') || ($cate_arr[0] == 'Cleaning & Repair kits')|| ($cate_arr[0] == 'Trims')|| ($cate_arr[0] == 'Scotia')) && (($new_cate_arr[0] == 'Rugs') || ($new_cate_arr[0] == 'Rugs Clearance'))) {
                // Cart restriction
                $observer->getRequest()->setParam('product', false);
                if (empty($message)) {
                    $message = 'You can not add Rugs category product to cart along with Hard Flooring products.';
                    $this->messageManager->addError($message);
                }

            }elseif ((($new_cate_arr[0] == 'Hard Flooring') || ($new_cate_arr[0] == 'Underlay') || ($new_cate_arr[0] == 'Cleaning & Repair kits') || ($new_cate_arr[0] == 'Trims') || ($new_cate_arr[0] == 'Scotia')) && (($cate_arr[0] == 'Rugs') || ($cate_arr[0] == 'Rugs Clearance'))) {
                // Cart restriction
                $observer->getRequest()->setParam('product', false);
                
                if (empty($message)) {
                    $message = 'You can not add Hard Flooring category product to cart along with Rugs products.';
                    $this->messageManager->addError($message);
                }
                
            }
        }else{
            // Add product to cart as cart is empty
        }
        
        }
    
    }

    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }

}    