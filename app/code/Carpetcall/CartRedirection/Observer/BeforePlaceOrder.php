<?php
namespace Carpetcall\CartRedirection\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\Observer;
use Magento\Catalog\Model\ProductRepository;


class BeforePlaceOrder implements ObserverInterface
{
	/**
     * @var ManagerInterface
     */
    protected $messageManager;

    protected $_categoryCollectionFactory;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var Cart
     */
    protected $cart;
    protected $_responseFactory;
    protected $_url;

    /**
     * @param ManagerInterface $messageManager
     * @param RedirectInterface $redirect
     * @param CustomerCart $cart
     */
    public function __construct(
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        CustomerCart $cart,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        ProductRepository $productRepository,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url

    ) {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->cart = $cart;
        $this->_productRepository = $productRepository;
        $this->_responseFactory = $responseFactory;
        $this->_url = $url;
    }

    /**
     * Validate Cart Before going to checkout
     * - event: controller_action_predispatch_checkout_index_index
     *
     * @param Observer $observer
     * @return void
     */

	public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /*$order = $observer->getEvent()->getOrder();
        $order->setOrderCat('HARDTEST55');*/
    	
    	$quote = $this->cart->getQuote();
        $controller = $observer->getControllerAction();
        $cartItemsQty = $quote->getItemsQty();


        //If cart item qty is lower than 4 then redirect to the cart with a message
        // if ($cartItemsQty < 4) {
        //     $this->messageManager->addNoticeMessage('Reacxc');
        //     $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
        // }

        $productInfo = $this->cart->getQuote()->getItemsCollection();
        //$productInfo = $this->_cart->getQuote()->getAllItems(); /*****For All items *****/
        $cate_arr = array();
        foreach ($productInfo as $item)
        {
            $product_data = $this->_productRepository->getById($item->getProductId());

            $categoryIds = $product_data->getCategoryIds();
            $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
            

            foreach ($categories as $category) {
                $cate_arr[] = $category->getName();
            }

        }

        $ass_cat_arr = array('Underlay','Cleaning & Repair kits','Trims','Scotia');

        $result=array_diff($cate_arr,$ass_cat_arr);
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        //$logger->info(print_r($ass_cat_arr,true));
        $logger->info(print_r($result,true));

        if (empty($result)) {
            $this->messageManager->addNoticeMessage(
                __('This accessory/accessories can only be purchased with hard flooring.')
            );
            //$this->redirect->redirect($controller->getResponse(), 'checkout/cart');
             $RedirectUrl = $this->_url->getUrl('checkout/cart');
             $this->_responseFactory->create()->setRedirect($RedirectUrl)->sendResponse();
             die();
        }

        
        if (((in_array('Rugs', $cate_arr)) || (in_array('Rugs Clearance', $cate_arr))) && ((in_array('Hard Flooring', $cate_arr)) || (in_array('Underlay', $cate_arr)) || (in_array('Cleaning & Repair kits', $cate_arr))|| (in_array('Trims', $cate_arr))|| (in_array('Scotia', $cate_arr))) ) {
            $this->messageManager->addNoticeMessage(
                __('You cannot purchase hard flooring & rugs products together.')
            );
            $this->redirect->redirect($controller->getResponse(), 'checkout/cart');   
        }


        //save category name in sales_order table
        if((in_array('Rugs', $cate_arr)) || (in_array('Rugs Clearance', $cate_arr))) {
            $catName = 'RUGS';
        } else if(in_array('Hard Flooring', $cate_arr)) {
            $catName = 'HARD';
        } else if(in_array('Blinds', $cate_arr)) {
            $catName = 'BLINDS';
        } else if(in_array('Shutters', $cate_arr)) {
            $catName = 'SHUTTERS';
        } else if(in_array('Accessories', $cate_arr)) {
            $catName = 'ACCESSORIES';
        } else {
            $catName = '';   
        }

        $order = $observer->getEvent()->getOrder();
        $order->setOrderCat($catName);

        // Already added product to cart details
    }

    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
}