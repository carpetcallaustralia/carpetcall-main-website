<?php
namespace Carpetcall\CartRedirection\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\Observer;
use Magento\Catalog\Model\ProductRepository;


class CartItemRemoved implements ObserverInterface
{
	/**
     * @var ManagerInterface
     */
    protected $messageManager;

    protected $_categoryCollectionFactory;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @param ManagerInterface $messageManager
     * @param RedirectInterface $redirect
     * @param CustomerCart $cart
     */
    public function __construct(
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        CustomerCart $cart,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        ProductRepository $productRepository
    ) {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->cart = $cart;
        $this->_productRepository = $productRepository;
    }

    /**
     * Validate Cart Before going to checkout
     * - event: controller_action_predispatch_checkout_index_index
     *
     * @param Observer $observer
     * @return void
     */

	public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	//echo 'Reachde';die();
    	$quote = $this->cart->getQuote();
        $controller = $observer->getControllerAction();
        $cartItemsQty = $quote->getItemsQty();

        //If cart item qty is lower than 4 then redirect to the cart with a message
        // if ($cartItemsQty < 4) {
        //     $this->messageManager->addNoticeMessage(
        //         __('This accessory/accessories can only be purchased with hard flooring.')
        //     );
        //     $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
        // }

        // $productInfo = $this->cart->getQuote()->getItemsCollection();
        $productInfo = $this->cart->getQuote()->getAllVisibleItems();
        //$productInfo = $this->_cart->getQuote()->getAllItems(); /*****For All items *****/
        $cate_arr = array();
        foreach ($productInfo as $item)
        {
            $product_data = $this->_productRepository->getById($item->getProductId());

            $categoryIds = $product_data->getCategoryIds();
            $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
            

            foreach ($categories as $category) {
                $cate_arr[] = $category->getName();
            }

        }

        $ass_cat_arr = array('Underlay','Cleaning & Repair kits','Trims','Scotia');

        $result=array_diff($cate_arr,$ass_cat_arr);
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        //$logger->info(print_r($ass_cat_arr,true));
        $logger->info(print_r($result,true));

        if (empty($result)) {
            $this->messageManager->addNoticeMessage(
                __('This accessory/accessories can only be purchased with hard flooring.')
            );
            $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
        }


        // Already added product to cart details
    }

    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
}