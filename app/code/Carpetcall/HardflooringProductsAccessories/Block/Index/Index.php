<?php
declare(strict_types=1);
use Magento\Catalog\Block\Product\ListProduct;

namespace Carpetcall\HardflooringProductsAccessories\Block\Index;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $_storeManager;
    protected $_categoryCollection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
         \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
         \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
         \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_categoryCollection = $categoryCollection;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->listProductBlock = $listProductBlock;
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
        $this->messageManager = $messageManager;
        parent::__construct($context, $data);
    }


    public function getCategoryIds(){
         $categories = $this->_categoryCollection->create()
             ->addAttributeToSelect('*')
             ->setStore($this->_storeManager->getStore()); //categories from current store will be fetched

        $categoriesIds = array();

        foreach ($categories as $cate_data) {
            if ($cate_data->getName() == 'Underlay') {
                $categoriesIds[] =  $cate_data->getId();
            }
        }
        return $categoriesIds;
    }

    public function getProductCollection()
    {
        $categoriesIds = $this->getCategoryIds();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoriesFilter(array('in' => $categoriesIds))->load();
        return $collection;
    }

    public function getCategoryIdsofcleaningkits(){
        $categories = $this->_categoryCollection->create()
             ->addAttributeToSelect('*')
             ->setStore($this->_storeManager->getStore()); //categories from current store will be fetched

        $categoriesIds = array();

        foreach ($categories as $cate_data) {
            if ($cate_data->getName() == 'Cleaning & Repair kits') {
                $categoriesIds[] =  $cate_data->getId();
            }
        }
        return $categoriesIds;
    }

    public function getProductCollectionofcleaningkits()
    {
        $categoriesIds = $this->getCategoryIdsofcleaningkits();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoriesFilter(array('in' => $categoriesIds))->load();
        return $collection;
    }

    public function getCategoryIdsofTrims(){
        $categories = $this->_categoryCollection->create()
             ->addAttributeToSelect('*')
             ->setStore($this->_storeManager->getStore()); //categories from current store will be fetched

        $categoriesIds = array();

        foreach ($categories as $cate_data) {
            if ($cate_data->getName() == 'Trims') {
                $categoriesIds[] =  $cate_data->getId();
            }
        }
        return $categoriesIds;
    }

    public function getProductCollectionofTrims()
    {
        $categoriesIds = $this->getCategoryIdsofTrims();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoriesFilter(array('in' => $categoriesIds))->load();
        return $collection;
    }

    public function getCategoryIdsofScotia(){
        $categories = $this->_categoryCollection->create()
             ->addAttributeToSelect('*')
             ->setStore($this->_storeManager->getStore()); //categories from current store will be fetched

        $categoriesIds = array();

        foreach ($categories as $cate_data) {
            if ($cate_data->getName() == 'Scotia') {
                $categoriesIds[] =  $cate_data->getId();
            }
        }
        return $categoriesIds;
    }

    public function getProductCollectionofScotia()
    {
        $categoriesIds = $this->getCategoryIdsofScotia();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoriesFilter(array('in' => $categoriesIds))->load();
        return $collection;
    }

    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }

    public function getItemImage($productId)
    {
        try {
            $_product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            return 'product not found';
        }
        $image_url = $this->imageHelper->init($_product, 'product_base_image')->getUrl();
        return $image_url;
    }

    public function baseUrl()
    {
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        return $baseUrl;
    }

    public function getRecommendedQty()
    {
        return (int)$this->getRequest()->getParam('qty');
    }
}

