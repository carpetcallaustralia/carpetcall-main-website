<?php
namespace Carpetcall\Enquire\Model\ResourceModel;

class Enquire extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
public function _construct()
{
    $this->_init('enquire_now_details', 'id');
}
}