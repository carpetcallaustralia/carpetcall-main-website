<?php

namespace Carpetcall\Enquire\Model\ResourceModel\Enquire;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
public function _construct()
{
$this->_init('Carpetcall\Enquire\Model\Enquire', 'Carpetcall\Enquire\Model\ResourceModel\Enquire');
}
}