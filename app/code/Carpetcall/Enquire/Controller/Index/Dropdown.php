<?php
declare(strict_types=1);

namespace Carpetcall\Enquire\Controller\Index;
use Mageplaza\StoreLocator\Model\LocationFactory;

class Dropdown extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
         \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
         LocationFactory $locationFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->locationFactory = $locationFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        $result = $this->resultJsonFactory->create();
        $html='<option value="">Select A Store</option>';
             
             $frameName = $this->getRequest()->getParam('select_state');
             if($frameName!='')
                {
                switch ($frameName)
                {
                    case 'Tasmania' :
                        $locations = $this->locationFactory->create()->getCollection();
                        foreach ($locations as $key => $value) {
                            if (strtoupper($value['state_province']) == 'TASMANIA') {
                                 // Condition when user lands on contact us page from store locator details page
                                $select_store = $this->getRequest()->getParam('select_store');

                                if ((!empty($select_store)) && (strtoupper($select_store) == $value['name'])) {
                                    $html.='<option value="'.$value['name'].'" selected>'.$value['name'].'</option>';
                                }else{
                                    $html.='<option value="'.$value['name'].'">'.$value['name'].'</option>';
                                }
                            }
                        }
                        break;
                                     
                    case 'New South Wales' :
                        $locations = $this->locationFactory->create()->getCollection();
                        foreach ($locations as $key => $value) {
                            if (strtoupper($value['state_province']) == 'NEW SOUTH WALES') {

                                // Condition when user lands on contact us page from store locator details page
                                $select_store = $this->getRequest()->getParam('select_store');

                                if ((!empty($select_store)) && (strtoupper($select_store) == $value['name'])) {
                                    $html.='<option value="'.$value['name'].'" selected>'.$value['name'].'</option>';
                                }else{
                                    $html.='<option value="'.$value['name'].'">'.$value['name'].'</option>';
                                }
                            }
                        }
                        break;

                    case 'Queensland' :
                        $locations = $this->locationFactory->create()->getCollection();
                        foreach ($locations as $key => $value) {
                            if (strtoupper($value['state_province']) == 'QUEENSLAND') {
                                 // Condition when user lands on contact us page from store locator details page
                                $select_store = $this->getRequest()->getParam('select_store');

                                if ((!empty($select_store)) && (strtoupper($select_store) == $value['name'])) {
                                    $html.='<option value="'.$value['name'].'" selected>'.$value['name'].'</option>';
                                }else{
                                    $html.='<option value="'.$value['name'].'">'.$value['name'].'</option>';
                                }
                            }
                        }
                        break;

                    case 'South Australia' :
                             $locations = $this->locationFactory->create()->getCollection();
                        foreach ($locations as $key => $value) {
                            if (strtoupper($value['state_province']) == 'SOUTH AUSTRALIA') {
                                 // Condition when user lands on contact us page from store locator details page
                                $select_store = $this->getRequest()->getParam('select_store');

                                if ((!empty($select_store)) && (strtoupper($select_store) == $value['name'])) {
                                    $html.='<option value="'.$value['name'].'" selected>'.$value['name'].'</option>';
                                }else{
                                    $html.='<option value="'.$value['name'].'">'.$value['name'].'</option>';
                                }
                            }
                        }
                        break;
                                     
                    case 'Victoria' :
                             $locations = $this->locationFactory->create()->getCollection();
                        foreach ($locations as $key => $value) {
                            if (strtoupper($value['state_province']) == 'VICTORIA') {
                                 // Condition when user lands on contact us page from store locator details page
                                $select_store = $this->getRequest()->getParam('select_store');

                                if ((!empty($select_store)) && (strtoupper($select_store) == $value['name'])) {
                                    $html.='<option value="'.$value['name'].'" selected>'.$value['name'].'</option>';
                                }else{
                                    $html.='<option value="'.$value['name'].'">'.$value['name'].'</option>';
                                }
                            }
                        }
                        break;

                    case 'Western Australia' :
                             $locations = $this->locationFactory->create()->getCollection();
                        foreach ($locations as $key => $value) {
                            if (strtoupper($value['state_province']) == 'WESTERN AUSTRALIA') {
                                 // Condition when user lands on contact us page from store locator details page
                                $select_store = $this->getRequest()->getParam('select_store');

                                if ((!empty($select_store)) && (strtoupper($select_store) == $value['name'])) {
                                    $html.='<option value="'.$value['name'].'" selected>'.$value['name'].'</option>';
                                }else{
                                    $html.='<option value="'.$value['name'].'">'.$value['name'].'</option>';
                                }
                            }
                        }
                        break;
                }    
             }
             
             
             return $result->setData(['success' => true,'value'=>$html]);
        
    }

    public function getlocations($country)
    {

        
        return $html;
    }

}


