<?php
declare(strict_types=1);

namespace Carpetcall\Enquire\Controller\Index;

use Mageplaza\StoreLocator\Model\LocationFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $scopeConfig;
    protected $resultFactory;
    protected $locationFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Carpetcall\Enquire\Model\EnquireFactory  $enquiry,
        LocationFactory $locationFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->resultFactory = $resultFactory;
        $this->_enquiry = $enquiry;
        $this->locationFactory = $locationFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();

        $this->saveData($data);
        $this->sendEmail($data);
        $this->adminEmail($data);

        //return $this->resultPageFactory->create();
        $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);

        $redirect->setRefererOrBaseUrl();


        return $redirect;
    }

    private function sendEmail($data)
    {

        $firstname = $data['name'];
        $email = $data['email'];
        $phone = $data['telephone'];
        $productname = $data['productname'];
        $productcode = (!empty($data['productcode'])) ? $data['productcode'] : '';
        $productsize = (!empty($data['productsize'])) ? $data['productsize'] : '';
        $comment = $data['comment'];
        $state = $data['state'];
        $store = (!empty($data['store'])) ? ucwords(strtolower($data['store'])) : '';

        $templateVars = [
                        'name' => $firstname,
                        'lastname' => $data['lastname'],
                        'email'   => $email,
                        'telephone' => $phone,
                        'productname' => $productname,
                        'productcode' => $productcode,
                        'productsize' => $productsize,
                        'comment' => $comment,
                        'state' => $state,
                        'selectedstore' => $store
                    ];
        $templateOptions = ['area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->storeManager->getStore()->getId()];

    // echo "<pre>";
    //     print_r($templateVars);
    //     exit;

        $email_Template = 'customer_enquiry_template';
        $formemail = $this->storeManager->getStore()->getConfig('trans_email/ident_general/email');
        $formname = $this->storeManager->getStore()->getConfig('trans_email/ident_general/name');

        $transport = $this->transportBuilder
            ->setTemplateIdentifier($email_Template)
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom(['name' => $formname,'email' => $formemail])
            ->addTo($email)
            ->getTransport();
        $transport->sendMessage();
    }

    private function adminEmail($data)
    {

        $firstname = $data['name'];
        $email = $data['email'];
        $phone = $data['telephone'];
        $productname = $data['productname'];
        $productcode = (!empty($data['productcode'])) ? $data['productcode'] : '';
        $productsize = (!empty($data['productsize'])) ? $data['productsize'] : '';
        $comment = $data['comment'];
        $state = $data['state'];
        $store = (!empty($data['store'])) ? ucwords(strtolower($data['store'])) : '';

        $templateVars = [
                        'date' => $data['date'],
                        'time' => $data['time'],
                        'name' => $firstname,
                        'lastname' => $data['lastname'],
                        'email'   => $email,
                        'telephone' => $phone,
                        'productname' => $productname,
                        'productcode' => $productcode,
                        'productsize' => $productsize,
                        'comment' => $comment,
                        'state' => $state,
                        'selectedstore' => $store
                    ];
        $templateOptions = ['area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->storeManager->getStore()->getId()];

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        // 1. Carpets, Hard Flooring, Rugs, Accessories > send email confirmation to customer and default admin@carpetcall.com.au
        // 2. Blinds & Shutters > logic to be implemented that user has to select a state > this then sends to the (To and BCC) Sales enquiry email addresses"


        $email_Template = 'admin_template';
        $formemail = $this->storeManager->getStore()->getConfig('trans_email/ident_general/email');
        $formname = $this->storeManager->getStore()->getConfig('trans_email/ident_general/name');
        $from = ['email' => $formemail, 'name' => $formname];

       
        //echo $data['store'];die();

        $cate_list = array("Rugs", "Carpet", "Hard Flooring");
        if (in_array(trim($data['productcategory']), $cate_list)) {
            
            // For this 3 category state email from sales enquiry
            // to: should be the email set in the individual store
            // cc: should be the email set in "Sales enquiry admin email [name of state] State To"
            // bcc: should be the email set in "Sales enquiry admin email [name of state] State Bcc"

            switch ($data['state']) {
                case "Tasmania":

                        $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_to', $storeScope);
                        //$bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_bcc', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_tas_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }

                    break;
                case "New South Wales":

                        $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_to', $storeScope);
                        //$bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_bcc', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }

                    break;
                case "Queensland":

                        $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_to', $storeScope);
                        //$bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_bcc', $storeScope);

                        $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_qld_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }

                    break;
                case "South Australia":

                        $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_to', $storeScope);
                        //$bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_bcc', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_sa_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }

                    break;
                case "Victoria":

                        $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_to', $storeScope);
                        //$bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_bcc', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_vic_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }

                    break;
                case "Western Australia":

                        $emails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_to', $storeScope);
                        //$bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_bcc', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_nsw/sales_enquiry_admin_email_wa_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }
                    break;
                default:
            }

            $locations = $this->locationFactory->create()->getCollection();
            foreach ($locations as $key => $value) {
                if (strtoupper($value['name']) == $data['store']) {
                     $store_email_add_for_to = $value['email'];
                }
            }

            $this->transportBuilder
                ->setTemplateIdentifier($email_Template)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($store_email_add_for_to);
                if (!empty($emails)) {
                    $this->transportBuilder->addCc($emails);
                }
                if (!empty($final_bccemails)) {
                    $this->transportBuilder->addBcc($final_bccemails);
                }
                $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
            
        }else{
             

            // $emails = 'testmscgoriteeps@gmail.com';
            // $bccemails = 'arjun.cmarix@gmail.com';

            

            // For Blinds & Shutters service enquiry state vice
            // 2. Blinds or Shutters
            // a. User must select state (Store selection not mandatory)
            // b. Email confirmation sent to 1. Customer, 2. To: State email (outlined in Service enquiry)"

            switch ($data['state']) {
                case "Tasmania":
                    
                        $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_tas_to', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_tas_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }
                    
                    break;
                case "New South Wales":
                    
                        $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw_to', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }
                    
                    break;
                case "Queensland":
                    
                         $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_qld_to', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_qld_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }
                    
                    break;
                case "South Australia":
                    
                        $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_sa_to', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_sa_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }
                    
                    break;
                case "Victoria":
                    
                         $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_vic_to', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_vic_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }

                    break;
                case "Western Australia":
                        $emails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_wa_to', $storeScope);
                        $bccemails = $this->scopeConfig->getValue('service_enquiry_admin_email_nsw/service_enquiry_admin_email_nsw/service_enquiry_admin_email_wa_bcc', $storeScope);
                        $bccemails_format = explode(',', $bccemails);
                        
                        foreach ($bccemails_format as $value) {
                            $final_bccemails[] = trim($value);
                        }
                        
                    break;
                default:
            }

            // echo $emails;
            // print_r($final_bccemails);die();

            $this->transportBuilder
                ->setTemplateIdentifier($email_Template)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($emails);
                if (!empty($final_bccemails)) {
                    $this->transportBuilder->addBcc($final_bccemails);
                }
                $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();

        }


    }

    public function saveData($data)
    {

        $firstname = $data['name'];
        $email = $data['email'];
        $phone = $data['telephone'];
        $productname = $data['productname'];
        $productcode = (!empty($data['productcode'])) ? $data['productcode'] : '';
        $productsize = (!empty($data['productsize'])) ? $data['productsize'] : '';
        $comment = $data['comment'];
        $state = $data['state'];
        $store = (!empty($data['store'])) ? ucwords(strtolower($data['store'])) : '';
        $datetime = $data['datetime'];

        $model = $this->_enquiry->create();
        $model->addData([
            "firstname" => $firstname,
            "lastname" => $data['lastname'],
            "email" => $email,
            "phone" => $phone,
            "state" => $state,
            "store" => $store,
            "message" => $comment,
            "product_name" => $productname,
            "code" => $productcode,
            "size" => $productsize,
            "created_at" => $datetime
            ]);
        $saveData = $model->save();

        
    }
}


