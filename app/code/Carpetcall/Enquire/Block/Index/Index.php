<?php
declare(strict_types=1);

namespace Carpetcall\Enquire\Block\Index;


class Index extends \Magento\Framework\View\Element\Template
{

    protected $scopeConfig;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */

    /**
     * @var LocationFactory
     */
    

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        
        parent::__construct($context, $data);
    }

    public function getSiteKey()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('recaptcha_frontend/type_recaptcha/public_key', $storeScope);
    }

    public function getframeAction()
    {
        return $this->getUrl('enquire/index/dropdown', ['_secure' => true]);
    }
}

