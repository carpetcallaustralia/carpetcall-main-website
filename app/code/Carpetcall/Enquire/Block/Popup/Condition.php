<?php

namespace Carpetcall\Enquire\Block\Popup;

class Condition implements \Magento\Framework\View\Layout\Condition\VisibilityConditionInterface
{
    
    private static $conditionName = 'show_popup_form';

    public function isVisible($product)
    {
        // $product = $this->_registry->registry('current_product');
        $blockobj = $block->getLayout()->createBlock('Carpetcall\Productdetailspage\Block\Categoriesdetails');
        $categoryIds = $product->getCategoryIds();
        $categories = $blockobj->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
        $cate_arr = array();
        foreach ($categories as $category) {
            $cate_arr[] = $category->getName();
        }
        $blocked_categories = ['Accessories', 'Underlay', 'Cleaning & Repair kits', 'Trims', 'Scotia'];
        return (count(array_intersect($blocked_categories, $cate_arr)) <= 0);
    }

    public function getName()
    {
        return self::$conditionName;
    }
}