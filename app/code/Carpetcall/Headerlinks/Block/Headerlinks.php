<?php
namespace Carpetcall\Headerlinks\Block;
class Headerlinks extends \Magento\Framework\View\Element\Template
{
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Customer\Model\Session $session,
		\Magento\Wishlist\Model\Wishlist $wishlist,
		\Magento\Framework\App\Http\Context $contextdata
	)
	{
		parent::__construct($context);
		$this->session  = $session;
		$this->wishlist  = $wishlist;
		$this->contextdata = $contextdata;
	}

	public function getSession()
	{
		return $this->contextdata->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
		//return $this->session;
	}

	public function getWishlist()
	{
		return $this->wishlist;
	}

};

