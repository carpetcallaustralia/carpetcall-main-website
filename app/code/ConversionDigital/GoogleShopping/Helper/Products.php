<?php

namespace ConversionDigital\GoogleShopping\Helper;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class Products extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Eav\ModelAttributeSetRepository
     */
    protected $_attributeSetRepo;

    /**
     * @var \ConversionDigital\GoogleShopping\Helper\Data
     */
    protected $_helper;

    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    public $_storeManager;

    /**
    * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
    */
    public $_productStatus;

    /**
    * @var \Magento\Catalog\Model\Product\Visibility
    */
    public $_productVisibility;

    private $categoryCollection;

    private $categoryNameArr;

    protected $stockFilter;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Eav\Model\AttributeSetRepository $attributeSetRepo,
        \ConversionDigital\GoogleShopping\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        CollectionFactory $categoryCollection,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_attributeSetRepo = $attributeSetRepo;
        $this->_helper = $helper;
        $this->_storeManager = $storeManager;
        $this->_productStatus = $productStatus;
        $this->_productVisibility = $productVisibility;
        $this->categoryCollection = $categoryCollection;
        $this->stockFilter = $stockFilter;
        $this->categoryNameArr = [];
        parent::__construct($context);
    }

    public function getFilteredProducts()
    {
        // 'in' => [
        //     '6', Rugs
        //     '11' Accessories
        // ]
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        $collection->addCategoriesFilter([
            'in' => [
                6, 
                11
            ]
        ]);
        // $collection->setPageSize(5);

        // get only 'in stock' products
        $this->stockFilter->addInStockFilterToCollection($collection);

        return $collection;
    }

    public function getAttributeSet($product)
    {
        $attributeSetId = $product->getAttributeSetId();
        $attributeSet = $this->_attributeSetRepo->get($attributeSetId);

        return $attributeSet->getAttributeSetName();

    }

    public function getProductValue($product, $attributeCode)
    {
        $attributeCodeFromConfig = $this->_helper->getConfig($attributeCode.'_attribute');
        $defaultValue = $this->_helper->getConfig('default_'.$attributeCode);

        if (!empty($attributeCodeFromConfig))
        {
            return $product->getAttributeText($attributeCodeFromConfig);
        }

        if (!empty($defaultValue))
        {
            return $defaultValue;
        }

        return false;
    }

    public function getCurrentCurrencySymbol()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }

    public function getFilteredCategoryCollection($categoryIds)
    {
        $availableCategoryNames = array_keys($this->categoryNameArr);
        $fetchCategoryIds = array_values(array_diff($categoryIds, $availableCategoryNames));
        $collection = $this->categoryCollection->create();
        $filtered_colection = $collection
            ->addFieldToSelect(['name'])
            ->addFieldToFilter(
                'entity_id',
                ['in' => $fetchCategoryIds]
            )
            ->setOrder('level', 'ASC')
            ->load();
        return $filtered_colection;
    }

    public function getCategories($filtered_colection)
    {
        $separator = ' > ';
        $categories = count($filtered_colection) > 0 ? 'Home' : '';
        foreach ($filtered_colection as $categoriesData) {
            if ($categoriesData->getId() > 2) {
                if (!array_key_exists($categoriesData->getId(), $this->categoryNameArr)) {
                    $this->categoryNameArr[$categoriesData->getId()] = $categoriesData->getName();
                }
                $categories .=  $separator . $this->categoryNameArr[$categoriesData->getId()];
            }
        }
        return $categories;
    }

    public function getProductBreadcrumbs($categoryIds)
    {
        $separator = ' > ';
        $filtered_colection = $this->getFilteredCategoryCollection($categoryIds);
        $categories = $this->getCategories($filtered_colection);
        return $categories;
    }

}
