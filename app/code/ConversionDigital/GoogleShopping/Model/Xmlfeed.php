<?php

namespace ConversionDigital\GoogleShopping\Model;

class Xmlfeed
{
    /**
     * General Helper
     *
     * @var \ConversionDigital\GoogleShopping\Helper\Data
     */
    private $_helper;

    /**
     * Product Helper
     *
     * @var \ConversionDigital\GoogleShopping\Helper\Products
     */
    private $_productFeedHelper;

    /**
     * Store Manager
     *
     * @var \ConversionDigital\GoogleShopping\Helper\Products
     */
    private $_storeManager;

    public function __construct(
        \ConversionDigital\GoogleShopping\Helper\Data $helper,
        \ConversionDigital\GoogleShopping\Helper\Products $productFeedHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager

    ) {
        $this->_helper = $helper;
        $this->_productFeedHelper = $productFeedHelper;
        $this->_storeManager = $storeManager;
    }

    public function getFeed()
    {
        $xml = $this->getXmlHeader();
        $xml .= $this->getProductsXml();
        $xml .= $this->getXmlFooter();

        return $xml;
    }

    public function getXmlHeader()
    {

        header("Content-Type: application/xml; charset=utf-8");

        $xml =  '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
        $xml .= '<channel>';
        $xml .= '<title><![CDATA['.$this->_helper->getConfig('google_default_title').']]></title>';
        $xml .= '<link>'.$this->_helper->getConfig('google_default_url').'</link>';
        $xml .= '<description>'.$this->_helper->getConfig('google_default_description').'</description>';

        return $xml;

    }

    public function getXmlFooter()
    {
        return  '</channel></rss>';
    }

    public function getProductsXml()
    {
        $productCollection = $this->_productFeedHelper->getFilteredProducts();
        $xml = "";

        foreach ($productCollection as $product)
        {
            $xml .= "<item>".$this->buildProductXml($product)."</item>";
            // handle configurable product variants
            if ($product->getTypeId() == 'configurable') {
                $productTypeInstance = $product->getTypeInstance();
                $variantsUsed = $productTypeInstance->getUsedProductAttributes($product);
                $variantsNeeded = [];
                foreach ($variantsUsed  as $variant) {
                    array_push($variantsNeeded, $variant->getAttributeCode());
                }
                if (count($variantsNeeded) == 0) {
                    $variantsNeeded = null;
                }
                $extraAttribues = array_merge(['image', 'special_price', 'paragraph_1'], $variantsNeeded);
                $usedProducts = $productTypeInstance->getUsedProducts($product, $extraAttribues);
                foreach ($usedProducts  as $child) {
                    $xml .= "<item>".$this->buildProductChildXml(
                        $child, 
                        $variantsNeeded, 
                        $product->getSku(), 
                        $product->getProductUrl(),
                        $product->getImage(),
                        $product->getData('paragraph_1')
                    )."</item>";
                }
            }
        }
        
        return $xml;
    }
    
    public function buildProductXml($product)
    {
        $_description = $this->fixDescription($product->getData('paragraph_1'));
        $xml = $this->createNode("title", $product->getName(), true);
        $xml .= $this->createNode("link", $product->getProductUrl());
        $xml .= $this->createNode("description", $_description, true);
        $xml .= $this->createNode("g:id", $product->getSku());
        $xml .= $this->createNode("g:image_link", $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, true).'catalog/product'.$product->getImage());
        $xml .= $this->createNode('g:price', number_format($product->getFinalPrice(),2,'.','').' '.$this->_productFeedHelper->getCurrentCurrencySymbol());
        if ($product->getSpecialPrice() > 0 && $product->getSpecialPrice() != $product->getFinalPrice()) {
            $xml .= $this->createNode('g:sale_price', number_format($product->getSpecialPrice(),2,'.','').' '.$this->_productFeedHelper->getCurrentCurrencySymbol());
        }
        // $xml .= $this->createNode("g:size", $product->getSku()); TODO
        $xml .= $this->createNode("g:availability", 'in stock', true);
        $xml .= $this->createNode("g:condition", 'new', true);
        $xml .= $this->createNode("g:brand", 'Carpet Call', true);
        $xml .= $this->createNode("g:product_type", $this->_productFeedHelper->getProductBreadcrumbs($product->getCategoryIds(), $product->getName()), true);
        $xml .= $this->createNode('g:google_product_category', $this->_productFeedHelper->getProductValue($product, 'google_product_category'), true);
        // $xml .= $this->createNode("g:gtin", $product->getAttributeText('gr_ean'));
        $xml .= $this->createNode("g:mpn", $product->getSku(), true);
        
        return $xml;
    }
    
    public function buildProductChildXml($product, $product_variants, $parent_sku, $parent_url, $parent_image_url, $parent_description)
    {
        $product_description = $product->getData('paragraph_1');
        if ($product_description == '') {
            $product_description = $parent_description;
        }
        $product_image_link = $product->getImage();
        if (empty($product->getImage())) {
            $product_image_link = $parent_image_url;
        }
        $_description = $this->fixDescription($product_description);
        $xml = $this->createNode("title", $product->getName(), true);
        $xml .= $this->createNode("link", $parent_url);
        $xml .= $this->createNode("description", $_description, true);
        $xml .= $this->createNode("g:id", $product->getSku());
        if (!empty($product_image_link)) {
            $xml .= $this->createNode("g:image_link", $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, true) . 'catalog/product' . $product_image_link);
        }
        $xml .= $this->createNode('g:price', number_format($product->getPrice(),2,'.','').' '.$this->_productFeedHelper->getCurrentCurrencySymbol());
        if ($product->getSpecialPrice() > 0 && $product->getSpecialPrice() != $product->getPrice()) {
            $xml .= $this->createNode('g:sale_price', number_format($product->getSpecialPrice(),2,'.','').' '.$this->_productFeedHelper->getCurrentCurrencySymbol());
        }
        if (!empty($product_variants)) {
            foreach ($product_variants as $product_variant) {
                $google_feed_attribute = $this->getChildAttributeMap($product_variant);
                $variant_value = $product->getAttributeText($product_variant);
                if (strpos($variant_value, ' - $')) {
                    $_variant_value_explode = explode(' - $', $variant_value);
                    $variant_value = $_variant_value_explode[0];
                }
                $xml .= $this->createNode("g:" . $google_feed_attribute, $variant_value);
            }
        }
        $xml .= $this->createNode("g:availability", 'in stock', true);
        $xml .= $this->createNode("g:condition", 'new', true);
        $xml .= $this->createNode("g:brand", 'Carpet Call', true);
        $xml .= $this->createNode("g:product_type", $this->_productFeedHelper->getProductBreadcrumbs($product->getCategoryIds()), true);
        $xml .= $this->createNode('g:google_product_category', $this->_productFeedHelper->getProductValue($product, 'google_product_category'), true);
        $xml .= $this->createNode("g:mpn", $product->getSku(), true);
        $xml .= $this->createNode("g:item_group_id", $parent_sku, true);

        return $xml;
    }

    public function fixDescription($data)
    {
        $description = $data;
        $encode = mb_detect_encoding($data);
        $description = mb_convert_encoding($description, 'UTF-8', $encode);

        return $description;
    }

    public function createNode($nodeName, $value, $cData = false)
    {
        if (empty($value) || empty ($nodeName))
        {
            return false;
        }

        $cDataStart = "";
        $cDataEnd = "";

        if ($cData === true)
        {
            $cDataStart = "<![CDATA[";
            $cDataEnd = "]]>";
        }

        $node = "<".$nodeName.">".$cDataStart.$value.$cDataEnd."</".$nodeName.">";

        return $node;
    }

    public function getChildren($product_id, $product_type) 
    {
        $productCollection = $this->_productFeedHelper->getChildProducts($product_id, $product_type);

        return $productCollection;
    }

    public function getChildAttributeMap ($attribute_code)
    {
        $map = [
            'size' => 'size',
            'towel_type' => 'material'
        ];

        return (array_key_exists($attribute_code, $map)) ? $map[$attribute_code] : $attribute_code;
    }
}
