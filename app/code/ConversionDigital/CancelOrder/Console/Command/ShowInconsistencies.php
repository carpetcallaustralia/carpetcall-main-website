<?php
declare(strict_types=1);

namespace ConversionDigital\CancelOrder\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\InventoryReservationCli\Model\GetSalableQuantityInconsistencies;
use Magento\InventoryReservationCli\Model\SalableQuantityInconsistency;
use Magento\InventoryReservationCli\Model\SalableQuantityInconsistency\FilterCompleteOrders;
use Magento\InventoryReservationCli\Model\SalableQuantityInconsistency\FilterIncompleteOrders;
use Magento\Sales\Model\Order;

class ShowInconsistencies extends Command
{

    const COMPLETE_OPTION = "complete";
    const INCOMPLETE_OPTION = "incomplete";
    const RAW_OPTION = "raw";

    /**
     * @var GetSalableQuantityInconsistencies
     */
    private $getSalableQuantityInconsistencies;

    /**
     * @var FilterCompleteOrders
     */
    private $filterCompleteOrders;

    /**
     * @var FilterIncompleteOrders
     */
    private $filterIncompleteOrders;

    /**
     * @param GetSalableQuantityInconsistencies $getSalableQuantityInconsistencies
     * @param FilterCompleteOrders $filterCompleteOrders
     * @param FilterIncompleteOrders $filterIncompleteOrders
     */
    public function __construct(
        GetSalableQuantityInconsistencies $getSalableQuantityInconsistencies,
        FilterCompleteOrders $filterCompleteOrders,
        FilterIncompleteOrders $filterIncompleteOrders
    ) {
        parent::__construct();
        $this->getSalableQuantityInconsistencies = $getSalableQuantityInconsistencies;
        $this->filterCompleteOrders = $filterCompleteOrders;
        $this->filterIncompleteOrders = $filterIncompleteOrders;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $inconsistencies = $this->getSalableQuantityInconsistencies->execute();

        if ($input->getOption(self::COMPLETE_OPTION)) {
            $inconsistencies = $this->filterCompleteOrders->execute($inconsistencies);
        } elseif ($input->getOption(self::INCOMPLETE_OPTION)) {
            $inconsistencies = $this->filterIncompleteOrders->execute($inconsistencies);
        }

        if (empty($inconsistencies)) {
            $output->writeln('<info>No order inconsistencies were found</info>');
            return 0;
        }

        if ($input->getOption(self::RAW_OPTION)) {
            $this->rawOutput($output, $inconsistencies);
        } else {
            $this->prettyOutput($output, $inconsistencies);
        }

        return -1;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("conversiondigital_cancelorder:showinconsistencies");
        $this->setDescription("Show all orders and products with salable quantity inconsistencies");
        $this->setDefinition([
            new InputOption(self::COMPLETE_OPTION, "c", InputOption::VALUE_NONE, "Show only inconsistencies for complete orders"),
            new InputOption(self::INCOMPLETE_OPTION, "i", InputOption::VALUE_NONE, "Show only inconsistencies for incomplete orders"),
            new InputOption(self::RAW_OPTION, "r", InputOption::VALUE_NONE, "Raw output")
        ]);
        parent::configure();
    }

    /**
     * Formatted output
     *
     * @param OutputInterface $output
     * @param SalableQuantityInconsistency[] $inconsistencies
     */
    private function prettyOutput(OutputInterface $output, array $inconsistencies): void
    {
        $output->writeln('<info>Inconsistencies found on following entries:</info>');

        /** @var Order $order */
        foreach ($inconsistencies as $inconsistency) {
            $inconsistentItems = $inconsistency->getItems();

            $output->writeln(sprintf(
                'Order <comment>%s</comment>:',
                $inconsistency->getOrder()->getId()
            ));

            foreach ($inconsistentItems as $sku => $qty) {
                $output->writeln(
                    sprintf(
                        '  - Product <comment>%s</comment> should be compensated by '
                        . '<comment>%+f</comment> for stock <comment>%s</comment>',
                        $sku,
                        -$qty,
                        $inconsistency->getStockId()
                    )
                );
            }
        }
    }

    /**
     * Output without formatting
     *
     * @param OutputInterface $output
     * @param SalableQuantityInconsistency[] $inconsistencies
     */
    private function rawOutput(OutputInterface $output, array $inconsistencies): void
    {
        /** @var Order $order */
        foreach ($inconsistencies as $inconsistency) {
            $inconsistentItems = $inconsistency->getItems();

            foreach ($inconsistentItems as $sku => $qty) {
                $output->writeln(
                    sprintf(
                        '%s:%s:%f:%s',
                        $inconsistency->getOrder()->getId(),
                        $sku,
                        -$qty,
                        $inconsistency->getStockId()
                    )
                );
            }
        }
    }
}

