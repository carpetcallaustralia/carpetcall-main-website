<?php
declare(strict_types=1);

namespace ConversionDigital\CancelOrder\Cron;

class Cancel
{
    const ORDER_CUSTOM_COMMENT_TEXT = 'Automatic cancellation by system.';

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $cancelOrder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Aopen\Openpay\Model\Api
     */
    protected $openpayApi;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Sales\Api\Data\OrderInterface $cancelOrder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Aopen\Openpay\Model\Api $openpayApi
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\Data\OrderInterface $cancelOrder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Aopen\Openpay\Model\Api $openpayApi,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->cancelOrder = $cancelOrder;
        $this->scopeConfig = $scopeConfig;
        $this->openpayApi = $openpayApi;
        $this->logger = $logger;
        $this->_timezone = $timezone;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $currentTimestamp = $this->_timezone->date();
        $this->logger->info("Order cancel process started: " . $currentTimestamp->format('Y-m-d H:i:s'));
        try {
            if ($cancelPendingOrders = $this->scopeConfig->getValue(
                'cancel_order/settings/is_enable',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )) {

    //            Pending Openpay Approval
                $frequencyOpenPay = $this->scopeConfig->getValue(
                    'cancel_order/settings/open_pay_orders',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $currentTimestamp->sub(new \DateInterval('P' . $frequencyOpenPay . 'D'));
                $ordersOpenPay = $this->orderCollectionFactory->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('created_at', ['lt' => $currentTimestamp->format('Y-m-d 00:00:00')])
                    ->addFieldToFilter('state', 'new')
                    ->addFieldToFilter('status', 'pending_approval');
                $this->logger->info("Total Pending Openpay Approval orders: " . count($ordersOpenPay) . " before " . $currentTimestamp->format('Y-m-d 00:00:00'));
                foreach ($ordersOpenPay as $order) {
                    $planApproved = $this->openpayApi->getOpenpayOrderStatus($order->getId());
                    if (!$planApproved) {
                        $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
                        $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                        $order->addStatusToHistory($order->getStatus(), 'Plan is not active. ' . self::ORDER_CUSTOM_COMMENT_TEXT);
                    } else {
                        $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                        $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                        $order->addStatusToHistory($order->getStatus(), 'Plan is active. ' . self::ORDER_CUSTOM_COMMENT_TEXT);
                    }
                    $order->save();
                    $this->logger->info('Cancelled Openpay order: ' . $order->getIncrementId() . " (" . $order->getId() . ")");
                }
                $currentTimestamp->add(new \DateInterval('P' . $frequencyOpenPay . 'D'));

    //            'Pending', 'Pending Payment', 'Pending PayPal'
                $this->logger->info("Other orders Order cancel process started: " . $currentTimestamp->format('Y-m-d H:i:s'));
                $frequencyOthers = $this->scopeConfig->getValue(
                    'cancel_order/settings/is_enable',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $currentTimestamp->sub(new \DateInterval('P' . $frequencyOthers . 'D'));
                $ordersOthers = $this->orderCollectionFactory->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('created_at', ['lt' => $currentTimestamp->format('Y-m-d 00:00:00')])
                    ->addFieldToFilter('status', ['in' => ['pending', 'pending_payment', 'pending_paypal']]);
                $this->logger->info("Total Other Pending orders: " . count($ordersOthers) . " before " . $currentTimestamp->format('Y-m-d 00:00:00'));
                foreach ($ordersOthers as $order) {
                    $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                    $order->addStatusToHistory($order->getStatus(),  self::ORDER_CUSTOM_COMMENT_TEXT);
                    $order->save();
                    $this->logger->info('Cancelled Other order: ' . $order->getIncrementId() . " (" . $order->getId() . ")");
                }
                $currentTimestamp->add(new \DateInterval('P' . $frequencyOthers . 'D'));
            } else {
                $this->logger->info('Order cancel process is DISABLED in the backend');
            }
        } catch (\Exception $e) {
            $this->logger->info("Cancel Order Error: " . $e->getMessage());
        }
        $this->logger->info('Order cancel process finished: ' . $currentTimestamp->format('Y-m-d H:i:s'));
    }
}

