# Mage2 Module ConversionDigital CancelOrder

    ``conversiondigital/module-cancelorder``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Module to cancel orders with pending status and check inconsistencies.

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/ConversionDigital`
 - Enable the module by running `php bin/magento module:enable ConversionDigital_CancelOrder`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require conversiondigital/module-cancelorder`
 - enable the module by running `php bin/magento module:enable ConversionDigital_CancelOrder`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Enabled (general/cancel_order/is_enable)

 - Cancel OpenPay Orders Older Than (days) (general/cancel_order/open_pay_orders)

 - Cancel Other Orders Older Than (days) (general/cancel_order/other_orders)


## Specifications

 - Console Command
	- ShowInconsistencies

 - Cronjob
	- conversiondigital_cancelorder_cancel


## Attributes



