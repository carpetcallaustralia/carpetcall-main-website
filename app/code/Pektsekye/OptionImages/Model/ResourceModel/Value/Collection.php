<?php

namespace Pektsekye\OptionImages\Model\ResourceModel\Value;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     */
    public function _construct()
    {
        $this->_init('Pektsekye\OptionImages\Model\Value', 'Pektsekye\OptionImages\Model\ResourceModel\Value');
    }

}
