  
**Custom Option Images 1.0 for Magento 2.1**  Feb 21 2019

Check the latest README file:
http://hottons.com/demo/m2/oi/README.html


This is "Custom Option Images" addition for apparel stores.  
It adds ability to add (upload) images for custom product options.  
So if you have an apparel store you can add custom option Color and upload small images for each color option.  
Then if a customer selects Color option he will see the small image above the options dropdown (or check boxes, radio buttons, multiple select)  




### Installation


**1)** Upload 50 new files :  

    app/code/Pektsekye/OptionImages/Block/Adminhtml/Oi/Export.php
    app/code/Pektsekye/OptionImages/Block/Adminhtml/Product/Edit/Js.php
    app/code/Pektsekye/OptionImages/Block/Product/View/Js.php
    app/code/Pektsekye/OptionImages/composer.json
    app/code/Pektsekye/OptionImages/Controller/Adminhtml/Oi/Export/Export.php
    app/code/Pektsekye/OptionImages/Controller/Adminhtml/Oi/Export/Import.php
    app/code/Pektsekye/OptionImages/Controller/Adminhtml/Oi/Export/Index.php
    app/code/Pektsekye/OptionImages/Controller/Adminhtml/Oi/Export.php
    app/code/Pektsekye/OptionImages/etc/acl.xml
    app/code/Pektsekye/OptionImages/etc/adminhtml/di.xml
    app/code/Pektsekye/OptionImages/etc/adminhtml/events.xml
    app/code/Pektsekye/OptionImages/etc/adminhtml/menu.xml
    app/code/Pektsekye/OptionImages/etc/adminhtml/routes.xml
    app/code/Pektsekye/OptionImages/etc/adminhtml/system.xml
    app/code/Pektsekye/OptionImages/etc/config.xml
    app/code/Pektsekye/OptionImages/etc/module.xml
    app/code/Pektsekye/OptionImages/Helper/Data.php
    app/code/Pektsekye/OptionImages/i18n/en_US.csv
    app/code/Pektsekye/OptionImages/LICENSE.txt
    app/code/Pektsekye/OptionImages/Model/CsvImportHandler.php
    app/code/Pektsekye/OptionImages/Model/Observer/OptionSaveAfter.php
    app/code/Pektsekye/OptionImages/Model/Plugin/DuplicateProduct.php
    app/code/Pektsekye/OptionImages/Model/ResourceModel/Value/Collection.php
    app/code/Pektsekye/OptionImages/Model/ResourceModel/Value.php
    app/code/Pektsekye/OptionImages/Model/Value.php
    app/code/Pektsekye/OptionImages/Plugin/Catalog/Ui/DataProvider/Product/Form/Modifier/CustomOptions.php
    app/code/Pektsekye/OptionImages/Plugin/Catalog/Ui/DataProvider/Product/ProductCustomOptionsDataProvider.php    
    app/code/Pektsekye/OptionImages/README.md
    app/code/Pektsekye/OptionImages/registration.php
    app/code/Pektsekye/OptionImages/Setup/InstallSchema.php
    app/code/Pektsekye/OptionImages/view/adminhtml/layout/catalog_product_new.xml
    app/code/Pektsekye/OptionImages/view/adminhtml/layout/optionimages_oi_export_index.xml
    app/code/Pektsekye/OptionImages/view/adminhtml/templates/oi/export.phtml
    app/code/Pektsekye/OptionImages/view/adminhtml/templates/product/edit/js.phtml
    app/code/Pektsekye/OptionImages/view/adminhtml/web/main.css
    app/code/Pektsekye/OptionImages/view/adminhtml/web/main.js
    app/code/Pektsekye/OptionImages/view/adminhtml/web/template/form/components/js.html
    app/code/Pektsekye/OptionImages/view/adminhtml/web/template/form/element/input_image.html
    app/code/Pektsekye/OptionImages/view/frontend/layout/catalog_product_view.xml
    app/code/Pektsekye/OptionImages/view/frontend/requirejs-config.js
    app/code/Pektsekye/OptionImages/view/frontend/templates/product/view/js.phtml
    app/code/Pektsekye/OptionImages/view/frontend/web/images/border1.png
    app/code/Pektsekye/OptionImages/view/frontend/web/images/border2.png
    app/code/Pektsekye/OptionImages/view/frontend/web/images/loading.gif
    app/code/Pektsekye/OptionImages/view/frontend/web/images/spacer.gif
    app/code/Pektsekye/OptionImages/view/frontend/web/js/jquery.oxcolorbox-min.js
    app/code/Pektsekye/OptionImages/view/frontend/web/js/main.js
    app/code/Pektsekye/OptionImages/view/frontend/web/js/widget.js
    app/code/Pektsekye/OptionImages/view/frontend/web/main.css
    app/code/Pektsekye/OptionImages/view/frontend/web/oxcolorbox.css  
 

**2)** Connect to your website via SSH:  
Type in Terminal of your computer:  
```
ssh -p 2222 username@yourdomain.com  
```
Then enter your server password  

If you are connected to your server change directory with command:  
```
cd /full_path_to_your_magento_root_directory  
```
Update magento modules with command:  
```
./bin/magento setup:upgrade  
```
>NOTE: If it shows permission error make it executable with command:` chmod +x bin/magento `  


**3)** Manually remove cached _requirejs diectory:
  
    pub/static/_requirejs  


**4)** Refresh magento cache:  
Go to _Magento admin panel -> System -> Cache Managment_ 
 
Click the "Flush Magento Cache" button  





### To disable popup


Go to:  
   _Magento admin panel -> Stores -> Configuration -> Catalog -> Catalog -> Custom Option Images_ 




### To change image size / style


Find and edit ->resize(100) in the file:  

    app/code/local/Pektsekye/OptionImages/Block/Product/View/Js.php  

Then adjust image style in the CSS file:  

    app/code/Pektsekye/OptionImages/view/frontend/web/main.css  




### To export options


**1)** Go to _Magento admin panel -> System -> Product Option Images_  
**2)** Click the "Export Option Images" button. 

**3)** Find product_options.csv file in the "downloads" directory specified in your browser  




### To import options


**1)** Prepare options data with the Excel program:  
Required fields are four:  
"product_sku", "option_title", "type" and "value_title" (when type is "drop_down", "radio", "checkbox" or "multiple").  
Valid option types are:  
"field", "area", "file", "drop_down", "radio", "checkbox", "multiple", "date", "date_time", "time" 
Valid price types are:  
"fixed", "percent" 

The "**image**" field contains the name of the image file that was uploaded into

    pub/media/catalog/product 

directory.  


Sample import file:


```csv
"product_sku","option_title","type","is_require","option_sort_order","max_characters","file_extension","image_size_x","image_size_y","value_title","price","price_type","sku","value_sort_order","image"
"HTC Touch Diamond","Quantity","field","0","0","0","","","","","0.0000","fixed","","",""
"HTC Touch Diamond","Paper Size","drop_down","1","1","","","","","A7","0.0000","fixed","","1","/b/a/banner_5_11.jpg"
"HTC Touch Diamond","Paper Size","drop_down","1","1","","","","","A6","0.0000","fixed","","2","/b/a/banner2_7_2.jpg"
"HTC Touch Diamond","Paper Size","drop_down","1","1","","","","","DL","0.0000","fixed","","3","/c/l/cloth_effect_1.png"
"HTC Touch Diamond","Paper Weight","radio","0","2","","","","","300gsm","0.0000","fixed","","1","/b/a/banner_6.jpg"
"HTC Touch Diamond","Paper Weight","radio","0","2","","","","","325gsm","0.0000","fixed","","2","/b/a/banner2_8.jpg"
"HTC Touch Diamond","Orientation","checkbox","1","3","","","","","Landscape","1.0000","fixed","","1","/b/a/banner_5_12.jpg"
"HTC Touch Diamond","Orientation","checkbox","1","3","","","","","Portrait","1.2000","fixed","","2","/b/a/banner2_7_3.jpg"
"HTC Touch Diamond","Orientation","checkbox","1","3","","","","","Square","1.4000","fixed","","3","/b/a/banner2_7_3.jpg"
"HTC Touch Diamond","dddd","file","0","4","","","0","0","","0.0000","fixed","","",""
```
  

**2)** Go to _Magento admin panel -> System -> Product Option Images_  
**3)** Choose your product_options.csv file and click the "Import Option Images" button.



