<?php

namespace Pektsekye\OptionImages\Controller\Adminhtml\Oi\Export;

class Index extends \Pektsekye\OptionImages\Controller\Adminhtml\Oi\Export
{


  public function execute()
  {  
      $this->_view->loadLayout();
      $this->_setActiveMenu('Pektsekye_OptionImages::oi_export')
          ->_addBreadcrumb(
              __('Catalog'),
              __('Catalog'))
          ->_addBreadcrumb(
              __('Product Option Images'),
              __('Product Option Images')
      );     
      $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Product Option Images'));       
      $this->_view->renderLayout();
  } 

}
