<?php

namespace Pektsekye\OptionImages\Controller\Adminhtml\Oi\Export;

class Export extends \Pektsekye\OptionImages\Controller\Adminhtml\Oi\Export
{


  public function execute()
  {
    $content = $this->_objectManager->create('Pektsekye\OptionImages\Model\Value')->getOptionsCsv();
    
    return $this->_fileFactory->create('product_options.csv', $content); 
                    
  }  

}
