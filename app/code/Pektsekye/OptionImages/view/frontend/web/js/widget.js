
define([
    "jquery",    
    "Pektsekye_OptionImages/js/main",
    "jquery/ui"                                   
],function($, main) {
  "use strict";
  
  $.widget("pektsekye.optionImages", main);
   
});  