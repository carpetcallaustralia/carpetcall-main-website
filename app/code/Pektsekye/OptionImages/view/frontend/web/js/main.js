
define([
    'jquery',
    'mage/template',    
    'jquery/ui',
    'Pektsekye_OptionImages/js/jquery.oxcolorbox-min'
], function ($, mageTemplate) {  

  return {

    oldO   : [],	
    preloadedThumbnail : [],
    
    
    _create : function(){

      $.extend(this, this.options);  
      $.extend(this, this.options.config);
                
      this.imageTemplate = mageTemplate('[data-template=oi-thumbnail]'); 	
    
      this.loadImages();
      
      $(".oi-image").click(function() {
        var src = $(this).attr("ox-data-popup");
        if (src){  
          $.oxcolorbox({
            maxWidth  : '95%', 
            maxHeight : '95%',
            photo     : true, 
            href      : src
          });
        }      
      });
      
    },

  
    loadImages : function(){
      var e,optionId,dd,isNewOption,valueId,prevVId;
      var elements = $('.product-custom-option');
      for (var n = 0;n < elements.length; n++){
        e = $(elements[n]);
        
        var optionIdStartIndex, optionIdEndIndex;
        if (e.is(":file")) {
            optionIdStartIndex = e.attr('name').indexOf('_') + 1;
            optionIdEndIndex = e.attr('name').lastIndexOf('_');
        } else {
            optionIdStartIndex = e.attr('name').indexOf('[') + 1;
            optionIdEndIndex = e.attr('name').indexOf(']');
        }
        
        optionId = parseInt(e.attr('name').substring(optionIdStartIndex, optionIdEndIndex), 10);
      
        if (!this.oldO[optionId]){	
          this.oldO[optionId] = {};	
          dd = e[0].type == 'radio' || e[0].type == 'checkbox' ? e.closest('.options-list').closest('.field') : e.closest('.field');
          isNewOption = true;        
        }

        if (e[0].type == 'radio') { 
        
            valueId = e.val();      
            if (this.thumbnail[valueId]){												
                this.preloadedThumbnail[valueId] = new Image();
                this.preloadedThumbnail[valueId].src = this.thumbnail[valueId];
                if (isNewOption){
                  dd.addClass('oi-above-radio');
                  dd.find('.control').before(this.imageTemplate({id : optionId, image : this.spacer}));				
                  isNewOption = false;
                }
            }																							     
            e.click($.proxy(this.observeRadio, this, optionId, valueId));
						if (e[0].checked){		
						  this.observeRadio(optionId, valueId);
						}                                       
        } else if (e[0].type == 'checkbox') {
        
            valueId = e.val();       
            if (this.thumbnail[valueId]){
              var imageHtml = this.imageTemplate({id : 'value_'+valueId, image : this.thumbnail[valueId]});												
              if (isNewOption){
                dd.addClass('oi-above-checkbox');
                dd.find('.control').before(imageHtml);	            				
                isNewOption = false;								
              } else {	
                $('#option_image_value_' + prevVId).after(imageHtml);                            														
              }
              
              this.addPopupToImage($('#option_image_value_' + valueId), this.image[valueId]);
              
              prevVId = valueId; 
              e.click($.proxy(this.observeCheckbox, this, e, valueId)); 
              if (e[0].checked){		
                this.observeCheckbox(e, valueId);
              }                         								
            }                          
            
        } else if (e[0].type == 'select-one' && !e.hasClass('datetime-picker')) {
        
            var options = e[0].options;
            for (var i = 0, len = options.length; i < len; ++i){
              if (options[i].value){
                valueId = options[i].value;
                if (this.thumbnail[valueId]){												
                  this.preloadedThumbnail[valueId] = new Image();
                  this.preloadedThumbnail[valueId].src = this.thumbnail[valueId];
                }	   
              }      
            }     
            dd.addClass('oi-above-select');           
            dd.find('.control').before(this.imageTemplate({id : optionId, image : this.spacer}));														
            e.change($.proxy(this.observeSelectOne, this, e, optionId));     	
            if (e[0].selectedIndex > 0){		
              this.observeSelectOne(e, optionId);
            }                
        } else if (e[0].type == 'select-multiple') {
        
            var options = e[0].options;
            for (var i = 0, len = options.length; i < len; ++i){
              if (options[i].value){
                valueId = options[i].value;
                if (this.thumbnail[valueId]){
                  var imageHtml = this.imageTemplate({id : 'value_'+valueId, image : this.thumbnail[valueId]});												
                  if (isNewOption){
                    dd.addClass('oi-above-select-multiple');
                    dd.find('.control').before(imageHtml);	            				
                    isNewOption = false;								
                  } else {	
                    $('#option_image_value_' + prevVId).after(imageHtml);                            														
                  }
                  this.addPopupToImage($('#option_image_value_' + valueId), this.image[valueId]);
                  prevVId = valueId;	                 
                }	   
              }      
            }            
            e.change($.proxy(this.observeSelectMultiple, this, e));
            if (e[0].selectedIndex > -1){		
              this.observeSelectMultiple(e);
            }                     
        } 
      
      };
    },


    observeRadio : function(optionId, valueId){
      this.updateImage(optionId, valueId);
    },  


    observeCheckbox : function(element, valueId){
      if (element[0].checked){								
        $('#option_image_value_' + valueId).show();								
      } else {								
        $('#option_image_value_' + valueId).hide();								
      }	
    },
 
 
    observeSelectOne : function(element, optionId){
      var valueId = element.val();  
      this.updateImage(optionId, valueId);				
    },
 
 
    observeSelectMultiple : function(element){
      var vId;  
      var options = element[0].options;
      for (var i = 0; i < options.length; ++i){
        if (options[i].value){
          vId = options[i].value;
          if (options[i].selected){										
            $('#option_image_value_' + vId).show();										
          } else {										
            $('#option_image_value_' + vId).hide();										
          }	  
        }      
      }   
    },
    
    
    updateImage : function(optionId, valueId){
      var image = $('#option_image_'+optionId);
      if (image.length == 0)
        return;

      if (valueId != '' && this.thumbnail[valueId]){		      
        this.addPopupToImage(image, this.image[valueId]);           
        image[0].src = this.preloadedThumbnail[valueId].src;
        image.show();
      } else {	     
        this.removeImagePopup(image);    
        image.hide();
      }
    },    


    addPopupToImage : function(image, popupSrc){      
      if (popupSrc && this.popupEnabled){		           
        image[0].style.cursor = 'pointer';
        image[0].title = this.clickToEnlargeText;
        image.attr('ox-data-popup', popupSrc);
      }   
    },
    
    
    removeImagePopup : function(image){      
      if (this.popupEnabled){		           
        image[0].style.cursor = null;
        image[0].title = '';
        image.removeAttr('ox-data-popup');
      }   
    }           
        
  };


});














