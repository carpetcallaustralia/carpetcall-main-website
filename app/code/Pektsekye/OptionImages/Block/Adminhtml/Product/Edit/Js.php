<?php

namespace Pektsekye\OptionImages\Block\Adminhtml\Product\Edit;

class Js extends \Magento\Backend\Block\Widget
{

    protected $_oiValue;    
    protected $_fileSizeService;
    protected $_coreRegistry;    
    protected $_imageHelper;    
    protected $_jsonEncoder;

    public function __construct(  
        \Pektsekye\OptionImages\Model\Value $oiValue,        
        \Magento\Framework\File\Size $fileSize,          
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,      
        \Magento\Backend\Block\Widget\Context $context,                           
        array $data = array()
    ) {
        $this->_oiValue = $oiValue;   
        $this->_fileSizeService = $fileSize;               
        $this->_coreRegistry = $registry;
        $this->_imageHelper = $imageHelper;
        $this->_jsonEncoder = $jsonEncoder;                  
        parent::__construct($context, $data);
    }
   
    
    public function getProduct()
    {
      if (!$this->hasData('product')) {
        $this->setData('product', $this->_coreRegistry->registry('product'));
      }     
      return $this->getData('product');
    }


	
    public function getDataJson()
    { 
      $config = array();
      
      return $this->_jsonEncoder->encode($config);
    }



    public function getUploadUrl()
    {
      return $this->_urlBuilder->getUrl('catalog/product_gallery/upload');
    }
    
    

    public function getFileSizeService()
    {
      return $this->_fileSizeService;
    }
    	
}