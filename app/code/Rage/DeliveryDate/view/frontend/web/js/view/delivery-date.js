define([
    'jquery',
    'ko',
    'uiComponent'
], function ($, ko, Component) {
    'use strict';
    var date_hint = window.checkoutConfig.shipping.rgdd_delivery_date.date_hint;
    var notes_hint = window.checkoutConfig.shipping.rgdd_delivery_date.notes_hint;
    return Component.extend({
        defaults: {
            template: 'Rage_DeliveryDate/delivery-date'
        },
        initialize: function () {
            this._super();
            var is_unavailable_day = window.checkoutConfig.shipping.rgdd_delivery_date.is_unavailable_day;
            var disabled = window.checkoutConfig.shipping.rgdd_delivery_date.disabled;
            console.log('test disbale');
            console.log(disabled);
            var format = window.checkoutConfig.shipping.rgdd_delivery_date.format;
            if (!format) {
                format = 'yy-mm-dd';
            }
            if (is_unavailable_day == 1 && disabled) {
                var disabledDay = disabled.split(",").map(function (item) {
                    return parseInt(item, 10);
                });
            }
            ko.bindingHandlers.datetimepicker = {
                init: function (element, valueAccessor, allBindingsAccessor) {
                    var $el = $(element);
                    if (is_unavailable_day == 0) {
                        var options = {
                            minDate: 0,
                            dateFormat: format,
                            'showTimepicker': false,
                            showButtonPanel: false,
                        };
                    } else {
                        var options = {
                            minDate: 0,
                            dateFormat: format,
                            'showTimepicker': false,
                            showButtonPanel: false,
                            beforeShowDay: function (date) {
                                var day = date.getDay();
                                if (disabledDay.indexOf(day) > -1) {
                                    return [false];
                                } else {
                                    return [true];
                                }
                            }
                        };
                    }
                    $el.datetimepicker(options);
                    var writable = valueAccessor();
                    if (!ko.isObservable(writable)) {
                        var propWriters = allBindingsAccessor()._ko_property_writers;
                        if (propWriters && propWriters.datetimepicker) {
                            writable = propWriters.datetimepicker;
                        } else {
                            return;
                        }
                    }
                    writable($(element).datetimepicker("getDate"));
                },
                update: function (element, valueAccessor) {
                    var widget = $(element).data("DateTimePicker");
                    if (widget) {
                        var date = ko.utils.unwrapObservable(valueAccessor());
                        widget.date(date);
                    }
                }
            };

            return this;
        },
        ko_date_hint: ko.observable(date_hint),
        ko_notes_hint: ko.observable(notes_hint),
    });
});
