<?php

/**
 * MagePrince
 * Copyright (C) 2020 Mageprince <info@mageprince.com>
 *
 * @package Mageprince_Faq
 * @copyright Copyright (c) 2020 Mageprince (http://www.mageprince.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author MagePrince <info@mageprince.com>
 */

namespace Mageprince\Faq\Model;

use Mageprince\Faq\Model\ResourceModel\Faq\CollectionFactory;
use Mageprince\Faq\Helper\Data as HelperData;
use Mageprince\Faq\Api\FaqsCategoryRepositoryInterface;
use Magento\Framework\View\Element\Template;

class FaqsCategoryRepository implements FaqsCategoryRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    protected $faqCollectionFactory;
    /**
     * @var FaqGroupCollectionFactory
     */
    protected $faqGroupCollectionFactory;

     /**
     * @var HelperData
     */
    protected $helper;

    /**
     * Index constructor.
     * @param CollectionFactory $faqCollectionFactory
    * @param FaqGroupCollectionFactory $faqGroupCollectionFactory
    * @param HelperData $helper
    */

    public function __construct(
        Template\Context $context,
        CollectionFactory $faqCollectionFactory,
        HelperData $helper
    ) {
        $this->faqCollectionFactory = $faqCollectionFactory;
        $this->helper = $helper;
        $this->storeManager = $context->getStoreManager();
    }
                        
    public function getFaqsByCategory($category_id)
    {
        //echo $category_id;die();
        if ($category_id) {
            $group = $category_id;
        }
        $faqCollection = $this->faqCollectionFactory->create();
        $faqCollection->addFieldToFilter('group', ['like' => '%'.$group.'%']);
        $this->filterCollectionData($faqCollection);
        
        $faqGroupCollectionArray = array();
        $faqGroupCollectionArray = $faqCollection->getData();
        // echo "<pre>";
        // print_r($faqGroupCollectionArray);
        // die();
        return $faqGroupCollectionArray;   
    }

    /**
     * Filter collection data
     *
     * @param $collection
     * @throws NoSuchEntityException
     */
    private function filterCollectionData($collection)
    {
        $collection->addFieldToFilter('status', 1);
        $collection->addFieldToFilter(
            'customer_group',
            [
                ['null' => true],
                ['finset' => $this->helper->getCurrentCustomer()]
            ]
        );
        $collection->addFieldToFilter(
            'storeview',
            [
                ['eq' => 0],
                ['finset' => $this->getCurrentStore()]
            ]
        );
        $collection->setOrder('sortorder', 'ASC');
    }

    public function getCurrentStore()
    {
        return $this->storeManager->getStore()->getId();
    }
}
