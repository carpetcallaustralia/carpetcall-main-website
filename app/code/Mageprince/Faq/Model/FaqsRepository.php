<?php

/**
 * MagePrince
 * Copyright (C) 2020 Mageprince <info@mageprince.com>
 *
 * @package Mageprince_Faq
 * @copyright Copyright (c) 2020 Mageprince (http://www.mageprince.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author MagePrince <info@mageprince.com>
 */

namespace Mageprince\Faq\Model;

use Mageprince\Faq\Model\ResourceModel\FaqGroup\CollectionFactory as FaqGroupCollectionFactory;
use Mageprince\Faq\Helper\Data as HelperData;
use Mageprince\Faq\Api\FaqsRepositoryInterface;
use Magento\Framework\View\Element\Template;

class FaqsRepository implements FaqsRepositoryInterface
{

    /**
     * @var FaqGroupCollectionFactory
     */
    protected $faqGroupCollectionFactory;

     /**
     * @var HelperData
     */
    protected $helper;

    /**
     * Index constructor.
    * @param FaqGroupCollectionFactory $faqGroupCollectionFactory
    * @param HelperData $helper
    */

    public function __construct(
        Template\Context $context,
        FaqGroupCollectionFactory $faqGroupCollectionFactory,
        HelperData $helper
    ) {
        $this->faqGroupCollectionFactory = $faqGroupCollectionFactory;
        $this->helper = $helper;
        $this->storeManager = $context->getStoreManager();
    }

    /**
     * {@inheritdoc}
     */
    public function getFaqCategoryList()
    {
        $faqGroupCollection = $this->faqGroupCollectionFactory->create();
        $this->filterCollectionData($faqGroupCollection);

        $faqGroupCollectionArray = array();
        $faqGroupCollectionArray = $faqGroupCollection->getData();
        // echo "<pre>";
        // print_r($faqGroupCollectionArray);
        // die();
        return $faqGroupCollectionArray;   
    }

    /**
     * Filter collection data
     *
     * @param $collection
     * @throws NoSuchEntityException
     */
    private function filterCollectionData($collection)
    {
        $collection->addFieldToFilter('status', 1);
        $collection->addFieldToFilter(
            'customer_group',
            [
                ['null' => true],
                ['finset' => $this->helper->getCurrentCustomer()]
            ]
        );
        $collection->addFieldToFilter(
            'storeview',
            [
                ['eq' => 0],
                ['finset' => $this->getCurrentStore()]
            ]
        );
        $collection->setOrder('sortorder', 'ASC');
    }

    public function getCurrentStore()
    {
        return $this->storeManager->getStore()->getId();
    }
}
