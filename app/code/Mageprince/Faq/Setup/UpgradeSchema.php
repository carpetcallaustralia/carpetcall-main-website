<?php 
namespace Mageprince\Faq\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{ 
	public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
	{
		$setup->startSetup();
        if (version_compare($context->getVersion(), '2.0.5') < 0) {

            // Get module table
            $tableName = $setup->getTable('prince_faqgroup');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'groupdescription' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '2M',
                        'nullable' => true,
                        'comment' => 'Group Description',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }

            }
        }
	}
}	