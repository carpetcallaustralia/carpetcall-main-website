<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Cron;

/**
 * Openpay Check Version
 */  
class Checkversion
{
    const URL_VERSION = 'https://openpaytestandtrain.com.au/openpayversionchkv2/';
    const URL_SAMPLE = 'https://openpaytestandtrain.com.au/magento2/';

    /**
     * @var \Magento\Framework\Module\ResourceInterface
     */    
    protected $moduleResource;

    /**
     * @var \Magento\AdminNotification\Model\ResourceModel\Inbox\Collection
     */    
    protected $notification;

    /**
     * @var \Magento\Framework\Notification\NotifierPool
     */    
    protected $notifierPool;

    /**
     * @var \Magento\Framework\HTTP\Client\CurlFactory
     */    
    private $curlFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */    
    private $logger;
    
    /**
     * Construct
     *
     * @param \Magento\Framework\Module\ResourceInterface $moduleResource
     * @param \Magento\AdminNotification\Model\ResourceModel\Inbox\Collection $notification
     * @param \Magento\Framework\Notification\NotifierPool $notifierPool
     * @param \Magento\Framework\HTTP\Client\CurlFactory $curlFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Module\ResourceInterface $moduleResource,
        \Magento\AdminNotification\Model\ResourceModel\Inbox\Collection $notification,
        \Magento\Framework\Notification\NotifierPool $notifierPool,
        \Magento\Framework\HTTP\Client\CurlFactory $curlFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->moduleResource = $moduleResource;
        $this->notification = $notification;
        $this->notifierPool = $notifierPool;
        $this->curlFactory = $curlFactory;
        $this->logger = $logger;
    }
    
    /**
     * Method executed when cron runs in server
     */
    public function execute()
    {
        $currentVersion = $this->moduleResource->getDbVersion('Aopen_Openpay');
        $curl = $this->curlFactory->create();
        $curl->get(self::URL_VERSION);
        if ($curl->getStatus() === 200) {
            $result =  $curl->getBody();
        } else {
            $this->_logger->critical('Error Processing in Request.');
            return false;
        }
  
        if ($result) {
            if ($result != $currentVersion) {
                $notices = $this->notification->addFieldToFilter(
                    'title',
                    'Your Openpay Payment extension is outdated.Please update to its latest version.'
                )->load();
                if (count($notices)==0) {
                        $sampleUrl = self::URL_SAMPLE;
                        $this->notifierPool->addMajor(
                            'Your Openpay Payment extension is outdated.Please update to its latest version.',
                            '',
                            $sampleUrl
                        );
                }
            } else {
                $messages = $this->notification->addFieldToFilter(
                    'title',
                    'Your Openpay Payment extension is outdated.Please update to its latest version.'
                )->load();
                if (count($messages)) {
                    foreach ($messages as $notify) {
                        $notify->delete();
                    }
                }
            }
        }
    }
}
