<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Cron;

/**
 * Openpay cancel Pending order
 */ 
class Cancelpending
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */    
    protected $cancelOrder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;    

    /**
     * @var \Aopen\Openpay\Model\Api
     */
    protected $openpayApi;

    /**
     * Construct
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Sales\Api\Data\OrderInterface $cancelOrder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Aopen\Openpay\Model\Api $openpayApi
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\Data\OrderInterface $cancelOrder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Aopen\Openpay\Model\Api $openpayApi
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->logger = $logger;
        $this->cancelOrder = $cancelOrder;
        $this->scopeConfig = $scopeConfig;
        $this->getOpenpayApi = $openpayApi;
    }
    
    /**
     * Method executed when cron runs in server
     */
    public function execute()
    {
        if ($cancelPendingOrders = $this->scopeConfig->getValue(
            'payment/openpay/cancel_pending',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )) {
            $frequency = $this->scopeConfig->getValue(
                'payment/openpay/cancel_pending_frequency',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        
            $orders = $this->orderCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('state', 'new')
            ->addFieldToFilter('status', 'pending_approval');
            foreach ($orders as $order) {
                $orderAge = (-1*(strtotime($order->getUpdatedAt()) - time()));
                
                if ($orderAge > $frequency) {
                    $planApproved = $this->getOpenpayApi->getOpenpayOrderStatus($order->getId());
                    if (!$planApproved) {
                        $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
                        $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                        $order->addStatusToHistory($order->getStatus(), 'Plan is not active.');
                        $order->save();
                    } else {
                        $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                        $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                        $order->addStatusToHistory($order->getStatus(), 'Plan is active.');
                        $order->save();
                    }
                }
            }
        }
        return $this;
    }
}
