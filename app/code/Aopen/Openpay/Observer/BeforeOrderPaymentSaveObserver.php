<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Observer;

use Magento\Framework\Event\ObserverInterface;
use Aopen\Openpay\Model\Openpay;

/**
 * BeforeOrderPaymentSaveObserver Observer
 */
class BeforeOrderPaymentSaveObserver implements ObserverInterface
{
    /**
     * Sets current instructions for bank transfer account
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $observer->getEvent()->getPayment();
        $instructionMethods = [
            Openpay::PAYMENT_METHOD_OPENPAY_CODE
        ];
        if (in_array($payment->getMethod(), $instructionMethods)) {
            $payment->setAdditionalInformation(
                'instructions',
                $payment->getMethodInstance()->getInstructions()
            );
        }
    }
}
