<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;

/**
 * FraudObserver Observer
 */
class FraudObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */    
    protected $orderRepository;

    /**
     * @var \Aopen\Openpay\Model\Api
     */    
    protected $openpayApi;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Aopen\Openpay\Model\Api $openpayApi
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Aopen\Openpay\Model\Api $openpayApi
    ) {
          $this->request = $request;
          $this->orderRepository = $orderRepository;
          $this->getOpenpayApi = $openpayApi;
    }

    /**
     * InitOrder
     *
     * @return bool|\Magento\Sales\Api\OrderRepositoryInterfac
     */
    protected function _initOrder() {      
        $id = $this->request->getParam('order_id');
        try {
            $order = $this->orderRepository->get($id);
        } catch (NoSuchEntityException $e) {
            return false;
        } catch (InputException $e) {
            return false;
        }
        return $order;
    }

    /**
     * Execute
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $this->_initOrder();
        if ($order) {
            $paymentMethod = $order->getPayment()->getMethodInstance()->getCode();
            $data = $this->request->getPost('history');
            $comment = $data['comment'];
            $status  = $data['status'];
            $notify = $data['is_customer_notified'] ?? false;
            $visible = $data['is_visible_on_front'] ?? false;
            $order_id = $this->request->getParam('order_id');
            if ($paymentMethod == 'openpay' && $status == 'fraud') {
                $planId = $this->getOpenpayApi->getOrderPlanId($order_id);
                $result = $this->getOpenpayApi->onlineFraudCall($planId, $comment);
                $xml = new \SimpleXMLElement($result);
                $array = get_object_vars($xml);
            
                if ($array['status'] == 0) {
                        $history = $order->addStatusHistoryComment('Fraud Reported to Openpay');
                        $history->setIsVisibleOnFront($visible);
                        $history->setIsCustomerNotified($notify);
                        $history->save();
                } else {
                    $history = $order->addStatusHistoryComment('Fraud Call Rejected - please contact to Openpay.');
                    $history->save();
                }
            }
        }
    }
}
