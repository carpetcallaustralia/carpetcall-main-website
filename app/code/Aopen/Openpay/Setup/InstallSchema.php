<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Setup;

/**
 * Class InstallSchema 
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $_resourceConfig;

    /**
     * Construct
     *
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     */
    public function __construct(
        \Magento\Config\Model\ResourceModel\Config $resourceConfig
    ) {
        $this->_resourceConfig = $resourceConfig;
    }

    /**
     * Install 
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {

        $installer = $setup;

        // Required tables
        $statusTable = $installer->getTable('sales_order_status');
        $statusStateTable = $installer->getTable('sales_order_status_state');

        $installer->startSetup();

        // Insert statuses
        $installer->getConnection()->insertArray(
            $statusTable,
            [
                'status',
                'label'
            ],
            [
                ['status' => 'pending_approval', 'label' => 'Pending Openpay Approval']
            ]
        );
         
        // Insert states and mapping of statuses to states
        $installer->getConnection()->insertArray(
            $statusStateTable,
            [
                'status',
                'state',
                'is_default'
            ],
            [
                [
                    'status' => 'pending_approval',
                    'state' => 'new',
                    'is_default' => 0
                ]
            ]
        );

        $installer->endSetup();
    }
}
