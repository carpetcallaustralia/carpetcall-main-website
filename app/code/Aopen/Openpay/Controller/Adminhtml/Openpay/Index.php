<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Controller\Adminhtml\Openpay;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Openpay Min Max Update Controller
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Aopen\Openpay\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */    
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */    
    protected $configWriter;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */    
    protected $messageManager;

    /**
     * Construct
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Aopen\Openpay\Helper\Data $helper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */     
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Aopen\Openpay\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->getHelper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->messageManager = $messageManager;
    }

    /**
     * Method Execute
     */     
    public function execute() {
        $path = ['MinMaxPurchasePrice'];
        $jamAuthToken = $this->scopeConfig->getValue(
            'payment/openpay/jam_auth_token',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $urlMethodName = "MinMaxPurchasePrice";
        $xml = "<MinMaxPurchasePrice> 
        <JamAuthToken>".$jamAuthToken."</JamAuthToken>
        </MinMaxPurchasePrice>";
        $xmlResponse = $this->getHelper->makecurlCall($path, $xml);
        if ($xmlResponse) {
            $xmlRes = simplexml_load_string($xmlResponse);
            $array = get_object_vars($xmlRes);
            if ($array['status'] == 0) {
                    $this->configWriter->save(
                        'payment/openpay/min_order_total',
                        $array['MinPrice'],
                        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                        $scopeId = 0
                    );

                    $this->configWriter->save(
                        'payment/openpay/max_order_total',
                        $array['MaxPrice'],
                        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                        $scopeId = 0
                    );
                    $this->messageManager->addSuccess(__("Min Max value imported successfully!"));
            } else {
                $this->messageManager->addError(__("Can not import Min Max Value. Please try after 24 Hour!"));
            }
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
