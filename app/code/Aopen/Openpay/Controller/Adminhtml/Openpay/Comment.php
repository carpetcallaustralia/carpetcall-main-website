<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Controller\Adminhtml\Openpay;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Openpay Comment Controller
 */
class Comment extends \Magento\Backend\App\Action
{
    /**
     * @var \Aopen\Openpay\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */    
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */    
    protected $configWriter;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */    
    protected $messageManager;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */    
    protected $fileDriver;    

    /**
     * Construct
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Aopen\Openpay\Helper\Data $helper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Filesystem\Driver\File $fileDriver
     */    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Aopen\Openpay\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Filesystem\Driver\File $fileDriver
    ) {
        parent::__construct($context);
        $this->getHelper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->messageManager = $messageManager;
        $this->fileDriver = $fileDriver;
    }

    /**
     * Method Execute
     */    
    public function execute()
    {
        $html = '';
        if ($this->fileDriver->isExists(BP . $this->getHelper->logfile)) {
            $html .= '<pre>';
            $html .= htmlentities($this->fileDriver->fileGetContents(BP . $this->getHelper->logfile));
            $html .= '</pre>';
        } else {
            $html .= 'logfile: '.$this->getHelper->logfile . ' does not exist';
            $this->messageManager->addError(__($html));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        return $resultRaw->setContents($html);
    }
}
