<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Controller\Handoverurl;

/**
 * Openpay Handoverurl Controller
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Aopen\Openpay\Model\Openpay
     */
    protected $openpayApi;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Aopen\Openpay\Model\Api $openpayApi
     * @param \Magento\Checkout\Model\Session $session
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Aopen\Openpay\Model\Api $openpayApi,
        \Magento\Checkout\Model\Session $session
    ) {
        parent::__construct($context);
        $this->getOpenpayApi = $openpayApi;
        $this->getSession = $session;
    }

    protected function getLastOrder() {
        return $this->getSession->getLastRealOrder();
    }
    
    /**
     * Method Execute
     */
    public function execute() {
        if ($this->getLastOrder()) {
            $response = [];
            $lastOrder = $this->getLastOrder()->getData();
            $address = $this->getLastOrder()->getBillingAddress()->getData();
            $street = explode("\n", $address['street']);
            $street[1] = (isset($street[1])) ? $street[1] : '';
             /*shipping add*/
            $_shipping_address = $this->getLastOrder()->getShippingAddress()->getData();
            $_shippingstreet = explode("\n", $_shipping_address['street']);
            $_shippingstreet[1] = (isset($_shippingstreet[1])) ? $_shippingstreet[1] : '';
            if ($this->getLastOrder()->getCustomerIsGuest()) {
                $customer_firstname = $address['firstname'];
                $customer_lastname = $address['lastname'];
            } else {
                $customer_firstname =  $lastOrder['customer_firstname'];
                $customer_lastname  =  $lastOrder['customer_lastname'];
            }
           
            $this->getSession->clearStorage();
            $url = $this->getOpenpayApi->getOpenpayUrl(
                $lastOrder['entity_id'],
                $lastOrder['grand_total'],
                $lastOrder['increment_id'],
                $customer_firstname,
                $customer_lastname,
                $lastOrder['customer_email'],
                $lastOrder['customer_dob'],
                $street[0],
                $street[1],
                $address['city'],
                $address['region'],
                $address['postcode'],
                $address['telephone'],
                $_shippingstreet[0],
                $_shippingstreet[1],
                $_shipping_address['city'],
                $_shipping_address['region'],
                $_shipping_address['postcode']
            );
            if (substr($url, 0, 4) == 'http') {
                return $this->resultRedirectFactory->create()->setUrl($url);
            } else {
                return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
            }
        }
    }
}
