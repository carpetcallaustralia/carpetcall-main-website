<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Controller\Order;

/**
 * Openpay Callback Controller
 */
class Callback extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Aopen\Openpay\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $sendEmail;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $transaction;

    /**
     * @var \Aopen\Openpay\Model\Api
     */
    protected $openpayApi;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Aopen\Openpay\Model\ValidateOrder
     */
    private $validateOrder;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Aopen\Openpay\Helper\Data $helper
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $sendEmail
     * @param \Magento\Sales\Model\Order $orderFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\DB\Transaction $transaction
     * @param \Aopen\Openpay\Model\Api $openpayApi
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Aopen\Openpay\Helper\Data $helper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $sendEmail,
        \Magento\Sales\Model\Order $orderFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Aopen\Openpay\Model\Api $openpayApi,
        \Aopen\Openpay\Model\ValidateOrder $validateOrder,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($context);
        $this->getHelper = $helper;
        $this->sendEmail = $sendEmail;
        $this->getOrder = $orderFactory;
        $this->scopeConfig = $scopeConfig;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->getOpenpayApi = $openpayApi;
        $this->checkoutSession = $checkoutSession;
        $this->validateOrder = $validateOrder;
    }

    /**
     * Save Transaction
     *
     * @param \Magento\Sales\Model\Order $order
     * @param string $planId
     * @return void
     */
    protected function saveTransaction($order, $planId) {
        $order->setState('processing')->setStatus('processing');
        $payment = $order->getPayment();
        $payment->setTransactionId($planId);
        $transaction = $payment->addTransaction(
            \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE,
            null,
            false,
            'Approved by openpay'
        );
        $transaction->setParentTxnId($planId);
        $transaction->setAdditionalInformation('planId', $planId);
        $transaction->setIsClosed(0);
        $transaction->save();
        $order->save();
        $this->sendEmail->send($order, true);
    }

    /**
     * Method Execute
     */
    public function execute() {
        $can_invoice = $this->scopeConfig->getValue(
            'payment/openpay/automatic_invoice',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $id = $this->getRequest()->getParam('orderid');
        $urlPlanId = $this->getRequest()->getParam('planid');
        $orderId =  $this->getHelper->getOrderId($id);
        $hash = hash('sha256', $orderId . $this->getHelper->getJamAuthToken() . $id);
        $status = $this->getRequest()->getParam('status');
        $order = $this->getHelper->valid($orderId, $hash);

        /**
         * Add Some Additional validation
         */
        if($order){
            if(!$this->validateOrder->validate($order,true,$urlPlanId) || !$urlPlanId){
                $this->messageManager->addErrorMessage('Invalid order/Session Expired');
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout/cart');
                return $resultRedirect;
            }
        }
        if ($order) {
            if ($status == 'LODGED') {
                /*call 3*/
                $res = $this->getOpenpayApi->getOnlineOrderCapturePayment($urlPlanId);
                $xml = new \SimpleXMLElement($res);
                $array = get_object_vars($xml);
                if ($array['status'] ==0) {
                    $this->saveTransaction($order, $urlPlanId);
                    $ext = '/id/' . $orderId . '/hash/' . $hash;
                    if ($order->canInvoice()) {
                        $invoice = $this->invoiceService->prepareInvoice($order);
                        $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                        $invoice->register();
                        $invoice->save();
                        $transactionSave = $this->transaction->addObject(
                            $invoice
                        )->addObject(
                            $invoice->getOrder()
                        );
                        $transactionSave->save();

                        $order->addStatusHistoryComment(
                            __('Notified customer about invoice #%1.', $invoice->getId())
                        )
                        ->setIsCustomerNotified(true)
                        ->save();
                    }
                    if ($order) {
                        $this->checkoutSession
                        ->setLastQuoteId($order->getQuoteId())
                        ->setLastSuccessQuoteId($order->getQuoteId())
                        ->clearHelperData();

                        $this->checkoutSession->setLastOrderId($order->getId())
                        ->setLastRealOrderId($order->getIncrementId())
                        ->setLastOrderStatus($order->getStatus());
                        $this->_redirect('checkout/onepage/success');
                    }
                } else {
                    $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                    $order->addStatusToHistory($order->getStatus(), 'Openpay Payment Cancelled');
                    $order->save();
                    $this->checkoutSession->setLastQuoteId($order->getQuoteId());
                    $this->checkoutSession->setLastOrderId($order->getId());
                    $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
                    $this->checkoutSession->setErrorMessage('Your transaction with openpay failed!');
                    $this->_redirect('checkout/onepage/failure');
                }
            } else {
                    $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                    $order->addStatusToHistory($order->getStatus(), 'Openpay Payment Cancelled');
                    $order->save();
                    $this->checkoutSession->setLastQuoteId($order->getQuoteId());
                    $this->checkoutSession->setLastOrderId($order->getId());
                    $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
                    $this->checkoutSession->setErrorMessage('Your transaction with openpay failed!');
                    $this->_redirect('checkout/onepage/failure');
            }
        } else {
            $this->messageManager->addWarningMessage('Error processing in openpay order.');
            $this->_redirect('checkout/cart');
        }
    }
}
