<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Controller\Order;

/**
 * Openpay Cancel Controller
 */
class Cancel extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Aopen\Openpay\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;
    /**
     * @var \Aopen\Openpay\Model\ValidateOrder
     */
    private $validateOrder;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Aopen\Openpay\Helper\Data $helper
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Aopen\Openpay\Helper\Data $helper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Aopen\Openpay\Model\ValidateOrder $validateOrder,
        \Magento\Quote\Model\QuoteFactory $quoteFactory
    ) {
        parent::__construct($context);
        $this->getHelper = $helper;
        $this->checkoutSession = $checkoutSession;
        $this->quoteFactory = $quoteFactory;
        $this->quoteRepository = $quoteRepository;
        $this->validateOrder = $validateOrder;
    }

    /**
     * Method Execute
     */
    public function execute() {
        $id = $this->getRequest()->getParam('orderid');
        $orderId =  $this->getHelper->getOrderId($id);
        $hash = hash('sha256', $orderId . $this->getHelper->getJamAuthToken() . $id);
        $order = $this->getHelper->valid($orderId, $hash);
        $planId = $this->getRequest()->getParam('planid');

        /**
         * Add Some Additional validation
         */
        if($order){
            if(!$this->validateOrder->validate($order,true,$planId) || !$planId){
                $this->messageManager->addErrorMessage('Invalid order/Session Expired');
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout/cart');
                return $resultRedirect;
            }
        }

        if($order){
            $quote = $this->quoteFactory->create()->loadByIdWithoutStore($order->getQuoteId());
            if ($quote->getId()) {
                $quote->setIsActive(1)->setReservedOrderId(null)->save();
                $this->checkoutSession->replaceQuote($quote);
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout/cart');
                $this->messageManager->addWarningMessage('Openpay payment canceled.');
                return $resultRedirect;
            }
        }
    }
}
