<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Controller\Order;

/**
 * Openpay Fail Controller
 */
class Fail extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Aopen\Openpay\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Aopen\Openpay\Model\ValidateOrder
     */
    private $validateOrder;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Aopen\Openpay\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Aopen\Openpay\Helper\Data $helper,
        \Aopen\Openpay\Model\ValidateOrder $validateOrder,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($context);
        $this->getHelper = $helper;
        $this->checkoutSession = $checkoutSession;
        $this->validateOrder = $validateOrder;
    }

    /**
     * Method Execute
     */
    public function execute() {
        $id = $this->getRequest()->getParam('orderid');
        $orderId =  $this->getHelper->getOrderId($id);
        $hash = hash('sha256', $orderId . $this->getHelper->getJamAuthToken() . $id);
        $order = $this->getHelper->valid($orderId, $hash);
        $planId = $this->getRequest()->getParam('planid');

        /**
         * Add Some Additional validation
         */
        if($order){
            if(!$this->validateOrder->validate($order,true,$planId) || !$planId){
                $this->messageManager->addErrorMessage('Invalid order/Session Expired');
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout/cart');
                return $resultRedirect;
            }
        }
        if ($order) {
            $order->setState('processing')->setStatus('processing');
            $this->checkoutSession->setLastQuoteId($order->getQuoteId());
            $this->checkoutSession->setLastOrderId($order->getId());
            $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
            $this->checkoutSession->setErrorMessage('Your transaction with openpay failed!');
            $this->_redirect('checkout/onepage/failure');
        }
    }
}
