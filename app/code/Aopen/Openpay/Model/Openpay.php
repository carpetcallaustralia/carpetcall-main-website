<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Model;

use Magento\Directory\Helper\Data as DirectoryHelper;

/**
 * Openpay payment method model
 */
class Openpay extends \Magento\Payment\Model\Method\AbstractMethod
{
    
    const PAYMENT_METHOD_OPENPAY_CODE = 'openpay';

    /**
     * Payment method code
     *
     * @var string
     */

    protected $_code = self::PAYMENT_METHOD_OPENPAY_CODE;

    /**
     * Openpay payment block paths
     *
     * @var string
     */
    protected $_formBlockType = \Aopen\Openpay\Block\Form\Openpay::class;
    
    /**
     * Info instructions block path
     *
     * @var string
     */
    protected $_infoBlockType = \Magento\Payment\Block\Info\Instructions::class;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = false;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canOrder                   = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canCapture                 = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canRefund                  = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canUseForMultishipping     = false;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isInitializeNeeded         = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canFetchTransactionInfo    = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canReviewPayment           = true;

    /**
     * Get instructions text from config
     *
     * @return string
     */    
    protected $openpayApi;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Aopen\Openpay\Model\Api $openpayApi
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @param DirectoryHelper $directory
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Aopen\Openpay\Model\Api $openpayApi,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );
        $this->getOpenpayApi = $openpayApi;
    }

    /**
     * GetInstructions
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }

    /**
     * Refund
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount 
     * @return $this
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $order = $payment->getOrder();
        $parentTransactionId = $payment->getParentTransactionId();
        $grand_total = $order->getGrandTotal();
        $total_refunded = $order->getTotalRefunded();
        $refunded_amt = $grand_total - $total_refunded;
        if ($refunded_amt == 0) {
            $fullrefund = 'True';
        } else {
            $fullrefund = 'False';
        }
        $this->getOpenpayApi->onlineOrderReduction($parentTransactionId, $refunded_amt, $fullrefund);
        return $this;
    }
}
