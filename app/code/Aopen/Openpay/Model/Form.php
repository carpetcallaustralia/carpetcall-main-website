<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Model;

/**
 * Openpay Form model
 */
class Form implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * To OptionArray
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value'=>3600, 'label'=>__('After one hour')],
            ['value'=>7200, 'label'=>__('After two hours')],
            ['value'=>14400, 'label'=>__('After four hours')],
            ['value'=>21600, 'label'=>__('After six hours')],
            ['value'=>43200, 'label'=>__('After twelve hours')],
            ['value'=>86400, 'label'=>__('After one day')],
        ];
    }
}
