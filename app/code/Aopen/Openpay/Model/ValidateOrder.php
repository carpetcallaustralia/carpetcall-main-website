<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */

namespace Aopen\Openpay\Model;


use Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory;

class ValidateOrder
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;
    /**
     * @var CollectionFactory
     */
    private $historyCollectionFactory;

    public function __construct(
        CollectionFactory $historyCollectionFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    ){
        $this->checkoutSession = $checkoutSession;
        $this->historyCollectionFactory = $historyCollectionFactory;
    }

    public function validate(\Magento\Sales\Model\Order $order,$checkPlanId = false,$planId = null)
    {
        $orderPlanId = $this->getOrderPlanInfo($order->getEntityId());
        $orderStatus = $order->getStatus();
        $paymentMethodCode = $order->getPayment()->getMethod();
        if($orderStatus === 'pending_approval'
            && ($paymentMethodCode == \Aopen\Openpay\Model\Openpay::PAYMENT_METHOD_OPENPAY_CODE)
        ){
            if($checkPlanId && $planId!== null && $orderPlanId && ($orderPlanId === $planId)){
                return true;
            }elseif (!$checkPlanId && $orderPlanId){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    /**
     * Will be use for Future
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    private function checkOrderBySession(\Magento\Sales\Model\Order $order)
    {
        $orderId = (int) $order->getId();
        if ($this->checkoutSession->getLastOrderId()
            && ($this->checkoutSession->getLastRealOrderId() === $order->getIncrementId())
           &&  ( (int) $this->checkoutSession->getLastOrderId() === $orderId )
        ) {
            return true;
        }
        return false;
    }
    private function getOrderPlanInfo($orderId)
    {
        $historyCollection = $this->historyCollectionFactory->create();
        $historyCollection->addFieldToSelect(['parent_id','status','comment'])
            ->addFieldToFilter('parent_id', ['eq' => $orderId])
        ->addFieldToFilter('status', ['eq' => 'pending_approval'])
        ->addFieldToFilter('comment', ['like' => 'PlanId:%'])
        ->setPageSize(1)->setCurPage(1) ;

        if ($historyCollection->count() < 1){
            return false;
        }

        return trim(str_replace(
            'PlanId: ',
            '',
            $historyCollection
                ->getFirstItem()
                ->getComment()
        ));

    }
}