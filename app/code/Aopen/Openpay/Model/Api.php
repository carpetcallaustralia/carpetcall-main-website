<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Model;

use Magento\Sales\Api\Data\OrderStatusHistoryInterface;

/**
 * Openpay API model
 *
 */
class Api extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $orderdata;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $orderFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory
     */
    protected $_historyCollectionFactory;

    /**
     * Construct
     *    
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Sales\Model\Order $orderFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\Order $orderFactory,
        \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory
    ) {
        $this->messageManager = $messageManager;
        $this->getOrder = $orderFactory;
        $this->_init(\Aopen\Openpay\Model\ResourceModel\Api::class);
        $this->_historyCollectionFactory = $historyCollectionFactory;
    }

    /**
     * Format String
     *    
     * @var string
     * @var int
     * @return string
     */
    protected function formatString($string, $maxLength) {
        return (strlen($string) > ($maxLength-1)) ? substr($string, 0, ($maxLength-3)).'...' : $string;
    }

    /**
     * Abbreviate State
     *     
     * @var string $state
     * @return string
     */
    protected function abbreviateState($state) {
        $states = [
            'australian capital territory' => 'ACT',
            'new south wales' => 'NSW',
            'northern territory' => 'NT',
            'queensland' => 'QLD',
            'south australia' => 'SA',
            'tasmania' => 'TAS',
            'victoria' => 'VIC',
            'western australia' => 'WA'
        ];
        if (strlen($state) > 3) {
            return ($states[strtolower($state)]);
        } else {
            return strtoupper($state);
        }
    }

    /**
     * Set History PlanId
     *     
     * @var string $planId
     * @var int $orderId
     * @return bool
     */
    protected function setHistoryPlanId($planId, $orderId) {
       
        if ($this->orderdata === null) {
            $order = $this->getOrder->load($orderId);
        } else {
            $order = $this->orderdata;
        }
        $order->addStatusHistoryComment(
            __('PlanId: ' .  $planId)
        )
            ->setIsCustomerNotified(true)
            ->save();
        return true;
    }

    /**
     * get History PlanId
     *     
     * @var int $orderId
     * @return string
     */
    public function getHistoryPlanId($orderId)
    {
        $this->orderdata =  $this->getOrder->load($orderId);
        return trim(str_replace(
            'PlanId: ',
            '',
            $this->getOrder
                ->getStatusHistoryCollection()
                ->addFieldToFilter('parent_id', ['eq' => $orderId])
                ->addFieldToFilter('status', ['eq' => 'pending_approval'])
                ->addFieldToFilter('comment', ['like' => 'PlanId:%'])
                ->getFirstItem()
                ->getComment()
        ));
    }

    /**
     * get Order PlanId
     *     
     * @var int $orderId
     * @return string
     */
    public function getOrderPlanId($orderId)
    {
        $this->orderdata =  $this->getOrder->load($orderId);

        $collection = $this->_historyCollectionFactory->create()->setOrderFilter($this->orderdata);
        $commentObj = $collection
        ->addFieldToFilter('parent_id', ['eq' => $orderId])
        ->addFieldToFilter('comment', ['like' => 'PlanId:%'])
        ->setPageSize(1)
        ->setCurPage(1);

        if (count($commentObj)) {
            return trim(str_replace('PlanId: ', '', $commentObj->getFirstItem()->getComment()));
        }
    }

    /**
     * get Openpay Order Status
     *     
     * @var int $orderId
     * @return string
     */    
    public function getOpenpayOrderStatus($orderId)
    {
        $planId = $this->getHistoryPlanId($orderId);
        $result = $this->_getResource()->getOpenpayOrderStatus($planId);
        return $result;
    }

    /**
     * get Openpay Url
     *     
     * @var int $id
     * @var floate $price
     * @var int $orderNumber
     * @var string $firstName
     * @var string $lastName
     * @var string $email
     * @var string $dob
     * @var string $address1
     * @var string $address2
     * @var string $city
     * @var string $state
     * @var string $postcode
     * @var string $telephone
     * @var string $shippingaddress1
     * @var string $shippingaddress2
     * @var string $shippingcity
     * @var string $shippingstate
     * @var string $shippingpostcode                     
     * @return string
     */
    public function getOpenpayUrl(
        $id,
        $price,
        $orderNumber,
        $firstName,
        $lastName,
        $email,
        $dob,
        $address1,
        $address2,
        $city,
        $state,
        $postcode,
        $telephone,
        $shippingaddress1,
        $shippingaddress2,
        $shippingcity,
        $shippingstate,
        $shippingpostcode
    ) {
        $price = number_format($price, 2);
        $firstName = $this->formatString($firstName, 50);
        $flastName = $this->formatString($firstName, 50);
        $email = $this->formatString($email, 150);
        $address1 = $this->formatString($address1, 100);
        $address2 = $this->formatString($address2, 100);
        $city = $this->formatString($city, 100);
        $state = $state;
        $postcode = $postcode;
        /*shipping address*/
        $shippingaddress1 = $this->formatString($shippingaddress1, 100);
        $shippingaddress2 = $this->formatString($shippingaddress2, 100);
        $shippingcity = $this->formatString($shippingcity, 100);
        $shippingstate = $shippingstate;
        $shippingpostcode = $shippingpostcode;
        /**/
        $dob = date('d M Y', strtotime($dob));
        $result = $this->_getResource()->getOpenpayUrl(
            $id,
            $price,
            $orderNumber,
            $firstName,
            $lastName,
            $email,
            $dob,
            $address1,
            $address2,
            $city,
            $state,
            $postcode,
            $telephone,
            $shippingaddress1,
            $shippingaddress2,
            $shippingcity,
            $shippingstate,
            $shippingpostcode
        );
        if (array_key_exists('success', $result)) {
            if ($result['planId'] && $id) {
                $this->setHistoryPlanId($result['planId'], $id);
                return $result['url'];
            }
        } elseif (array_key_exists('error', $result)) {
            $this->messageManager->addError($result['error']);
        } else {
            return $result['url'];
        }
    }

    /**
     * Online Order Reduction
     *     
     * @var string $planId
     * @var float $newPurchasePrice
     * @var float $fullrefund
     * @return bool
     */
    public function onlineOrderReduction($planId, $newPurchasePrice, $fullrefund)
    {
        $result = $this->_getResource()->onlineOrderReduction($planId, $newPurchasePrice, $fullrefund);
        return $result;
    }

    /**
     * Get OnlineOrder Capture Payment
     *     
     * @var string $planId
     * @return float
     */
    public function getOnlineOrderCapturePayment($planId)
    {
        $result = $this->_getResource()->getOnlineOrderCapturePayment($planId);
        return $result;
    }

    /**
     * Online Fraud Call
     *     
     * @var string $planId
     * @var string $fraudText
     * @return string
     */
    public function onlineFraudCall($planId, $fraudText)
    {
        $result = $this->_getResource()->onlineFraudCall($planId, $fraudText);
        return $result;
    }
}
