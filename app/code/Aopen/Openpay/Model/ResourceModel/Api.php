<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Model\ResourceModel;

/**
 * Openpay Api Resource model
 */
class Api extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Aopen\Openpay\Helper\Data
     */  
    public $helper;
 
    /**
     * Construct
     *
     * @param \Aopen\Openpay\Helper\Data $helper
     */ 
    public function __construct(
        \Aopen\Openpay\Helper\Data $helper
    ) {
        $this->getHelper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    protected function getPlanId(
        $price,
        $orderNumber,
        $firstName,
        $lastName,
        $email,
        $dob,
        $address1,
        $address2,
        $city,
        $state,
        $postcode,
        $telephone,
        $shippingaddress1,
        $shippingaddress2,
        $shippingcity,
        $shippingstate,
        $shippingpostcode
    ) {
        $path = ['NewOnlineOrder'];
        $data = $this->createXmlPayload(
            $path[0],
            $price,
            '',
            '',
            '',
            '',
            $orderNumber,
            $firstName,
            $lastName,
            $email,
            $dob,
            $address1,
            $address2,
            $city,
            $state,
            $postcode,
            $telephone,
            $shippingaddress1,
            $shippingaddress2,
            $shippingcity,
            $shippingstate,
            $shippingpostcode
        );
        $this->getHelper->log('****** CALL 1 Started ******');
        $result = $this->getHelper->makecurlCall($path, $data);
        $this->getHelper->log('****** CALL 1 End ******');
        return $this->result($result);
    }

    /**
     * {@inheritdoc}
     */    
    protected function approved($result)
    {
        $return = [];
        $xml = new \SimpleXMLElement($result);
        $array = get_object_vars($xml);
        if ($array['OrderStatus'] == 'Approved') {
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */    
    public function getOpenpayOrderStatus($planId)
    {
        $path = ['OnlineOrderStatus'];
        $data = $this->createXmlPayload($path[0], '', $planId);
        $result = $this->getHelper->makecurlCall($path, $data);
        return $this->approved($result);
    }

    /**
     * {@inheritdoc}
     */    
    protected function result($result)
    {
        
        if (!$result) {
            return ['error' => 'Api Error'];
        }

        $xml = new \SimpleXMLElement($result);
        $array = get_object_vars($xml);
        if (!$array['status']) {
            $return['success'] = $array['reason'];
            $return['planId'] = $array['PlanID'];
            if (isset($array['TransactionToken'])) {
                $return['transactionToken'] = $array['TransactionToken'];
            }
        } else {
            $return['error'] = $array['reason'];
        }
        return $return;
    }

    /**
     * {@inheritdoc}
     */
    protected function createXmlPayload(
        $path,
        $price = null,
        $planId = null,
        $newPurchasePrice = null,
        $reducePriceBy = '0.00',
        $fullRefund = 'true',
        $orderNumber = null,
        $firstName = null,
        $lastName = null,
        $email = null,
        $dob = null,
        $address1 = null,
        $address2 = null,
        $city = null,
        $state = null,
        $postcode = null,
        $telephone = null,
        $shippingaddress1 = null,
        $shippingaddress2 = null,
        $shippingcity = null,
        $shippingstate = null,
        $shippingpostcode = null
    ) {
        $xml = '<' . $path . '>' . "\n";
        $xml .= "\t" . '<JamAuthToken>';
        $xml .= $this->getHelper->getJamAuthToken();
        $xml .= '</JamAuthToken>' . "\n";
        if ($path == 'NewOnlineOrder') {
            $xml .= "\t" . '<CallbackURL>';
            $xml .= $this->getHelper->getJamCallbackUrl();
            $xml .= '</CallbackURL>' . "\n";

            $xml .= "\t" . '<CancelURL>';
            $xml .= $this->getHelper->getJamCancelUrl();
            $xml .= '</CancelURL>' . "\n";

            $xml .= "\t" . '<FailURL>';
            $xml .= $this->getHelper->getJamFailUrl();
            $xml .= '</FailURL>' . "\n";
        }
        if ($price) {
            $xml .= "\t" . '<PurchasePrice>';
            $xml .= $price;
            $xml .= '</PurchasePrice>' . "\n";
        }
        if ($path == 'NewOnlineOrder') {
            $xml .= "\t" . '<PlanCreationType>';
            $xml .= "pending";
            $xml .= '</PlanCreationType>' . "\n";

            $xml .= "\t" . '<RetailerOrderNo>';
            $xml .= $orderNumber;
            $xml .= '</RetailerOrderNo>' . "\n";

            $xml .= "\t" . '<FirstName>';
            $xml .= $firstName;
            $xml .= '</FirstName>' . "\n";

            $xml .= "\t" . '<FamilyName>';
            $xml .= $lastName;
            $xml .= '</FamilyName>' . "\n";

            $xml .= "\t" . '<Email>';
            $xml .= $email;
            $xml .= '</Email>' . "\n";

            $xml .= "\t" . '<DateOfBirth>';
            $xml .= $dob;
            $xml .= '</DateOfBirth>' . "\n";

            $xml .= "\t" . '<PhoneNumber>';
            $xml .= $telephone;
            $xml .= '</PhoneNumber>' . "\n";

            $xml .= "\t" . '<ResAddress1>';
            $xml .= $address1;
            $xml .= '</ResAddress1>' . "\n";

            $xml .= "\t" . '<ResAddress2>';
            $xml .= $address2;
            $xml .= '</ResAddress2>' . "\n";

            $xml .= "\t" . '<ResSuburb>';
            $xml .= $city;
            $xml .= '</ResSuburb>' . "\n";

            $xml .= "\t" . '<ResState>';
            $xml .= $state;
            $xml .= '</ResState>' . "\n";

            $xml .= "\t" . '<ResPostCode>';
            $xml .= $postcode;
            $xml .= '</ResPostCode>' . "\n";

            $xml .= "\t" . '<DelAddress1>';
            $xml .= $shippingaddress1;
            $xml .= '</DelAddress1>' . "\n";

            $xml .= "\t" . '<DelAddress2>';
            $xml .= $shippingaddress2;
            $xml .= '</DelAddress2>' . "\n";

            $xml .= "\t" . '<DelSuburb>';
            $xml .= $shippingcity;
            $xml .= '</DelSuburb>' . "\n";

            $xml .= "\t" . '<DelState>';
            $xml .= $shippingstate;
            $xml .= '</DelState>' . "\n";

            $xml .= "\t" . '<DelPostCode>';
            $xml .= $shippingpostcode;
            $xml .= '</DelPostCode>' . "\n";
        }
        if ($planId) {
            $xml .= "\t" . '<PlanID>';
            $xml .= $planId;
            $xml .= '</PlanID>' . "\n";
        }
        if ($path == 'OnlineOrderReduction') {
            $xml .= "\t" . '<NewPurchasePrice>';
            $xml .= $newPurchasePrice;
            $xml .= '</NewPurchasePrice>' . "\n";
            $xml .= "\t" . '<FullRefund>';
            $xml .= $fullRefund;
            $xml .= '</FullRefund>' . "\n";
        }
        $xml .= '</' . $path . '>' . "\n";
        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function createXmlPayloadFraud($path, $planId, $fraudText)
    {
        $xml = '<' . $path . '>' . "\n";
        $xml .= "\t" . '<JamAuthToken>';
        $xml .= $this->getHelper->getJamAuthToken();
        $xml .= '</JamAuthToken>' . "\n";
        if ($planId) {
            $xml .= "\t" . '<PlanID>';
            $xml .= $planId;
            $xml .= '</PlanID>' . "\n";
        }
        if ($fraudText) {
            $xml .= "\t" . '<Details>';
            $xml .= $fraudText;
            $xml .= '</Details>' . "\n";
        }
         $xml .= '</' . $path . '>' . "\n";
        
         return $xml;
    }

    /**
     * {@inheritdoc}
     */
    public function getOpenpayUrl(
        $id,
        $price,
        $orderNumber,
        $firstName,
        $lastName,
        $email,
        $dob,
        $address1,
        $address2,
        $city,
        $state,
        $postcode,
        $telephone,
        $shippingaddress1,
        $shippingaddress2,
        $shippingcity,
        $shippingstate,
        $shippingpostcode
    ) {
        $result = $this->getPlanId(
            $price,
            $orderNumber,
            $firstName,
            $lastName,
            $email,
            $dob,
            $address1,
            $address2,
            $city,
            $state,
            $postcode,
            $telephone,
            $shippingaddress1,
            $shippingaddress2,
            $shippingcity,
            $shippingstate,
            $shippingpostcode
        );
        if (isset($result['planId'])) {
            $hash = hash('sha256', $id . $this->getHelper->getJamAuthToken() . $orderNumber);
            $url = $this->getHelper->getHandoverUrl();
            $url .= '?TransactionToken=' . urlencode($result['transactionToken']);
            $result['url'] = $url;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function onlineOrderReduction($planId, $newPurchasePrice, $fullrefund)
    {
        $path = ['OnlineOrderReduction'];
        $data = $this->createXmlPayload($path[0], '', $planId, $newPurchasePrice, '', $fullrefund);
        $result = $this->getHelper->makecurlCall($path, $data);
        return $this->result($result);
    }

    /**
     * {@inheritdoc}
     */    
    public function getOnlineOrderCapturePayment($planId)
    {
        $path = ['OnlineOrderCapturePayment'];
        $data = $this->createXmlPayload($path[0], '', $planId);
        $this->getHelper->log('****** CALL 3 Started ******');
        $result = $this->getHelper->makecurlCall($path, $data);
        $this->getHelper->log('****** CALL 3 End ******');
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function onlineFraudCall($planId, $fraudText)
    {
        $path = ['OnlineOrderFraudAlert'];
        $data = $this->createXmlPayloadFraud($path[0], $planId, $fraudText);
        $result = $this->getHelper->makecurlCall($path, $data);
        return $result;
    }
}
