<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Block\System\Config;

class Button extends \Magento\Config\Block\System\Config\Form\Field
{
    // @codingStandardsIgnoreStart 
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
    // @codingStandardsIgnoreSEnd        
            $url = $this->getUrl('openpay/openpay/index');
            $button  = $this->getLayout()->createBlock(\Magento\Backend\Block\Widget\Button::class)
            ->setType('button')
                    ->setClass('scalable')
                    ->setLabel('Run Now !')
                    ->setOnClick("setLocation('$url')");
 
        return $button->toHtml();
    }
}
