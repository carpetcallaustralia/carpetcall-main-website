<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

class Disable extends \Magento\Config\Block\System\Config\Form\Field
{
	// @codingStandardsIgnoreStart
    protected function _getElementHtml(AbstractElement $element)
    {
    // @codingStandardsIgnoreEnd
        $element->setDisabled('disabled');
        return $element->getElementHtml();
    }
}
