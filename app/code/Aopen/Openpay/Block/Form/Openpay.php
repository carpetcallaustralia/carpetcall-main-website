<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Block\Form;

/**
 * Block for Cash On Delivery payment method form
 */
class Openpay extends \Aopen\Openpay\Block\Form\AbstractInstruction
{
    /**
     * Cash on delivery template
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_template = 'form/openpay.phtml';
    // @codingStandardsIgnoreEnd
}
