<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Block\Adminhtml\System;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Config\Model\Config\CommentInterface;

/**
 * Openpay Dynamic Comment Block
 */
class DynamicComment extends AbstractBlock implements CommentInterface
{
    public function getCommentText($elementValue)
    {
        $url = $this->getUrl('openpay/openpay/comment');
        return "Switch on debugging to create a log file located at var/log/openpay.
        	log which you can view <a target='_blank' href='$url'>here</a>";
    }
}
