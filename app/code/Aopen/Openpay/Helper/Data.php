<?php
/**
 * @package   Aopen_Openpay
 * @author    Openpay Team <ecommercesupport@openpay.com.au>
 * @copyright 2019-2020 Openpay. All rights reserved.
 */
namespace Aopen\Openpay\Helper;

/**
 * Openpay Data Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const JAM_API_URL_LIVE_AU = 'https://retailer.myopenpay.com.au/ServiceLive/JAMServiceImpl.svc/';
    const JAM_API_URL_DEMO_AU = 'https://retailer.myopenpay.com.au/ServiceTraining/JAMServiceImpl.svc/';
    const DEMO_HAND_OVER_URL_AU = 'https://retailer.myopenpay.com.au/WebSalesTraining/';
    const LIVE_HAND_OVER_URL_AU = 'https://retailer.myopenpay.com.au/WebSalesLive/';

    const JAM_API_URL_LIVE_UK = 'https://integration.myopenpay.co.uk/JamServiceImpl.svc/';
    const JAM_API_URL_DEMO_UK = 'https://integration.training.myopenpay.co.uk/JamServiceImpl.svc/';
    const DEMO_HAND_OVER_URL_UK = 'https://websales.training.myopenpay.co.uk/';
    const LIVE_HAND_OVER_URL_UK = 'https://websales.myopenpay.co.uk/';

    const JAM_CALLBACK_URL = 'openpay/order/callback';
    const JAM_CANCEL_URL = 'openpay/order/cancel';
    const JAM_FAIL_URL = 'openpay/order/fail';

    /**
     * @var $logfile
     */
    public $logfile = '/var/log/openpay.log';
    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactoryCreate;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\HTTP\Adapter\CurlFactory
     */
    private $curlFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Sales\Model\OrderFactory $orderFactoryCreate
     * @param \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory
     * @param \Psr\Log\LoggerInterface $logger
     */

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\OrderFactory $orderFactoryCreate,
        \Magento\Framework\HTTP\Client\CurlFactory $curlFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->getOrderFactory = $orderFactoryCreate;
        $this->messageManager = $messageManager;
        $this->curlFactory = $curlFactory;
        $this->logger = $logger;
    }

    /**
     * @param $path
     * @return string
     */
    protected function getConfigData($path)
    {
        $prefix = 'payment/openpay/';
        return $this->scopeConfig->getValue($prefix . $path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {

        $country = $this->getConfigData('specificcountry');

        if ($country == 'GB') {
            return self::JAM_API_URL_UK;
        }
        if ($country == 'AU') {
            return self::JAM_API_URL_AU;
        }
    }
    public function getJamApiUrl()
    {

        $country = $this->getConfigData('specificcountry');

        if ($country == 'AU') {
            if ($this->getConfigData('test')) {
                return self::JAM_API_URL_DEMO_AU;
            } else {
                return self::JAM_API_URL_LIVE_AU;
            }
        }
        if ($country == 'GB') {
            if ($this->getConfigData('test')) {
                return self::JAM_API_URL_DEMO_UK;
            } else {
                return self::JAM_API_URL_LIVE_UK;
            }
        }
    }

    /**
     * @return string
     */
    public function getApiPath()
    {
        if ($this->getConfigData('test')) {
            return self::JAM_DEMO_PATH;
        } else {
            return self::JAM_LIVE_PATH;
        }
    }

    /**
     * @return boolean
     */
    public function getOpenpayActive()
    {
        return $this->getConfigData('active');
    }

    /**
     * @return bullean
     */
    public function getOpenpayDebug()
    {
        return $this->getConfigData('debug');
    }

    /**
     * @return string
     */
    public function getJamAuthToken()
    {
        if (!$jamAuthToken = $this->getConfigData('jam_auth_token')) {
            $this->messageManager->addError('Openpay Jam Auth Token not set.');
        }
        return $jamAuthToken;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->getJamAuthToken();
    }

    /**
     * @return string
     */
    public function getHandOverUrl()
    {
        $country = $this->getConfigData('specificcountry');

        if ($country == 'AU') {
            if ($this->getConfigData('test')) {
                return self::DEMO_HAND_OVER_URL_AU;
            } else {
                return self::LIVE_HAND_OVER_URL_AU;
            }
        }
        if ($country == 'GB') {
            if ($this->getConfigData('test')) {
                return self::DEMO_HAND_OVER_URL_UK;
            } else {
                return self::LIVE_HAND_OVER_URL_UK;
            }
        }
    }

    /**
     * @return string
     */
    public function getJamCallbackUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl() . self::JAM_CALLBACK_URL;
    }

    /**
     * @return string
     */
    public function getJamCancelUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl() . self::JAM_CANCEL_URL;
    }

    /**
     * @return string
     */
    public function getJamFailUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl() . self::JAM_FAIL_URL;
    }

    /**
     * @param $message
     * @return Mage_Log
     */
    public function log($message)
    {
        if ($this->getOpenpayDebug() && $this->scopeConfig->getValue(
            'dev/debug/debug_logging',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )) {
            $writer = new \Zend\Log\Writer\Stream(BP . $this->logfile);
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($message);
        }
        return false;
    }

    /**
     * @param (int) $id
     * @param (string) $hash
     * @return $object
     */
    public function getOrderId($incrementId)
    {
        $orderobj = $this->getOrderFactory->create()->loadByIncrementId($incrementId);
        return $orderobj->getId();
    }
    public function valid($id, $hash)
    {
        $order = $this->getOrderFactory->create()->load($id);
        if ($hash === hash('sha256', $id . $this->getJamAuthToken() . $order->getIncrementId())) {
            return $order;
        }
        return false;
    }

    public function makecurlCall($path, $data)
    {
        $this->log('***Request***');
        $this->log($data);
        $sandboxmode= $this->scopeConfig->getValue(
            'payment/openpay/test',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $country = $this->getConfigData('specificcountry');
        if (count(explode(',', $country)) > 1) {
            $this->messageManager->addError(
                'Payments from Specific Countries: Select either Australia or United Kingdom.'
            );
        }
        if ($country == 'AU') {
            if ($sandboxmode==1) {
                $serviceBaseApiURL = self::JAM_API_URL_DEMO_AU;
            } else {
                $serviceBaseApiURL =  self::JAM_API_URL_LIVE_AU;
            }
        }
        if ($country == 'GB') {
            if ($sandboxmode==1) {
                $serviceBaseApiURL = self::JAM_API_URL_DEMO_UK;
            } else {
                $serviceBaseApiURL =  self::JAM_API_URL_LIVE_UK;
            }
        }
        $jamAuthToken = $this->scopeConfig->getValue(
            'payment/openpay/jam_auth_token',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        try {
            $curl = $this->curlFactory->create();
            $curl->addHeader("cache-control", "no-cache");
            $curl->addHeader("content-type", "application/xml");
            $curl->setOption(CURLOPT_RETURNTRANSFER, true);
            $curl->setOption(CURLOPT_CUSTOMREQUEST, "POST");
            $curl->setOption(CURLOPT_POSTFIELDS, $data);
            $curl->post($serviceBaseApiURL.$path[0], []);
            $this->log('***Response***');
            $this->log($curl->getBody());
            if ($curl->getStatus() === 200 || $curl->getStatus() === 100) {
                return $curl->getBody();
            } else {
                $this->logger->critical('Error Processing in Request.');
                return false;
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        return false;
    }
}
