<?php
/**
 * BelVG LLC.
 *
 *  NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *  ********************************************************************
 * @category   Belvg
 * @package   Belvg_POboxes
 * @copyright  Copyright (c) 2010 - 2018 BelVG LLC. (https://belvg.com/)
 * @author     Alexander Simonchik <support@belvg.com>
 * @license    https://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *
 */

namespace Belvg\POboxes\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public $regexp = "/([[P|p]*(OST|ost)*\.*\s*[O|o|0]*(ffice|FFICE)*\.*\s*]*[B|b][O|o|0][X|x]\b)|(p[\-\.\s]*o[\-\.\s]*)/";

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroup,
        \Magento\Customer\Model\Session\Proxy $customerSession
    ) {
        $this->customerGroup = $customerGroup;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }

    /**
     * Return if extension is disabled for customer's customer group
     *
     * @return bool
     */
    public function getConfig($field, $store = null)
    {
        return $this->scopeConfig->getValue(
            'poboxes/general/' . $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Return the customer id of the current customer
     *
     * @return int
     */
    public function getCustomerGroupId()
    {
        return $this->customerSession->getCustomer()->getGroupId();
    }

    /**
     * Check group + is module active
     *
     * @return bool
     */
    public function isAllowed()
    {
        $is_enabled = $this->getConfig('enable');
        $disabled_groups = explode(',', $this->getConfig('disabled_customergroup'));
        $is_group_allowed = !in_array($this->getCustomerGroupId(), $disabled_groups);

        return (bool)($is_enabled && $is_group_allowed);
    }

    /**
     * @param $street
     * @return bool
     */
    public function hasPOBoxesMentions($street)
    {
        /**
         * it is possible to check tested data source in file: app/code/Belvg/POboxes/Test/Unit/Helper/DataTest.php
         * please take notice to the case: `34 Box Handler Road` that is INVALID in case of regexp:
         * "/([[P|p]*(OST|ost)*\.*\s*[O|o|0]*(ffice|FFICE)*\.*\s*]*[B|b][O|o|0][X|x]\b)|(p[\-\.\s]*o[\-\.\s]*)/"
         * so maybe you will need to change the regexp,
         * the most simple regexp is: if (preg_match("/p\.* *o\.* *box/i", $street))
         */

        if (preg_match($this->regexp, $street)) {
            return true;
        }
        return false;
    }
}
