<?php
/**
 * BelVG LLC.
 *
 *  NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *  ********************************************************************
 * @category   Belvg
 * @package   Belvg_POboxes
 * @copyright  Copyright (c) 2010 - 2018 BelVG LLC. (https://belvg.com/)
 * @author     Alexander Simonchik <support@belvg.com>
 * @license    https://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *
 */

namespace Belvg\POboxes\Test\Unit\Helper;

class DataTest extends \PHPUnit\Framework\TestCase
{
    public $helperData;

    public function setUp()
    {
        $this->helperData = $this->getMockBuilder(\Belvg\POboxes\Helper\Data::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getConfig',
                'getCustomerGroupId',
            ])
            ->getMock();
    }

    /**
     * @dataProvider getPOBoxesMentions
     */
    public function testHasPOBoxesMentions(
        $street,
        $expected_result
    ) {
    
        $result = $this->helperData->hasPOBoxesMentions($street);

        $this->assertEquals($expected_result, $result);
    }

    public function getPOBoxesMentions()
    {
        return [
            [
                'po box',
                true,
            ],
            [
                'p.o.b.',
                true,
            ],
            [
                'pob',
                true,
            ],
            [
                'pob.',
                true,
            ],
            [
                'p.o. box',
                true,
            ],
            [
                'po-box',
                true,
            ],
            [
                'p.o.-box',
                true,
            ],
            [
                'PO-Box',
                true,
            ],
            [
                'p.o box',
                true,
            ],
            [
                'pobox',
                true,
            ],
            [
                'p-o-box',
                true,
            ],
            [
                'p-o box',
                true,
            ],
            [
                'post office box',
                true,
            ],
            [
                'P.O. Box',
                true,
            ],
            [
                'PO Box',
                true,
            ],
            [
                'PO box',
                true,
            ],
            [
                'box PO',
                true,
            ],
            [
                'box 122',
                true,
            ],
            [
                'Box-122',
                true,
            ],
            [
                'post office 122',
                true,
            ],
            [
                '34 Box PO Handler Road',
                true,
            ],
            [
                '34 Box Handler Road', // maybe this should be FALSE??? any mentions of `box`
                true,
            ],
            [
                'mother',
                false,
            ],
            [
                'Box122',
                false,
            ],
            [
                '123 Boxing Street',
                false,
            ],
            [
                '424 PO Dance drive',
                false,
            ],
            [
                '234 P.O.D. Wasn\'t terrible Circle',
                false,
            ],
            [
                'Postal Code 12323',
                false,
            ],
            [
                'Roma city 12323',
                false,
            ],
            [
                'ROMA city 12323',
                false,
            ],
        ];
    }

    /**
     * @dataProvider getIsAllowed
     */
    public function testIsAllowed(
        $enable,
        $disabled_customergroup,
        $cusotmer_group_id,
        $expected_result
    ) {
        // Create a map of arguments to return values.
        $map = [
            ['enable', null, $enable],
            ['disabled_customergroup', null, $disabled_customergroup]
        ];
        $this->helperData->expects($this->any())
            ->method('getConfig')
            ->will(
                $this->returnValueMap($map)
            );
        $this->helperData->expects($this->any())
            ->method('getCustomerGroupId')
            ->will(
                $this->returnValue($cusotmer_group_id)
            );

        $this->assertEquals($expected_result, $this->helperData->isAllowed());
    }

    public function getIsAllowed()
    {
        return [
            [
                1,
                '100, 200',
                101,
                true,
            ],
            [
                1,
                '100, 201',
                101,
                true,
            ],
            [
                1,
                '',
                101,
                true,
            ],
            [
                0,
                '100, 201',
                101,
                false,
            ],
            [
                1,
                '100, 201',
                201,
                false,
            ],
        ];
    }
}
