<?php
/**
 * BelVG LLC.
 *
 *  NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *  ********************************************************************
 * @category   Belvg
 * @package   Belvg_POboxes
 * @copyright  Copyright (c) 2010 - 2018 BelVG LLC. (https://belvg.com/)
 * @author     Alexander Simonchik <support@belvg.com>
 * @license    https://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *
 */

namespace Belvg\POboxes\Plugin\Model\Address;

class AbstractAddress
{
    /**
     * @var \Belvg\POboxes\Helper\Data
     */
    public $helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    public $checkoutSession;

    public function __construct(
        \Belvg\POboxes\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->helper = $helper;
        $this->checkoutSession = $checkoutSession;
    }

    public function afterValidate($subject, $result)
    {
        if ($this->helper->getConfig('only_for_shipping_address')) {
            if (($subject->getId() != $this->checkoutSession->getQuote()->getShippingAddress()->getId()) &&
                ($subject->getData(\Magento\Customer\Api\Data\AddressInterface::DEFAULT_SHIPPING) == 0)
            ) {
                return $result;
            }
        }

        if ($this->helper->isAllowed() && $this->helper->hasPOBoxesMentions($subject->getStreetFull())) {
            //$error[] = __('Field "street" is not valid. It is not allowed to enter a PO Box in the shipping address.');
            $error[] = __("Please note, we don't deliver to PO box addresses");

            if ($result === true) {
                return $error;
            } else {
                return array_merge($result, $error);
            }
        }

        return $result;
    }
}
