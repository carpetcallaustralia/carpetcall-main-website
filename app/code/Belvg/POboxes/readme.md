# Belvg POboxes Extension

Magento 2 extension that allows to forbid shipping to Post-office box

## Disclaimer

Module analyzes the address using regexp and show warning if needed.

## Installation

 * Enable module with `php bin/magento module:enable Belvg_POboxes`
 * Run `php bin/magento setup:upgrade`
 * Recompile DI `php bin/magento setup:di:compile`
 * Recompile static files: `php bin/magento setup:static-content:deploy`
 * Flush cache: `php bin/magento cache:flush`

## Usage

Module has the next configuration:
Stores -> Settings-> Configuration -> Belvg -> PO Boxes
 * Module Enable - enable/disable module;
 * Disable for Customer groups - comma-separated list of customer groups IDs that should be ignored by module;
 * Only for shipping address - if enabled the `PO Box` will be forbidden only for shipping type of address.
    ![Config](docs/img/module_config.png)

Some examples of the frontend view are available below:
    ![Frontend example 1](docs/img/frontend_example-1.png)
    ![Frontend example 2](docs/img/frontend_example-2.png)
    
Some examples of the forbidden/allowed addresses you can find in phpUnit test file: 
    [Belvg/POboxes/Test/Unit/Helper/DataTest.php](Test/Unit/Helper/DataTest.php)

Run tests: 
`./vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist app/code/Belvg/POboxes/Test/Unit/`

## Support

Get in touch with us: https://belvg.com/support

## Demo

@todo
