<?php
namespace Mageplaza\StoreLocator\Plugin\Shipping\Method;

use Magento\Quote\Model\ShippingMethodManagement;
class HidePickupMethod
{
    public function afterEstimateByExtendedAddress($shippingMethodManagement, $output)
    {
        return $this->filterOutput($output);
    }
    private function filterOutput($output)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $locations = $objectManager->get('Mageplaza\StoreLocator\Block\Frontend');
        $isLocation = $locations->getPickupLocationList();

        $free = [];
        foreach ($output as $shippingMethod) {
            if ($shippingMethod->getMethodCode() !== 'mpstorepickup' || count($isLocation)) {
                $free[] = $shippingMethod;
            }
        }
        return $free;
    }
}

