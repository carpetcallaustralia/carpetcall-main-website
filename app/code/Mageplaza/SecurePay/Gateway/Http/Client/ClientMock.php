<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\SecurePay\Gateway\Http\Client;

use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use Mageplaza\SecurePay\Helper\Request;
use Mageplaza\SecurePay\Model\Payment\XmlApi;

/**
 * Class ClientMock
 * @package Mageplaza\SecurePay\Gateway\Http\Client
 */
class ClientMock implements ClientInterface
{
    /**
     * @var Request
     */
    private $helper;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * ClientMock constructor.
     *
     * @param Request $helper
     * @param Logger $logger
     */
    public function __construct(
        Request $helper,
        Logger $logger
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * @param TransferInterface $transferObject
     *
     * @return array
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $body = $transferObject->getBody();

        if (empty($body)) {
            return [];
        }

        $url     = $this->helper->getApiUrl(XmlApi::URL);
        $headers = ['Content-type: text/xml'];
        $body    = $body['xml'];

        $this->logger->debug(['xml api request' => $body]);

        $response = $this->helper->sendRequest($url, $headers, $body, 'xml');

        $this->logger->debug(['xml api response' => $response]);

        return $response;
    }
}
