<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Response;

use Magento\Payment\Gateway\Response\HandlerInterface;

/**
 * Class RefundResponseHandler
 * @package Mageplaza\SecurePay\Gateway\Response
 */
class RefundResponseHandler extends AbstractResponseHandler implements HandlerInterface
{
    /**
     * @param array $handlingSubject
     * @param array $response
     */
    public function handle(array $handlingSubject, array $response)
    {
        $payment = $this->getValidPaymentInstance($handlingSubject)->setIsTransactionClosed(true);

        $txnId = $this->helper->getInfo($response, ['Payment', 'TxnList', 'Txn', 'txnID']) // xml api
            ?: $this->helper->getInfo($response, 'txnid') // direct post
                ?: $this->helper->getInfo($response, 'providerReferenceNumber'); // paypal

        if ($txnId) {
            $payment->setTransactionId($txnId);
        }
    }
}
