<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Validator;

use InvalidArgumentException;
use Magento\Payment\Gateway\Validator\ResultInterface;
use Magento\Payment\Gateway\Validator\ResultInterfaceFactory;
use Mageplaza\SecurePay\Helper\Response;

/**
 * Class AbstractValidator
 * @package Mageplaza\SecurePay\Gateway\Validator
 */
abstract class AbstractValidator extends \Magento\Payment\Gateway\Validator\AbstractValidator
{
    /**
     * @var Response
     */
    protected $helper;

    /**
     * AbstractResponseValidator constructor.
     *
     * @param ResultInterfaceFactory $resultFactory
     * @param Response $helper
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory,
        Response $helper
    ) {
        $this->helper = $helper;

        parent::__construct($resultFactory);
    }

    /**
     * Performs validation of result code
     *
     * @param array $validationSubject
     *
     * @return ResultInterface
     */
    public function validate(array $validationSubject)
    {
        if (empty($validationSubject['response'])) {
            throw new InvalidArgumentException(__('Response does not exist'));
        }

        if ($error = $this->getError($validationSubject['response'])) {
            return $this->createResult(false, [__($error)]);
        }

        return $this->createResult(true);
    }

    /**
     * @param array $response
     *
     * @return string
     */
    abstract protected function getError($response);
}
