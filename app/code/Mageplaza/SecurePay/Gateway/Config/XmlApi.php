<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Config;

use Magento\Payment\Gateway\Config\Config;

/**
 * Class XmlApi
 * @package Mageplaza\SecurePay\Gateway\Config
 */
class XmlApi extends Config
{
    /**
     * @return array
     */
    public function getCcTypes()
    {
        return explode(',', $this->getValue('cctypes'));
    }
}
