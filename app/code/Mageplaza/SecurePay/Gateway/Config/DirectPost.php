<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Config;

use Magento\Payment\Gateway\Config\Config;
use Mageplaza\SecurePay\Model\Payment\DirectPost as DirectPostPayment;
use Mageplaza\SecurePay\Model\Source\PaymentAction;

/**
 * Class DirectPost
 * @package Mageplaza\SecurePay\Gateway\Config
 */
class DirectPost extends Config
{
    /**
     * @return array
     */
    public function getCcTypes()
    {
        return explode(',', $this->getValue('cctypes'));
    }

    /**
     * @return bool
     */
    public function is3DSecure()
    {
        return (bool) $this->getValue('three_d');
    }

    /**
     * @return string
     */
    public function getPaymentAction()
    {
        return $this->getValue('payment_action');
    }

    /**
     * @return int
     */
    public function getTxnType()
    {
        $txnType = DirectPostPayment::TXN_PAYMENT_FRAUD;

        if ($is3D = $this->is3DSecure()) {
            $txnType = DirectPostPayment::TXN_PAYMENT_FRAUD_3D;
        }

        if ($this->getPaymentAction() === PaymentAction::ACTION_AUTHORIZE) {
            $txnType = DirectPostPayment::TXN_AUTH_FRAUD;

            if ($is3D) {
                $txnType = DirectPostPayment::TXN_AUTH_FRAUD_3D;
            }
        }

        return $txnType;
    }
}
