<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\SecurePay\Helper\Request;
use Mageplaza\SecurePay\Model\Payment\XmlApi as XmlApiPayment;

/**
 * Class AbstractRequest
 * @package Mageplaza\SecurePay\Gateway\Request
 */
abstract class AbstractRequest
{
    /**
     * @var Request
     */
    protected $helper;

    /**
     * AbstractRequest constructor.
     *
     * @param Request $helper
     */
    public function __construct(
        Request $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param array $buildSubject
     *
     * @return InfoInterface
     */
    protected function getValidPaymentInstance(array $buildSubject)
    {
        $paymentDataObject = SubjectReader::readPayment($buildSubject);

        $payment = $paymentDataObject->getPayment();

        ContextHelper::assertOrderPayment($payment);

        return $payment;
    }

    /**
     * @param array $buildSubject
     * @param array $params
     *
     * @return array
     * @throws LocalizedException
     */
    protected function prepareXmlApi($buildSubject, $params)
    {
        /** @var Payment $payment */
        $payment = $this->getValidPaymentInstance($buildSubject);
        $info    = $payment->getAdditionalInformation();
        $order   = $payment->getOrder();
        $quoteId = $order->getQuoteId();

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<SecurePayMessage>';

        $xml .= '<MessageInfo>';
        $xml .= '<messageID>' . $this->helper->getMessageId($quoteId) . '</messageID>';
        $xml .= '<messageTimestamp>' . $this->helper->getTimeStamp() . '</messageTimestamp>';
        $xml .= '<timeoutValue>' . XmlApiPayment::TIMEOUT . '</timeoutValue>';
        $xml .= '<apiVersion>' . XmlApiPayment::VERSION . '</apiVersion>';
        $xml .= '</MessageInfo>';

        $xml .= '<MerchantInfo>';
        $xml .= '<merchantID>' . $this->helper->getMerchantAccountId() . '</merchantID>';
        $xml .= '<password>' . $this->helper->getTransPwd() . '</password>';
        $xml .= '</MerchantInfo>';

        $xml .= '<RequestType>Payment</RequestType>';

        $xml .= '<Payment>';

        $xml .= '<TxnList count="1">';

        $xml .= '<Txn ID="1">';

        $xml .= '<txnType>' . $params['txnType'] . '</txnType>';
        $xml .= '<txnSource>' . XmlApiPayment::SOURCE . '</txnSource>';

        $amount = $params['amount'] ?? $this->helper->convertAmount($buildSubject['amount'], $order);
        $xml    .= '<amount>' . $amount . '</amount>';

        $orderNo = $params['orderNo'] ?? $this->helper->formatOrderNo($quoteId);
        $xml     .= '<purchaseOrderNo>' . $orderNo . '</purchaseOrderNo>';

        if (isset($params['txnID'])) {
            $xml .= '<txnID>' . $params['txnID'] . '</txnID>';
        } elseif (isset($params['preauthID'])) {
            $xml .= '<preauthID>' . $params['preauthID'] . '</preauthID>';
        } else {
            $xml .= '<currency>' . $order->getOrderCurrencyCode() . '</currency>';

            $this->helper->recollectInformation($payment);

            $ccNumber    = $this->helper->getInfo($info, 'cc_number');
            $expiryMonth = sprintf('%02d', $this->helper->getInfo($info, 'cc_exp_month'));
            $expiryYear  = substr($this->helper->getInfo($info, 'cc_exp_year'), -2);

            $xml .= '<CreditCardInfo>';
            $xml .= '<cardNumber>' . preg_replace('/\D/', '', $ccNumber) . '</cardNumber>';
            $xml .= '<expiryDate>' . sprintf('%s/%s', $expiryMonth, $expiryYear) . '</expiryDate>';
            $xml .= '<cvv>' . $this->helper->getInfo($info, 'cc_cid') . '</cvv>';
            $xml .= '</CreditCardInfo>';

            if ($billing = $order->getBillingAddress()) {
                /** FraudGuard verification */
                $xml .= '<BuyerInfo>';
                $xml .= '<firstName>' . $billing->getFirstname() . '</firstName>';
                $xml .= '<lastName>' . $billing->getLastname() . '</lastName>';
                $xml .= '<zipCode>' . $billing->getPostcode() . '</zipCode>';
                $xml .= '<town>' . $billing->getCity() . '</town>';
                $xml .= '<billingCountry>' . $billing->getCountryId() . '</billingCountry>';
                if ($shipping = $order->getShippingAddress()) {
                    $xml .= '<deliveryCountry>' . $shipping->getCountryId() . '</deliveryCountry>';
                }
                $xml .= '<emailAddress>' . $billing->getEmail() . '</emailAddress>';
                $xml .= '<ip>' . $this->helper->getIpAddress() . '</ip>';
                $xml .= '</BuyerInfo>';
            }
        }

        $xml .= '</Txn>';

        $xml .= '</TxnList>';

        $xml .= '</Payment>';

        $xml .= '</SecurePayMessage>';

        return ['xml' => $xml];
    }
}
