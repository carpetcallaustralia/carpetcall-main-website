<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\SecurePay\Model\Payment\DirectPost;
use Mageplaza\SecurePay\Model\Payment\Paypal;
use Mageplaza\SecurePay\Model\Payment\XmlApi;
use Mageplaza\SecurePay\Model\Source\PaymentInfo as Info;

/**
 * Class CaptureRequest
 * @package Mageplaza\SecurePay\Gateway\Request
 */
class CaptureRequest extends AbstractRequest implements BuilderInterface
{
    /**
     * Builds request
     *
     * @param array $buildSubject
     *
     * @return array
     * @throws LocalizedException
     */
    public function build(array $buildSubject)
    {
        /** @var Payment $payment */
        $payment = $this->getValidPaymentInstance($buildSubject);

        switch ($payment->getMethod()) {
            case XmlApi::CODE:
            case DirectPost::CODE:
                if ($payment->getAdditionalInformation(Info::TXN_ID)) {
                    return $this->prepareXmlApi($buildSubject, [
                        'txnType'   => XmlApi::TXN_COMPLETE,
                        'preauthID' => $payment->getAdditionalInformation(Info::AUTH_ID),
                        'orderNo'   => $payment->getAdditionalInformation(Info::ORDER_NO),
                    ]);
                }

                return $this->prepareXmlApi($buildSubject, ['txnType' => XmlApi::TXN_PAYMENT]);
            case Paypal::CODE:
                $orderId = $payment->getAdditionalInformation('order_id');

                return [
                    'url'          => $this->helper->getPaypalApiUrl(Paypal::EXEC_URL, $orderId),
                    'amount'       => $payment->getAdditionalInformation('amount'),
                    'merchantCode' => $this->helper->getMerchantId(),
                    'ip'           => $this->helper->getIpAddress(),
                    'payerId'      => $payment->getAdditionalInformation('payer_id'),
                ];
            default:
                return [];
        }
    }
}
