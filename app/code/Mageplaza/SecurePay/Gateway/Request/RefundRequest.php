<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\SecurePay\Model\Payment\DirectPost;
use Mageplaza\SecurePay\Model\Payment\Paypal;
use Mageplaza\SecurePay\Model\Payment\XmlApi;
use Mageplaza\SecurePay\Model\Source\PaymentInfo as Info;

/**
 * Class RefundRequest
 * @package Mageplaza\SecurePay\Gateway\Request
 */
class RefundRequest extends AbstractRequest implements BuilderInterface
{
    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     *
     * @return array
     * @throws LocalizedException
     */
    public function build(array $buildSubject)
    {
        /** @var Payment $payment */
        $payment = $this->getValidPaymentInstance($buildSubject);
        $amount  = $this->helper->convertAmount($buildSubject['amount'], $payment->getOrder(), 'refund');

        switch ($payment->getMethod()) {
            case XmlApi::CODE:
            case DirectPost::CODE:
                return $this->prepareXmlApi($buildSubject, [
                    'txnType' => XmlApi::TXN_REFUND,
                    'txnID'   => $payment->getAdditionalInformation(Info::TXN_ID),
                    'orderNo' => $payment->getAdditionalInformation(Info::ORDER_NO),
                    'amount'  => $amount,
                ]);
            case Paypal::CODE:
                $orderId = $payment->getAdditionalInformation(Info::ORDER_ID);

                return [
                    'url'          => $this->helper->getPaypalApiUrl(Paypal::REFUND_URL, $orderId),
                    'amount'       => $amount,
                    'merchantCode' => $this->helper->getMerchantId(),
                    'ip'           => $this->helper->getIpAddress(),
                ];
            default:
                return [];
        }
    }
}
