<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Observer;

use InvalidArgumentException;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\Quote\Payment;
use Mageplaza\SecurePay\Gateway\Config\DirectPost;
use Mageplaza\SecurePay\Gateway\Config\XmlApi;
use Mageplaza\SecurePay\Helper\Data;
use Mageplaza\SecurePay\Model\Payment\DirectPost as DirectPostPayment;
use Mageplaza\SecurePay\Model\Payment\XmlApi as XmlApiPayment;
use Mageplaza\SecurePay\Model\Source\CardTypeXml;
use Mageplaza\SecurePay\Model\Source\PaymentInfo;

/**
 * Class DataAssignObserver
 * @package Mageplaza\SecurePay\Observer
 */
class DataAssignObserver extends AbstractDataAssignObserver
{
    /**
     * @var XmlApi
     */
    private $xmlConfig;

    /**
     * @var DirectPost
     */
    private $postConfig;

    /**
     * DataAssignObserver constructor.
     *
     * @param XmlApi $xmlConfig
     * @param DirectPost $postConfig
     */
    public function __construct(
        XmlApi $xmlConfig,
        DirectPost $postConfig
    ) {
        $this->xmlConfig  = $xmlConfig;
        $this->postConfig = $postConfig;
    }

    /**
     * @param Observer $observer
     *
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $additionalData = $this->readDataArgument($observer)->getData(PaymentInterface::KEY_ADDITIONAL_DATA);

        if (!is_array($additionalData)) {
            return;
        }

        /** @var Payment $payment */
        $payment = $this->readPaymentModelArgument($observer);
        $method  = $payment->getMethod();

        switch ($method) {
            case XmlApiPayment::CODE:
                $ccTypes = $this->xmlConfig->getCcTypes();
                break;
            case DirectPostPayment::CODE:
                $ccTypes = $this->postConfig->getCcTypes();
                break;
            default:
                $ccTypes = false;
        }

        $ccNumber = $additionalData['cc_number'] ?? '';
        $ccNumber = preg_replace('/\D/', '', $ccNumber);

        foreach ($additionalData as $key => $datum) {
            if (is_object($datum)) {
                continue;
            }

            if ($key === PaymentInfo::CARD_TYPE) {
                if (!$datum || !in_array($datum, CardTypeXml::getOptionArray(), true)) {
                    $datum = Data::determineCcType($ccNumber);
                }

                $this->validateCcType($datum, $ccTypes);
            }

            if ($method !== DirectPostPayment::CODE) {
                $payment->setAdditionalInformation($key, $datum);
            }
        }
    }

    /**
     * @param string $ccType
     * @param array|bool $ccTypes
     */
    private function validateCcType($ccType, $ccTypes)
    {
        if ($ccType && is_array($ccTypes) && !in_array($ccType, $ccTypes, true)) {
            throw new InvalidArgumentException(__('Card type "%1" is not allowed.', CardTypeXml::getCardType($ccType)));
        }
    }
}
