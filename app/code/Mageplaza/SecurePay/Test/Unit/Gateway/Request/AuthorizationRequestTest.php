<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Test\Unit\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\SecurePay\Gateway\Config\DirectPost;
use Mageplaza\SecurePay\Gateway\Config\XmlApi;
use Mageplaza\SecurePay\Gateway\Request\AuthorizationRequest;
use Mageplaza\SecurePay\Helper\Request;
use Mageplaza\SecurePay\Model\Payment\XmlApi as XmlApiPayment;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class AuthorizationRequestTest
 * @package Mageplaza\SecurePay\Test\Unit\Gateway\Request
 */
class AuthorizationRequestTest extends TestCase
{
    /**
     * @var Request|PHPUnit_Framework_MockObject_MockObject
     */
    private $helper;

    /**
     * @var XmlApi|PHPUnit_Framework_MockObject_MockObject
     */
    private $xmlConfig;

    /**
     * @var DirectPost|PHPUnit_Framework_MockObject_MockObject
     */
    private $postConfig;

    /**
     * @var UrlInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilder;

    /**
     * @var AuthorizationRequest
     */
    private $object;

    protected function setUp()
    {
        $this->helper     = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $this->xmlConfig  = $this->getMockBuilder(XmlApi::class)->disableOriginalConstructor()->getMock();
        $this->postConfig = $this->getMockBuilder(DirectPost::class)->disableOriginalConstructor()->getMock();
        $this->urlBuilder = $this->getMockBuilder(UrlInterface::class)->getMock();

        $this->object = new AuthorizationRequest($this->helper, $this->xmlConfig, $this->postConfig, $this->urlBuilder);
    }

    /**
     * @throws LocalizedException
     */
    public function testBuild()
    {
        $paymentDataObject = $this->getMockBuilder(PaymentDataObjectInterface::class)->getMock();

        /** @var Payment|PHPUnit_Framework_MockObject_MockObject $payment */
        $payment = $this->getMockBuilder(Payment::class)->disableOriginalConstructor()->getMock();
        $paymentDataObject->method('getPayment')->willReturn($payment);

        /** @var Order|PHPUnit_Framework_MockObject_MockObject $order */
        $order = $this->getMockBuilder(Order::class)->disableOriginalConstructor()->getMock();
        $payment->method('getOrder')->willReturn($order);

        $quoteId      = 12;
        $currencyCode = 'GBP';
        $order->method('getQuoteId')->willReturn($quoteId);
        $order->method('getOrderCurrencyCode')->willReturn($currencyCode);

        $buildSubject = [
            'payment' => $paymentDataObject,
            'amount'  => 100
        ];

        $this->helper->expects($this->once())->method('convertAmount')->willReturn($buildSubject['amount']);

        $params = ['txnType' => XmlApiPayment::TXN_AUTH];

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<SecurePayMessage>';

        $xml .= '<MessageInfo>';

        $messageId = 'message id';
        $this->helper->expects($this->once())->method('getMessageId')->with($quoteId)->willReturn($messageId);
        $xml .= '<messageID>' . $messageId . '</messageID>';

        $timeStamp = 'time stamp';
        $this->helper->expects($this->once())->method('getTimeStamp')->willReturn($timeStamp);
        $xml .= '<messageTimestamp>' . $timeStamp . '</messageTimestamp>';

        $xml .= '<timeoutValue>' . XmlApiPayment::TIMEOUT . '</timeoutValue>';
        $xml .= '<apiVersion>' . XmlApiPayment::VERSION . '</apiVersion>';
        $xml .= '</MessageInfo>';

        $xml .= '<MerchantInfo>';

        $merchantAccountId = 'merchant account id';
        $this->helper->method('getMerchantAccountId')->willReturn($merchantAccountId);
        $xml .= '<merchantID>' . $this->helper->getMerchantAccountId() . '</merchantID>';

        $transPwd = 'transaction password';
        $this->helper->method('getTransPwd')->willReturn($transPwd);
        $xml .= '<password>' . $this->helper->getTransPwd() . '</password>';

        $xml .= '</MerchantInfo>';

        $xml .= '<RequestType>Payment</RequestType>';

        $xml .= '<Payment>';

        $xml .= '<TxnList count="1">';

        $xml .= '<Txn ID="1">';

        $xml .= '<txnType>' . $params['txnType'] . '</txnType>';
        $xml .= '<txnSource>' . XmlApiPayment::SOURCE . '</txnSource>';

        $xml .= '<amount>' . $buildSubject['amount'] . '</amount>';

        $orderNo = 'order no';
        $this->helper->expects($this->once())->method('formatOrderNo')->with($quoteId)->willReturn($orderNo);
        $xml .= '<purchaseOrderNo>' . $orderNo . '</purchaseOrderNo>';

        $order->expects($this->once())->method('getOrderCurrencyCode')->willReturn($currencyCode);
        $xml .= '<currency>' . $currencyCode . '</currency>';

        $this->helper->expects($this->once())->method('recollectInformation')->with($payment);

        $info = [
            'cc_number'    => '4444333322221111',
            'cc_exp_month' => 10,
            'cc_exp_year'  => 22,
            'cc_cid'       => 123,
        ];
        $payment->method('getAdditionalInformation')->willReturn($info);

        $ccNumber    = $this->helper->getInfo($info, 'cc_number');
        $expiryMonth = sprintf('%02d', $this->helper->getInfo($info, 'cc_exp_month'));
        $expiryYear  = substr($this->helper->getInfo($info, 'cc_exp_year'), -2);

        $xml .= '<CreditCardInfo>';
        $xml .= '<cardNumber>' . preg_replace('/\D/', '', $ccNumber) . '</cardNumber>';
        $xml .= '<expiryDate>' . sprintf('%s/%s', $expiryMonth, $expiryYear) . '</expiryDate>';
        $xml .= '<cvv>' . $this->helper->getInfo($info, 'cc_cid') . '</cvv>';
        $xml .= '</CreditCardInfo>';

        /** @var Address|PHPUnit_Framework_MockObject_MockObject $billing */
        $billing = $this->getMockBuilder(Address::class)->disableOriginalConstructor()->getMock();
        $order->method('getBillingAddress')->willReturn($billing);

        $firstname = 'firstname';
        $lastname  = 'lastname';
        $city      = 'city';
        $postcode  = 'postcode';
        $country   = 'country';
        $email     = 'email';
        $billing->method('getFirstname')->willReturn($firstname);
        $billing->method('getLastname')->willReturn($lastname);
        $billing->method('getCity')->willReturn($city);
        $billing->method('getPostcode')->willReturn($postcode);
        $billing->method('getCountryId')->willReturn($country);
        $billing->method('getEmail')->willReturn($email);

        /** FraudGuard verification */
        $xml .= '<BuyerInfo>';
        $xml .= '<firstName>' . $firstname . '</firstName>';
        $xml .= '<lastName>' . $lastname . '</lastName>';
        $xml .= '<zipCode>' . $postcode . '</zipCode>';
        $xml .= '<town>' . $city . '</town>';
        $xml .= '<billingCountry>' . $country . '</billingCountry>';

        /** @var Address|PHPUnit_Framework_MockObject_MockObject $shipping */
        $shipping = $this->getMockBuilder(Address::class)->disableOriginalConstructor()->getMock();
        $order->method('getShippingAddress')->willReturn($shipping);

        $shipping->method('getCountryId')->willReturn($country);
        $xml .= '<deliveryCountry>' . $country . '</deliveryCountry>';

        $xml .= '<emailAddress>' . $email . '</emailAddress>';

        $ipAddress = '127.0.0.1';
        $this->helper->method('getIpAddress')->willReturn($ipAddress);
        $xml .= '<ip>' . $ipAddress . '</ip>';
        $xml .= '</BuyerInfo>';

        $xml .= '</Txn>';

        $xml .= '</TxnList>';

        $xml .= '</Payment>';

        $xml .= '</SecurePayMessage>';

        $this->assertEquals(['xml' => $xml], $this->object->build($buildSubject));
    }
}
