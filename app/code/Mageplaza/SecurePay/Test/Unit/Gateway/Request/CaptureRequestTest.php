<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Test\Unit\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\SecurePay\Gateway\Config\DirectPost;
use Mageplaza\SecurePay\Gateway\Config\XmlApi;
use Mageplaza\SecurePay\Gateway\Request\CaptureRequest;
use Mageplaza\SecurePay\Helper\Request;
use Mageplaza\SecurePay\Model\Payment\Paypal;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class CaptureRequestTest
 * @package Mageplaza\SecurePay\Test\Unit\Gateway\Request
 */
class CaptureRequestTest extends TestCase
{
    /**
     * @var Request|PHPUnit_Framework_MockObject_MockObject
     */
    private $helper;

    /**
     * @var XmlApi|PHPUnit_Framework_MockObject_MockObject
     */
    private $xmlConfig;

    /**
     * @var DirectPost|PHPUnit_Framework_MockObject_MockObject
     */
    private $postConfig;

    /**
     * @var UrlInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilder;

    /**
     * @var CaptureRequest
     */
    private $object;

    protected function setUp()
    {
        $this->helper     = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $this->xmlConfig  = $this->getMockBuilder(XmlApi::class)->disableOriginalConstructor()->getMock();
        $this->postConfig = $this->getMockBuilder(DirectPost::class)->disableOriginalConstructor()->getMock();
        $this->urlBuilder = $this->getMockBuilder(UrlInterface::class)->getMock();

        $this->object = new CaptureRequest($this->helper, $this->xmlConfig, $this->postConfig, $this->urlBuilder);
    }

    /**
     * @throws LocalizedException
     */
    public function testBuild()
    {
        $paymentDataObject = $this->getMockBuilder(PaymentDataObjectInterface::class)->getMock();

        /** @var Payment|PHPUnit_Framework_MockObject_MockObject $payment */
        $payment = $this->getMockBuilder(Payment::class)->disableOriginalConstructor()->getMock();
        $paymentDataObject->method('getPayment')->willReturn($payment);

        $buildSubject = ['payment' => $paymentDataObject];

        $payment->method('getMethod')->willReturn(Paypal::CODE);

        $count = 0;

        $orderId = 'order id';
        $payment->expects($this->at(++$count))->method('getAdditionalInformation')->willReturn($orderId);

        $url = Paypal::EXEC_URL;
        $this->helper->method('getPaypalApiUrl')->with(Paypal::EXEC_URL, $orderId)->willReturn($url);

        $amount = 500;
        $payment->expects($this->at(++$count))->method('getAdditionalInformation')->willReturn($amount);

        $merchantCode = 'merchant code';
        $this->helper->method('getMerchantId')->willReturn($merchantCode);

        $ipAddress = '127.0.0.1';
        $this->helper->method('getIpAddress')->willReturn($ipAddress);

        $payerId = 'payer id';
        $payment->expects($this->at(++$count))->method('getAdditionalInformation')->willReturn($payerId);

        $txnArray = [
            'url'          => $url,
            'amount'       => $amount,
            'merchantCode' => $merchantCode,
            'ip'           => $ipAddress,
            'payerId'      => $payerId
        ];

        $this->assertEquals($txnArray, $this->object->build($buildSubject));
    }
}
