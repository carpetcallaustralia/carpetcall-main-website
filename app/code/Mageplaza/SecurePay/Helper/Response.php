<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\SecurePay\Model\Payment\DirectPost;
use Mageplaza\SecurePay\Model\Payment\Paypal;
use Mageplaza\SecurePay\Model\Payment\XmlApi;
use Mageplaza\SecurePay\Model\Source\PaymentInfo as Info;

/**
 * Class Response
 * @package Mageplaza\SecurePay\Helper
 */
class Response extends Data
{
    /**
     * @param Payment $payment
     * @param array $response
     *
     * @return InfoInterface|Payment
     * @throws LocalizedException
     */
    public function handleResponse($payment, $response)
    {
        $this->recollectInformation($payment);

        if (empty($response)) {
            return $payment;
        }

        switch ($payment->getMethod()) {
            case XmlApi::CODE:
                $this->handleXmlApiResponse($payment, $response);
                break;
            case DirectPost::CODE:
                $this->handleDirectPostResponse($payment, $response);
                break;
            case Paypal::CODE:
                $this->handlePaypalResponse($payment, $response);
                break;
        }

        return $payment;
    }

    /**
     * @param Payment $payment
     * @param array $response
     *
     * @throws LocalizedException
     */
    protected function handleXmlApiResponse($payment, $response)
    {
        $txn  = $this->getInfo($response, ['Payment', 'TxnList', 'Txn']);
        $card = $this->getInfo($txn, 'CreditCardInfo');

        if ($txnId = $this->getInfo($txn, 'txnID')) {
            $payment->setTransactionId($txnId);
            $payment->setAdditionalInformation(Info::TXN_ID, $txnId);
        }

        $this->setInfo($payment, Info::ORDER_NO, $this->getInfo($txn, 'purchaseOrderNo'));
        $this->setInfo($payment, Info::AUTH_ID, $this->getInfo($txn, 'preauthID'));
        $this->setInfo($payment, Info::FRAUD_CODE, $this->getInfo($txn, 'antiFraudResponseCode'));
        $this->setInfo($payment, Info::FRAUD_TEXT, $this->getInfo($txn, 'antiFraudResponseText'));
        $this->setInfo($payment, Info::FRAUD_GUARD, $this->getInfo($txn, 'FraudGuard'));

        if (!$payment->getAdditionalInformation(Info::CARD_TYPE)) {
            $this->setInfo($payment, Info::CARD_TYPE, $this->getInfo($card, 'cardDescription'));
        }
        $this->setInfo($payment, Info::LAST_CARD_4, $this->getInfo($card, 'pan'));

        if ($expiryDate = $this->getInfo($card, 'expiryDate')) {
            $payment->setAdditionalInformation(Info::EXPIRY_DATE, $expiryDate);
            $payment->setAdditionalInformation(Info::EXPIRY_MONTH, substr($expiryDate, 0, 2));
            $payment->setAdditionalInformation(Info::EXPIRY_YEAR, substr($expiryDate, -2, 2));
        }
    }

    /**
     * @param Payment $payment
     * @param array $response
     *
     * @throws LocalizedException
     */
    protected function handleDirectPostResponse($payment, $response)
    {
        if ($txnId = $this->getInfo($response, 'txnid')) {
            $payment->setTransactionId($txnId);
            $payment->setAdditionalInformation(Info::TXN_ID, $txnId);
        }

        $this->setInfo($payment, Info::ORDER_NO, $this->getInfo($response, 'refid'));
        $this->setInfo($payment, Info::AUTH_ID, $this->getInfo($response, 'preauthid'));
        $this->setInfo($payment, Info::FRAUD_CODE, $this->getInfo($response, 'afrescode'));
        $this->setInfo($payment, Info::FRAUD_TEXT, $this->getInfo($response, 'afrestext'));
        $this->setInfo($payment, Info::LAST_CARD_4, $this->getInfo($response, 'pan'));

        if ($expiryDate = $this->getInfo($response, 'expirydate')) {
            $payment->setAdditionalInformation(Info::EXPIRY_DATE, substr_replace($expiryDate, '/', 2, 0));
            $payment->setAdditionalInformation(Info::EXPIRY_MONTH, substr($expiryDate, 0, 2));
            $payment->setAdditionalInformation(Info::EXPIRY_YEAR, substr($expiryDate, -2, 2));
        }
    }

    /**
     * @param Payment $payment
     * @param array $response
     *
     * @throws LocalizedException
     */
    protected function handlePaypalResponse($payment, $response)
    {
        if ($txnId = $this->getInfo($response, 'providerReferenceNumber')) {
            $payment->setTransactionId($txnId);
            $payment->setAdditionalInformation(Info::TXN_ID, $txnId);
        }

        $payment->setAdditionalInformation(Info::ORDER_ID, $this->getInfo($response, 'orderId'));
        $payment->setAdditionalInformation(Info::STATUS, $this->getInfo($response, 'status'));
    }

    /**
     * @param Payment $payment
     * @param string $code
     * @param string $value
     *
     * @throws LocalizedException
     */
    protected function setInfo($payment, $code, $value)
    {
        if ($value) {
            $payment->setAdditionalInformation($code, $value);
        }
    }

    /**
     * @param array|string $response
     *
     * @return string|bool
     */
    public function hasErrorXml($response)
    {
        if (!is_array($response)) {
            return $response;
        }

        $txn = $this->getInfo($response, ['Payment', 'TxnList', 'Txn']);

        $sttCode = $this->getInfo($txn, 'responseCode') ?: $this->getInfo($response, ['Status', 'statusCode']);

        if (in_array($sttCode, ['00', '000', '08'], true)) {
            return false;
        }

        $message = '';

        $this->appendMessage($message, $sttCode, __('Status Code'));
        $this->appendMessage($message, $this->getInfo($txn, 'responseText'));
        $this->appendMessage($message, $this->getInfo($response, ['Status', 'statusDescription']), __('Description:'));

        return $message;
    }

    /**
     * @param array|string $response
     *
     * @return string|bool
     */
    public function hasErrorPost($response)
    {
        $sttCode = $this->getInfo($response, 'rescode');

        if (in_array($sttCode, ['00', '08'], true)) {
            return false;
        }

        $message = '';

        $this->appendMessage($message, $sttCode, __('Status Code'));
        $this->appendMessage($message, $this->getInfo($response, 'restext'));

        return $message;
    }

    /**
     * @param array|string $response
     *
     * @return string|bool
     */
    public function hasErrorPaypal($response)
    {
        $errors = $this->getInfo($response, 'errors');

        if (empty($errors)) {
            return false;
        }

        $message = '';

        foreach ((array)$errors as $error) {
            $this->appendMessage($message, $this->getInfo($error, 'code'));
            $this->appendMessage($message, $this->getInfo($error, ['source', 'pointer']));
            $this->appendMessage($message, $this->getInfo($error, 'detail'));
        }

        return $message;
    }

    /**
     * @param string $message
     * @param string $string
     * @param string $prefix
     */
    protected function appendMessage(&$message, $string, $prefix = '')
    {
        if ($string) {
            if ($message) {
                $message .= ' - ';
            }
            if ($prefix) {
                $prefix .= ' ';
            }

            $message .= $prefix . $string;
        }
    }
}
