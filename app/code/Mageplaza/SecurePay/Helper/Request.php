<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Helper;

use Mageplaza\SecurePay\Model\Payment\Paypal;
use Mageplaza\SecurePay\Model\Source\Environment;
use Zend\Mime\Decode;
use Zend_Http_Client;
use Zend_Http_Response;

/**
 * Class Request
 * @package Mageplaza\SecurePay\Helper
 */
class Request extends Data
{
    const TOKEN_URL_TEST = 'https://hello.sandbox.auspost.com.au/oauth2/ausujjr7T0v0TTilk3l5/v1/token';
    const TOKEN_URL_LIVE = 'https://hello.auspost.com.au/oauth2/ausrkwxtmx9Jtwp4s356/v1/token';

    /**
     * @param string $url
     * @param null $env
     *
     * @return string
     */
    public function getApiUrl($url, $env = null)
    {
        if (!$env) {
            $env = $this->getEnvironment();
        }

        $param = $env === Environment::SANDBOX ? 'test.' : '';

        return str_replace('%env%', $param, $url);
    }

    /**
     * @param string $path
     * @param string $orderId
     *
     * @return string
     */
    public function getPaypalApiUrl($path, $orderId = '')
    {
        $url = $this->isSandbox() ? Paypal::URL_TEST : Paypal::URL_LIVE;
        $url .= $path;

        if ($orderId) {
            return str_replace('%orderId%', $orderId, $url);
        }

        return $url;
    }

    /**
     * @param string $url
     * @param array $headers
     * @param string $body
     * @param string $type
     * @param array $config
     * @param string $method
     *
     * @return array
     */
    public function sendRequest($url, $headers, $body, $type = '', $config = [], $method = Zend_Http_Client::POST)
    {
        $curl = $this->curlFactory->create();

        $curl->setConfig(array_merge(['timeout' => 120, 'verifyhost' => 2], $config));
        $curl->write($method, $url, Zend_Http_Client::HTTP_1, $headers, $body);

        $response = $curl->read();

        switch ($type) {
            case 'xml':
                $response = $this->rawResponseToArray($response);
                break;
            case 'json':
                $response = self::jsonDecode(Zend_Http_Response::extractBody($response));
                break;
        }

        $curl->close();

        return $response;
    }

    /**
     * @param string $response
     *
     * @return array
     */
    public function rawResponseToArray($response)
    {
        $responseString = preg_split('/^\r?$/m', $response, 2);

        $responseArray = explode("\n", $responseString[1]);
        $xmlString     = $this->versionCompare('2.3.0') ?
            Decode::decodeQuotedPrintable($responseArray[1]) :
            iconv_mime_decode($responseArray[1], ICONV_MIME_DECODE_CONTINUE_ON_ERROR, 'UTF-8');

        $xml = simplexml_load_string($xmlString, 'SimpleXMLElement', LIBXML_NOCDATA);

        return self::jsonDecode(self::jsonEncode($xml));
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        $url = $this->isSandbox() ? self::TOKEN_URL_TEST : self::TOKEN_URL_LIVE;

        $val     = base64_encode("{$this->getPubKey()}:{$this->getSecretKey()}");
        $headers = ["Authorization: Basic {$val}"];

        $scope = [
            'https://api.payments.auspost.com.au/payhive/payments/read',
            'https://api.payments.auspost.com.au/payhive/payments/write',
            'https://api.payments.auspost.com.au/payhive/payment-instruments/read',
            'https://api.payments.auspost.com.au/payhive/payment-instruments/write'
        ];

        $body = [
            'grant_type' => 'client_credentials',
            'scope'      => implode(' ', $scope),
        ];

        $accessToken = $this->sendRequest($url, $headers, http_build_query($body), 'json');

        return $this->getInfo($accessToken, 'access_token');
    }
}
