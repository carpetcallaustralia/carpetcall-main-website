<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote\Payment;
use Magento\Sales\Model\Order;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Core\Helper\AbstractData;
use Mageplaza\SecurePay\Model\Source\CardPattern;
use Mageplaza\SecurePay\Model\Source\Environment;
use Mageplaza\SecurePay\Model\Source\PaymentInfo;

/**
 * Class Data
 * @package Mageplaza\SecurePay\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'mpsecurepay';

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var CurlFactory
     */
    protected $curlFactory;

    /**
     * @var Resolver
     */
    protected $resolver;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\Request
     */
    protected $envRequest;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param EncryptorInterface $encryptor
     * @param PriceCurrencyInterface $priceCurrency
     * @param CurlFactory $curlFactory
     * @param Resolver $resolver
     * @param \Magento\Framework\HTTP\PhpEnvironment\Request $envRequest
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        EncryptorInterface $encryptor,
        PriceCurrencyInterface $priceCurrency,
        CurlFactory $curlFactory,
        Resolver $resolver,
        \Magento\Framework\HTTP\PhpEnvironment\Request $envRequest
    ) {
        $this->encryptor     = $encryptor;
        $this->priceCurrency = $priceCurrency;
        $this->curlFactory   = $curlFactory;
        $this->resolver      = $resolver;
        $this->envRequest    = $envRequest;

        parent::__construct($context, $objectManager, $storeManager);
    }

    /**
     * @param string $code
     * @param null $storeId
     *
     * @return mixed
     */
    public function getConfigGeneral($code = '', $storeId = null)
    {
        $value = parent::getConfigGeneral($code, $storeId);

        if (in_array($code, ['trans_pwd', 'secret_key'], true)) {
            return $this->encryptor->decrypt($value);
        }

        return $value;
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getEnvironment($store = null)
    {
        return $this->getConfigGeneral('environment', $store);
    }

    /**
     * @param null $store
     *
     * @return bool
     */
    public function isSandbox($store = null)
    {
        return $this->getEnvironment($store) === Environment::SANDBOX;
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getMerchantAccountId($store = null)
    {
        return $this->getConfigGeneral('merchant_account_id', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getTransPwd($store = null)
    {
        return $this->getConfigGeneral('trans_pwd', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getMerchantId($store = null)
    {
        return $this->getConfigGeneral('merchant_id', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getPubKey($store = null)
    {
        return $this->getConfigGeneral('pub_key', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getSecretKey($store = null)
    {
        return $this->getConfigGeneral('secret_key', $store);
    }

    /**
     * @param float $amount
     * @param string|null $currency
     *
     * @return int
     */
    public function formatAmount($amount, $currency = null)
    {
        $multiplier = $currency === 'JPY' ? 1 : 100;

        return (int) ($amount * $multiplier);
    }

    /**
     * @param float $amount
     * @param Order $order
     * @param string $type
     *
     * @return int
     */
    public function convertAmount($amount, $order, $type = '')
    {
        $currency = $order->getOrderCurrencyCode();
        $scope    = $order->getStoreId();
        $max      = $order->getGrandTotal();

        if ($type === 'refund' && $creditMemos = $order->getCreditmemosCollection()) {
            foreach ($creditMemos->getItems() as $item) {
                $max -= $item->getGrandTotal();
            }
        }

        $amount = $this->priceCurrency->convert($amount, $scope, $currency);

        return $this->formatAmount(min($amount, $max), $currency);
    }

    /**
     * @param array $response
     * @param array|string $keys
     *
     * @return mixed
     */
    public function getInfo($response, $keys)
    {
        if (is_string($keys)) {
            return isset($response[$keys]) ? $response[$keys] : null;
        }

        if (is_array($keys)) {
            foreach ($keys as $key) {
                if (isset($response[$key])) {
                    if ($key === array_values(array_slice($keys, -1))[0]) {
                        return $response[$key];
                    }

                    array_shift($keys);

                    return $this->getInfo($response[$key], $keys);
                }
            }
        }

        return null;
    }

    /**
     * @param string $orderNo
     *
     * @return string
     */
    public function formatOrderNo($orderNo)
    {
        return date('dmy-Gis') . '-' . $orderNo;
    }

    /**
     * @param string $orderNo
     *
     * @return string
     */
    public function getMessageId($orderNo)
    {
        return $orderNo . '-' . date('His') . current(preg_split('/\s/', microtime()));
    }

    /**
     * return a timestamp formatted as per requirement in the Secure XML API documentation
     *
     * @return string
     */
    public function getTimeStamp()
    {
        if ($tz_minutes = date('Z') / 60) {
            $tz_minutes = '+' . $tz_minutes;
        }

        return date('YdmGis000000') . $tz_minutes;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->envRequest->getClientIp();
    }

    /**
     * @return bool
     */
    public function isOscPage()
    {
        return $this->isModuleOutputEnabled('Mageplaza_Osc') && $this->_request->getRouteName() === 'onestepcheckout';
    }

    /**
     * @return string|null
     */
    public function getLocaleCode()
    {
        return $this->resolver->getLocale();
    }

    /**
     * @param Payment|Order\Payment $payment
     *
     * @throws LocalizedException
     */
    public function recollectInformation($payment)
    {
        $ccType = $payment->getAdditionalInformation(PaymentInfo::CARD_TYPE);

        $payment->unsAdditionalInformation();

        if ($ccType) {
            $payment->setAdditionalInformation(PaymentInfo::CARD_TYPE, $ccType);
        }
    }

    /**
     * @param string $ccNumber
     *
     * @return string
     */
    public static function determineCcType($ccNumber)
    {
        foreach (CardPattern::getOptionArray() as $patterns => $ccType) {
            foreach (explode(',', $patterns) as $pattern) {
                if (preg_match('/' . trim($pattern) . '/', $ccNumber)) {
                    return $ccType;
                }
            }
        }

        return '';
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isSecureRequest()
    {
        /** @var Store $store */
        $store = $this->storeManager->getStore();

        return $store->isCurrentlySecure();
    }
}
