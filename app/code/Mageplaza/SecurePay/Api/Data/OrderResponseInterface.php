<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Api\Data;

/**
 * Interface OrderResponseInterface
 * @package Mageplaza\SecurePay\Api\Data
 */
interface OrderResponseInterface
{
    /**
     * Constants defined for keys of array, makes typos less likely
     */
    const FINGERPRINT = 'fingerprint';
    const ORDER_NO    = 'order_no';
    const GRAND_TOTAL = 'grand_total';
    const TIMESTAMP   = 'timestamp';

    /**
     * @return string
     */
    public function getFingerprint();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setFingerprint($value);

    /**
     * @return string
     */
    public function getOrderNo();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setOrderNo($value);

    /**
     * @return float
     */
    public function getGrandTotal();

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setGrandTotal($value);

    /**
     * @return string
     */
    public function getTimestamp();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setTimestamp($value);
}
