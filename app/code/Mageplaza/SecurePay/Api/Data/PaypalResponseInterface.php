<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Api\Data;

/**
 * Interface PaypalResponseInterface
 * @package Mageplaza\SecurePay\Api\Data
 */
interface PaypalResponseInterface
{
    /**
     * Constants defined for keys of array, makes typos less likely
     */
    const AMOUNT      = 'amount';
    const ORDER_ID    = 'order_id';
    const PAYMENT_ID  = 'payment_id';
    const PAYMENT_URL = 'payment_url';
    const PAYER_ID    = 'payer_id';
    const ERROR       = 'error';

    /**
     * @return string
     */
    public function getAmount();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setAmount($value);

    /**
     * @return string
     */
    public function getOrderId();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setOrderId($value);

    /**
     * @return string
     */
    public function getPaymentId();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setPaymentId($value);

    /**
     * @return string
     */
    public function getPaymentUrl();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setPaymentUrl($value);

    /**
     * @return string
     */
    public function getPayerId();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setPayerId($value);

    /**
     * @return string
     */
    public function getError();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setError($value);
}
