<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Mageplaza\SecurePay\Api\Data\OrderResponseInterface;
use Mageplaza\SecurePay\Api\Data\PaypalResponseInterface;

/**
 * Interface GuestPaymentInterface
 * @package Mageplaza\SecurePay\Api
 */
interface GuestPaymentInterface
{
    /**
     * @param string $cartId
     *
     * @return OrderResponseInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getFingerprint($cartId);

    /**
     * @param string $cartId
     *
     * @return PaypalResponseInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function initPaypal($cartId);
}
