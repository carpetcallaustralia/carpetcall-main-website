<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Controller\Adminhtml\Credential;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Mageplaza\SecurePay\Helper\Request;
use Mageplaza\SecurePay\Helper\Response;
use Mageplaza\SecurePay\Model\Payment\XmlApi;

/**
 * Class Test
 * @package Mageplaza\SecurePay\Controller\Adminhtml\Credential
 */
class Test extends Action
{
    /**
     * @var Response
     */
    private $helper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Request
     */
    private $requestHelper;

    /**
     * Test constructor.
     *
     * @param Context $context
     * @param Response $helper
     * @param Request $requestHelper
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        Response $helper,
        Request $requestHelper,
        JsonFactory $resultJsonFactory
    ) {
        $this->helper            = $helper;
        $this->requestHelper     = $requestHelper;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $request = $this->getRequest();

        $merchantId  = $request->getParam('merchant_account_id');
        $transPwd    = $request->getParam('trans_pwd');
        $environment = $request->getParam('environment');

        if ($transPwd === '******') {
            $transPwd = $this->helper->getTransPwd();
        }

        $url     = $this->requestHelper->getApiUrl(XmlApi::URL, $environment);
        $headers = ['Content-type: text/xml'];
        $body    = $this->createXml($merchantId, $transPwd);

        $response = $this->requestHelper->sendRequest($url, $headers, $body, 'xml');

        if ($error = $this->helper->hasErrorXml($response)) {
            return $resultJson->setData(['type' => 'error', 'message' => $error]);
        }

        return $resultJson->setData(['type' => 'success', 'message' => __('Credentials are valid')]);
    }

    /**
     * @param string $merchantId
     * @param string $transPwd
     *
     * @return string
     */
    protected function createXml($merchantId, $transPwd)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<SecurePayMessage>';

        $xml .= '<MessageInfo>';
        $xml .= '<messageID>' . $this->requestHelper->getMessageId('test') . '</messageID>';
        $xml .= '<messageTimestamp>' . $this->requestHelper->getTimeStamp() . '</messageTimestamp>';
        $xml .= '<timeoutValue>' . XmlApi::TIMEOUT . '</timeoutValue>';
        $xml .= '<apiVersion>' . XmlApi::VERSION . '</apiVersion>';
        $xml .= '</MessageInfo>';

        $xml .= '<MerchantInfo>';
        $xml .= '<merchantID>' . $merchantId . '</merchantID>';
        $xml .= '<password>' . $transPwd . '</password>';
        $xml .= '</MerchantInfo>';

        $xml .= '<RequestType>Payment</RequestType>';

        $xml .= '<Payment>';

        $xml .= '<TxnList count="1">';

        $xml .= '<Txn ID="1">';

        $xml .= '<txnType>' . XmlApi::TXN_AUTH . '</txnType>';
        $xml .= '<txnSource>' . XmlApi::SOURCE . '</txnSource>';
        $xml .= '<amount>500</amount>';
        $xml .= '<currency>AUD</currency>';
        $xml .= '<purchaseOrderNo>TEST-' . date('dmy-Gis') . '</purchaseOrderNo>';

        $xml .= '<CreditCardInfo>';
        $xml .= '<cardNumber>4444333322221111</cardNumber>';
        $xml .= '<expiryDate>11/' . (substr(date('Y'), 2) + 3) . '</expiryDate>';
        $xml .= '<cvv>123</cvv>';
        $xml .= '</CreditCardInfo>';

        /** FraudGuard verification */
        $xml .= '<BuyerInfo>';
        $xml .= '<firstName>John</firstName>';
        $xml .= '<lastName>Smith</lastName>';
        $xml .= '<zipCode>000</zipCode>';
        $xml .= '<town>Melbourne</town>';
        $xml .= '<billingCountry>AU</billingCountry>';
        $xml .= '<deliveryCountry>AU</deliveryCountry>';
        $xml .= '<emailAddress>test@order.com</emailAddress>';
        $xml .= '<ip>' . $this->helper->getIpAddress() . '</ip>';
        $xml .= '</BuyerInfo>';

        $xml .= '</Txn>';

        $xml .= '</TxnList>';

        $xml .= '</Payment>';

        $xml .= '</SecurePayMessage>';

        return $xml;
    }
}
