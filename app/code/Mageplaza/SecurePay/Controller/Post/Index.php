<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Controller\Post;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use Mageplaza\SecurePay\Controller\PlaceOrder;

/**
 * Class Index
 * @package Mageplaza\SecurePay\Controller\Post
 */
class Index extends PlaceOrder
{
    /**
     * @param Quote $quote
     *
     * @throws LocalizedException
     */
    public function paymentHandler($quote)
    {
        $data = $this->getRequest()->getParams();

        $this->paymentLogger->debug(['direct post response' => $data]);

        $payment = $quote->getPayment();

        $this->helper->recollectInformation($payment);

        $payment->setAdditionalInformation('hostedResponse', $data);
    }
}
