<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Plugin\Model;

use Mageplaza\Osc\Model\DefaultConfigProvider;
use Mageplaza\SecurePay\Model\Payment\Paypal;

/**
 * Class OscConfigProvider
 * @package Mageplaza\SecurePay\Plugin\Model
 */
class OscConfigProvider
{
    /**
     * @param DefaultConfigProvider $subject
     * @param array $result
     *
     * @return array
     */
    public function afterGetPaymentCustomBtn(DefaultConfigProvider $subject, $result)
    {
        $result[] = Paypal::CODE;

        return $result;
    }
}
