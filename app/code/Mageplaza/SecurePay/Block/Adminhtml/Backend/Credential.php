<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Block\Adminhtml\Backend;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;
use Magento\Paypal\Model\Config;

/**
 * Class Credential
 * @package Mageplaza\SecurePay\Block\Adminhtml\Backend
 */
class Credential extends Field
{
    /**
     * @var string
     */
    protected $_template = 'Mageplaza_SecurePay::system/config/credential.phtml';

    /**
     * @var Config
     */
    private $config;

    /**
     * Credential constructor.
     *
     * @param Context $context
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        array $data = []
    ) {
        $this->config = $config;

        parent::__construct($context, $data);
    }

    /**
     * Remove scope label
     *
     * @param AbstractElement|\Magento\Framework\Data\Form\Element\Button $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * @return string
     */
    public function getButtonHtml()
    {
        /** @var Button $button */
        try {
            $button = $this->getLayout()->createBlock(Button::class);
        } catch (LocalizedException $e) {
            $this->_logger->critical($e->getMessage());

            return '';
        }

        $button->setData([
            'id'    => 'mpsecurepay-test-button',
            'label' => __('Test Credential'),
            'class' => 'primary'
        ]);

        return $button->toHtml();
    }

    /**
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('mpsecurepay/credential/test', ['form_key' => $this->getFormKey()]);
    }

    /**
     * @return string
     */
    public function getMerchantCountry()
    {
        return strtolower($this->config->getMerchantCountry());
    }
}
