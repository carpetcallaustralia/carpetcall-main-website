<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Block;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\ConfigurableInfo;
use Magento\Payment\Gateway\ConfigInterface;
use Mageplaza\SecurePay\Helper\Data;
use Mageplaza\SecurePay\Model\Source\CardTypeXml;
use Mageplaza\SecurePay\Model\Source\PaymentInfo;

/**
 * Class Info
 * @package Mageplaza\SecurePay\Block
 */
class Info extends ConfigurableInfo
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * Info constructor.
     *
     * @param Context $context
     * @param ConfigInterface $config
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigInterface $config,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;

        parent::__construct($context, $config, $data);
    }

    /**
     * @param string $field
     *
     * @return mixed
     */
    protected function getLabel($field)
    {
        return PaymentInfo::getOptionArray()[$field];
    }

    /**
     * Sets data to transport
     *
     * @param DataObject $transport
     * @param string|array $field
     * @param string|array $value
     *
     * @return void
     */
    protected function setDataToTransfer(DataObject $transport, $field, $value)
    {
        if ($field === PaymentInfo::CARD_TYPE) {
            parent::setDataToTransfer($transport, $field, CardTypeXml::getCardType($value));

            return;
        }

        $fields = [
            PaymentInfo::AUTH_ID,
            PaymentInfo::ORDER_NO,
            PaymentInfo::FRAUD_CODE,
            PaymentInfo::FRAUD_TEXT,
            PaymentInfo::ORDER_ID,
            PaymentInfo::STATUS,
        ];

        if (in_array($field, $fields, true) && !$this->helper->isAdmin()) {
            return;
        }

        parent::setDataToTransfer($transport, $field, $value);
    }
}
