/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(['jquery'], function ($) {
    "use strict";

    $.widget('mageplaza.securepay', {
        _create: function () {
            var url  = this.options.url,
                path = '#payment_' + this.options.country + '_mpsecurepay_credentials_';

            $('#mpsecurepay-test-button').click(function () {
                var self = this, data = {
                    'environment': $(path + 'environment').val(),
                    'merchant_account_id': $(path + 'merchant_account_id').val(),
                    'trans_pwd': $(path + 'trans_pwd').val()
                };

                $(self).addClass('disabled');
                $('#mpsecurepay-spinner').removeClass('no-display');

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (response) {
                        $(self).removeClass('disabled');
                        $('#mpsecurepay-spinner').addClass('no-display');
                        $('#mpsecurepay-messages').html(
                            '<div class="message message-' + response.type + ' ' + response.type + ' ">' +
                            '<span>' + response.message + '</span>' +
                            '</div>'
                        );
                    }
                });
            });
        }
    });

    return $.mageplaza.securepay;
});
