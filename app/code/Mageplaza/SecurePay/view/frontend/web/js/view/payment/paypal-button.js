/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote'
], function (ko, Component, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Mageplaza_SecurePay/payment/paypal-button'
        },
        isVisible: ko.observable(false),

        initObservable: function () {
            var self = this;

            this._super();

            this.togglePlaceOrderButton(quote.paymentMethod());

            quote.paymentMethod.subscribe(function (value) {
                self.togglePlaceOrderButton(value);
            });

            return this;
        },

        togglePlaceOrderButton: function (payment) {
            var isPaypal = payment && payment.method === 'mpsecurepay_paypal';

            this.isVisible(isPaypal);
        }
    });
});
