/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'Mageplaza_SecurePay/js/view/payment/method-renderer/paypal',
    'Mageplaza_Osc/js/action/set-checkout-information'
], function (Component, setCheckoutInformationAction) {
    'use strict';

    return Component.extend({
        onClick: function () {
            this._super();

            setCheckoutInformationAction();
        },

        afterResolveDocument: function () {
            this.initButton('#mpsecurepay-paypal-button-osc');
        }
    });
});
