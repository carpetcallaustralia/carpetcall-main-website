/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'Mageplaza_SecurePay/js/view/payment/method-renderer/cards',
    'Magento_Checkout/js/action/set-payment-information',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Mageplaza_SecurePay/js/action/send-request',
    'Mageplaza_SecurePay/js/model/resource-url-manager',
    'Magento_Checkout/js/model/quote',
    'mage/dataPost'
], function (
    $,
    Component,
    setPaymentInformationAction,
    additionalValidators,
    sendRequest,
    resourceUrlManager,
    quote
) {
    'use strict';

    return Component.extend({
        placeOrder: function (data, event) {
            var self = this, url = resourceUrlManager.getUrlForGetFingerprint(quote);

            if (event) {
                event.preventDefault();
            }

            if (!this.validate() || !additionalValidators.validate()) {
                $('body, html').animate({scrollTop: $('#' + this.getCode()).offset().top}, 'slow');

                return false;
            }

            setPaymentInformationAction(this.messageContainer, this.getData()).done(function () {
                sendRequest(url, self.messageContainer).done(function (response) {
                    self.submitForm(response);
                });
            });

            return true;
        },

        submitForm: function (response) {
            var billing   = quote.billingAddress(),
                shipping  = quote.shippingAddress(),
                urlParams = {
                    'action': this.getConfig('postUrl'),
                    'data': {
                        'EPS_MERCHANT': this.getConfig('merchantKey'),
                        'EPS_TXNTYPE': this.getConfig('txnType'),
                        'EPS_AMOUNT': parseFloat(response.grand_total).toFixed(2),
                        'EPS_REFERENCEID': response.order_no,
                        'EPS_TIMESTAMP': response.timestamp,
                        'EPS_FINGERPRINT': response.fingerprint,
                        'EPS_CARDNUMBER': this.creditCardNumber().replace(/\D/g, ''),
                        'EPS_EXPIRYMONTH': this.formatExpMonth(this.creditCardExpMonth()),
                        'EPS_EXPIRYYEAR': this.creditCardExpYear(),
                        'EPS_CCV': this.creditCardVerificationNumber(),
                        'EPS_RESULTURL': this.getConfig('callbackUrl'),
                        'EPS_CURRENCY': window.checkoutConfig.quoteData.quote_currency_code,
                        'EPS_REDIRECT': 'true',
                        'EPS_CALLBACKURL': this.getConfig('callbackUrl'),
                        'EPS_IP': this.getConfig('ipAddress'),
                        'EPS_FIRSTNAME': billing.firstname,
                        'EPS_LASTNAME': billing.lastname,
                        'EPS_ZIPCODE': billing.postcode,
                        'EPS_TOWN': billing.city,
                        'EPS_BILLINGCOUNTRY': billing.countryId,
                        'EPS_DELIVERYCOUNTRY': shipping.countryId
                    }
                };

            $.mage.dataPost().postData(urlParams);
        },

        formatExpMonth: function (expMonth) {
            return ('0' + expMonth).slice(-2);
        }
    });
});
