/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/set-payment-information',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Mageplaza_SecurePay/js/action/send-request',
    'Mageplaza_SecurePay/js/model/resource-url-manager',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-payment-method',
    'rjsResolver'
], function (
    $,
    Component,
    setPaymentInformationAction,
    additionalValidators,
    sendRequest,
    resourceUrlManager,
    quote,
    selectPaymentMethodAction,
    resolver
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Mageplaza_SecurePay/payment/paypal'
        },
        amount: null,
        order_id: null,
        payer_id: null,

        initialize: function () {
            this._super();

            resolver(this.afterResolveDocument.bind(this));

            return this;
        },

        afterResolveDocument: function () {
            this.initButton('#mpsecurepay-paypal-button');
        },

        /**
         * @returns {String}
         */
        getCode: function () {
            return this.index;
        },

        getConfig: function (key) {
            if (window.checkoutConfig.payment[this.getCode()].hasOwnProperty(key)) {
                return window.checkoutConfig.payment[this.getCode()][key];
            }

            return null;
        },

        isActive: function () {
            return this.getCode() === this.isChecked();
        },

        /**
         * @returns {*}
         */
        getData: function () {
            var data = this._super();

            if (!data.hasOwnProperty('additional_data') || !data.additional_data) {
                data.additional_data = {};
            }

            data.additional_data.amount   = this.amount;
            data.additional_data.order_id = this.order_id;
            data.additional_data.payer_id = this.payer_id;

            return data;
        },

        onClick: function () {
            selectPaymentMethodAction(this.getData());
        },

        initButton: function (element) {
            var self = this;

            if (window.checkoutConfig.payment.paypalExpress) {
                require(['paypalInContextExpressCheckout'], function (paypal) {
                    self.renderButton(paypal, element);
                });
            } else {
                require(['paypalSecurePayCheckout'], function (paypal) {
                    self.renderButton(paypal, element);
                });
            }
        },

        renderButton: function (paypal, element) {
            var self = this;

            paypal.Button.render({
                env: self.getConfig('env'),
                locale: self.getConfig('locale'),
                style: {size: 'responsive', color: 'gold', shape: 'pill', label: 'pay', tagline: false},
                commit: true,

                /**
                 * Execute logic on Paypal button click
                 */
                onClick: function () {
                    if (self.validate() && additionalValidators.validate()) {
                        self.onClick();
                    }
                },

                /**
                 * Initiate payment
                 *
                 * @returns {string|Promise<T>}
                 */
                payment: function () {
                    if (self.validate() && additionalValidators.validate()) {
                        return self.getPaymentId().then(function (paymentId) {
                            return paymentId;
                        });
                    }

                    return '';
                },

                /**
                 * Execute payment
                 *
                 * @param response
                 */
                onAuthorize: function (response) {
                    self.payer_id = response.payerID;
                    self.placeOrder();
                }
            }, element);
        },

        getPaymentId: function () {
            var self = this, url = resourceUrlManager.getUrlForInitPaypal(quote), promise = $.Deferred();

            setPaymentInformationAction(self.messageContainer, self.getData()).done(function () {
                return sendRequest(url, self.messageContainer).done(function (response) {
                    if (response.error) {
                        $('body, html').animate({scrollTop: $('#' + self.getCode()).offset().top}, 'slow');

                        self.messageContainer.addErrorMessage({message: response.error});
                    } else {
                        self.amount   = response.amount;
                        self.order_id = response.order_id;
                    }

                    return promise.resolve(response.payment_id);
                });
            });

            return promise;
        }
    });
});
