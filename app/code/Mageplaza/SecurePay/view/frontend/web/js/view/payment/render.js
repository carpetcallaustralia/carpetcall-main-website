/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
], function (Component, rendererList) {
    'use strict';

    var isOscPage = window.checkoutConfig.payment.mpsecurepay_paypal.isOscPage ? '-osc' : '';

    rendererList.push(
        {
            type: 'mpsecurepay_xml_api',
            component: 'Mageplaza_SecurePay/js/view/payment/method-renderer/cards'
        },
        {
            type: 'mpsecurepay_direct_post',
            component: 'Mageplaza_SecurePay/js/view/payment/method-renderer/direct-post'
        },
        {
            type: 'mpsecurepay_paypal',
            component: 'Mageplaza_SecurePay/js/view/payment/method-renderer/paypal' + isOscPage
        }
    );

    return Component.extend({});
});
