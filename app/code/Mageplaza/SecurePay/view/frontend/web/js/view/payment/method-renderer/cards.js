/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'ko',
    'jquery',
    'Magento_Payment/js/view/payment/cc-form',
    'Magento_Checkout/js/model/payment/additional-validators'
], function (ko, $, Component, additionalValidators) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Mageplaza_SecurePay/payment/cards',
            isValidated: ko.observable(false)
        },

        initialize: function () {
            this._super();

            if (window.checkoutConfig && window.checkoutConfig.hasOwnProperty('oscConfig')) {
                additionalValidators.registerValidator(this);
            }

            return this;
        },

        /**
         * @return {Boolean}
         */
        selectPaymentMethod: function () {
            this.isValidated(false);

            return this._super();
        },

        /**
         * @returns {String}
         */
        getCode: function () {
            return this.index;
        },

        getConfig: function (key) {
            if (window.checkoutConfig.payment[this.getCode()].hasOwnProperty(key)) {
                return window.checkoutConfig.payment[this.getCode()][key];
            }

            return null;
        },

        isProvided: function () {
            if (this.hasVerification() && !this.creditCardVerificationNumber()) {
                return false;
            }

            return !!(this.creditCardNumber() && this.creditCardExpMonth() && this.creditCardExpYear());
        },

        isActive: function () {
            return this.getCode() === this.isChecked();
        },

        validate: function () {
            if (!this.isActive()) {
                return true;
            }

            this.isValidated(true);

            return this._super() && this.isProvided();
        },

        placeOrder: function (data, event) {
            if (event) {
                event.preventDefault();
            }

            if (!this.validate() || !additionalValidators.validate()) {
                $('body, html').animate({scrollTop: $('#' + this.getCode()).offset().top}, 'slow');

                return false;
            }

            return this._super(data, event);
        }
    });
});
