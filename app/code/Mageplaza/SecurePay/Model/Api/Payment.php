<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Api;

use Magento\Framework\UrlInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Mageplaza\SecurePay\Api\Data\OrderResponseInterface;
use Mageplaza\SecurePay\Api\Data\OrderResponseInterfaceFactory;
use Mageplaza\SecurePay\Api\Data\PaypalResponseInterface;
use Mageplaza\SecurePay\Api\Data\PaypalResponseInterfaceFactory;
use Mageplaza\SecurePay\Api\PaymentInterface;
use Mageplaza\SecurePay\Gateway\Config\DirectPost;
use Mageplaza\SecurePay\Helper\Request;
use Mageplaza\SecurePay\Helper\Response;
use Mageplaza\SecurePay\Model\Payment\Paypal;

/**
 * Class Payment
 * @package Mageplaza\SecurePay\Model
 */
class Payment implements PaymentInterface
{
    /**
     * @var Request
     */
    private $helper;

    /**
     * @var Response
     */
    private $resHelper;

    /**
     * @var DirectPost
     */
    private $postConfig;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var OrderResponseInterfaceFactory
     */
    private $orderResponseFactory;

    /**
     * @var PaypalResponseInterfaceFactory
     */
    private $paypalResponseFactory;

    /**
     * Payment constructor.
     *
     * @param Request $helper
     * @param Response $resHelper
     * @param DirectPost $postConfig
     * @param UrlInterface $urlBuilder
     * @param CartRepositoryInterface $cartRepository
     * @param OrderResponseInterfaceFactory $orderResponseFactory
     * @param PaypalResponseInterfaceFactory $paypalResponseFactory
     */
    public function __construct(
        Request $helper,
        Response $resHelper,
        DirectPost $postConfig,
        UrlInterface $urlBuilder,
        CartRepositoryInterface $cartRepository,
        OrderResponseInterfaceFactory $orderResponseFactory,
        PaypalResponseInterfaceFactory $paypalResponseFactory
    ) {
        $this->helper                = $helper;
        $this->resHelper             = $resHelper;
        $this->postConfig            = $postConfig;
        $this->urlBuilder            = $urlBuilder;
        $this->cartRepository        = $cartRepository;
        $this->orderResponseFactory  = $orderResponseFactory;
        $this->paypalResponseFactory = $paypalResponseFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function getFingerprint($cartId)
    {
        /** @var Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);

        $orderNo    = $this->helper->formatOrderNo($cartId);
        $grandTotal = number_format((float) $quote->getGrandTotal(), 2, '.', '');
        $timestamp  = date('YmdHis');

        $fingerprint = sprintf(
            '%s|%s|%s|%s|%s|%s',
            $this->helper->getMerchantAccountId(),
            $this->helper->getTransPwd(),
            $this->postConfig->getTxnType(),
            $orderNo,
            $grandTotal,
            $timestamp
        );

        /** @var OrderResponseInterface $orderRes */
        $orderRes = $this->orderResponseFactory->create();

        $orderRes
            ->setFingerprint(hash_hmac('sha256', $fingerprint, $this->helper->getTransPwd()))
            ->setOrderNo($orderNo)
            ->setGrandTotal($grandTotal)
            ->setTimestamp($timestamp);

        return $orderRes;
    }

    /**
     * {@inheritDoc}
     */
    public function initPaypal($cartId)
    {
        /** @var Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);

        $url = $this->helper->getPaypalApiUrl(Paypal::INIT_URL);

        $headers = [
            'Content-type: application/json',
            'Authorization: Bearer ' . $this->helper->getAccessToken()
        ];

        $amount = $this->helper->formatAmount($quote->getGrandTotal(), $quote->getQuoteCurrencyCode());
        $body   = Request::jsonEncode([
            'amount'       => $amount,
            'merchantCode' => $this->helper->getMerchantId(),
            'ip'           => $this->helper->getIpAddress(),
            'noShipping'   => true,
            'redirectUrls' => [ // required but unnecessary
                'successUrl' => $this->urlBuilder->getUrl('', ['_secure' => true]),
                'cancelUrl'  => $this->urlBuilder->getUrl('', ['_secure' => true]),
            ]
        ]);

        $response = $this->helper->sendRequest($url, $headers, $body, 'json');

        /** @var PaypalResponseInterface $paypalRes */
        $paypalRes = $this->paypalResponseFactory->create();

        if ($error = $this->resHelper->hasErrorPaypal($response)) {
            return $paypalRes->setError($error);
        }

        return $paypalRes
            ->setAmount($this->helper->getInfo($response, ['amount']))
            ->setOrderId($this->helper->getInfo($response, ['orderId']))
            ->setPaymentId($this->helper->getInfo($response, ['paymentId']))
            ->setPaymentUrl($this->helper->getInfo($response, ['paymentUrl']));
    }
}
