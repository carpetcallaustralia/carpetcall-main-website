<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Api;

use Magento\Framework\Model\AbstractExtensibleModel;
use Mageplaza\SecurePay\Api\Data\PaypalResponseInterface;

/**
 * Class PaypalResponse
 * @package Mageplaza\SecurePay\Model\Api
 */
class PaypalResponse extends AbstractExtensibleModel implements PaypalResponseInterface
{
    /**
     * {@inheritDoc}
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }

    /**
     * {@inheritDoc}
     */
    public function setAmount($value)
    {
        return $this->setData(self::AMOUNT, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderId($value)
    {
        return $this->setData(self::ORDER_ID, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getPaymentId()
    {
        return $this->getData(self::PAYMENT_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setPaymentId($value)
    {
        return $this->setData(self::PAYMENT_ID, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getPaymentUrl()
    {
        return $this->getData(self::PAYMENT_URL);
    }

    /**
     * {@inheritDoc}
     */
    public function setPaymentUrl($value)
    {
        return $this->setData(self::PAYMENT_URL, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayerId()
    {
        return $this->getData(self::PAYER_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setPayerId($value)
    {
        return $this->setData(self::PAYER_ID, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getError()
    {
        return $this->getData(self::ERROR);
    }

    /**
     * {@inheritDoc}
     */
    public function setError($value)
    {
        return $this->setData(self::ERROR, $value);
    }
}
