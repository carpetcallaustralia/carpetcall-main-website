<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Api;

use Magento\Framework\Model\AbstractExtensibleModel;
use Mageplaza\SecurePay\Api\Data\OrderResponseInterface;

/**
 * Class OrderResponse
 * @package Mageplaza\SecurePay\Model\Api
 */
class OrderResponse extends AbstractExtensibleModel implements OrderResponseInterface
{
    /**
     * {@inheritDoc}
     */
    public function getFingerprint()
    {
        return $this->getData(self::FINGERPRINT);
    }

    /**
     * {@inheritDoc}
     */
    public function setFingerprint($value)
    {
        return $this->setData(self::FINGERPRINT, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderNo()
    {
        return $this->getData(self::ORDER_NO);
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderNo($value)
    {
        return $this->setData(self::ORDER_NO, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getGrandTotal()
    {
        return $this->getData(self::GRAND_TOTAL);
    }

    /**
     * {@inheritDoc}
     */
    public function setGrandTotal($value)
    {
        return $this->setData(self::GRAND_TOTAL, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getTimestamp()
    {
        return $this->getData(self::TIMESTAMP);
    }

    /**
     * {@inheritDoc}
     */
    public function setTimestamp($value)
    {
        return $this->setData(self::TIMESTAMP, $value);
    }
}
