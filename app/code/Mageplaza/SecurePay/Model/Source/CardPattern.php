<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Source;

/**
 * Class CardPattern
 * @package Mageplaza\SecurePay\Model\Source
 */
class CardPattern extends AbstractSource
{
    const VISA             = '^4[0-9]{12}(?:[0-9]{3})?$, ^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$';
    const MASTERCARD       = '^5[1-5][0-9]{14}$';
    const AMERICAN_EXPRESS = '^3[47][0-9]{13}$';
    const DINERS           = '^3(?:0[0-5]|[68][0-9])[0-9]{11}$';
    const JCB              = '^(?:2131|1800|35\d{3})\d{11}$';
    const BANKCARD         = '^56\d{14}$';

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::VISA             => CardTypeXml::VISA,
            self::MASTERCARD       => CardTypeXml::MASTERCARD,
            self::AMERICAN_EXPRESS => CardTypeXml::AMERICAN_EXPRESS,
            self::DINERS           => CardTypeXml::DINERS,
            self::JCB              => CardTypeXml::JCB,
            self::BANKCARD         => CardTypeXml::BANKCARD,
        ];
    }
}
