<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Source;

/**
 * Class PaymentInfo
 * @package Mageplaza\SecurePay\Model\Source
 */
class PaymentInfo extends AbstractSource
{
    const TXN_ID   = 'txn_id';
    const AUTH_ID  = 'mpsecurepay_auth_id';
    const ORDER_NO = 'mpsecurepay_order_no';

    const FRAUD_CODE  = 'mpsecurepay_fraud_code';
    const FRAUD_TEXT  = 'mpsecurepay_fraud_text';
    const FRAUD_GUARD = 'mpsecurepay_fraud_guard';

    const CARD_TYPE    = 'cc_type';
    const LAST_CARD_4  = 'mpsecurepay_card_last4';
    const EXPIRY_DATE  = 'mpsecurepay_expiry_date';
    const EXPIRY_MONTH = 'mpsecurepay_expiry_month';
    const EXPIRY_YEAR  = 'mpsecurepay_expiry_year';

    /** PAYPAL RESPONSE FIELD */
    const ORDER_ID = 'mpsecurepay_order_id';
    const STATUS   = 'mpsecurepay_status';

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::AUTH_ID     => __('Authorisation ID'),
            self::ORDER_NO    => __('Purchase Order No'),
            self::CARD_TYPE   => __('Card Type'),
            self::LAST_CARD_4 => __('Last Card Number'),
            self::EXPIRY_DATE => __('Expiration Date'),
            self::FRAUD_CODE  => __('Anti Fraud Code'),
            self::FRAUD_TEXT  => __('Anti Fraud Text'),
            self::FRAUD_GUARD => __('Fraud Guard'),
            self::ORDER_ID    => __('Paypal Order ID'),
            self::STATUS      => __('Paypal Order Status'),
        ];
    }
}
