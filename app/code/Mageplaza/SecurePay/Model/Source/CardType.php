<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Source;

/**
 * Class CardType
 * @package Mageplaza\SecurePay\Model\Source
 */
class CardType extends AbstractSource
{
    const VISA             = 'VI';
    const MASTERCARD       = 'MC';
    const AMERICAN_EXPRESS = 'AE';
    const DINERS           = 'DN';
    const JCB              = 'JCB';

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::VISA             => __('Visa'),
            self::MASTERCARD       => __('Mastercard'),
            self::AMERICAN_EXPRESS => __('American Express'),
            self::DINERS           => __('Diners Club'),
            self::JCB              => __('JCB'),
        ];
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public static function getCardType($value)
    {
        $options = static::getOptionArray();

        return isset($options[$value]) ? (string) $options[$value] : $value;
    }
}
