<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Payment;

/**
 * Class Paypal
 * @package Mageplaza\SecurePay\Model\Payment
 */
class Paypal extends AbstractPayment
{
    const CODE = 'mpsecurepay_paypal';

    const URL_TEST   = 'https://payments-stest.npe.auspost.zone/';
    const URL_LIVE   = 'https://payments.auspost.net.au.zone/';
    const INIT_URL   = 'v2/wallets/paypal/payments/initiate';
    const EXEC_URL   = 'v2/wallets/paypal/payments/orders/%orderId%/execute';
    const REFUND_URL = 'v2/wallets/paypal/orders/%orderId%/refunds';

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'isOscPage' => $this->helper->isOscPage(),
            'env'       => $this->helper->isSandbox() ? 'sandbox' : 'production',
            'locale'    => $this->helper->getLocaleCode(),
        ];
    }
}
