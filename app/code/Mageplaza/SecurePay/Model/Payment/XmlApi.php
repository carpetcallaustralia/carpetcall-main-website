<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Payment;

/**
 * Class XmlApi
 * @package Mageplaza\SecurePay\Model\Payment
 */
class XmlApi extends AbstractPayment
{
    const CODE    = 'mpsecurepay_xml_api';
    const URL     = 'https://%env%api.securepay.com.au/antifraud/payment';
    const TIMEOUT = 120;
    const VERSION = 'xml-4.2';
    const SOURCE  = '23';

    const TXN_PAYMENT  = 0;
    const TXN_REFUND   = 4;
    const TXN_AUTH     = 10;
    const TXN_COMPLETE = 11;
}
