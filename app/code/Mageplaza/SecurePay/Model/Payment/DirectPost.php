<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SecurePay
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SecurePay\Model\Payment;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\CartInterface;

/**
 * Class DirectPost
 * @package Mageplaza\SecurePay\Model\Payment
 */
class DirectPost extends AbstractPayment
{
    const CODE = 'mpsecurepay_direct_post';
    const URL  = 'https://%env%api.securepay.com.au/directpost/authorise';

    const TXN_PAYMENT_FRAUD    = 2;
    const TXN_PAYMENT_FRAUD_3D = 6;

    const TXN_AUTH_FRAUD    = 3;
    const TXN_AUTH_FRAUD_3D = 7;

    /**
     * @param CartInterface|null $quote
     *
     * @return bool|mixed
     * @throws NoSuchEntityException
     */
    public function isAvailable(CartInterface $quote = null)
    {
          return true;
	    return parent::isAvailable($quote) && $this->helper->isSecureRequest();
//        return $this->helper->isSecureRequest();
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'postUrl'     => $this->helper->getApiUrl(self::URL),
            'merchantKey' => $this->helper->getMerchantAccountId(),
            'txnType'     => $this->postConfig->getTxnType(),
            'callbackUrl' => $this->urlBuilder->getUrl('mpsecurepay/post/index', ['_secure' => true]),
            'ipAddress'   => $this->helper->getIpAddress(),
        ];
    }
}
