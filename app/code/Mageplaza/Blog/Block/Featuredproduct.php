<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Blog
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Blog\Block;

use Magento\Catalog\Helper\Product\Compare;

/**
 * Class Listpost
 * @package Mageplaza\Blog\Block\Post
 */


class Featuredproduct extends \Magento\Framework\View\Element\Template
{
    protected $productrepository; 
    /**
     * @var compareProductHelper
     */
    protected $compareProductHelper;

    public function __construct(\Magento\Catalog\Helper\Image $imageHelper, \Magento\Catalog\Api\ProductRepositoryInterface $productrepository,Compare $compareProductHelper) { 
        $this->imageHelper = $imageHelper;
        $this->productrepository = $productrepository;
        $this->compareProductHelper = $compareProductHelper;
    }

    public function getProductDataUsingSku($productsku) {
        try {
            $product = $this->productrepository->get($productsku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
            $product = false;
        }
        return $product;
    }
    
    /**
     * @param int $id
     * @return string
     */
    public function getItemImage($productId)
    {
        //echo 'test';exit;
        $_product = $this->productrepository->getById($productId);
        $image_url = $this->imageHelper->init($_product, 'product_base_image')->getUrl();
        return $image_url;
    }

    public function getComparelink($product)
    {
        $compareUrl=$this->compareProductHelper->getPostDataParams($product);
        return $compareUrl;
    }
}
