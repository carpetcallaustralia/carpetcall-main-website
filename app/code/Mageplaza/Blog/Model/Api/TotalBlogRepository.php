<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Blog
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Blog\Model\Api;

use Exception;

use Mageplaza\Blog\Helper\Data;
use Mageplaza\Blog\Api\TotalBlogRepositoryInterface;
/**
 * Class PostRepositoryInterface
 * @package Mageplaza\Blog\Model\Api
 */
class TotalBlogRepository implements TotalBlogRepositoryInterface
{
    /**
     * @var Data
     */
    protected $_helperData;

    public function __construct(
        Data $helperData
    ) {
        $this->_helperData                  = $helperData;
    }


    public function getTotalAllPost()
    {
        $collection = $this->_helperData->getFactoryByType()->create()->getCollection();        
        //echo count($collection);die();
        $total_count = count($collection);
        return $total_count;
    }

    
    public function getTotalAllPostCategoryvice($categoryKey)
    { ///echo $categoryKey;die();
        $category = $this->_helperData->getFactoryByType('category')->create()->getCollection()
            ->addFieldToFilter('url_key', $categoryKey)->getFirstItem();
        //print_r($category->getData());die();
        $total_post_via_cate_count = count($category->getSelectedPostsCollection()->getItems());
        
        return $total_post_via_cate_count;
    }
}
