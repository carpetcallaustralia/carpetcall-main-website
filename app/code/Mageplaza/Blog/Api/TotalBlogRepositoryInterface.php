<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Blog
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Blog\Api;

use Exception;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class PostInterface
 * @package Mageplaza\Blog\Api
 */
interface TotalBlogRepositoryInterface
{
    /**
     * @return \Mageplaza\Blog\Api\Data\PostInterface[]
     */
    public function getTotalAllPost();

    /**
     * @param string $categoryKey
     *
     * @return \Mageplaza\Blog\Api\Data\PostInterface[]
     */
    public function getTotalAllPostCategoryvice($categoryKey);
}
