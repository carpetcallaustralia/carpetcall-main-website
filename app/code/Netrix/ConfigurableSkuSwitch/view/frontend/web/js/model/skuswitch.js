/**
 * Created by thomas on 2017-01-30.
 */

define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
    'use strict';
    return function(targetModule){

        var reloadPrice = targetModule.prototype._reloadPrice;
        targetModule.prototype.configurableSku = $('div.product-info-main .sku .value').html();

        var reloadPriceWrapper = wrapper.wrap(reloadPrice, function(original){

            var result = original();
            var weight = this.options.spConfig.weight[this.simpleProduct];
            //do extra stuff
            var simpleSku = this.configurableSku;

            if(this.simpleProduct){
                simpleSku = this.options.spConfig.skus[this.simpleProduct];
            }

            //$('div.product-info-main .sku .value').html(simpleSku);
            $('div.product-info-main .hard-floor-code li .value').html(simpleSku);

            if(weight != null) {
                $('.weight-custom-label').html('Weight');
                $('.weight-custom').html(weight);
            }else{
                $('.weight-custom-label').html('');
                $('.weight-custom').html('');
            }

            //return original value
            //return original();
            return result;
        });

        targetModule.prototype._reloadPrice = reloadPriceWrapper;
        return targetModule;
    };
});