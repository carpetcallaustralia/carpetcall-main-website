<?php
/**
 * Created by PhpStorm.
 * User: thomasnordkvist
 * Date: 17-01-30
 * Time: 08:15
 */
 
namespace Netrix\ConfigurableSkuSwitch\Plugin\Magento\Swatches\Block\Product\Renderer;

class Configurable
{
    public function afterGetJsonConfig(\Magento\Swatches\Block\Product\Renderer\Configurable $subject, $result) {

        $jsonResult = json_decode($result, true);

        $jsonResult['skus'] = [];
        $jsonResult['weight'] = [];

        foreach ($subject->getAllowProducts() as $simpleProduct) {
            $jsonResult['skus'][$simpleProduct->getId()] = $simpleProduct->getSku();
            $jsonResult['weight'][$simpleProduct->getId()] = number_format((float)$simpleProduct->getWeight(), 2, '.', '').' kg';
            
        }

        $result = json_encode($jsonResult);

        return $result;
    }
}