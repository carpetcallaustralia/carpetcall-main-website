/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/totals',
    'mage/url'
], function (Component, totals, url) {
    'use strict';

    return Component.extend({
        isLoading: totals.isLoading,

        getCheckoutUrl: function () {
            return url.build('checkout/cart');
        }
    });
});
