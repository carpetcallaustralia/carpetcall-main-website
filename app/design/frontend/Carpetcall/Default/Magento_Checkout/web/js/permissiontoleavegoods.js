require(['jquery', 'jquery/ui','mage/url'], function($,url){ 
   $( document ).ready(function() {

      //wait until the last element (.payment-method) being rendered
      var existCondition = setInterval(function() {
       if ($('.payment-method').length) { 
        clearInterval(existCondition);
        runMyFunction();
       }
      }, 100);

      function runMyFunction(){
       console.log("Last");
       $('label[for="termsandcondition"]').hide();
       $('[name="termsandcondition"]').change(function(){
            
            if($('input[name="termsandcondition"]').is(':checked')) {
              var status = 1;
            }
            else {
                var status = 0;
            }
            //alert(status);
            var shippingurl = $('input[name="baseurl"]').val();
            
                    $.ajax({
                        type: 'POST',
                        url: shippingurl,
                        data: {status: status},
                        success: function(response) {             
                            jQuery('.valid-email-error').remove();
                        },
                        error: function (xhr, status, errorThrown) {
                        
                        }
                    });
        });
      }

    }); 
 }); 