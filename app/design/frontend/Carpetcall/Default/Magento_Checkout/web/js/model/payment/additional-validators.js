/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define(['jquery'], function ($) {
    'use strict';

    var validators = [];

    return {
        /**
         * Register unique validator
         *
         * @param {*} validator
         */
        registerValidator: function (validator) {
            validators.push(validator);
        },

        /**
         * Returns array of registered validators
         *
         * @returns {Array}
         */
        getValidators: function () {
            return validators;
        },

        /**
         * Process validators
         *
         * @returns {Boolean}
         */
        validate: function (hideError) {
            var validationResult = true;

            hideError = hideError || false;

            if (validators.length <= 0) {
                return validationResult;
            }

            validators.forEach(function (item) {
                if (item.validate(hideError) == false) { //eslint-disable-line eqeqeq
                    validationResult = false;
                    
                    if (item.index == 'billingAddress') {
                        $('#billing').collapsible().collapsible('enable');
                        $('#billing').collapsible().collapsible('activate');
                        $('#payment').collapsible().collapsible('deactivate');
                    }

                    if(item.index == 'shippingAddress'){
                        $('#shipping').collapsible().collapsible('enable');
                        $('#shipping').collapsible().collapsible('activate');
                    }
                    
                    return false;
                }
            });
            
            return validationResult;
        }
    };
});
