require([
    'jquery',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/action/get-totals',
    'mage/collapsible',
    'mage/validation'
], function(
    $,
    additionalValidators,
    getPaymentInformationAction,
    getTotalsAction
){
    $( document ).ready(function() {
        if (!$('#s_method_flatrate_flatrate').length) {
            $('#billing').collapsible().collapsible('enable');
            $('#billing').collapsible().collapsible('activate');
        }
        shippingDetailsNext = function () {
            if (validateStep('shippingAddress')) {
                $('#shipping').collapsible().collapsible('deactivate');
                if (jQuery('#billing-address-same-as-shipping').prop('checked')) {
                    $('#billing').collapsible().collapsible('disable');
                    $('#billing').collapsible().collapsible('deactivate');
                    $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('enable');
                    $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('activate');
                    autoPopupStoreSelectorBind();
                    autoPopupStoreSelector();
                    smoothScrolling("opc-shipping_method"); 
                } else {
                    $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('disable');
                    $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('deactivate');
                    $('#billing').collapsible().collapsible('enable');
                    $('#billing').collapsible().collapsible('activate');
                    smoothScrolling("billing"); 
                }
            } else {
                var offsetHeight = $(window).height() / 2,
                    errorMsgSelector = $('#maincontent .mage-error:visible:first').closest('.field');
                errorMsgSelector = errorMsgSelector.length ? errorMsgSelector : $('#maincontent .field-error:visible:first').closest('.field');
                if (errorMsgSelector.length) {
                    if (errorMsgSelector.find('select').length) {
                        $('html, body').scrollTop(
                            errorMsgSelector.find('select').offset().top - offsetHeight
                        );
                        errorMsgSelector.find('select').focus();
                    } else if (errorMsgSelector.find('input').length) {
                        $('html, body').scrollTop(
                            errorMsgSelector.find('input').offset().top - offsetHeight
                        );
                        errorMsgSelector.find('input').focus();
                    }
                } else if ($('.message-error:visible').length) {
                    $('html, body').scrollTop(
                        $('.message-error:visible:first').closest('div').offset().top - offsetHeight
                    );
                }
            }
            return true;
        };

        billingDetailsPrev = function () {
            $('#shipping').collapsible().collapsible('activate');
            $('#billing').collapsible().collapsible('deactivate');
            $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('deactivate');
            $('#payment').collapsible().collapsible('deactivate');
            return true;
        };

        billingDetailsNext = function () {
            if (validateStep('billingAddress')) {
                $('#shipping').collapsible().collapsible('deactivate');
                $('#billing').collapsible().collapsible('deactivate');
                $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('enable');
                $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('activate');
                autoPopupStoreSelectorBind();
                autoPopupStoreSelector();
                smoothScrolling("opc-shipping_method"); 
            } else {
                var offsetHeight = $(window).height() / 2,
                    errorMsgSelector = $('#maincontent .mage-error:visible:first').closest('.field');
                errorMsgSelector = errorMsgSelector.length ? errorMsgSelector : $('#maincontent .field-error:visible:first').closest('.field');
                if (errorMsgSelector.length) {
                    if (errorMsgSelector.find('select').length) {
                        $('html, body').scrollTop(
                            errorMsgSelector.find('select').offset().top - offsetHeight
                        );
                        errorMsgSelector.find('select').focus();
                    } else if (errorMsgSelector.find('input').length) {
                        $('html, body').scrollTop(
                            errorMsgSelector.find('input').offset().top - offsetHeight
                        );
                        errorMsgSelector.find('input').focus();
                    }
                } else if ($('.message-error:visible').length) {
                    $('html, body').scrollTop(
                        $('.message-error:visible:first').closest('div').offset().top - offsetHeight
                    );
                }
            }
            return true;
        };

        deliveryOptionsPrev = function () {
            if (jQuery('#billing-address-same-as-shipping').prop('checked')) {
                $('#shipping').collapsible().collapsible('activate');
                $('#billing').collapsible().collapsible('deactivate');
            } else {
                $('#shipping').collapsible().collapsible('deactivate');
                $('#billing').collapsible().collapsible('enable');
                $('#billing').collapsible().collapsible('activate');
            }
            $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('deactivate');
            $('#payment').collapsible().collapsible('deactivate');
            return true;
        };

        deliveryOptionsNext = function () {
            var deferred = jQuery.Deferred();
            getTotalsAction([], deferred);
            console.log(getPaymentInformationAction(deferred));
            if (validateStep('shippingMethod')) {
                $('#shipping').collapsible().collapsible('deactivate');
                $('#billing').collapsible().collapsible('deactivate');
                $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('deactivate');
                $('#payment').collapsible().collapsible('enable');
                $('#payment').collapsible().collapsible('activate');
                smoothScrolling("payment"); 
            } else {
                var offsetHeight = $(window).height() / 2,
                    errorMsgSelector = $('#maincontent .mage-error:visible:first').closest('.field');
                errorMsgSelector = errorMsgSelector.length ? errorMsgSelector : $('#maincontent .field-error:visible:first').closest('.field');
                if (errorMsgSelector.length) {
                    if (errorMsgSelector.find('select').length) {
                        $('html, body').scrollTop(
                            errorMsgSelector.find('select').offset().top - offsetHeight
                        );
                        errorMsgSelector.find('select').focus();
                    } else if (errorMsgSelector.find('input').length) {
                        $('html, body').scrollTop(
                            errorMsgSelector.find('input').offset().top - offsetHeight
                        );
                        errorMsgSelector.find('input').focus();
                    }
                } else if ($('.message-error:visible').length) {
                    $('html, body').scrollTop(
                        $('.message-error:visible:first').closest('div').offset().top - offsetHeight
                    );
                }
            }
            return true;
        };
        
        paymentOptionsPrev = function () {
            $('#shipping').collapsible().collapsible('deactivate');
            $('#billing').collapsible().collapsible('deactivate');
            $('#opc-shipping_method .checkout-shipping-method').collapsible().collapsible('activate');
            $('#payment').collapsible().collapsible('deactivate');
            return true;
        };

        validateStep = function (step, hideError) {
            step = step || 'shippingAddress';
            hideError = hideError || false;

            onlyShippingAddress = (step == 'shippingAddress') ? true : false;
            step = (step == 'shippingMethod') ? 'shippingAddress' : step;
            // ^^^ validating the shipping method is part of the shipping address validation
            
            let validationResult = true;
            let validators = additionalValidators.getValidators();
            validators.forEach(function (item) {
                if (item.index == step && item.validate(hideError, onlyShippingAddress) == false) { //eslint-disable-line eqeqeq
                    validationResult = false;
                    return false;
                }
            });
            return validationResult;
        };

        smoothScrolling = function (div) {
            div = div || '';
            if (div !== '') {
                document.getElementById(div).scrollIntoView(); 
            }
            return true;
        };

        autoPopupStoreSelectorBind = function () {
            $('#s_method_mpstorepickup').bind('click', autoPopupStoreSelector);
        };

        autoPopupStoreSelector = function () {
            if (checkoutConfig.shippingMethods.some(el => el.carrier_code === "mpstorepickup") && 
                $('#s_method_mpstorepickup_mpstorepickup').prop('checked') && 
                    $('#mpstorepickup-loc-id-selected').val() == "") {
                        $('#mpstorepickup_select_store .btn-select-store').click();
                        let shippingAddressPostcode = $('#shipping').find('#co-shipping-form').find('input[name="postcode"]').val();
                        if (shippingAddressPostcode != '') {
                            $('#bh-sl-address').val(shippingAddressPostcode);
                            $('.find-store-bottom').click();
                        }
            }
        };
    });
});
