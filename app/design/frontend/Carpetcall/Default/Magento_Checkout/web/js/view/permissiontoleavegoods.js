define(
    [
        'ko',
        'uiComponent',
        'mage/url'
    ],
    function (ko, Component, url) {
        "use strict";

        console.log('aaaa');


        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/permissiontoleavegoods'
            },
            termsandcondition: false,
            getBaseUrl: function() {
                return url.build('termsandconditions/ajax/getemailinfo');
            },
            terms_and_condition: function() {
                return url.build('terms-and-conditions');
            },
        });
    }
);