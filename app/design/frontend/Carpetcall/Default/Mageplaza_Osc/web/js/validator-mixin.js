define([
    'jquery'
], function ($) {
    'use strict';

    return function (validator) {
        validator.addRule(
            'custom-telephone',
            function (value, params, additionalParams) {
                return $.mage.isEmptyNoTrim(value) || /^\d{10}$/.test(value); // customer enter minimum 10 digit 
                //return $.mage.isEmptyNoTrim(value) || /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/gm.test(value);
                // as per australian telephone number all validation pattern
                // e.g. example patter
                // +61(02)89876544
                // +61 2 8986 6544
                // 02 8986 6544
                // +61289876544
                // 0414 570776
                // 0414570776
                // 0414 570 776
                // 04 1457 0776
                // +61 414 570776
                // +61 (04)14 570776
                // +61 (04)14-570-776
            },
            $.mage.__("Please ensure phone number contains 10 digits with no spaces")
        );

        validator.addRule(
            'custom-postcode',
            function (value, params, additionalParams) {
                return $.mage.isEmptyNoTrim(value) || /^\d{4}$/.test(value); // customer enter minimum 4 digit 
            },
            $.mage.__("Provided postal code seems to be invalid. Example: 1234")
        );

        return validator;
    };
});