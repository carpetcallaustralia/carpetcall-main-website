  define([
    'jquery',
    'collapsible',
    'domReady!',
], function ($) {
    'use strict';

    var helpers = {
        domReady: function() {

            var $container = $('.accordion'),
                $accordions = [],
                accordionOptions = {
                    collapsible: true,
                    header: '.heading',
                    trigger: '',
                    content: '.content',
                    openedState: 'active',
                    animate: false
                };

            $container.children("div").each(function(index, elem){
                var $this = $(elem),
                    $accordion = $this.collapsible(accordionOptions);

                $accordions.push($accordion);
            });
        }
    };

    helpers.domReady();
});