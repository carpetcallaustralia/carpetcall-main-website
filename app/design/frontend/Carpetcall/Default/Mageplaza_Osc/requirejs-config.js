/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */


    config = {
        map: {
            '*': {
                'step_collapsible': 'Mageplaza_Osc/js/step-collapsible'
            }
        },
        config: {
            mixins: {
                'Magento_Ui/js/lib/validation/validator': {
                    'Mageplaza_Osc/js/validator-mixin': true
                }
            }
        }
    };

