/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'owlCarousel': "Magento_Theme/js/owl.carousel"
        }
    },
    "shim": {
        "Magento_Theme/js/owl.carousel": ["jquery"]
    }
};
    