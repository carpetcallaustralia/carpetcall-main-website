/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            priceOptions:         'Magento_Catalog/js/price-options'
        }
    }
};
