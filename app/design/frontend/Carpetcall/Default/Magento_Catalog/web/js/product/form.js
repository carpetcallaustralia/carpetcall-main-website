define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/mage',
    'jquery/ui',
    'jquery/jquery.cookie'
], function ($, modal) {
    'use strict';
    $.widget('nutranext.processPopupNewsletter', {
        _create: function () {
            var self = this,
                popup_newsletter = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: this.options.popupTitle,
                    buttons: false,
                    modalClass : 'contact-form'
                };
            modal(popup_newsletter, this.element);

            if (!$.cookie('popup_newsletter_status')) {
                setTimeout(function () {
                    self._setStyleCss();
                    self.element.modal('openModal');
                }, 3000);
            }
            $.cookie('popup_newsletter_status', 'closed');

            this.element.find('form').submit(function() {
                if ($(this).validation('isValid')) {
                    $.ajax({
                        url: $(this).attr('action'),
                        cache: true,
                        data: $(this).serialize(),
                        dataType: 'json',
                        type: 'POST',
                        showLoader: true
                    }).done(function (data) {
                        self.element.find('.messages .message div').html(data.message);
                        if (data.error) {
                            self.element.find('.messages .message').addClass('message-error error');
                        } else {
                            self.element.find('.messages .message').addClass('message-success success');
                            setTimeout(function() {
                                self.element.modal('closeModal');
                            }, 1000);
                        }
                        self.element.find('.messages').show();
                        setTimeout(function() {
                            self.element.find('.messages').hide();
                        }, 5000);
                        //Welt Pixel - Submission
                        $("#submit1").click();

                    });
                }
                return false;
            });
            this._resetStyleCss();
            
        },
        _setStyleCss: function(width) {

            width = width || 400;

            if (window.innerWidth > 786) {
                this.element.parent().parent('.modal-inner-wrap').css({'max-width': width+'px'});
            }
        },
        _resetStyleCss: function() {
            var self = this;
            $( window ).resize(function() {
                if (window.innerWidth <= 786) {
                    self.element.parent().parent('.modal-inner-wrap').css({'max-width': 'initial'});
                } else {
                    self._setStyleCss(self.options.innerWidth);
                }
            });
        }
    });

    return $.nutranext.processPopupNewsletter;
});
