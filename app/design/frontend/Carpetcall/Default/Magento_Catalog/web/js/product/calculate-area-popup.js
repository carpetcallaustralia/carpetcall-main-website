  /**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function($) {
        "use strict";
        //creating jquery widget
        $.widget('Product.calculateArea', {
            options: {
                modalForm: '#calculate-area-popup',
                modalButton: 'button.Calculate_link'
            },
            _create: function() {
                this.options.modalOption = this._getModalOptions();
                this._bind();
            },
            _getModalOptions: function() {
                /**
                 * Modal options
                 */
                var options = {
                    'type': 'popup',
                    'title': 'Square Meter Calculator',
                    'trigger': '[data-trigger=trigger]',
                    'responsive': true,
                    'modalClass': 'calculator-modal',
                    buttons: [{
                        text: jQuery.mage.__('Confirm'),
                        class: 'action',
                        click: function () {
                            jQuery('#qty').attr('value',jQuery('#totoalpackrequired').text());
                            var totalcoveragedata = parseFloat(jQuery('#h_total_coverage').val()) * parseFloat(jQuery('#qty').val());
                            if(!isNaN(totalcoveragedata)){
                                jQuery('#coverage_value').html(totalcoveragedata.toFixed(2)+' SQM');
                                jQuery('#recommended_qty').val(totalcoveragedata.toFixed(2));
                            }
                            var totalperpackprice = parseFloat(jQuery('#h_per_pack_price').val()) * parseFloat(jQuery('#totoalpackrequired').text());
                            if(!isNaN(totalperpackprice)){
                                jQuery('#perpackprice').html(totalperpackprice);
                            }
                            this.closeModal();
                        }
                    }]
                };

                return options;
            },
            _bind: function(){
                var modalOption = this.options.modalOption;
                var modalForm = this.options.modalForm;

                $(document).on('click', this.options.modalButton,  function(){
                    //Initialize modal
                    $(modalForm).modal(modalOption);
                    //open modal
                    $(modalForm).trigger('openModal');
                });
            }
        });

        return $.Product.calculateArea;
    }
);
